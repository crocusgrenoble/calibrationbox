<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SamacSys_Parts">
<description>&lt;b&gt;https://componentsearchengine.com&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOD3716X145N">
<description>&lt;b&gt;1N6263W-7-F&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.7" y="0" dx="1.2" dy="0.75" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.2" dy="0.75" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.55" y1="1.675" x2="2.55" y2="1.675" width="0.05" layer="51"/>
<wire x1="2.55" y1="1.675" x2="2.55" y2="-1.675" width="0.05" layer="51"/>
<wire x1="2.55" y1="-1.675" x2="-2.55" y2="-1.675" width="0.05" layer="51"/>
<wire x1="-2.55" y1="-1.675" x2="-2.55" y2="1.675" width="0.05" layer="51"/>
<wire x1="-1.35" y1="0.775" x2="1.35" y2="0.775" width="0.1" layer="51"/>
<wire x1="1.35" y1="0.775" x2="1.35" y2="-0.775" width="0.1" layer="51"/>
<wire x1="1.35" y1="-0.775" x2="-1.35" y2="-0.775" width="0.1" layer="51"/>
<wire x1="-1.35" y1="-0.775" x2="-1.35" y2="0.775" width="0.1" layer="51"/>
<wire x1="-1.35" y1="0.175" x2="-0.75" y2="0.775" width="0.1" layer="51"/>
<wire x1="-2.3" y1="0.775" x2="1.35" y2="0.775" width="0.2" layer="21"/>
<wire x1="-1.35" y1="-0.775" x2="1.35" y2="-0.775" width="0.2" layer="21"/>
</package>
<package name="RESC1005X40N">
<description>&lt;b&gt;RC0402&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.55" y="0" dx="0.75" dy="0.6" layer="1"/>
<smd name="2" x="0.55" y="0" dx="0.75" dy="0.6" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.175" y1="0.55" x2="1.175" y2="0.55" width="0.05" layer="51"/>
<wire x1="1.175" y1="0.55" x2="1.175" y2="-0.55" width="0.05" layer="51"/>
<wire x1="1.175" y1="-0.55" x2="-1.175" y2="-0.55" width="0.05" layer="51"/>
<wire x1="-1.175" y1="-0.55" x2="-1.175" y2="0.55" width="0.05" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.1" layer="51"/>
</package>
<package name="CAPC1005X55N">
<description>&lt;b&gt;0402_21&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.46" y="0" dx="0.62" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="0.46" y="0" dx="0.62" dy="0.6" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-0.91" y1="0.46" x2="0.91" y2="0.46" width="0.05" layer="51"/>
<wire x1="0.91" y1="0.46" x2="0.91" y2="-0.46" width="0.05" layer="51"/>
<wire x1="0.91" y1="-0.46" x2="-0.91" y2="-0.46" width="0.05" layer="51"/>
<wire x1="-0.91" y1="-0.46" x2="-0.91" y2="0.46" width="0.05" layer="51"/>
<wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="0.5" y1="-0.25" x2="-0.5" y2="-0.25" width="0.1" layer="51"/>
<wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.1" layer="51"/>
</package>
<package name="TO270P460X1020X2008-3P">
<description>&lt;b&gt;TO-220&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="0" drill="1.32" diameter="1.98" shape="square"/>
<pad name="2" x="2.7" y="0" drill="1.32" diameter="1.98"/>
<pad name="3" x="5.4" y="0" drill="1.32" diameter="1.98"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.75" y1="3.32" x2="8.15" y2="3.32" width="0.05" layer="51"/>
<wire x1="8.15" y1="3.32" x2="8.15" y2="-1.78" width="0.05" layer="51"/>
<wire x1="8.15" y1="-1.78" x2="-2.75" y2="-1.78" width="0.05" layer="51"/>
<wire x1="-2.75" y1="-1.78" x2="-2.75" y2="3.32" width="0.05" layer="51"/>
<wire x1="-2.5" y1="3.07" x2="7.9" y2="3.07" width="0.1" layer="51"/>
<wire x1="7.9" y1="3.07" x2="7.9" y2="-1.53" width="0.1" layer="51"/>
<wire x1="7.9" y1="-1.53" x2="-2.5" y2="-1.53" width="0.1" layer="51"/>
<wire x1="-2.5" y1="-1.53" x2="-2.5" y2="3.07" width="0.1" layer="51"/>
<wire x1="-2.5" y1="1.72" x2="-1.15" y2="3.07" width="0.1" layer="51"/>
<wire x1="7.9" y1="-1.53" x2="7.9" y2="3.07" width="0.2" layer="21"/>
<wire x1="7.9" y1="3.07" x2="-2.5" y2="3.07" width="0.2" layer="21"/>
<wire x1="-2.5" y1="3.07" x2="-2.5" y2="0" width="0.2" layer="21"/>
</package>
<package name="1984617">
<description>&lt;b&gt;1984617-3&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="0" drill="1.2" diameter="1.8" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="1.8"/>
<text x="1.75" y="-0.725" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="1.75" y="-0.725" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.75" y1="-4.5" x2="5.25" y2="-4.5" width="0.2" layer="51"/>
<wire x1="5.25" y1="-4.5" x2="5.25" y2="3.05" width="0.2" layer="51"/>
<wire x1="5.25" y1="3.05" x2="-1.75" y2="3.05" width="0.2" layer="51"/>
<wire x1="-1.75" y1="3.05" x2="-1.75" y2="-4.5" width="0.2" layer="51"/>
<wire x1="-1.75" y1="3.05" x2="5.25" y2="3.05" width="0.1" layer="21"/>
<wire x1="5.25" y1="3.05" x2="5.25" y2="-4.5" width="0.1" layer="21"/>
<wire x1="5.25" y1="-4.5" x2="-1.75" y2="-4.5" width="0.1" layer="21"/>
<wire x1="-1.75" y1="-4.5" x2="-1.75" y2="3.05" width="0.1" layer="21"/>
<wire x1="-2.75" y1="4.05" x2="6.25" y2="4.05" width="0.1" layer="51"/>
<wire x1="6.25" y1="4.05" x2="6.25" y2="-5.5" width="0.1" layer="51"/>
<wire x1="6.25" y1="-5.5" x2="-2.75" y2="-5.5" width="0.1" layer="51"/>
<wire x1="-2.75" y1="-5.5" x2="-2.75" y2="4.05" width="0.1" layer="51"/>
</package>
<package name="SOP254P1030X508-8N">
<description>&lt;b&gt;FOD3180SDV-1&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-4.95" y="3.81" dx="1.9" dy="1.3" layer="1" rot="R90"/>
<smd name="2" x="-4.95" y="1.27" dx="1.9" dy="1.3" layer="1" rot="R90"/>
<smd name="3" x="-4.95" y="-1.27" dx="1.9" dy="1.3" layer="1" rot="R90"/>
<smd name="4" x="-4.95" y="-3.81" dx="1.9" dy="1.3" layer="1" rot="R90"/>
<smd name="5" x="4.95" y="-3.81" dx="1.9" dy="1.3" layer="1" rot="R90"/>
<smd name="6" x="4.95" y="-1.27" dx="1.9" dy="1.3" layer="1" rot="R90"/>
<smd name="7" x="4.95" y="1.27" dx="1.9" dy="1.3" layer="1" rot="R90"/>
<smd name="8" x="4.95" y="3.81" dx="1.9" dy="1.3" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-5.85" y1="5.205" x2="5.85" y2="5.205" width="0.05" layer="51"/>
<wire x1="5.85" y1="5.205" x2="5.85" y2="-5.205" width="0.05" layer="51"/>
<wire x1="5.85" y1="-5.205" x2="-5.85" y2="-5.205" width="0.05" layer="51"/>
<wire x1="-5.85" y1="-5.205" x2="-5.85" y2="5.205" width="0.05" layer="51"/>
<wire x1="-3.302" y1="4.828" x2="3.302" y2="4.828" width="0.1" layer="51"/>
<wire x1="3.302" y1="4.828" x2="3.302" y2="-4.828" width="0.1" layer="51"/>
<wire x1="3.302" y1="-4.828" x2="-3.302" y2="-4.828" width="0.1" layer="51"/>
<wire x1="-3.302" y1="-4.828" x2="-3.302" y2="4.828" width="0.1" layer="51"/>
<wire x1="-3.302" y1="2.288" x2="-0.762" y2="4.828" width="0.1" layer="51"/>
<wire x1="-3.65" y1="4.828" x2="3.65" y2="4.828" width="0.2" layer="21"/>
<wire x1="3.65" y1="4.828" x2="3.65" y2="-4.828" width="0.2" layer="21"/>
<wire x1="3.65" y1="-4.828" x2="-3.65" y2="-4.828" width="0.2" layer="21"/>
<wire x1="-3.65" y1="-4.828" x2="-3.65" y2="4.828" width="0.2" layer="21"/>
<wire x1="-5.9" y1="4.81" x2="-4" y2="4.81" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="1N4148W-7-F">
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<text x="11.43" y="5.08" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="11.43" y="2.54" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="K" x="0" y="0" visible="pad" length="middle"/>
<pin name="A" x="15.24" y="0" visible="pad" length="middle" rot="R180"/>
<polygon width="0.254" layer="94">
<vertex x="5.08" y="0"/>
<vertex x="10.16" y="2.54"/>
<vertex x="10.16" y="-2.54"/>
</polygon>
</symbol>
<symbol name="ERA-2ARB221X">
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.254" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.254" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<text x="13.97" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="13.97" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" visible="pad" length="middle"/>
<pin name="2" x="17.78" y="0" visible="pad" length="middle" rot="R180"/>
</symbol>
<symbol name="LMK105BBJ475MVLF">
<wire x1="5.588" y1="2.54" x2="5.588" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.588" y2="0" width="0.254" layer="94"/>
<wire x1="7.112" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<text x="8.89" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="8.89" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" visible="pad" length="middle"/>
<pin name="2" x="12.7" y="0" visible="pad" length="middle" rot="R180"/>
</symbol>
<symbol name="STP16NF06L">
<wire x1="5.08" y1="2.54" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="16.51" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="16.51" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="G" x="0" y="0" length="middle"/>
<pin name="D" x="0" y="-2.54" length="middle"/>
<pin name="S" x="0" y="-5.08" length="middle"/>
</symbol>
<symbol name="1984617">
<wire x1="5.08" y1="2.54" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="16.51" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="16.51" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" length="middle"/>
<pin name="2" x="0" y="-2.54" length="middle"/>
</symbol>
<symbol name="FOD3180SDV">
<wire x1="5.08" y1="2.54" x2="45.72" y2="2.54" width="0.254" layer="94"/>
<wire x1="45.72" y1="-10.16" x2="45.72" y2="2.54" width="0.254" layer="94"/>
<wire x1="45.72" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<text x="46.99" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="46.99" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="NO___CONNECTION_1" x="0" y="0" length="middle"/>
<pin name="ANODE" x="0" y="-2.54" length="middle"/>
<pin name="CATHODE" x="0" y="-5.08" length="middle"/>
<pin name="NO___CONNECTION_2" x="0" y="-7.62" length="middle"/>
<pin name="VCC" x="50.8" y="0" length="middle" rot="R180"/>
<pin name="OUTPUT_2" x="50.8" y="-2.54" length="middle" rot="R180"/>
<pin name="OUTPUT_1" x="50.8" y="-5.08" length="middle" rot="R180"/>
<pin name="VEE" x="50.8" y="-7.62" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1N4148W-7-F" prefix="D">
<description>&lt;b&gt;Diode Switching 150mA 100V 400mW SOD123 Diodes Inc 1N4148W-7-F Switching Diode 100V, 2-Pin SOD-123&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/2/1N4148W-7-F.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="1N4148W-7-F" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD3716X145N">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="K" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Diode Switching 150mA 100V 400mW SOD123 Diodes Inc 1N4148W-7-F Switching Diode 100V, 2-Pin SOD-123" constant="no"/>
<attribute name="HEIGHT" value="1.45mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Diodes Inc." constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="1N4148W-7-F" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="621-1N4148W-F" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/1N4148W-7-F/?qs=LHX0FizJzg7Ae9ZM8LTAWw%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ERA-2ARB221X" prefix="R">
<description>&lt;b&gt;PANASONIC ELECTRONIC COMPONENTS - ERA2ARB221X - RES, THIN FILM, 220R, 0.1%, 0.063W, 0402&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/ERA-2ARB221X.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ERA-2ARB221X" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RESC1005X40N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="PANASONIC ELECTRONIC COMPONENTS - ERA2ARB221X - RES, THIN FILM, 220R, 0.1%, 0.063W, 0402" constant="no"/>
<attribute name="HEIGHT" value="0.4mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Panasonic" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ERA-2ARB221X" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="667-ERA-2ARB221X" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=667-ERA-2ARB221X" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LMK105BBJ475MVLF" prefix="C">
<description>&lt;b&gt;Capacitor MLCC 0402 X5R 10V 4.7uF&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/LMK105BBJ475MVLF.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="LMK105BBJ475MVLF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC1005X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Capacitor MLCC 0402 X5R 10V 4.7uF" constant="no"/>
<attribute name="HEIGHT" value="0.55mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="TAIYO YUDEN" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="LMK105BBJ475MVLF" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="963-LMK105BBJ475MVLF" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=963-LMK105BBJ475MVLF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STP16NF06L" prefix="IC">
<description>&lt;b&gt;STP16NF06L, N-channel MOSFET Transistor 16 A 60 V, 3-Pin TO-220&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.st.com/web/en/resource/technical/document/datasheet/CD00002848.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="STP16NF06L" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO270P460X1020X2008-3P">
<connects>
<connect gate="G$1" pin="D" pad="2"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="STP16NF06L, N-channel MOSFET Transistor 16 A 60 V, 3-Pin TO-220" constant="no"/>
<attribute name="HEIGHT" value="4.6mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="STMicroelectronics" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="STP16NF06L" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="511-STP16NF06L" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/STMicroelectronics/STP16NF06L?qs=RC432zO33OqodrhO5g7gPg%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1984617" prefix="J">
<description>&lt;b&gt;PCB terminal block, Nominal current: 17.5 A, Nom. voltage: 200 V, Pitch: 3.5 mm, Number of positions: 2, Connection method: Screw connection with wire protector, Mounting: Wave soldering, Conductor/PCB connection direction: 0 , Color: green&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.phoenixcontact.com/online/portal/in?uri=pxc-oc-itemdetail:pid=1984617&amp;library=inen&amp;tab=1"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="1984617" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1984617">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="PCB terminal block, Nominal current: 17.5 A, Nom. voltage: 200 V, Pitch: 3.5 mm, Number of positions: 2, Connection method: Screw connection with wire protector, Mounting: Wave soldering, Conductor/PCB connection direction: 0 , Color: green" constant="no"/>
<attribute name="HEIGHT" value="9.3mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Phoenix Contact" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="1984617" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="651-1984617" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/Phoenix-Contact/1984617/?qs=ATjOrvm3mOhUrGlJQoUcpQ%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FOD3180SDV" prefix="IC">
<description>&lt;b&gt;ON SEMICONDUCTOR - FOD3180SDV - OPTOCOUPLER, SMD, 2A, MOSFET, SMD&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.onsemi.com/pub/Collateral/FOD3180-D.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="FOD3180SDV" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP254P1030X508-8N">
<connects>
<connect gate="G$1" pin="ANODE" pad="2"/>
<connect gate="G$1" pin="CATHODE" pad="3"/>
<connect gate="G$1" pin="NO___CONNECTION_1" pad="1"/>
<connect gate="G$1" pin="NO___CONNECTION_2" pad="4"/>
<connect gate="G$1" pin="OUTPUT_1" pad="6"/>
<connect gate="G$1" pin="OUTPUT_2" pad="7"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VEE" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="FOD3180SDV" constant="no"/>
<attribute name="ARROW_PRICE-STOCK" value="https://www.arrow.com/en/products/fod3180sdv/on-semiconductor?region=nac" constant="no"/>
<attribute name="DESCRIPTION" value="ON SEMICONDUCTOR - FOD3180SDV - OPTOCOUPLER, SMD, 2A, MOSFET, SMD" constant="no"/>
<attribute name="HEIGHT" value="5.08mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="onsemi" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="FOD3180SDV" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="512-FOD3180SDV" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=512-FOD3180SDV" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="arduino">
<packages>
<package name="ARDUINO_NANO2">
<description>&lt;b&gt;Arduino Nano2&lt;/b&gt;&lt;p&gt;
Auto generated by &lt;i&gt;modulcreator.ulp&lt;/i&gt;&lt;br&gt;
\;</description>
<wire x1="-8.89" y1="-21.59" x2="8.89" y2="-21.59" width="0" layer="21"/>
<wire x1="8.89" y1="-21.59" x2="8.89" y2="21.59" width="0" layer="21"/>
<wire x1="8.89" y1="21.59" x2="-8.89" y2="21.59" width="0" layer="21"/>
<wire x1="-8.89" y1="21.59" x2="-8.89" y2="-21.59" width="0" layer="21"/>
<pad name="J1.1" x="-7.62" y="17.78" drill="0.9144" diameter="1.778" shape="square" rot="R270"/>
<pad name="J1.2" x="-7.62" y="15.24" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J1.3" x="-7.62" y="12.7" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J1.4" x="-7.62" y="10.16" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J1.5" x="-7.62" y="7.62" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J1.6" x="-7.62" y="5.08" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J1.7" x="-7.62" y="2.54" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J1.8" x="-7.62" y="0" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J1.9" x="-7.62" y="-2.54" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J1.10" x="-7.62" y="-5.08" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J1.11" x="-7.62" y="-7.62" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J1.12" x="-7.62" y="-10.16" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J1.13" x="-7.62" y="-12.7" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J1.14" x="-7.62" y="-15.24" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J1.15" x="-7.62" y="-17.78" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.1" x="7.62" y="17.78" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.2" x="7.62" y="15.24" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.3" x="7.62" y="12.7" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.4" x="7.62" y="10.16" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.5" x="7.62" y="7.62" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.6" x="7.62" y="5.08" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.7" x="7.62" y="2.54" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.8" x="7.62" y="0" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.9" x="7.62" y="-2.54" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.10" x="7.62" y="-5.08" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.11" x="7.62" y="-7.62" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.12" x="7.62" y="-10.16" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.13" x="7.62" y="-12.7" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.14" x="7.62" y="-15.24" drill="0.9144" diameter="1.778" rot="R270"/>
<pad name="J2.15" x="7.62" y="-17.78" drill="0.9144" diameter="1.778" rot="R270"/>
<text x="0" y="19.6" size="1" layer="25" font="vector">&gt;NAME</text>
<text x="0" y="-19.6" size="1" layer="27" font="vector">&gt;VALUE</text>
<hole x="-7.62" y="-20.32" drill="1.651"/>
<hole x="7.62" y="-20.32" drill="1.651"/>
<hole x="7.62" y="20.32" drill="1.651"/>
<hole x="-7.62" y="20.32" drill="1.651"/>
</package>
</packages>
<symbols>
<symbol name="ARDUINO_NANO2">
<wire x1="-20.32" y1="30.48" x2="20.32" y2="30.48" width="0.254" layer="94"/>
<wire x1="20.32" y1="30.48" x2="20.32" y2="-30.48" width="0.254" layer="94"/>
<wire x1="20.32" y1="-30.48" x2="-20.32" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-30.48" x2="-20.32" y2="30.48" width="0.254" layer="94"/>
<text x="-17.78" y="-25.4" size="1.778" layer="95">&gt;NAME</text>
<text x="-17.78" y="-27.94" size="1.778" layer="96">&gt;VALUE</text>
<pin name="D1/TX" x="25.4" y="27.94" length="middle" rot="R180"/>
<pin name="D0/RX" x="25.4" y="25.4" length="middle" rot="R180"/>
<pin name="RESET" x="-25.4" y="20.32" length="middle"/>
<pin name="COM" x="-2.54" y="-35.56" length="middle" direction="pwr" rot="R90"/>
<pin name="D2" x="25.4" y="22.86" length="middle" rot="R180"/>
<pin name="D3" x="25.4" y="20.32" length="middle" rot="R180"/>
<pin name="D4" x="25.4" y="17.78" length="middle" rot="R180"/>
<pin name="D5" x="25.4" y="15.24" length="middle" rot="R180"/>
<pin name="D6" x="25.4" y="12.7" length="middle" rot="R180"/>
<pin name="D7" x="25.4" y="10.16" length="middle" rot="R180"/>
<pin name="D8" x="25.4" y="7.62" length="middle" rot="R180"/>
<pin name="D9" x="25.4" y="5.08" length="middle" rot="R180"/>
<pin name="D10" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="D11/MOSI" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="D12/MISO" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="VIN" x="-10.16" y="35.56" length="middle" direction="pwr" rot="R270"/>
<pin name="COM@1" x="5.08" y="-35.56" length="middle" direction="pwr" rot="R90"/>
<pin name="RESET@2" x="-25.4" y="17.78" length="middle"/>
<pin name="+5V" x="-5.08" y="35.56" length="middle" direction="pwr" rot="R270"/>
<pin name="A7" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="A6" x="25.4" y="-12.7" length="middle" rot="R180"/>
<pin name="A5" x="25.4" y="-15.24" length="middle" rot="R180"/>
<pin name="A4" x="25.4" y="-17.78" length="middle" rot="R180"/>
<pin name="A3" x="25.4" y="-20.32" length="middle" rot="R180"/>
<pin name="A2" x="25.4" y="-22.86" length="middle" rot="R180"/>
<pin name="A1" x="25.4" y="-25.4" length="middle" rot="R180"/>
<pin name="A0" x="25.4" y="-27.94" length="middle" rot="R180"/>
<pin name="AREF" x="7.62" y="35.56" length="middle" direction="pwr" rot="R270"/>
<pin name="3V3" x="2.54" y="35.56" length="middle" direction="pwr" rot="R270"/>
<pin name="D13/SCK" x="25.4" y="-5.08" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ARDUINO_NANO2" prefix="MODUL">
<description>&lt;b&gt;Arduino Nano2&lt;/b&gt;&lt;p&gt;
Auto generated by &lt;i&gt;modulcreator.ulp&lt;/i&gt;&lt;br&gt;
\;</description>
<gates>
<gate name="MODUL" symbol="ARDUINO_NANO2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ARDUINO_NANO2">
<connects>
<connect gate="MODUL" pin="+5V" pad="J2.4"/>
<connect gate="MODUL" pin="3V3" pad="J2.14"/>
<connect gate="MODUL" pin="A0" pad="J2.12"/>
<connect gate="MODUL" pin="A1" pad="J2.11"/>
<connect gate="MODUL" pin="A2" pad="J2.10"/>
<connect gate="MODUL" pin="A3" pad="J2.9"/>
<connect gate="MODUL" pin="A4" pad="J2.8"/>
<connect gate="MODUL" pin="A5" pad="J2.7"/>
<connect gate="MODUL" pin="A6" pad="J2.6"/>
<connect gate="MODUL" pin="A7" pad="J2.5"/>
<connect gate="MODUL" pin="AREF" pad="J2.13"/>
<connect gate="MODUL" pin="COM" pad="J1.4"/>
<connect gate="MODUL" pin="COM@1" pad="J2.2"/>
<connect gate="MODUL" pin="D0/RX" pad="J1.2"/>
<connect gate="MODUL" pin="D1/TX" pad="J1.1"/>
<connect gate="MODUL" pin="D10" pad="J1.13"/>
<connect gate="MODUL" pin="D11/MOSI" pad="J1.14"/>
<connect gate="MODUL" pin="D12/MISO" pad="J1.15"/>
<connect gate="MODUL" pin="D13/SCK" pad="J2.15"/>
<connect gate="MODUL" pin="D2" pad="J1.5"/>
<connect gate="MODUL" pin="D3" pad="J1.6"/>
<connect gate="MODUL" pin="D4" pad="J1.7"/>
<connect gate="MODUL" pin="D5" pad="J1.8"/>
<connect gate="MODUL" pin="D6" pad="J1.9"/>
<connect gate="MODUL" pin="D7" pad="J1.10"/>
<connect gate="MODUL" pin="D8" pad="J1.11"/>
<connect gate="MODUL" pin="D9" pad="J1.12"/>
<connect gate="MODUL" pin="RESET" pad="J1.3"/>
<connect gate="MODUL" pin="RESET@2" pad="J2.3"/>
<connect gate="MODUL" pin="VIN" pad="J2.1"/>
</connects>
<technologies>
<technology name="">
<attribute name="ANZAHL" value="2"/>
<attribute name="MODUL" value="TRUE"/>
<attribute name="STECKPSI0" value=""/>
<attribute name="STECKPSI1" value=""/>
<attribute name="STECKX0" value="-76200"/>
<attribute name="STECKX1" value="76200"/>
<attribute name="STECKY0" value="177800"/>
<attribute name="STECKY1" value="177800"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-molex" urn="urn:adsk.eagle:library:165">
<description>&lt;b&gt;Molex Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="22-23-2051" urn="urn:adsk.eagle:footprint:8078262/1" library_version="5">
<description>&lt;b&gt;KK® 254 Solid Header, Vertical, with Friction Lock, 5 Circuits, Tin (Sn) Plating&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/022232051_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<wire x1="-6.35" y1="3.175" x2="6.35" y2="3.175" width="0.254" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="1.27" width="0.254" layer="21"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="-3.175" width="0.254" layer="21"/>
<wire x1="6.35" y1="-3.175" x2="-6.35" y2="-3.175" width="0.254" layer="21"/>
<wire x1="-6.35" y1="-3.175" x2="-6.35" y2="1.27" width="0.254" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="-6.35" y2="3.175" width="0.254" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="6.35" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1" shape="long" rot="R90"/>
<pad name="3" x="0" y="0" drill="1" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1" shape="long" rot="R90"/>
<text x="-6.35" y="3.81" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="22-23-2051" urn="urn:adsk.eagle:package:8078636/1" type="box" library_version="5">
<description>&lt;b&gt;KK® 254 Solid Header, Vertical, with Friction Lock, 5 Circuits, Tin (Sn) Plating&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/022232051_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<packageinstances>
<packageinstance name="22-23-2051"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MV" urn="urn:adsk.eagle:symbol:6783/2" library_version="5">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M" urn="urn:adsk.eagle:symbol:6785/2" library_version="5">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="22-23-2051" urn="urn:adsk.eagle:component:8078935/3" prefix="X" library_version="5">
<description>.100" (2.54mm) Center Header - 5 Pin</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="5.08" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="0" y="-5.08" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="22-23-2051">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8078636/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="22-23-2051" constant="no"/>
<attribute name="OC_FARNELL" value="1462952" constant="no"/>
<attribute name="OC_NEWARK" value="38C9178" constant="no"/>
<attribute name="POPULARITY" value="4" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.254" drill="0.508">
<clearance class="0" value="0.508"/>
</class>
</classes>
<parts>
<part name="D1" library="SamacSys_Parts" deviceset="1N4148W-7-F" device=""/>
<part name="R5" library="SamacSys_Parts" deviceset="ERA-2ARB221X" device="" value="120"/>
<part name="MODUL1" library="arduino" deviceset="ARDUINO_NANO2" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="X1" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2051" device="" package3d_urn="urn:adsk.eagle:package:8078636/1"/>
<part name="C1" library="SamacSys_Parts" deviceset="LMK105BBJ475MVLF" device=""/>
<part name="R1" library="SamacSys_Parts" deviceset="ERA-2ARB221X" device="" value="4.7"/>
<part name="SW1" library="SamacSys_Parts" deviceset="STP16NF06L" device=""/>
<part name="R9" library="SamacSys_Parts" deviceset="ERA-2ARB221X" device="" value="10k"/>
<part name="SUPPLY3" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="J1" library="SamacSys_Parts" deviceset="1984617" device=""/>
<part name="D3" library="SamacSys_Parts" deviceset="1N4148W-7-F" device=""/>
<part name="R2" library="SamacSys_Parts" deviceset="ERA-2ARB221X" device="" value="120"/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C2" library="SamacSys_Parts" deviceset="LMK105BBJ475MVLF" device=""/>
<part name="R3" library="SamacSys_Parts" deviceset="ERA-2ARB221X" device="" value="4.7"/>
<part name="SW3" library="SamacSys_Parts" deviceset="STP16NF06L" device=""/>
<part name="R4" library="SamacSys_Parts" deviceset="ERA-2ARB221X" device="" value="10k"/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="J3" library="SamacSys_Parts" deviceset="1984617" device=""/>
<part name="D2" library="SamacSys_Parts" deviceset="1N4148W-7-F" device=""/>
<part name="R6" library="SamacSys_Parts" deviceset="ERA-2ARB221X" device="" value="120"/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C3" library="SamacSys_Parts" deviceset="LMK105BBJ475MVLF" device=""/>
<part name="R7" library="SamacSys_Parts" deviceset="ERA-2ARB221X" device="" value="4.7"/>
<part name="SW2" library="SamacSys_Parts" deviceset="STP16NF06L" device=""/>
<part name="R8" library="SamacSys_Parts" deviceset="ERA-2ARB221X" device="" value="10k"/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="J2" library="SamacSys_Parts" deviceset="1984617" device=""/>
<part name="D4" library="SamacSys_Parts" deviceset="1N4148W-7-F" device=""/>
<part name="R10" library="SamacSys_Parts" deviceset="ERA-2ARB221X" device="" value="120"/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C4" library="SamacSys_Parts" deviceset="LMK105BBJ475MVLF" device=""/>
<part name="R11" library="SamacSys_Parts" deviceset="ERA-2ARB221X" device="" value="4.7"/>
<part name="SW4" library="SamacSys_Parts" deviceset="STP16NF06L" device=""/>
<part name="R12" library="SamacSys_Parts" deviceset="ERA-2ARB221X" device="" value="10k"/>
<part name="SUPPLY4" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="J4" library="SamacSys_Parts" deviceset="1984617" device=""/>
<part name="J5" library="SamacSys_Parts" deviceset="1984617" device=""/>
<part name="SUPPLY5" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="IC1" library="SamacSys_Parts" deviceset="FOD3180SDV" device=""/>
<part name="IC2" library="SamacSys_Parts" deviceset="FOD3180SDV" device=""/>
<part name="IC3" library="SamacSys_Parts" deviceset="FOD3180SDV" device=""/>
<part name="IC4" library="SamacSys_Parts" deviceset="FOD3180SDV" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="D1" gate="G$1" x="139.7" y="111.76" smashed="yes" rot="R270">
<attribute name="NAME" x="134.62" y="105.41" size="1.778" layer="95" rot="R270" align="center-left"/>
</instance>
<instance part="R5" gate="G$1" x="2.54" y="96.52" smashed="yes">
<attribute name="NAME" x="8.382" y="99.06" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="9.906" y="96.52" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="MODUL1" gate="MODUL" x="-137.16" y="154.94" smashed="yes">
<attribute name="NAME" x="-154.94" y="129.54" size="1.778" layer="95"/>
<attribute name="VALUE" x="-154.94" y="127" size="1.778" layer="96"/>
</instance>
<instance part="P+1" gate="VCC" x="76.2" y="137.16" smashed="yes">
<attribute name="VALUE" x="73.66" y="134.62" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X1" gate="-1" x="-40.64" y="83.82" smashed="yes" rot="R180">
<attribute name="NAME" x="-43.18" y="84.582" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="-39.878" y="82.423" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="X1" gate="-2" x="-40.64" y="86.36" smashed="yes" rot="R180">
<attribute name="NAME" x="-43.18" y="87.122" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X1" gate="-3" x="-40.64" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="-43.18" y="89.662" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X1" gate="-4" x="-40.64" y="91.44" smashed="yes" rot="R180">
<attribute name="NAME" x="-43.18" y="92.202" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X1" gate="-5" x="-40.64" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="-43.18" y="94.742" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="C1" gate="G$1" x="78.74" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="80.01" y="80.01" size="1.778" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="R1" gate="G$1" x="81.28" y="93.98" smashed="yes">
<attribute name="NAME" x="88.9" y="97.028" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="88.646" y="93.726" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="SW1" gate="G$1" x="106.68" y="99.06" smashed="yes" rot="R90">
<attribute name="NAME" x="101.6" y="105.41" size="1.778" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="R9" gate="G$1" x="106.68" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="103.632" y="83.566" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="106.68" y="83.566" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="SUPPLY3" gate="GND" x="106.68" y="73.66" smashed="yes">
<attribute name="VALUE" x="104.775" y="70.485" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="149.86" y="116.84" smashed="yes">
<attribute name="NAME" x="166.37" y="124.46" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="D3" gate="G$1" x="157.48" y="53.34" smashed="yes" rot="R270">
<attribute name="NAME" x="152.4" y="46.99" size="1.778" layer="95" rot="R270" align="center-left"/>
</instance>
<instance part="R2" gate="G$1" x="7.62" y="38.1" smashed="yes">
<attribute name="NAME" x="13.208" y="40.894" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="14.986" y="38.354" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="P+2" gate="VCC" x="93.98" y="63.5" smashed="yes">
<attribute name="VALUE" x="96.52" y="63.5" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C2" gate="G$1" x="93.98" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="95.25" y="26.67" size="1.778" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="R3" gate="G$1" x="99.06" y="35.56" smashed="yes">
<attribute name="NAME" x="106.68" y="38.608" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="106.426" y="35.306" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="SW3" gate="G$1" x="124.46" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="119.38" y="46.99" size="1.778" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="R4" gate="G$1" x="124.46" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="121.412" y="25.146" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="124.46" y="25.146" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="124.46" y="15.24" smashed="yes">
<attribute name="VALUE" x="122.555" y="12.065" size="1.778" layer="96"/>
</instance>
<instance part="J3" gate="G$1" x="167.64" y="58.42" smashed="yes">
<attribute name="NAME" x="184.15" y="66.04" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="D2" gate="G$1" x="157.48" y="-5.08" smashed="yes" rot="R270">
<attribute name="NAME" x="152.4" y="-11.43" size="1.778" layer="95" rot="R270" align="center-left"/>
</instance>
<instance part="R6" gate="G$1" x="7.62" y="-20.32" smashed="yes">
<attribute name="NAME" x="13.208" y="-17.272" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="14.986" y="-20.32" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="P+3" gate="VCC" x="93.98" y="12.7" smashed="yes">
<attribute name="VALUE" x="91.44" y="10.16" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C3" gate="G$1" x="93.98" y="-33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="95.25" y="-31.75" size="1.778" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="R7" gate="G$1" x="99.06" y="-22.86" smashed="yes">
<attribute name="NAME" x="106.68" y="-19.812" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="106.426" y="-23.114" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="SW2" gate="G$1" x="124.46" y="-17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="119.38" y="-11.43" size="1.778" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="R8" gate="G$1" x="124.46" y="-40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="121.412" y="-33.274" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="124.46" y="-33.274" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="124.46" y="-43.18" smashed="yes">
<attribute name="VALUE" x="122.555" y="-46.355" size="1.778" layer="96"/>
</instance>
<instance part="J2" gate="G$1" x="167.64" y="0" smashed="yes">
<attribute name="NAME" x="184.15" y="7.62" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="D4" gate="G$1" x="157.48" y="-66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="152.4" y="-72.39" size="1.778" layer="95" rot="R270" align="center-left"/>
</instance>
<instance part="R10" gate="G$1" x="7.62" y="-81.28" smashed="yes">
<attribute name="NAME" x="13.97" y="-78.74" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="14.986" y="-81.28" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="P+4" gate="VCC" x="93.98" y="-48.26" smashed="yes">
<attribute name="VALUE" x="91.44" y="-50.8" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="93.98" y="-93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="95.25" y="-92.71" size="1.778" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="R11" gate="G$1" x="99.06" y="-83.82" smashed="yes">
<attribute name="NAME" x="106.68" y="-80.772" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="106.426" y="-84.074" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="SW4" gate="G$1" x="124.46" y="-78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="119.38" y="-72.39" size="1.778" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="R12" gate="G$1" x="124.46" y="-101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="121.412" y="-94.234" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="124.46" y="-94.234" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="SUPPLY4" gate="GND" x="124.46" y="-104.14" smashed="yes">
<attribute name="VALUE" x="122.555" y="-107.315" size="1.778" layer="96"/>
</instance>
<instance part="J4" gate="G$1" x="167.64" y="-60.96" smashed="yes">
<attribute name="NAME" x="184.15" y="-53.34" size="1.778" layer="95" align="center-left"/>
</instance>
<instance part="J5" gate="G$1" x="91.44" y="132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="83.82" y="148.59" size="1.778" layer="95" rot="R90" align="center-left"/>
</instance>
<instance part="SUPPLY5" gate="GND" x="93.98" y="124.46" smashed="yes">
<attribute name="VALUE" x="92.075" y="121.285" size="1.778" layer="96"/>
</instance>
<instance part="IC1" gate="G$1" x="22.86" y="99.06" smashed="yes">
<attribute name="NAME" x="26.67" y="106.68" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="26.67" y="104.14" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="IC2" gate="G$1" x="33.02" y="-17.78" smashed="yes">
<attribute name="NAME" x="97.79" y="-10.16" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="97.79" y="-12.7" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="IC3" gate="G$1" x="33.02" y="38.1" smashed="yes">
<attribute name="NAME" x="39.37" y="45.72" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="39.37" y="43.18" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="IC4" gate="G$1" x="30.48" y="-78.74" smashed="yes">
<attribute name="NAME" x="95.25" y="-71.12" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="95.25" y="-73.66" size="1.778" layer="96" align="center-left"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="3.3V" class="0">
<segment>
<wire x1="-38.1" y1="93.98" x2="2.54" y2="93.98" width="0.1524" layer="91"/>
<pinref part="X1" gate="-5" pin="S"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="2.54" y1="93.98" x2="7.62" y2="93.98" width="0.1524" layer="91"/>
<wire x1="7.62" y1="93.98" x2="7.62" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="7.62" y1="38.1" x2="7.62" y2="-20.32" width="0.1524" layer="91"/>
<junction x="7.62" y="38.1"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="7.62" y1="-20.32" x2="7.62" y2="-81.28" width="0.1524" layer="91"/>
<junction x="7.62" y="-20.32"/>
<label x="-38.1" y="93.98" size="1.778" layer="95"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="2.54" y1="96.52" x2="2.54" y2="93.98" width="0.1524" layer="91"/>
<junction x="2.54" y="93.98"/>
</segment>
</net>
<net name="D3" class="0">
<segment>
<pinref part="X1" gate="-4" pin="S"/>
<label x="-38.1" y="91.44" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="CATHODE"/>
<wire x1="15.24" y1="91.44" x2="-38.1" y2="91.44" width="0.1524" layer="91"/>
<wire x1="22.86" y1="93.98" x2="15.24" y2="93.98" width="0.1524" layer="91"/>
<wire x1="15.24" y1="93.98" x2="15.24" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<junction x="106.68" y="76.2"/>
<pinref part="SW1" gate="G$1" pin="S"/>
<wire x1="78.74" y1="76.2" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
<wire x1="111.76" y1="99.06" x2="111.76" y2="76.2" width="0.1524" layer="91"/>
<wire x1="111.76" y1="76.2" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="78.74" y1="78.74" x2="78.74" y2="76.2" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VEE"/>
<wire x1="73.66" y1="91.44" x2="73.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="73.66" y1="78.74" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<junction x="78.74" y="78.74"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<junction x="124.46" y="17.78"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="93.98" y1="17.78" x2="124.46" y2="17.78" width="0.1524" layer="91"/>
<wire x1="93.98" y1="25.4" x2="93.98" y2="17.78" width="0.1524" layer="91"/>
<pinref part="SW3" gate="G$1" pin="S"/>
<wire x1="129.54" y1="40.64" x2="129.54" y2="17.78" width="0.1524" layer="91"/>
<wire x1="129.54" y1="17.78" x2="124.46" y2="17.78" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="VEE"/>
<wire x1="83.82" y1="30.48" x2="83.82" y2="25.4" width="0.1524" layer="91"/>
<wire x1="83.82" y1="25.4" x2="93.98" y2="25.4" width="0.1524" layer="91"/>
<junction x="93.98" y="25.4"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="88.9" y1="-25.4" x2="88.9" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="88.9" y1="-40.64" x2="93.98" y2="-40.64" width="0.1524" layer="91"/>
<junction x="124.46" y="-40.64"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-40.64" x2="124.46" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-33.02" x2="93.98" y2="-40.64" width="0.1524" layer="91"/>
<junction x="93.98" y="-40.64"/>
<pinref part="SW2" gate="G$1" pin="S"/>
<wire x1="129.54" y1="-17.78" x2="129.54" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-40.64" x2="124.46" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="VEE"/>
<wire x1="83.82" y1="-25.4" x2="88.9" y2="-25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="88.9" y1="-86.36" x2="88.9" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="88.9" y1="-101.6" x2="93.98" y2="-101.6" width="0.1524" layer="91"/>
<junction x="124.46" y="-101.6"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-101.6" x2="124.46" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-93.98" x2="93.98" y2="-101.6" width="0.1524" layer="91"/>
<junction x="93.98" y="-101.6"/>
<pinref part="SW4" gate="G$1" pin="S"/>
<wire x1="129.54" y1="-78.74" x2="129.54" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-101.6" x2="124.46" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="VEE"/>
<wire x1="81.28" y1="-86.36" x2="88.9" y2="-86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="2"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="93.98" y1="132.08" x2="93.98" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="106.68" y1="93.98" x2="99.06" y2="93.98" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="G"/>
<wire x1="106.68" y1="99.06" x2="106.68" y2="93.98" width="0.1524" layer="91"/>
<junction x="106.68" y="93.98"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="76.2" y1="134.62" x2="76.2" y2="129.54" width="0.1524" layer="91"/>
<wire x1="76.2" y1="129.54" x2="76.2" y2="116.84" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="K"/>
<wire x1="139.7" y1="111.76" x2="139.7" y2="116.84" width="0.1524" layer="91"/>
<wire x1="139.7" y1="116.84" x2="76.2" y2="116.84" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="139.7" y1="116.84" x2="149.86" y2="116.84" width="0.1524" layer="91"/>
<junction x="139.7" y="116.84"/>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="91.44" y1="132.08" x2="91.44" y2="129.54" width="0.1524" layer="91"/>
<wire x1="91.44" y1="129.54" x2="76.2" y2="129.54" width="0.1524" layer="91"/>
<junction x="76.2" y="129.54"/>
<wire x1="76.2" y1="116.84" x2="76.2" y2="99.06" width="0.1524" layer="91"/>
<junction x="76.2" y="116.84"/>
<pinref part="IC1" gate="G$1" pin="VCC"/>
<wire x1="76.2" y1="99.06" x2="73.66" y2="99.06" width="0.1524" layer="91"/>
<wire x1="76.2" y1="99.06" x2="78.74" y2="99.06" width="0.1524" layer="91"/>
<junction x="76.2" y="99.06"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="78.74" y1="99.06" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="93.98" y1="60.96" x2="93.98" y2="58.42" width="0.1524" layer="91"/>
<wire x1="93.98" y1="58.42" x2="93.98" y2="38.1" width="0.1524" layer="91"/>
<junction x="93.98" y="38.1"/>
<pinref part="D3" gate="G$1" pin="K"/>
<wire x1="157.48" y1="53.34" x2="157.48" y2="58.42" width="0.1524" layer="91"/>
<wire x1="157.48" y1="58.42" x2="93.98" y2="58.42" width="0.1524" layer="91"/>
<junction x="93.98" y="58.42"/>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="157.48" y1="58.42" x2="167.64" y2="58.42" width="0.1524" layer="91"/>
<junction x="157.48" y="58.42"/>
<pinref part="IC3" gate="G$1" pin="VCC"/>
<wire x1="83.82" y1="38.1" x2="93.98" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+3" gate="VCC" pin="VCC"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="93.98" y1="10.16" x2="93.98" y2="0" width="0.1524" layer="91"/>
<wire x1="93.98" y1="0" x2="93.98" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="K"/>
<wire x1="93.98" y1="-17.78" x2="93.98" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="157.48" y1="-5.08" x2="157.48" y2="0" width="0.1524" layer="91"/>
<wire x1="157.48" y1="0" x2="93.98" y2="0" width="0.1524" layer="91"/>
<junction x="93.98" y="0"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="157.48" y1="0" x2="167.64" y2="0" width="0.1524" layer="91"/>
<junction x="157.48" y="0"/>
<pinref part="IC2" gate="G$1" pin="VCC"/>
<wire x1="83.82" y1="-17.78" x2="93.98" y2="-17.78" width="0.1524" layer="91"/>
<junction x="93.98" y="-17.78"/>
</segment>
<segment>
<pinref part="P+4" gate="VCC" pin="VCC"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="93.98" y1="-50.8" x2="93.98" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-60.96" x2="93.98" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="K"/>
<wire x1="93.98" y1="-78.74" x2="93.98" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="157.48" y1="-66.04" x2="157.48" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="157.48" y1="-60.96" x2="93.98" y2="-60.96" width="0.1524" layer="91"/>
<junction x="93.98" y="-60.96"/>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="157.48" y1="-60.96" x2="167.64" y2="-60.96" width="0.1524" layer="91"/>
<junction x="157.48" y="-60.96"/>
<pinref part="IC4" gate="G$1" pin="VCC"/>
<wire x1="81.28" y1="-78.74" x2="93.98" y2="-78.74" width="0.1524" layer="91"/>
<junction x="93.98" y="-78.74"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="SW1" gate="G$1" pin="D"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="109.22" y1="99.06" x2="109.22" y2="96.52" width="0.1524" layer="91"/>
<wire x1="109.22" y1="96.52" x2="139.7" y2="96.52" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="149.86" y1="114.3" x2="149.86" y2="96.52" width="0.1524" layer="91"/>
<wire x1="149.86" y1="96.52" x2="139.7" y2="96.52" width="0.1524" layer="91"/>
<junction x="139.7" y="96.52"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="124.46" y1="35.56" x2="116.84" y2="35.56" width="0.1524" layer="91"/>
<pinref part="SW3" gate="G$1" pin="G"/>
<wire x1="124.46" y1="40.64" x2="124.46" y2="35.56" width="0.1524" layer="91"/>
<junction x="124.46" y="35.56"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="SW3" gate="G$1" pin="D"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="127" y1="40.64" x2="127" y2="38.1" width="0.1524" layer="91"/>
<wire x1="127" y1="38.1" x2="157.48" y2="38.1" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="167.64" y1="55.88" x2="167.64" y2="38.1" width="0.1524" layer="91"/>
<wire x1="167.64" y1="38.1" x2="157.48" y2="38.1" width="0.1524" layer="91"/>
<junction x="157.48" y="38.1"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="124.46" y1="-22.86" x2="116.84" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="SW2" gate="G$1" pin="G"/>
<wire x1="124.46" y1="-17.78" x2="124.46" y2="-22.86" width="0.1524" layer="91"/>
<junction x="124.46" y="-22.86"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="83.82" y1="-22.86" x2="99.06" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="OUTPUT_1"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="SW2" gate="G$1" pin="D"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="127" y1="-17.78" x2="127" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="127" y1="-20.32" x2="157.48" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="167.64" y1="-2.54" x2="167.64" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-20.32" x2="157.48" y2="-20.32" width="0.1524" layer="91"/>
<junction x="157.48" y="-20.32"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="124.46" y1="-83.82" x2="116.84" y2="-83.82" width="0.1524" layer="91"/>
<pinref part="SW4" gate="G$1" pin="G"/>
<wire x1="124.46" y1="-78.74" x2="124.46" y2="-83.82" width="0.1524" layer="91"/>
<junction x="124.46" y="-83.82"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="81.28" y1="-83.82" x2="99.06" y2="-83.82" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="OUTPUT_1"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="SW4" gate="G$1" pin="D"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="127" y1="-78.74" x2="127" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="127" y1="-81.28" x2="157.48" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="167.64" y1="-63.5" x2="167.64" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-81.28" x2="157.48" y2="-81.28" width="0.1524" layer="91"/>
<junction x="157.48" y="-81.28"/>
</segment>
</net>
<net name="D4" class="0">
<segment>
<wire x1="33.02" y1="33.02" x2="2.54" y2="33.02" width="0.1524" layer="91"/>
<wire x1="2.54" y1="33.02" x2="2.54" y2="88.9" width="0.1524" layer="91"/>
<pinref part="X1" gate="-3" pin="S"/>
<wire x1="2.54" y1="88.9" x2="-38.1" y2="88.9" width="0.1524" layer="91"/>
<label x="-38.1" y="88.9" size="1.778" layer="95"/>
<pinref part="IC3" gate="G$1" pin="CATHODE"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<pinref part="X1" gate="-2" pin="S"/>
<wire x1="-2.54" y1="-22.86" x2="-2.54" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="86.36" x2="-38.1" y2="86.36" width="0.1524" layer="91"/>
<label x="-38.1" y="86.36" size="1.778" layer="95"/>
<pinref part="IC2" gate="G$1" pin="CATHODE"/>
<wire x1="33.02" y1="-22.86" x2="-2.54" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<pinref part="X1" gate="-1" pin="S"/>
<wire x1="-7.62" y1="-83.82" x2="-7.62" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="83.82" x2="-38.1" y2="83.82" width="0.1524" layer="91"/>
<label x="-38.1" y="83.82" size="1.778" layer="95"/>
<pinref part="IC4" gate="G$1" pin="CATHODE"/>
<wire x1="30.48" y1="-83.82" x2="-7.62" y2="-83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="IC1" gate="G$1" pin="ANODE"/>
<wire x1="20.32" y1="96.52" x2="22.86" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="OUTPUT_2"/>
<wire x1="71.12" y1="96.52" x2="73.66" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="OUTPUT_1"/>
<wire x1="83.82" y1="33.02" x2="88.9" y2="33.02" width="0.1524" layer="91"/>
<wire x1="88.9" y1="33.02" x2="88.9" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="88.9" y1="35.56" x2="99.06" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="25.4" y1="38.1" x2="25.4" y2="35.56" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="ANODE"/>
<wire x1="25.4" y1="35.56" x2="33.02" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="OUTPUT_1"/>
<wire x1="81.28" y1="93.98" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="IC2" gate="G$1" pin="ANODE"/>
<wire x1="25.4" y1="-20.32" x2="33.02" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="IC4" gate="G$1" pin="ANODE"/>
<wire x1="25.4" y1="-81.28" x2="30.48" y2="-81.28" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="204,1,40.64,127,MODUL1,COM,,,,"/>
<approved hash="204,1,33.02,198.12,MODUL1,VIN,,,,"/>
<approved hash="204,1,48.26,127,MODUL1,COM,,,,"/>
<approved hash="204,1,38.1,198.12,MODUL1,+5V,,,,"/>
<approved hash="204,1,50.8,198.12,MODUL1,AREF,,,,"/>
<approved hash="204,1,45.72,198.12,MODUL1,3V3,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
