﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="22308000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="UserConfigFile.vi" Type="VI" URL="../UserConfigFile.vi"/>
		<Item Name="UserCurrOFF(SubVI).vi" Type="VI" URL="../UserCurrOFF(SubVI).vi"/>
		<Item Name="UserCurrON(SubVI).vi" Type="VI" URL="../UserCurrON(SubVI).vi"/>
		<Item Name="UserSetCurr(SubVI).vi" Type="VI" URL="../UserSetCurr(SubVI).vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="Configure Current Limit.vi" Type="VI" URL="../Configure Current Limit.vi"/>
			<Item Name="Configure Output.vi" Type="VI" URL="../Configure Output.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
