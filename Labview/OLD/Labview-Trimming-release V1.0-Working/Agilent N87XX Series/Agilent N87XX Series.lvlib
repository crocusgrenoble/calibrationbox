﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="21008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.DefaultMenu" Type="Str">dir.mnu</Property>
	<Property Name="NI.Lib.Description" Type="Str">LabVIEW Plug and Play instrument driver for
the Agilent N87XX series of DC Power Supplies</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(Z!!!*Q(C=\&gt;4"&gt;&gt;J!%!&lt;A=6Y/O&gt;*"(CV-#\2!#^-#6R^J96KA"6K9&amp;GC"&amp;JQ0?:/85\AE,TZ9]G,R\WLWUUII9NO_28S.NW&gt;&lt;`((\\0`M`_T`S0X0@^[P4Q@^YSXC::.KI\677GZ8^T&lt;SE9^]Z#-@?=N&lt;XP+7N\TF*3^ZS5N?]J+H0/5J4XH+5\\N]J#(0/27:5Q_*BJ&amp;2Y%R'#8'R`!-T`!-D[]R0--T0--T0)&lt;%]!T0]!T0]$ANBG&gt;YBG&gt;YBE?J44,&lt;,O=:(O7D?:KH?:KH?5Q:T&gt;-!&lt;&lt;*7O"8"C.&lt;:$JKH?:L(9420]T20]T30\GC?ZGG?ZGE?JWSLUJPGM=NZF)HC+:\C+:\C54K+JXC+JXC+RX220-64%'8#5BQBSEFF10F30-8D8R20]220]230LOU/V&lt;9S$]VDF`-E4`)E4`)EDR+20-G40-G40-J']C20]C20]JAKEC&gt;ZEC&gt;"UK3GDV1MH:A'J3"Z`'V03WZXK4&gt;*&lt;PP[;&lt;Y`K,%?Q&amp;A06KQ(*N;$%/M'R\JRM7Z)L)7/N9#R&amp;C&lt;7"=?[E&amp;D!7"0(+BBL9.R^XL3L&gt;N(/WEE\;A&gt;NL_U?J`\FA@@\07[X7VSPV\B=,H%_H_.U/M8R?)T$Y2$\`4ZWO^WPN^6X_`PW]P/^^/LY@\4X^^*(?$@'FXD\\46PD8Y!85PX:!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">553680896</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Action-Status" Type="Folder">
			<Item Name="Action-Status.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Action-Status/Action-Status.mnu"/>
			<Item Name="Abort Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Action-Status/Abort Trigger.vi"/>
			<Item Name="Output Protection Status.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Action-Status/Output Protection Status.vi"/>
			<Item Name="Reset Output Protection.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Action-Status/Reset Output Protection.vi"/>
			<Item Name="Send Software Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Action-Status/Send Software Trigger.vi"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Configure.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Configure/Configure.mnu"/>
			<Item Name="Configure Current Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Configure/Configure Current Limit.vi"/>
			<Item Name="Configure Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Configure/Configure Output.vi"/>
			<Item Name="Configure Voltage Protection.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Configure/Configure Voltage Protection.vi"/>
			<Item Name="Arm Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Action-Status/Arm Trigger.vi"/>
			<Item Name="Enable Continuous Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Action-Status/Enable Continuous Trigger.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="Data.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Data/Data.mnu"/>
			<Item Name="Read Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Data/Read Output.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Utility.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Utility/Utility.mnu"/>
			<Item Name="Error Query.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Utility/Error Query.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Utility/Reset.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Utility/Revision Query.vi"/>
			<Item Name="Self-Test.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Utility/Self-Test.vi"/>
		</Item>
		<Item Name="dir.mnu" Type="Document" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/dir.mnu"/>
		<Item Name="Close.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Public/VI Tree.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Default Instrument Setup.vi" Type="VI" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Private/Default Instrument Setup.vi"/>
	</Item>
	<Item Name="Agilent N87XX Series Readme.html" Type="Document" URL="/&lt;instrlib&gt;/Agilent N87XX Series/Agilent N87XX Series Readme.html"/>
</Library>
