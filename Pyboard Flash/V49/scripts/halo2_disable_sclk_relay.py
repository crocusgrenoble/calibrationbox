output = []
from pyb import Pin
flag_sclk = Pin('Y9', Pin.IN)
out_sdata = Pin('X8', Pin.IN)
halo2_sclk_disconnect = Pin('X6', Pin.OUT_PP)
halo2_sdata_disconnect = Pin('X4', Pin.OUT_PP)
halo2_5v5_clk = Pin('Y2', Pin.OUT_PP) 


halo2_filter_sclk = Pin('X7', Pin.IN)
halo2_sclk_disconnect.low()