from pyb import Pin
output = []
dut_ps_en_pin = Pin('Y12', Pin.OUT_PP)
VDD_3V3 = Pin('X3', Pin.OUT_PP)
VDD_4V = Pin('X2', Pin.OUT_PP)
VDD_5V = Pin('X1', Pin.OUT_PP)
dut_ps_en_pin.low()