from pyb import Pin,SPI
output = []
halo2_sclk_disconnect = Pin('X6', Pin.OUT_PP)
halo2_sdata_disconnect = Pin('X4', Pin.OUT_PP)
halo2_sclk_disconnect.high()
halo2_sdata_disconnect.high()
halo2_5v5_clk = Pin('Y2', Pin.OUT_PP)    
out_sdata = Pin('X8', Pin.IN)
filter_sclk = Pin('X7', Pin.IN)
halo2_5v5_clk.low()
filter_sclk.low()
out_sdata.low()