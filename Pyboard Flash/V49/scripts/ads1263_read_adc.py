from pyb import Pin,SPI
import time
output = []
adc_drdy = Pin('X11', Pin.IN)
adc_cs = Pin('Y5', Pin.OUT_PP)
adc_start = Pin('X12', Pin.OUT_PP)
halo2_sclk_disconnect = Pin('X6', Pin.OUT_PP)
halo2_sdata_disconnect = Pin('X4', Pin.OUT_PP)
halo2_sdata_dir = Pin('Y3', Pin.OUT_PP)
halo2_sclk_disconnect.high()
halo2_sdata_disconnect.high()
halo2_sdata_dir.high()
adc_start.low()
spi = SPI(2, SPI.MASTER, baudrate=200000, polarity=1, phase=1)
time.sleep(0.001)
adc_start.high()
while(adc_drdy.value() == 1):
    ...
adc_start.low()
    
adc_cs.low()
time.sleep(0.001)

r = spi.send_recv('\0\0\0\0\0\0\0\0\0\0\0')
output.append(["".join("\\x%02x" % i for i in r)])
adc_cs.high()
