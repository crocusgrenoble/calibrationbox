from pyb import Pin
import time
def read_back_bits_from_halo2(bits=[]):
    # Keycode: 11110010
    bits = [True,True,True,True,False,False,True,False,\
    # read back opcode: 11
    True,True,False,False,False,False,False,False,False,False,False,False,\
    False,False,False,False,False,False]
    halo2_sdata_dir = Pin('Y3', Pin.OUT_PP)
    halo2_5v5_clk = Pin('Y2', Pin.OUT_PP)
    halo2_sdata_dir.high()
    halo2_5v5_clk.low()
    filter_sclk = Pin('X7', Pin.OUT_PP)
    out_sdata = Pin('X8', Pin.OUT_PP)
    halo2_sclk_disconnect = Pin('X6', Pin.OUT_PP)
    halo2_sdata_disconnect = Pin('X4', Pin.OUT_PP)
    halo2_sclk_disconnect.low()
    halo2_sdata_disconnect.low()
    filter_sclk.low()
    out_sdata.low()    
    halo2_sclk_disconnect.high()
    time.sleep(0.010)
    halo2_5v5_clk.high()
    time.sleep(0.000002)    
    halo2_5v5_clk.low()
    halo2_sclk_disconnect.low()  #Connect
    for i in range(26):
        if bits[i] == True:
            out_sdata.high()
        else:
            out_sdata.low()  
        filter_sclk.high()
        time.sleep(0.000001)
        filter_sclk.low()    
    out_sdata = Pin('X8', Pin.IN)
    halo2_sdata_dir.low()
    output = []
    for i in range(80):    
        filter_sclk.high()
        filter_sclk.low()
        if out_sdata.value() == True:
            output.append(1)
        else:
            output.append(0)    
    print(output)
    time.sleep(0.002)
    halo2_sclk_disconnect.high()
    halo2_sdata_disconnect.high()    
    out_sdata.low()
    filter_sclk.low()