from pyb import Pin, LED
import time
def send_bits_to_halo2(bits=[],length=0):
    # test bits:
    #bits = [True,False,True,True,False,False,True,False]    
    halo2_sdata_dir = Pin('X9', Pin.OUT_PP)
    halo2_5v5_clk = Pin('X10', Pin.OUT_PP)
    halo2_sdata_dir.high()
    halo2_5v5_clk.low()
    filter_sclk = Pin('X7', Pin.OUT_PP)
    out_sdata = Pin('X8', Pin.OUT_PP)
    halo2_sclk_disconnect = Pin('X6', Pin.OUT_PP)
    halo2_sdata_disconnect = Pin('X4', Pin.OUT_PP)
    halo2_sclk_disconnect.low()
    halo2_sdata_disconnect.low()
    filter_sclk.low()
    out_sdata.low()    
    halo2_sclk_disconnect.high()
    time.sleep(0.010)
    halo2_5v5_clk.high()
    time.sleep(0.000002)
    halo2_5v5_clk.low()
    halo2_sclk_disconnect.low()  #Connect   
    for i in range(len(bits)):
        if bits[i] == True:
            out_sdata.high()
        else:
            out_sdata.low()
        filter_sclk.high()
        time.sleep(0.000001)
        filter_sclk.low()    
    time.sleep(0.002)
    halo2_sdata_disconnect.high()
    halo2_sclk_disconnect.high()
    #halo2_sdata_dir.low()
    out_sdata.low()
    filter_sclk.low()