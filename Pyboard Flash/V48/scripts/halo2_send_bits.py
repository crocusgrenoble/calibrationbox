from pyb import Pin
def send_bits_to_halo2(bits=[]):
    # 01001101
    global output
    bits = [True,True,True,True,False,False,True,False,\
    # 01
    True,True,\
    ]
    filter_sclk = Pin('X7', Pin.OPEN_DRAIN)
    out_sdata = Pin('X8', Pin.OUT_PP)
    filter_sclk.low()
    filter_sclk.high()
    filter_sclk.low()
    filter_sclk = Pin('X7', Pin.OUT_PP)
    for i in range(10):
        if bits[i] == True:
            out_sdata.high()
        else:
            out_sdata.low()
        filter_sclk.high()
        filter_sclk.low()
    out_sdata = Pin('X8', Pin.IN)
    output = []
    for i in range(95):
        filter_sclk.high()
        filter_sclk.low()
        if(out_sdata.value()):
            output.append(1)
        else:
            output.append(0)
    return output