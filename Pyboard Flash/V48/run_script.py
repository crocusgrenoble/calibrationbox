import sys
sys.path.insert(1, 'scripts')
def run_script_file(filename):
    sys.path.insert(1, 'scripts')
    m = __import__(filename)
    try:
        return m.output
    except ImportError:
        return []   
    