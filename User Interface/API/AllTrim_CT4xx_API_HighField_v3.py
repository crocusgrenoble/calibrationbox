# -*- coding: utf-8 -*-
"""
Created on Fri Mar  19 13:37:21 2021

@author: rmazrae
"""
##########   Import   #########################################################
import sys
import time
import numpy as np
import serial
import logging
import os
from os.path import exists
import subprocess
import pyvisa

sys.path.append(".\\Lib\\")
sys.path.append(".\\Lib\\pyboard\\")
sys.path.append(".\\Lib\\Halo2_1fClassPy3\\")

from halo2_1F import halo2_1F
from F_binToDec import F_binToDec
from pyboard import PyBoard
from pyboard_dvm import Pyboard_DVM
from pyboard_dut_ps import Pyboard_DUT_PS

class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, level):
       self.logger = logger
       self.level = level
       self.linebuf = ''

    def write(self, buf):
       for line in buf.rstrip().splitlines():
          self.logger.log(self.level, line.rstrip())

    def flush(self):
        pass
def CurrentSet(curr):
    ret = subprocess.run(["CurrentControl.bat",str(curr)], shell=True, stdout=subprocess.PIPE) 
    return ret.returncode
    return ret
def TrimmingProcess(port, sensor_type, flavor, looplimit, verbose, fuse=False):
    virgin_device = False ## All zero bits device
    #print(verbose)
    if (not verbose):
        loggerEnabled = True
    else:
        loggerEnabled = False
    
    print('Trimming Process being started. ')        
    pyb_com_port = port#"COM6"#input('please input pyboard come port like COM21 =');
    flavorStr = flavor#"3p3V,B,1200A"#input('please input sensor flavor in format:(5p0 or 3p3)V,(B or U),(max current)A like 3p3V,B,1200A = ');    
    burnFuseBool = fuse#"False"#input('Burn fuses True or False? =');
    loopLimit = int (looplimit); # 15 seconds
        
    if(exists("CurrentControlFunction.py")):
        currentControlExist = True
        #import CurrentControlFunction
    else:
        currentControlExist = False
        
    file_path = 'out.log'
    try:
        if os.path.isfile(file_path):
          os.remove(file_path)
          print("File has been deleted")
    except Exception as e: # work on python 3.x
        print('Failed: '+ str(e)) 
     
    if(loggerEnabled):         
        logging.basicConfig(
                level=logging.DEBUG,
                format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
                filename='out.log',
                filemode='a'
                )
        log = logging.getLogger('TrimmingProcess')
    
        sys.stdout = StreamToLogger(log,logging.INFO)
        sys.stderr = StreamToLogger(log,logging.ERROR)
        
    start = time.time()
    pyboard_1 = PyBoard(com_port = pyb_com_port, pcb_version='4.8');
    halo2_parms = {'device':'CTC4000', 'pyboard_object':pyboard_1};
    h = halo2_1F(halo2_parms);    
    
    currMax = float (flavorStr [7:-1]);
    currMin = -currMax;
    print(currMax)
    
    # New CT454:'rightSwap', Orian board CT43x:'leftSwap',CT45x :'noSwap';
    if sensor_type == 'CT450':
        swapStrBit  = 'noSwap';       
    elif sensor_type == 'CT43x' or sensor_type == 'CT42x' :
        swapStrBit  = 'leftSwap';
    elif sensor_type == 'CT452' or sensor_type == 'CT453' :
        swapStrBit  = 'rightSwap';
    else:
        print('This is not a correct sensor type: ', sensor_type)
        sys.exit('This is not a correct sensor type: ', sensor_type)
        return
    repowerBool = True;
    
    ##########   Modify Parameters   ##############################################    
    psDelay =  0.001; 
    equipDelay  =  0.001    
    nReads =  2; # Each nReads read 20 sample per sec of ADC
    
    h.opCodeMode.tryBeforeBuy();
    h.testBits.set.general.zeroAllTestBits();
    h.trimBits.set.zeroAllTrimBits();
    h.nSclkPeriods(107);
    h.setVddMax(5.5);
    
    ###  DVM #1
    pybrd_dvm = Pyboard_DVM(pyboard_1)
    time.sleep(equipDelay);
    ### Calibration box power supply
    pybrd_DUT_ps = Pyboard_DUT_PS(pyboard_1)    
    
    #Step #0 [Package]: Read Bandgap, PtatUp, Xdown, Marking, and Factory FBits initialization    
    h.opCodeMode.readFuse();
    pybrd_DUT_ps.power_off();time.sleep(equipDelay);
    pybrd_DUT_ps.set_voltage(3.3); 
    pybrd_DUT_ps.power_on(); time.sleep(psDelay);
    [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);
        
    if (readBitArrayDec[4:10] != [0, 0, 0, 0, 0, 0]) or (readBitArrayDec[12:16] != [0, 0, 0, 0]):
        pybrd_DUT_ps.power_off();
        pyboard_1.close();            
        sys.exit('Sensor is already trimmed and CAN NOT be trimmed again!!!');
          
    h.opCodeMode.tryBeforeBuy();
    if virgin_device:    # all-zero-bits device
        if readBitArrayDec.count(0) == 17:
            bgTrimCode      =  3;
            ptatUpTrimCode  =  14;    
        else:
                bgTrimCode      =  readBitArrayDec[0];
                ptatUpTrimCode  =  readBitArrayDec[2];
    else:
        if readBitArrayDec.count(0) == 17:
            pybrd_DUT_ps.power_off();
            pyboard_1.close();            
            sys.exit('Please check the wiring connection between sensor and CTC4000!!!');
    
        else:
            bgTrimCode      =  readBitArrayDec[0];
            ptatUpTrimCode  =  readBitArrayDec[2];
    print(bgTrimCode, ptatUpTrimCode)
    
    # Initialization of sensor and turn on Vcc=3.3v
    pybrd_DUT_ps.power_off();time.sleep(equipDelay);
    pybrd_DUT_ps.set_voltage(3.3)
    pybrd_DUT_ps.power_on();time.sleep(psDelay);
    
    h.testBits.set.general.zeroAllTestBits();
    h.trimBits.set.zeroAllTrimBits();
     
    if flavorStr[0] == '5':
        vddDut  =  5.0;
    else:
        vddDut  =  3.3;
    
    h.trimBits.set.vddSupply(str(vddDut) + 'V'); 
    if flavorStr[5] == 'B':
        h.trimBits.set.polarFlavor('bipolar');
    else:
        h.trimBits.set.polarFlavor('unipolar');
    
    xDownTrimCode = ptatUpTrimCode;
    # determine Zero Vref value
    zeroAvRefArray  =  [1.65, 0.65, 2.5, 0.5];
    idxVRef         =  F_binToDec( str( h.trimBits.read.vddSupply()[1] ) +  str( h.trimBits.read.polarFlavor()[1] ) );
    idealZeroAvRef  =  zeroAvRefArray[idxVRef];
    
    ###############################################################################
    def F_repowerTbbm(pybrd_DUT_ps, h, vddDut):    
        if repowerBool:
            pybrd_DUT_ps.power_off();time.sleep(equipDelay);
            pybrd_DUT_ps.set_voltage(3.3);
            pybrd_DUT_ps.power_on();time.sleep(psDelay);
        elif vddDut == 5:
            pybrd_DUT_ps.set_voltage(3.3);
            pybrd_DUT_ps.power_on();time.sleep(psDelay);
    
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard(); #time.sleep(equipDelay);
        
        pybrd_DUT_ps.set_voltage(vddDut);time.sleep(equipDelay);
    
    # Trim Left Only
    def F_trimLeftOnly(deltaCode, leftCurrCode):
    
        # Current Left Code is Between 128 and 255
        if (leftCurrCode >= 128) and (leftCurrCode <= 255):
            deltaCode = -deltaCode;
            
            if (leftCurrCode + deltaCode) < 128:
                nextCode = abs( 127 - (leftCurrCode + deltaCode) );
            else:
                nextCode = leftCurrCode + deltaCode;
                if nextCode > 255:
                    nextCode = 255;
                    
        # Current Left Code is Between 0 and 127        
        else:
            
            if (leftCurrCode + deltaCode) < 0:
                nextCode = 129 + leftCurrCode + abs(deltaCode);
            else:
                nextCode = leftCurrCode + deltaCode;
                if nextCode > 127:
                    nextCode = 127;
                    
        return(nextCode);
        
    # Trim Right Only
    def F_trimRightOnly(deltaCode, rightCurrCode):
    
        # Current Right Code is Between 128 and 255
        if (rightCurrCode >= 128) and (rightCurrCode <= 255):
            deltaCode = -deltaCode;
            
            if (rightCurrCode + deltaCode) < 128:
                nextCode = abs( 127 - (rightCurrCode + deltaCode) );
            
            else:
                nextCode = rightCurrCode + deltaCode;
                
                if nextCode > 255:
                    nextCode = 255;
                
        # Current Right Code is Between 0 and 127        
        else:            
            if (rightCurrCode + deltaCode) < 0:
                nextCode = 129 + rightCurrCode + abs(deltaCode);
                
            else:
                nextCode = rightCurrCode + deltaCode;
                
                if nextCode > 127:
                    nextCode = 127;                    
        return(nextCode);
        
    
    def F_sensorGainFbitSetting( setIdx ):
        
        ####################################################
        
        # 4mT_-15%
        if setIdx == 0:
            h.trimBits.set.marking( 4 );
            h.trimBits.set.currentFieldScale( '8mT(20A)' );
        
        # 8mT_-15%
        elif setIdx == 1:
            h.trimBits.set.marking( 6 );
            h.trimBits.set.currentFieldScale( '8mT(20A)' );
        
        # 12mT_-15%
        elif setIdx == 2:
            h.trimBits.set.marking( 6 );
            h.trimBits.set.currentFieldScale( '12mT(30A)' );
            
        # 20mT_-15%
        elif setIdx == 3:
            h.trimBits.set.marking( 4 );
            h.trimBits.set.currentFieldScale( '12mT(30A)' );
        
        ####################################################
            
        # 4mT_0%
        elif setIdx == 4:
            h.trimBits.set.marking( 0 );
            h.trimBits.set.currentFieldScale( '8mT(20A)' );
        
        # 8mT_0%
        elif setIdx == 5:
            h.trimBits.set.marking( 2 );
            h.trimBits.set.currentFieldScale( '8mT(20A)' );
        
        # 12mT_0%
        elif setIdx == 6:
            h.trimBits.set.marking( 2 );
            h.trimBits.set.currentFieldScale( '12mT(30A)' );
            
        # 20mT_0%
        elif setIdx == 7:
            h.trimBits.set.marking( 0 );
            h.trimBits.set.currentFieldScale( '12mT(30A)' );
            
        ####################################################
            
        # 4mT_+15%
        elif setIdx == 8:
            h.trimBits.set.marking( 8 );
            h.trimBits.set.currentFieldScale( '8mT(20A)' );
        
        # 8mT_+15%
        elif setIdx == 9:
            h.trimBits.set.marking( 10 );
            h.trimBits.set.currentFieldScale( '8mT(20A)' );
        
        # 12mT_+15%
        elif setIdx == 10:
            h.trimBits.set.marking( 10 );
            h.trimBits.set.currentFieldScale( '12mT(30A)' );
            
        # 20mT_+15%
        elif setIdx == 11:
            h.trimBits.set.marking( 8 );
            h.trimBits.set.currentFieldScale( '12mT(30A)' );
            
        ####################################################
    if (flavorStr[0] == '3') and (flavorStr[5] == 'B'):
        
        courseGainArray = [ 30.04984501 / 6.004504559,
                            15.10154756 / 6.004504559,
                            9.912730982 / 6.004504559,
                            6.004504559 / 6.004504559 ];
                           
    elif (flavorStr[0] == '3') and (flavorStr[5] == 'U'):
        
        courseGainArray = [ 59.98244407 / 11.97244772,
                            30.1233312 / 11.97244772,
                            19.77128319 / 11.97244772,
                            11.97244772 / 11.97244772 ];
                           
    elif (flavorStr[0] == '5') and (flavorStr[5] == 'B'):
        
        courseGainArray = [ 60.07182972 / 11.98184399,
                            30.14715664 / 11.98184399,
                            19.79349606 / 11.98184399,
                            11.98184399 / 11.98184399 ];
                           
    elif (flavorStr[0] == '5') and (flavorStr[5] == 'U'):
        
        courseGainArray = [ 118.8118693 / 23.70227854,
                            59.60713144 / 23.70227854,
                            39.14229785 / 23.70227854,
                            23.70227854 / 23.70227854 ];
    
    def F_repeatCodeCheck( cCode, cArray ):            
                repeatLim = 3;
                cArray[cCode] = cArray[cCode] + 1;
                boolCheck = any(list( idx > repeatLim for idx in cArray ));
                return( boolCheck, cArray );
            
    if h.trimBits.read.polarFlavor()[0] == 'unipolar':
        currArray   =  [0, currMax];        
    else:        
        currArray   =  [currMin, currMax];
            
    #ampGainDeltaArray   =  [2/float(factor), 4/float(factor)];
    ampGainDeltaArray   =  [2, 4]; # change =-18%
    idxAmpGain          =  h.trimBits.read.vddSupply()[1];
    ampGainDelta        =  ampGainDeltaArray[idxAmpGain];
    
    h.trimBits.set.vRefBandGapCorner(bgTrimCode);
    h.trimBits.set.vRefPtatUp(ptatUpTrimCode);
    h.trimBits.set.vRegBandGapXDown(xDownTrimCode);    
    h.trimBits.set.bridgeSwap(swapStrBit);
    h.opCodeMode.tryBeforeBuy();
     
     #%% Step #0: Determine Sensor Gain Bits    
    print('Step 0: Trimming Sensor Gain Bits ...');
    swapBitArray = [ 'leftSwap', 'rightSwap' ];
    nSwap = len( swapBitArray );
    
    deltaOutArray = [];
    
    for iSwap in range( nSwap ):
        
        h.trimBits.set.bridgeSwap( swapBitArray[ iSwap ] );
    
        for iGain in range( 2 ):
            
            if iGain == 0:
                h.trimBits.set.afeAmpGainLeft( 120 );  # Left Bridge Max Gain
                h.trimBits.set.afeAmpGainRight( 128 ); # Right Bridge Min Gain
            else:
                h.trimBits.set.afeAmpGainLeft( 128 );  # Left Bridge Min Gain
                h.trimBits.set.afeAmpGainRight( 120 ); # Right Bridge Max Gain 
            
            # 20mT_0% CMOS Gain
            F_sensorGainFbitSetting( 7 );
            
            # TBBM & Measure Delta
            F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
            outArray = [];
                
            for i in range (len(currArray)):
                    ret = CurrentSet(currArray[i])                
                    print (ret)   
                    print("current set to "+ str(currArray[i]));    
                    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);    
                    print(meanVal)
                    outArray.append(meanVal);
                    if currArray[i] != 0:            
                        ret = CurrentSet('0')
                        print (ret)               
            
            measuredOutDelta = outArray[-1] - outArray[0];        
            deltaOutArray.append( measuredOutDelta );
    
    nMeas = len( deltaOutArray );
    
    courseGainArray_m15per = 0.85*np.array( courseGainArray );
    courseGainArray_0per = np.array( courseGainArray );
    courseGainArray_p15per = 1.15*np.array( courseGainArray );
    
    ######### 8/15/2023 ####################
    for iMeas in range( nMeas ):
        
        m15PerArray = 100*( ampGainDelta - ( deltaOutArray[ iMeas ]*courseGainArray_m15per ) ) / ( deltaOutArray[ iMeas ]*courseGainArray_m15per );
        zeroPerArray = 100*( ampGainDelta - ( deltaOutArray[ iMeas ]*courseGainArray_0per ) ) / ( deltaOutArray[ iMeas ]*courseGainArray_0per );
        p15PerArray = 100*( ampGainDelta - ( deltaOutArray[ iMeas ]*courseGainArray_p15per ) ) / ( deltaOutArray[ iMeas ]*courseGainArray_p15per );
        
        combPerArray = np.concatenate( ( m15PerArray, zeroPerArray, p15PerArray )  );
        
        # Gain Needs to Only Increase ( deltaOut < ampGainDelta )
        if iMeas == 0:
            
            idxLocArray = np.where( ( combPerArray > 0 ) & ( combPerArray < 36 ) );
            
            if len( idxLocArray[0] ) != 0:
                
                inSpecArray = combPerArray[ idxLocArray[0] ];
                locIdx = np.argmin( inSpecArray );
                exactIdx = np.where( combPerArray == inSpecArray[ locIdx ] )[0];
                measLoc = iMeas;
                break;
            
        # Gain Needs to Only Decrease ( deltaOut > ampGainDelta )
        if iMeas == 1:
            
            idxLocArray = np.where( ( combPerArray < 0 ) & ( combPerArray > -36 ) );
            
            if len( idxLocArray[0] ) != 0:
                
                inSpecArray = combPerArray[ idxLocArray[0] ];
                locIdx = np.argmax( inSpecArray );
                exactIdx = np.where( combPerArray == inSpecArray[ locIdx ] )[0];
                measLoc = iMeas;
                break;
            
        # Gain Needs to Only Decrease ( deltaOut > ampGainDelta )
        if iMeas == 2:
            
            idxLocArray = np.where( ( combPerArray < 0 ) & ( combPerArray > -36 ) );
            
            if len( idxLocArray[0] ) != 0:
                
                inSpecArray = combPerArray[ idxLocArray[0] ];
                locIdx = np.argmax( inSpecArray );
                exactIdx = np.where( combPerArray == inSpecArray[ locIdx ] )[0];
                measLoc = iMeas;
                break;
            
        # Gain Needs to Only Increase ( deltaOut < ampGainDelta )
        if iMeas == 3:
            
            idxLocArray = np.where( ( combPerArray > 0 ) & ( combPerArray < 36 ) );
            
            if len( idxLocArray[0] ) != 0:
                
                inSpecArray = combPerArray[ idxLocArray[0] ];
                locIdx = np.argmin( inSpecArray );
                exactIdx = np.where( combPerArray == inSpecArray[ locIdx ] )[0];
                measLoc = iMeas;
                break;
    
    try:
        if measLoc == 0:
        
            h.trimBits.set.bridgeSwap( 'leftSwap' );
            h.trimBits.set.afeAmpGainLeft( 120 );
            h.trimBits.set.afeAmpGainRight( 128 );
            
        elif measLoc == 1:
            
            h.trimBits.set.bridgeSwap( 'leftSwap' );
            h.trimBits.set.afeAmpGainLeft( 128 );
            h.trimBits.set.afeAmpGainRight( 120 );
            
        elif measLoc == 2:
            
            h.trimBits.set.bridgeSwap( 'rightSwap' );
            h.trimBits.set.afeAmpGainLeft( 120 );
            h.trimBits.set.afeAmpGainRight( 128 );
            
        elif measLoc == 3:
            
            h.trimBits.set.bridgeSwap( 'rightSwap' );
            h.trimBits.set.afeAmpGainLeft( 128 );
            h.trimBits.set.afeAmpGainRight( 120 );
    except:
        print("Part is not trimmable!!!")
        
    F_sensorGainFbitSetting( exactIdx[ 0 ] );
    
    ######### 8/15/2023 ####################
    
    finalSwapBitStr = h.trimBits.read.bridgeSwap()[0];
    finalLeftCodeStr = str( h.trimBits.read.afeAmpGainLeft() );
    print('finalLeftCodeStr',finalLeftCodeStr);    
    finalRightCodeStr = str( h.trimBits.read.afeAmpGainRight() );
    print('finalRightCodeStr',finalRightCodeStr);
    
    finalCourseGainArray = [ '@ 4mT_-15%', '@ 8mT_-15%', '@ 12mT_-15%', '@ 20mT_-15%',
                             '@ 4mT_0%', '@ 8mT_0%', '@ 12mT_0%', '@ 20mT_0%',
                             '@ 4mT_+15%', '@ 8mT_+15%', '@ 12mT_+15%', '@ 20mT_+15%' ];

    finalCourseGainStr = finalCourseGainArray[ exactIdx[ 0 ] ];
    print('finalCourseGainStr',finalCourseGainStr);
        
    # TBBM & Measure Delta
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    outArray = [];
        
    for i in range (len(currArray)):
            ret = CurrentSet(currArray[i])                
            print (ret)   
            print("current set to "+ str(currArray[i]));    
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);    
            print(meanVal)
            outArray.append(meanVal);
            if currArray[i] != 0:            
                ret = CurrentSet('0')
                print (ret)                       
    
    measuredOutDelta = outArray[-1] - outArray[0];
    deltaOutArray.append( measuredOutDelta );
     
    #%% Step #1 : Trim Zero Current Voltage Reference
    # delAllVarClrConsole('cls');
    print('Step 1: Trimming vRef...');
    
    strTestName         =  'vRef';
    vRefCodesTested     =  [];
    measuredVref        =  [];
    
    h.testBits.set.afeMux.afeMuxToFltPin('en');
    h.testBits.set.afeMux.bufferedZeroCurrVref();
    codeRepeatArray = np.linspace(0, 0, 64);
    
    # Largest vRef Value
    sCode = 32;
    h.trimBits.set.vRefScaledBandGap(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    currCode = h.trimBits.read.vRefScaledBandGap();
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( currCode, codeRepeatArray );
    
    vRefCodesTested.append( currCode );
    measuredVref.append( meanVal );
    print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
    
    # Smallest vRef Value
    sCode = 31;
    h.trimBits.set.vRefScaledBandGap(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    currCode = h.trimBits.read.vRefScaledBandGap();
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( currCode, codeRepeatArray );
    
    vRefCodesTested.append( currCode );
    measuredVref.append( meanVal );
    print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
    
    sCode = 0;
    h.trimBits.set.vRefScaledBandGap(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);  
    currCode = h.trimBits.read.vRefScaledBandGap();
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( currCode, codeRepeatArray );
    
    vRefCodesTested.append( currCode );
    measuredVref.append( meanVal );
    print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
    
    vRefStepSize = ( measuredVref[-1] - measuredVref[-2] ) / 31.0;
    deltaCode = int( round( (measuredVref[-1] - idealZeroAvRef) / vRefStepSize ) );
    
    # Check if Ideal vRef is Smaller than Code 31
    if measuredVref[-2] > idealZeroAvRef:
        h.trimBits.set.vRefScaledBandGap(31);
        deltaCode = 0;    
    
    # Check if Ideal vRef is Larger than Code 32
    if measuredVref[-3] < idealZeroAvRef:
        h.trimBits.set.vRefScaledBandGap(32);
        deltaCode = 0;
    
    sTime = time.time();
    while (deltaCode != 0) and (not boolCheck):        
    
        nextCode = currCode + deltaCode;
        
        if (nextCode < 0) and (currCode - 32 < 0):
            nextCode = 64 + nextCode;
    			
        if (nextCode > 63) and (currCode - 31 > 0):
            nextCode = nextCode - 64;
        
        h.trimBits.set.vRefScaledBandGap(nextCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        
        currCode = h.trimBits.read.vRefScaledBandGap();
        vRefCodesTested.append( currCode );
        measuredVref.append( meanVal );
        
        print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
        
        deltaCode = int( round( (measuredVref[-1] - idealZeroAvRef) / vRefStepSize ) );
    
        [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( currCode, codeRepeatArray );
        
        cTime = time.time();
        timeElapsed = cTime - sTime;
        
        if timeElapsed > loopLimit:
            pybrd_DUT_ps.power_off();
            pyboard_1.close();            
            sys.exit('Timeout Error on Vref trimming!');
       
    h.testBits.set.general.zeroAllTestBits();
    
     #%% Step #2 : Trim Left & Right Amp Gain Together
    print('Step 2: Trimming Left & Right Amp Gain...');
    
    strTestName     =  'leftAndRightAmpGain';
    leftCombinedAmpGainCodesTested      =  [];
    rightCombinedAmpGainCodesTested     =  [];
    measuredLeftAndRightAmpGain         =  [];   
    
    sLeftCode = h.trimBits.read.afeAmpGainLeft();
    sRightCode = h.trimBits.read.afeAmpGainRight();
    
    deltaChange = 1000.0*( ampGainDelta - deltaOutArray[ -1 ] );
    
    ########## 8/15/2023 ####################
    
    if deltaChange < 0:
        
        if finalSwapBitStr == 'leftSwap':
            
            ampStr = 'rightAmpDecrease';
            h.trimBits.set.afeAmpGainRight( 0 );
            
            
        elif finalSwapBitStr == 'rightSwap':
            
            ampStr = 'leftAmpDecrease';
            h.trimBits.set.afeAmpGainLeft( 0 );
            
    else:
        
        if finalSwapBitStr == 'leftSwap':
            
            ampStr = 'rightAmpIncrease';
            h.trimBits.set.afeAmpGainRight( 255 );
            
        elif finalSwapBitStr == 'rightSwap':
            
            ampStr = 'leftAmpIncrease';
            h.trimBits.set.afeAmpGainLeft( 255 );
    
    ######### 8/15/2023 ####################
    
    currLeftCode = h.trimBits.read.afeAmpGainLeft();
    currRightCode = h.trimBits.read.afeAmpGainRight();
    
    outArray = [];
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
        
    for i in range (len(currArray)):
            ret = CurrentSet(currArray[i])                
            print (ret)   
            print("current set to "+ str(currArray[i]));    
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);    
            print(meanVal)
            outArray.append(meanVal);
            if currArray[i] != 0:            
                ret = CurrentSet('0')
                print (ret)          
    
    leftCombinedAmpGainCodesTested.append( currLeftCode );
    rightCombinedAmpGainCodesTested.append( currRightCode );
    measuredLeftAndRightAmpGain.append( outArray[-1] - outArray[0] );
    print('leftAmp @ Code ' + str(currLeftCode)  + ' & rightAmp @ Code ' + str(currRightCode)  + ' = ' + str(outArray[-1] - outArray[0])  + 'V');
    
    leftCombinedAmpGainCodesTested.append( sLeftCode ); 
    rightCombinedAmpGainCodesTested.append( sRightCode );
    measuredLeftAndRightAmpGain.append( deltaOutArray[ -1 ] );    
    
    if ampStr == 'leftAmpDecrease':
        
        finalStep = 1000 * ( measuredLeftAndRightAmpGain[ -1 ] - measuredLeftAndRightAmpGain[ -2 ] ) / 120.0;
        h.trimBits.set.afeAmpGainLeft( 120 );
        
    elif ampStr == 'leftAmpIncrease':
        
        finalStep = 1000 * ( measuredLeftAndRightAmpGain[ -2 ] - measuredLeftAndRightAmpGain[ -1 ] ) / 127.0;
        h.trimBits.set.afeAmpGainLeft( 128 );
        
    elif ampStr == 'rightAmpDecrease':
        
        finalStep = 1000 * ( measuredLeftAndRightAmpGain[ -1 ] - measuredLeftAndRightAmpGain[ -2 ] ) / 120.0;
        h.trimBits.set.afeAmpGainRight( 120 );
        
    elif ampStr == 'rightAmpIncrease':
        
        finalStep = 1000 * ( measuredLeftAndRightAmpGain[ -2 ] - measuredLeftAndRightAmpGain[ -1 ] ) / 127.0;
        h.trimBits.set.afeAmpGainRight( 128 );
    
    currLeftCode = h.trimBits.read.afeAmpGainLeft();
    currRightCode = h.trimBits.read.afeAmpGainRight();
    
    leftAndRightGainInScale = ( 1000*abs( ampGainDelta - measuredLeftAndRightAmpGain[-1] ) <= 0.5*finalStep ); 
    
    sTime = time.time();
    
    ########### 8/15/2023 ##############
    
    while leftAndRightGainInScale == False:
        
        outDelta = 1000*( ampGainDelta - measuredLeftAndRightAmpGain[-1] );
        
        expectCodeChange = round( outDelta / finalStep, 0 );
        
        if ampStr == 'leftAmpDecrease':
            
            if expectCodeChange <= -120: 
            
                finalLeftGainCode = 0;
                finalRightGainCode = currRightCode - ( expectCodeChange + 120 );
                ampStr == 'rightAmpIncrease'
                
            else:
                
                finalLeftGainCode = currLeftCode + expectCodeChange;
                finalRightGainCode = currRightCode;
                
                
        elif ampStr == 'leftAmpIncrease':
            
            if expectCodeChange >= 127: 
            
                finalLeftGainCode = 255;
                finalRightGainCode = currRightCode - ( expectCodeChange - 127 );
                ampStr == 'rightAmpDecrease'
                
            else:
                
                finalLeftGainCode = currLeftCode + expectCodeChange;
                finalRightGainCode = currRightCode;
        
        elif ampStr == 'rightAmpDecrease':
            
            if expectCodeChange <= -120:
                
                finalLeftGainCode = currLeftCode - ( expectCodeChange + 120 );
                finalRightGainCode = 0;
                ampStr == 'leftAmpIncrease'
                
            else:
                
                finalLeftGainCode = currLeftCode;
                finalRightGainCode = currRightCode + expectCodeChange;
                
        elif ampStr == 'rightAmpIncrease':
            
            if expectCodeChange >= 127: 
            
                finalLeftGainCode = currLeftCode - ( expectCodeChange - 127 );
                finalRightGainCode = 255;
                ampStr == 'leftAmpDecrease';
                
            else:
                
                finalLeftGainCode = currLeftCode;
                finalRightGainCode = currRightCode + expectCodeChange;
        
        ########### 8/15/2023 ##############
        
        h.trimBits.set.afeAmpGainLeft( finalLeftGainCode );
        h.trimBits.set.afeAmpGainRight( finalRightGainCode );
        
        currLeftCode    =  h.trimBits.read.afeAmpGainLeft();
        currRightCode   =  h.trimBits.read.afeAmpGainRight();
        
        outArray = [];
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
            
        for i in range (len(currArray)):
            ret = CurrentSet(currArray[i])                
            print (ret)   
            print("current set to "+ str(currArray[i]));    
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);    
            print(meanVal)
            outArray.append(meanVal);
            if currArray[i] != 0:            
                ret = CurrentSet('0')
                print (ret)
        
        leftCombinedAmpGainCodesTested.append( currLeftCode );
        rightCombinedAmpGainCodesTested.append( currRightCode );
        measuredLeftAndRightAmpGain.append( outArray[-1] - outArray[0] );
        print('leftAmp @ Code ' + str(currLeftCode)  + ' & rightAmp @ Code ' + str(currRightCode)  + ' = ' + str(outArray[-1] - outArray[0])  + 'V');
    
        leftAndRightGainInScale = ( 1000*abs( ampGainDelta - measuredLeftAndRightAmpGain[-1] ) <= 0.5*finalStep );
    
        cTime = time.time();
        timeElapsed = cTime - sTime;
        
        if timeElapsed > loopLimit:
            pybrd_DUT_ps.power_off();
            pyboard_1.close();            
            sys.exit('Timeout Error on Gain trimming!');
    
    h.testBits.set.general.zeroAllTestBits();
    
     #%% Step #3 : Trim Left & Right Electrical Offset Together
    print('Step 3: Trimming Left & Right Elect Offset...');
    
    strTestName     =  'leftAndRightElectOffset';
    leftCombinedElectOffsetCodesTested      =  [];
    rightCombinedElectOffsetCodesTested     =  [];
    measuredLeftAndRightElectOffset         =  [];
    
    h.testBits.set.general.rightBridge('short');
    h.testBits.set.general.leftBridge('short');
    
    codeRepeatArray = np.linspace(0, 0, 256);
            
    sCode = 128;
    h.trimBits.set.afeElectOffsetLeft(sCode);
    h.trimBits.set.afeElectOffsetRight(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    leftCurrCode = h.trimBits.read.afeElectOffsetLeft();
    rightCurrCode = h.trimBits.read.afeElectOffsetRight();
    
    leftCombinedElectOffsetCodesTested.append( leftCurrCode );
    rightCombinedElectOffsetCodesTested.append( rightCurrCode );
    measuredLeftAndRightElectOffset.append( meanVal );
    print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
    
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
            
    sCode = 255;
    h.trimBits.set.afeElectOffsetLeft(sCode);
    h.trimBits.set.afeElectOffsetRight(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    leftCurrCode = h.trimBits.read.afeElectOffsetLeft();
    rightCurrCode = h.trimBits.read.afeElectOffsetRight();
    
    leftCombinedElectOffsetCodesTested.append( leftCurrCode );
    rightCombinedElectOffsetCodesTested.append( rightCurrCode );
    measuredLeftAndRightElectOffset.append( meanVal );
    print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
    
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
    
    normOffsetStep = ( measuredLeftAndRightElectOffset[-1] - measuredLeftAndRightElectOffset[-2] ) / 127.0;
            
    sCode = 0;
    h.trimBits.set.afeElectOffsetLeft(sCode);
    h.trimBits.set.afeElectOffsetRight(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    leftCurrCode = h.trimBits.read.afeElectOffsetLeft();
    rightCurrCode = h.trimBits.read.afeElectOffsetRight();
    
    leftCombinedElectOffsetCodesTested.append( leftCurrCode );
    rightCombinedElectOffsetCodesTested.append( rightCurrCode );
    measuredLeftAndRightElectOffset.append( meanVal );
    print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
    
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
    
    deltaCode = int( round( (measuredLeftAndRightElectOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
    
    sTime = time.time();
    while (deltaCode != 0) and (not boolCheck):
        
        # Current Code is Between 128 and 255
        if (leftCurrCode >= 128) and (leftCurrCode <= 255):
            deltaCode = -deltaCode;
            
            if (leftCurrCode + deltaCode) < 128:
                nextCode = abs( 127 - (leftCurrCode + deltaCode) );
            
            else:
                nextCode = leftCurrCode + deltaCode;
                
                if nextCode > 255:
                    nextCode = 255;
                
        # Current Code is Between 0 and 127        
        else:
            
            if (leftCurrCode + deltaCode) < 0:
                nextCode = 129 + leftCurrCode + abs(deltaCode);
                
            else:
                nextCode = leftCurrCode + deltaCode;
                
                if nextCode > 127:
                    nextCode = 127;
            
        h.trimBits.set.afeElectOffsetLeft(nextCode); 
        h.trimBits.set.afeElectOffsetRight(nextCode); 
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        leftCurrCode = h.trimBits.read.afeElectOffsetLeft();
        rightCurrCode = h.trimBits.read.afeElectOffsetRight();
        
        leftCombinedElectOffsetCodesTested.append( leftCurrCode );
        rightCombinedElectOffsetCodesTested.append( rightCurrCode );
        measuredLeftAndRightElectOffset.append( meanVal );
        print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
        
        deltaCode = int( round( (measuredLeftAndRightElectOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
            
        [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );

        cTime = time.time();
        timeElapsed = cTime - sTime;
            
        if timeElapsed > loopLimit:
            pybrd_DUT_ps.power_off();
            pyboard_1.close();            
            sys.exit('Timeout Error on Elec trimming!');
    
    h.testBits.set.general.zeroAllTestBits(); 
     
     #%% Step #4 : Trim Left & Right Magnetic Offset Together     
    print('Step 4: Trimming Left & Right Magnetic Offset...');
    strTestName     =  'leftAndRightMagOffset';
    leftCombinedMagOffsetCodesTested    =  [];
    rightCombinedMagOffsetCodesTested   =  [];
    measuredLeftAndRightMagOffset       =  [];
    
    codeRepeatArray = np.linspace(0, 0, 256);
            
    sCode = 128;
    h.trimBits.set.afeMagOffsetLeft(sCode);
    h.trimBits.set.afeMagOffsetRight(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    leftCurrCode = h.trimBits.read.afeMagOffsetLeft();
    rightCurrCode = h.trimBits.read.afeMagOffsetRight();        
    
    leftCombinedMagOffsetCodesTested.append( leftCurrCode );
    rightCombinedMagOffsetCodesTested.append( rightCurrCode );
    measuredLeftAndRightMagOffset.append( meanVal );
    print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
    
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
    
    sCode = 255;
    h.trimBits.set.afeMagOffsetLeft(sCode);
    h.trimBits.set.afeMagOffsetRight(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    leftCurrCode = h.trimBits.read.afeMagOffsetLeft();
    rightCurrCode = h.trimBits.read.afeMagOffsetRight();        
    
    leftCombinedMagOffsetCodesTested.append( leftCurrCode );
    rightCombinedMagOffsetCodesTested.append( rightCurrCode );
    measuredLeftAndRightMagOffset.append( meanVal );
    print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
    
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
    
    normOffsetStep = ( measuredLeftAndRightMagOffset[-1] - measuredLeftAndRightMagOffset[-2] ) / 127.0;
    
    sCode = 0;
    h.trimBits.set.afeMagOffsetLeft(sCode);
    h.trimBits.set.afeMagOffsetRight(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    leftCurrCode = h.trimBits.read.afeMagOffsetLeft();
    rightCurrCode = h.trimBits.read.afeMagOffsetRight();        
    
    leftCombinedMagOffsetCodesTested.append( leftCurrCode );
    rightCombinedMagOffsetCodesTested.append( rightCurrCode );
    measuredLeftAndRightMagOffset.append( meanVal );
    print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
    
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
    
    # Create Finer Step Loop Here
    normOffsetStep = normOffsetStep / 2.0;
    
    deltaCode = int( round( (measuredLeftAndRightMagOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
    
    leftCodeBool = True;
    rightCodeBool = False;
    
    sTime = time.time();
    while (deltaCode != 0) and (not boolCheck):
        
        # Change Left Code Only
        if leftCodeBool:            
            nextCode = F_trimLeftOnly(deltaCode, leftCurrCode);
            leftCodeBool = False;
            rightCodeBool = True;
            h.trimBits.set.afeMagOffsetLeft(nextCode);
         
        # Change Right Code Only   
        else:                        
            nextCode = F_trimRightOnly(deltaCode, rightCurrCode);
            leftCodeBool = True;
            rightCodeBool = False;
            h.trimBits.set.afeMagOffsetRight(nextCode);
            
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        leftCurrCode = h.trimBits.read.afeMagOffsetLeft();
        rightCurrCode = h.trimBits.read.afeMagOffsetRight();  
        
        leftCombinedMagOffsetCodesTested.append( leftCurrCode );
        rightCombinedMagOffsetCodesTested.append( rightCurrCode );
        measuredLeftAndRightMagOffset.append( meanVal );
        print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
        
        deltaCode = int( round( (measuredLeftAndRightMagOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
            
        [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
        
        cTime = time.time();
        timeElapsed = cTime - sTime;
        if timeElapsed > loopLimit:
            pybrd_DUT_ps.power_off();
            pyboard_1.close();
            sys.exit('Timeout Error on Magnetic Offset trimming!');
        
    h.testBits.set.general.zeroAllTestBits();    
    
    #%% Burn Fuses: For burn fuses we need to have Sdata voltage of 3.6V and first clk pulse 3.6+1.4=5V 
    if burnFuseBool:
        # Burn Fuse Mode needs 4 V 
        print('Step 5: Burn Fuses according to trimming values...');       
        pybrd_DUT_ps.power_off();time.sleep(equipDelay);
        pybrd_DUT_ps.set_voltage(4);
        pybrd_DUT_ps.power_on(); time.sleep(psDelay);               
        # TBB Mode
        h.opCodeMode.tryBeforeBuy();time.sleep(equipDelay);
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);       
        # Burn Fuse Mode
        h.opCodeMode.burnFuse();time.sleep(equipDelay);
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);
       
        # Read Fuse Mode needs 3.3V and need repower
        pybrd_DUT_ps.power_off();time.sleep(equipDelay);
        pybrd_DUT_ps.set_voltage(3.3); 
        pybrd_DUT_ps.power_on(); time.sleep(psDelay);
        h.opCodeMode.readFuse();time.sleep(equipDelay);               
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);        
              
        if setBitArrayDec == readBitArrayDec:
            print('Trimming is done and Fuses Burn successfully!');
            finish = time.time() - start;
            print(finish)
            pybrd_DUT_ps.power_off();time.sleep(equipDelay);
            pybrd_DUT_ps.set_voltage(vddDut); 
            pybrd_DUT_ps.power_on(); time.sleep(psDelay);
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
            print('Trimmed Vout =', meanVal );
        else:
            print('Set and Read values are not the same!!!!');
            finish = time.time() - start;
            print(finish)
    else:
        print('Trimming is done successfully without Burn Fuse selection!');
        finish = time.time() - start;
        print(finish)        

    if(loggerEnabled):        
            sys.stdout = sys.__stdout__   
            
    #%% Finished and Turn off all power supplies
    pybrd_DUT_ps.power_off();
    pyboard_1.close();    

import argparse

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Command line trimming tool')
    parser.add_argument('p', metavar='COMx',
                        help='Serial port')

    parser.add_argument('y', metavar='SensorType',
                        help='Sensor type. Possible options: \'CT450\', \'CT43x\', \'CT42x\', \'CT452\' and \'CT453\' ')

    parser.add_argument('f',metavar='flavor',
                        help='Sensor flavor in format:(5p0 or 3p3)V,(B or U),(max current)A , e.g: 3p3V,B,1200A')
    
    parser.add_argument('t',metavar='timeloop',
                        help='timeloop determined how long you will stay on trimming steps in second e.g: 30')

    parser.add_argument('--FUSE', default=False,action='store_true',help='Burn the fuse bits. Non-reversible!')
    
    parser.add_argument('--NOFUSE', dest='FUSE', action='store_false',help='Default = True, Use --FUSE to cancel the effect and burn the fuse bits.')
    
    parser.add_argument('--verbose', default=False,action='store_true', help='Verbose mode')


    args = parser.parse_args()

    TrimmingProcess(port=args.p, sensor_type = args.y, flavor=args.f, looplimit=args.t, fuse=args.FUSE, verbose=args.verbose)