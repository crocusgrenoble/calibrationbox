# -*- coding: utf-8 -*-
"""
Created on Fri Mar  19 13:37:21 2021

@author: rmazrae
"""
##########   Import   #########################################################
import sys
import time
import numpy as np
import serial
import logging
import os
from os.path import exists
import subprocess
import pyvisa

sys.path.append(".\\Lib\\")
sys.path.append(".\\Lib\\pyboard\\")
sys.path.append(".\\Lib\\Halo2_1fClassPy3\\")

from halo2_1F import halo2_1F
from F_binToDec import F_binToDec
from pyboard import PyBoard
from pyboard_dvm import Pyboard_DVM
from pyboard_dut_ps import Pyboard_DUT_PS

class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, level):
       self.logger = logger
       self.level = level
       self.linebuf = ''

    def write(self, buf):
       for line in buf.rstrip().splitlines():
          self.logger.log(self.level, line.rstrip())

    def flush(self):
        pass
def CurrentSet(curr):
    ret = subprocess.run(["CurrentControl.bat",str(curr)], shell=True, stdout=subprocess.PIPE) 
    return ret.returncode
    return ret
def TotalError(port, sensor_type, Flavor, nDataPoints, cooltime, verbose, fuse=False):
    virgin_device = False ## All zero bits device
    #print(verbose)
    if (not verbose):
        loggerEnabled = True
    else:
        loggerEnabled = False
    
    print('Trimming Process being started. ')        
    pyb_com_port = port#"COM6"#input('please input pyboard come port like COM21 =');    
    burnFuseBool = fuse#"False"#input('Burn fuses True or False? =');
        
    file_path = 'out.log'
    try:
        if os.path.isfile(file_path):
          os.remove(file_path)
          print("File has been deleted")
    except Exception as e: # work on python 3.x
        print('Failed: '+ str(e)) 
     
    if(loggerEnabled):         
        logging.basicConfig(
                level=logging.DEBUG,
                format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
                filename='out.log',
                filemode='a'
                )
        log = logging.getLogger('TotalError')
    
        sys.stdout = StreamToLogger(log,logging.INFO)
        sys.stderr = StreamToLogger(log,logging.ERROR)
        
    start = time.time()
    pyboard_1 = PyBoard(com_port = pyb_com_port, pcb_version='4.8');
    halo2_parms = {'device':'CTC4000', 'pyboard_object':pyboard_1};
    h = halo2_1F(halo2_parms);
        
    #########################################################################    
    repowerBool = True;
    
    ##########   Modify Parameters   ########################################    
    psDelay =  0.001; 
    equipDelay  =  0.001    
    nReads =  2; # Each nReads read 20 sample per sec of ADC
    
    h.opCodeMode.tryBeforeBuy();
    h.testBits.set.general.zeroAllTestBits();
    h.trimBits.set.zeroAllTrimBits();
    h.nSclkPeriods(107);
    h.setVddMax(5.5);
    
    ###  DVM #1
    pybrd_dvm = Pyboard_DVM(pyboard_1)
    time.sleep(0.05);
    ### Calibration box power supply
    pybrd_DUT_ps = Pyboard_DUT_PS(pyboard_1)    
    
    #Step #0 [Package]: Read Bandgap, PtatUp, Xdown, Marking, and Factory FBits initialization    
    h.opCodeMode.readFuse();
    pybrd_DUT_ps.power_off();time.sleep(equipDelay);
    pybrd_DUT_ps.set_voltage(3.3); 
    pybrd_DUT_ps.power_on(); time.sleep(psDelay);
    [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);        
    
    if readBitArrayDec.count(0) == 17:
        pybrd_DUT_ps.power_off();
        pyboard_1.close();            
        sys.exit('Please check the wiring connection between sensor and CTC4000!!!');    
    else:
        bgTrimCode      =  readBitArrayDec[0];
        ptatUpTrimCode  =  readBitArrayDec[2];
        
    print(bgTrimCode, ptatUpTrimCode)
    
    # Initialization of sensor and turn on Vcc=3.3v
    pybrd_DUT_ps.power_off();time.sleep(equipDelay);
    pybrd_DUT_ps.set_voltage(3.3)
    pybrd_DUT_ps.power_on();time.sleep(psDelay);    
    h.testBits.set.general.zeroAllTestBits();
    h.trimBits.set.zeroAllTrimBits();    
    
###############################################################################
    def F_repowerTbbm(pybrd_DUT_ps, h, vddDut):    
        if repowerBool:
            pybrd_DUT_ps.power_off();time.sleep(equipDelay);
            pybrd_DUT_ps.set_voltage(3.3);
            pybrd_DUT_ps.power_on();time.sleep(psDelay);
        elif vddDut == 5:
            pybrd_DUT_ps.set_voltage(3.3);
            pybrd_DUT_ps.power_on();time.sleep(psDelay);
    
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard(); #time.sleep(equipDelay);
        
        pybrd_DUT_ps.set_voltage(vddDut);time.sleep(equipDelay);
    
    
    def F_bestFitCoeff(X, Y):
        
        # X and Y length check
        errorCheck = False;
        nX = len(X);
        nY = len(Y);
        if nX != nY:
            errorCheck = True;
            
        if errorCheck:
            sys.exit('X and Y lengths must be the same!');
        
        n = float(nX);
        xbar = sum(X)/n;
        ybar = sum(Y)/n;
    
        numer = sum([(xi - xbar)*(yi - ybar) for xi,yi in zip(X, Y)]);
        denum = sum([(xi - xbar)**2 for xi in X]);
    
        b = numer / denum
        a = ybar - b * xbar
    
        return a, b
    
#%% Initialization fuse bits##################################################
    if Flavor[0] == '5':
        vddDut  =  5.0;
        idealOutDelta = 4.0;
        idealOffsetStr = '1';
    else:
        vddDut  =  3.3;
        idealOutDelta = 2.0;
        idealOffsetStr = '0';
        
    h.trimBits.set.vddSupply(str(vddDut) + 'V');
    currMax = float (Flavor [7:-2]);
        
    if Flavor[5] == 'B':
        # h.trimBits.set.polarFlavor('bipolar');
        idealOffsetStr = idealOffsetStr + '0';
        currMin = -currMax;
    else:
        # h.trimBits.set.polarFlavor('unipolar');
        idealOffsetStr = idealOffsetStr + '1';
        currMin = 0;
    
    mIdeal = idealOutDelta / ( currMax - currMin );
        
    # Determine the Ideal Offset
    if idealOffsetStr == '00':
        
        bIdeal = 1.65;
        
    elif idealOffsetStr == '01':
        
        bIdeal = 0.65;
        
    elif idealOffsetStr == '10':
        
        bIdeal = 2.5;
        
    elif idealOffsetStr == '11':
        
        bIdeal = 0.5;
        
    if sensor_type == 'CT452' or sensor_type == 'CT453':
        
        swapStrBit  = 'noSwap';
        if readBitArrayDec[14] == 0:
            h.trimBits.set.tryBeforeBuyFeature('en')
        else:
            h.trimBits.set.tryBeforeBuyFeature('dis')
        
        if readBitArrayDec[15] == 1:
            h.trimBits.set.tryBeforeBuyFeature('murata')
        else:
            h.trimBits.set.tryBeforeBuyFeature('crocus')
        
        if readBitArrayDec[12] == 0:
            h.trimBits.set.currentFieldScale( '8mT(20A)' );
        else:
            h.trimBits.set.currentFieldScale( '12mT(30A)' ); 
        
        xDownTrimCode = ptatUpTrimCode;
        h.trimBits.set.vRefBandGapCorner(bgTrimCode);
        h.trimBits.set.vRefPtatUp(ptatUpTrimCode);
        h.trimBits.set.vRegBandGapXDown(xDownTrimCode);    
        h.trimBits.set.vRefScaledBandGap(readBitArrayDec[1]);
        h.trimBits.set.afeMagOffsetLeft(readBitArrayDec[4]);
        h.trimBits.set.afeMagOffsetRight(readBitArrayDec[5]);
        h.trimBits.set.afeElectOffsetLeft(readBitArrayDec[6]);
        h.trimBits.set.afeElectOffsetRight(readBitArrayDec[7]);
        h.trimBits.set.afeAmpGainLeft(readBitArrayDec[8]); 
        h.trimBits.set.afeAmpGainRight(readBitArrayDec[9]);        
        h.trimBits.set.marking(readBitArrayDec[13]);
        h.trimBits.set.bridgeSwap(swapStrBit);
                           
        h.opCodeMode.readFuse();
        pybrd_DUT_ps.power_off();time.sleep(equipDelay);
        pybrd_DUT_ps.set_voltage(vddDut); 
        pybrd_DUT_ps.power_on(); time.sleep(psDelay);
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);
        
        h.opCodeMode.tryBeforeBuy();
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    else:
        pybrd_DUT_ps.power_off();time.sleep(equipDelay);
        pybrd_DUT_ps.set_voltage(vddDut); 
        pybrd_DUT_ps.power_on(); time.sleep(psDelay);
                
#%% Swep the Field
    print('Sweep Field ...');
    
    zeroAHys = [];
    suZeroAOut = [];
    sdZeroAOut = [];    
    vRefArray = [];
    dutSupplyCurr = [];
    nDataPoints = int(nDataPoints)
    
    mTArray = np.concatenate( ( np.linspace( currMin, currMax, nDataPoints ), np.linspace( currMax, currMin, nDataPoints )[ 1 : ] ) );   
    nCurr = len(mTArray);    
     
    outArray =[];
    ZeroVout = [];
    
    for i in range (nCurr):                
            ret = CurrentSet(mTArray[i]);
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);        
            outArray.append(meanVal);            
            ret = CurrentSet(0);
            time.sleep(int(cooltime));
            if mTArray[i] == 0:
                ZeroVout.append(meanVal); 
    print('VOUT for noSwap:',outArray);
    ZeroOut= (max(ZeroVout)+ min(ZeroVout)) / 2;
    ret = CurrentSet(0)
        
    # Read offset value at zero for CT452/3 with right swap
    if sensor_type == 'CT452' or sensor_type == 'CT453':
        pybrd_DUT_ps.power_off();time.sleep(equipDelay);
        pybrd_DUT_ps.set_voltage(vddDut); 
        pybrd_DUT_ps.power_on(); time.sleep(psDelay);
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        RSZeroVout = meanVal
            
        # find offset from zero field 
        for i in range (nCurr):
            outArray[i] = outArray[i] - ZeroOut + RSZeroVout;
    
        print('ZeroVout',ZeroOut)
        print('RSZeroVout',RSZeroVout)
        print('VOUT by adding offset:',outArray)
        
    [bFit, mFit]    =  F_bestFitCoeff(mTArray, outArray);
    fitArray        =  mFit*np.array(mTArray) + bFit;
    bflLinErr       =  100*np.max( np.abs( np.array(outArray) - fitArray ) ) / ( max(outArray) - min(outArray) );
    
    if Flavor[5] == 'B':
        suZeroA = outArray[ int(nDataPoints / 2) ];
        sdZeroA = outArray[ int(( ( 3*nDataPoints ) - 2 ) / 2) ];
    else:
        suZeroA = outArray[ 0 ];
        sdZeroA = outArray[ -1 ];
    
    maxHys      =  np.max( np.abs( np.flip(outArray[ int(nCurr/2) : ]) - np.array(outArray[ : int((nCurr/2) + 1)])));
    idealOut    =  mIdeal*np.array(mTArray) + bIdeal;
    idealLinErr =  100*np.max( np.abs( np.array(outArray) - idealOut ) ) / ( max(outArray) - min(outArray) );
        
    suZeroAOut.append(suZeroA);
    sdZeroAOut.append(sdZeroA);
    zeroAHys.append(1000*abs(sdZeroAOut[0] - suZeroAOut[0]));              
        
    print('DUT Supply Current [mA]', dutSupplyCurr);    
    print('Actual Approx. B-Field [mT]', mTArray);
    print('Measured Output [V]', outArray);
    print('Measured vRef Pin [V]', vRefArray);    
    print('SU Measured 0A Output [V]', [suZeroAOut[0]]);
    print('SD Measured 0A Output [V]', [sdZeroAOut[0]]);
    print('Calculated BFL Output [V]', fitArray);
    print('Calculated Ideal Output [V]', idealOut);
    print('BFL Slope [mV/mT]', [1000*mFit]);
    print('BFL Offset [V]', [bFit]);
    print('Ideal Slope [mV/mT]', [1000*mIdeal]);
    print('Ideal OUT Offset [V]', [bIdeal]);               
    print('BFL Linearity Error [%FS]', [round(bflLinErr, 4)]);
    print('Ideal Linearity Error [%FS]', [round(idealLinErr, 4)]);    
    print('Max Hys ( ABSMAX(SU_OUTPUT - FLIPSD_OUT) ) [mV]', [round(1000*maxHys, 4)]);    
    print('0A Hys [mV]', [zeroAHys[0]]);
    print('Ideal OUT-VREF Offset [V]', [0]);
        
    
    #%% Burn Fuses: For burn fuses we need to have Sdata voltage of 3.6V and first clk pulse 3.6+1.4=5V 
    if burnFuseBool:
        # Burn Fuse Mode needs 4 V 
        print('Step 5: Burn Fuses according to trimming values...');       
        pybrd_DUT_ps.power_off();time.sleep(equipDelay);
        pybrd_DUT_ps.set_voltage(4);
        pybrd_DUT_ps.power_on(); time.sleep(psDelay);               
        # TBB Mode
        h.opCodeMode.tryBeforeBuy();time.sleep(equipDelay);
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);       
        # Burn Fuse Mode
        h.opCodeMode.burnFuse();time.sleep(equipDelay);
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);
       
        # Read Fuse Mode needs 3.3V and need repower
        pybrd_DUT_ps.power_off();time.sleep(equipDelay);
        pybrd_DUT_ps.set_voltage(3.3); 
        pybrd_DUT_ps.power_on(); time.sleep(psDelay);
        h.opCodeMode.readFuse();time.sleep(equipDelay);               
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);        
              
        if setBitArrayDec == readBitArrayDec:
            print('Trimming is done and Fuses Burn successfully!');
            finish = time.time() - start;
            print(finish)
            pybrd_DUT_ps.power_off();time.sleep(equipDelay);
            pybrd_DUT_ps.set_voltage(vddDut); 
            pybrd_DUT_ps.power_on(); time.sleep(psDelay);
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
            print('Trimmed Vout =', meanVal );
        else:
            print('Set and Read values are not the same!!!!');
            finish = time.time() - start;
            print(finish)
    else:
        print('Trimming is done successfully without Burn Fuse selection!');
        finish = time.time() - start;
        print(finish)        

    if(loggerEnabled):        
            sys.stdout = sys.__stdout__   
            
    #%% Finished and Turn off all power supplies
    pybrd_DUT_ps.power_off();
    pyboard_1.close();    

import argparse

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Command line trimming tool')    
   
    parser.add_argument('p', metavar='COMx',
                        help='Serial port')

    parser.add_argument('y', metavar='SensorType',
                        help='Sensor type. Possible options: \'CT450\', \'CT452\' and \'CT453\' ')
    
    parser.add_argument('f',metavar='flavor',
                        help='Sensor flavor in format:(5p0 or 3p3)V,(B or U),(max Field)mT , e.g: 3p3V,B,50mT')    

    parser.add_argument('n', metavar='nDataPoints',
                        help='Number of data point sample that must be odd value')
    
    parser.add_argument('c', metavar='cooltime',
                        help='Time to cooling the coil')
    
    parser.add_argument('--FUSE', default=False,action='store_true',help='Burn the fuse bits. Non-reversible!')
    
    parser.add_argument('--NOFUSE', dest='FUSE', action='store_false',help='Default = True, Use --FUSE to cancel the effect and burn the fuse bits.')
    
    parser.add_argument('--verbose', default=False,action='store_true', help='Verbose mode')


    args = parser.parse_args()

    TotalError(port=args.p, sensor_type = args.y, Flavor = args.f, nDataPoints=args.n, cooltime=args.c, fuse=args.FUSE, verbose=args.verbose)