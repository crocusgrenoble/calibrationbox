# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 11:43:19 2023

@author: eaerabi
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jan 26 11:23:09 2023

@author: AppLabPC
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 13:09:25 2023

@author: eaerabi
"""

##########   Import   #########################################################
import sys
import time,math
import numpy as np
import serial
import logging
import os
from os.path import exists
import subprocess
import pyvisa
from Devices.DCSs.dcs_keysight_n8731a import DCS_Keysight_N8731A

sys.path.append(".\\Lib\\")
sys.path.append(".\\Lib\\pyboard\\")
sys.path.append(".\\Lib\\Halo2_1fClassPy3\\")

from halo2_1F import halo2_1F
from F_binToDec import F_binToDec
from pyboard import PyBoard
from pyboard_dvm import Pyboard_DVM
from pyboard_dut_ps import Pyboard_DUT_PS

class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, level):
       self.logger = logger
       self.level = level
       self.linebuf = ''

    def write(self, buf):
       for line in buf.rstrip().splitlines():
          self.logger.log(self.level, line.rstrip())

    def flush(self):
        pass
def CurrentSet(curr):
    ret = subprocess.run(["CurrentControl.bat",str(curr)], shell=True, stdout=subprocess.PIPE) 
    return ret.returncode
    return ret               

port= 'COM13'
sensor_type='CT450'
flavor=' 3v3,B,50A'
ADClooplimit=30
verbose=True
virgin_device = False ## All zero bits device

#curr = dcs.setCurrent(1)
#dvs.output_ON()
#print(volt)
#print(verbose)
if (not verbose):
    loggerEnabled = True
else:
    loggerEnabled = False

print('Trimming Process being started. ')        
pyb_com_port = port#"COM6"#input('please input pyboard come port like COM21 =');
flavorStr = flavor#"3p3V,B,1200A"#input('please input sensor flavor in format:(5p0 or 3p3)V,(B or U),(max current)A like 3p3V,B,1200A = ');    
    
if(exists("CurrentControlFunction.py")):
    currentControlExist = True
    #import CurrentControlFunction
else:
    currentControlExist = False

file_path = 'out.log'
try:
    if os.path.isfile(file_path):
      os.remove(file_path)
      print("File has been deleted")
except Exception as e: # work on python 3.x
    print('Failed: '+ str(e)) 
 
if(loggerEnabled):         
    logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
            filename='out.log',
            filemode='a'
            )
    log = logging.getLogger('TrimmingProcess')

    sys.stdout = StreamToLogger(log,logging.INFO)
    sys.stderr = StreamToLogger(log,logging.ERROR)
    
start = time.time()
pyboard_1 = PyBoard(com_port = pyb_com_port, pcb_version='4.8');
halo2_parms = {'device':'CTC4000', 'pyboard_object':pyboard_1};
h = halo2_1F(halo2_parms);    

currMax = int (flavorStr [7:-1]);
currMin = -currMax;
print(currMax)

# New CT454:'rightSwap', Orian board CT43x:'leftSwap',CT45x :'noSwap';
if sensor_type == 'CT450':
    swapStrBit  = 'noSwap';       
elif sensor_type == 'CT430' or sensor_type == 'CT431' :
    swapStrBit  = 'leftSwap';       
elif sensor_type == 'CT452' or sensor_type == 'CT453' :
    swapStrBit  = 'rightSwap';
else:
    print('This is not a correct sensor type: ', sensor_type)
    sys.exit('This is not a correct sensor type: ', sensor_type)
    
repowerBool = True;

##########   Modify Parameters   ##############################################    
psDelay =  3; 
equipDelay  =  0.001    
nReads =  10; # Each nReads read 20 sample per sec of ADC

h.opCodeMode.tryBeforeBuy();
h.testBits.set.general.zeroAllTestBits();
h.trimBits.set.zeroAllTrimBits();
h.nSclkPeriods(107);
h.setVddMax(5.5);

###  DVM #1
pybrd_dvm = Pyboard_DVM(pyboard_1)
time.sleep(equipDelay);
### Calibration box power supply
pybrd_DUT_ps = Pyboard_DUT_PS(pyboard_1)    

#Step #0 [Package]: Read Bandgap, PtatUp, Xdown, Marking, and Factory FBits initialization    
h.opCodeMode.readFuse();
pybrd_DUT_ps.power_off();#time.sleep(equipDelay);
pybrd_DUT_ps.set_voltage(3.3); 
pybrd_DUT_ps.power_on(); time.sleep(psDelay);
[setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);
print(readBitArrayDec)
# =============================================================================
# TRIM00_vRefBgCornerTrim.base10Val
# TRIM01_vRefScaledBgTrim.base10Val
# TRIM02_vRegPtatUp.base10Val
# TRIM03_vRegBgXDown.base10Val
# TRIM04_vosMagLeft.base10Val
# TRIM05_vosMagRight.base10Val
# TRIM06_vosElectLeft.base10Val
# TRIM07_vosElectRight.base10Val
# TRIM08_gainInstrAmpLeft.base10Val
# TRIM09_gainInstrAmpRight.base10Val
# TRIM10_gainUnipolar.base10Val
# TRIM11_gainHiSupply.base10Val
# TRIM12_gainFor12mT.base10Val
# TRIM13_markingBits.base10Val
# TRIM14_blockTryBeforeBuy.base10Val
# TRIM15_crousMurataModes.base10Val
# TRIM16_bridgeSwaps.base10Val
# =============================================================================
h.opCodeMode.tryBeforeBuy();



#original values
h.trimBits.set.vRefBandGapCorner(readBitArrayDec[0]) 
h.trimBits.set.vRefScaledBandGap(readBitArrayDec[1]) 
h.trimBits.set.vRefPtatUp(readBitArrayDec[2]) 
h.trimBits.set.vRegBandGapXDown(readBitArrayDec[3]) 

h.trimBits.set.afeMagOffsetLeft(readBitArrayDec[4]) 
h.trimBits.set.afeMagOffsetRight(readBitArrayDec[5]) 
#print("afeAmpGainLeft", h.trimBits.read.afeAmpGainLeft())
h.trimBits.set.afeElectOffsetLeft(readBitArrayDec[6]) 
h.trimBits.set.afeElectOffsetRight(readBitArrayDec[7]) 
h.trimBits.set.afeAmpGainLeft(readBitArrayDec[8]) 
h.trimBits.set.afeAmpGainRight(readBitArrayDec[9]) 
h.__bits__.trim.TRIM10_gainUnipolar.base10Val = readBitArrayDec[10]
h.__bits__.trim.TRIM11_gainHiSupply.base10Val = 1#readBitArrayDec[11] #3v3 
h.__bits__.trim.TRIM12_gainFor12mT.base10Val = 0# readBitArrayDec[12] #8mT
h.trimBits.set.marking(readBitArrayDec[13]) 
h.__bits__.trim.TRIM14_blockTryBeforeBuy.base10Val = readBitArrayDec[14]
h.__bits__.trim.TRIM15_crousMurataModes.base10Val = readBitArrayDec[15]
h.__bits__.trim.TRIM16_bridgeSwaps.base10Val = readBitArrayDec[16]


# trimmed values
h.trimBits.set.vRefBandGapCorner(readBitArrayDec[0]) 
h.trimBits.set.vRefScaledBandGap(readBitArrayDec[1]) 
h.trimBits.set.vRefPtatUp(readBitArrayDec[2]) 
h.trimBits.set.vRegBandGapXDown(readBitArrayDec[3]) 

h.trimBits.set.afeMagOffsetLeft(6)#readBitArrayDec[4]) 
h.trimBits.set.afeMagOffsetRight(4)#readBitArrayDec[5]) 
h.trimBits.set.afeElectOffsetLeft(132)#readBitArrayDec[6]) 
h.trimBits.set.afeElectOffsetRight(131)#readBitArrayDec[7]) 
h.trimBits.set.afeAmpGainLeft(240)#readBitArrayDec[8]) 
h.trimBits.set.afeAmpGainRight(110)#readBitArrayDec[9]) 
h.__bits__.trim.TRIM10_gainUnipolar.base10Val = readBitArrayDec[10]
h.__bits__.trim.TRIM11_gainHiSupply.base10Val = 1#readBitArrayDec[11] #3v3 
h.__bits__.trim.TRIM12_gainFor12mT.base10Val = 0# readBitArrayDec[12] #8mT
h.trimBits.set.marking(readBitArrayDec[13]) 
h.__bits__.trim.TRIM14_blockTryBeforeBuy.base10Val = readBitArrayDec[14]
h.__bits__.trim.TRIM15_crousMurataModes.base10Val = readBitArrayDec[15]
h.__bits__.trim.TRIM16_bridgeSwaps.base10Val = readBitArrayDec[16]





############ Output evaluation

  #  h.testBits.set.general.zeroAllTestBits();
# =============================================================================
# h.opCodeMode.tryBeforeBuy();
# time.sleep(equipDelay);
# [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);
# =============================================================================
#time.sleep(1)

# =============================================================================
# for i in range (0,256,5):
#     print(i)
#     h.trimBits.set.afeAmpGainLeft(i)#readBitArrayDec[8]) 
# 
#     #print("Writing bits")
#     time.sleep(equipDelay);
#     [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);
#     time.sleep(1)
# =============================================================================

pyboard_1.close()
sys.exit(0) 
#%% Finished and Turn off all power supplies
  

