# -*- coding: utf-8 -*-
"""
Created on Fri Mar  19 13:37:21 2021
@author: rmazrae
"""
##########   Import   #########################################################
import time
from F_binToDec import F_binToDec
from halo2_1F import halo2_1F
from pyboard import PyBoard
from pyboard_dvm import Pyboard_DVM
from pyboard_dut_ps import Pyboard_DUT_PS
from gui_communication import GUI_Communication
import numpy as np
import pickle
        

import sys
import logging
import os
from os.path import exists
import subprocess
    
class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, level):
       self.logger = logger
       self.level = level
       self.linebuf = ''

    def write(self, buf):
       for line in buf.rstrip().splitlines():
          self.logger.log(self.level, line.rstrip())

    def flush(self):
        pass
#%%
def trimming_process(callbackFunc, gui_communication, flavour, com_port, wait_for_gui):
    # Check to see if Current Control Script is provided by the customer
    loggerEnabled = False
    
    if(exists("./CurrentControl.bat")):
        currentControlExist = True
    else:
        currentControlExist = False

    file_path = 'out.log'
    try:
        if os.path.isfile(file_path):
          os.remove(file_path)
          print("File has been deleted")
    except Exception as e: # work on python 3.x
        print('Failed: '+ str(e)) 
    if(loggerEnabled):         
        logging.basicConfig(
                level=logging.DEBUG,
                format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
                filename='out.log',
                filemode='a'
                )
        log = logging.getLogger('TrimmingProcess')

        sys.stdout = StreamToLogger(log,logging.INFO)
        sys.stderr = StreamToLogger(log,logging.ERROR)
    print('Trimming Process being started. ')    
    gui = GUI_Communication(callbackFunc, gui_communication, wait_for_gui)    
    
    gui.send_message_to_gui('Initialization');

    pyb_com_port = com_port
    pyboard_1 = PyBoard(com_port = pyb_com_port, pcb_version='4.8')
    halo2_parms = {'device':'CTC4000', 'pyboard_object':pyboard_1}
    h = halo2_1F(halo2_parms);   
    
    gui.send_message_to_gui('Connected to CTC4000.');
    
    ##########   Modify Parameters   ##############################################    
    psDelay =  0.001; # minimum 700ms for power supply        
    equipDelay  =  0.001;
    # Each nReads read 20 sample per sec of ADC
    nReads =  2;
    
    h.opCodeMode.tryBeforeBuy();
    h.testBits.set.general.zeroAllTestBits();
    h.trimBits.set.zeroAllTrimBits();
    h.nSclkPeriods(107);
    h.setVddMax(5.5);        
         ###  DVM #1
    pybrd_dvm = Pyboard_DVM(pyboard_1)
    time.sleep(0.05);    
    
    ### Calibration box power supply
    pybrd_DUT_ps = Pyboard_DUT_PS(pyboard_1)        
    
    #%%  Step #0 [Package]: Read Bandgap, PtatUp, Xdown, Marking, and Factory FBits initialization    
    h.opCodeMode.readFuse();
    
    pybrd_DUT_ps.power_off();time.sleep(equipDelay);
    pybrd_DUT_ps.set_voltage(3.3);
    pybrd_DUT_ps.power_on(); time.sleep(psDelay);        
    [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);
    
    if (readBitArrayDec[4:10] != [0, 0, 0, 0, 0, 0]) or (readBitArrayDec[12:16] != [0, 0, 0, 0]):
        pybrd_DUT_ps.power_off();
        pyboard_1.close();            
        gui.send_successful_end_to_gui("Failed: Sensor is already trimmed and CAN NOT be trimmed again!");            
      
    h.opCodeMode.tryBeforeBuy();
# =============================================================================
#         if readBitArrayDec.count(0) == 17:
#             pybrd_DUT_ps.power_off();
#             pyboard_1.close();
#             gui.send_successful_end_to_gui("Failed: Please check the wiring connection btw sensor and CTC4000!");            
#         else:
# =============================================================================
    bgTrimCode      =  readBitArrayDec[0];
    ptatUpTrimCode  =  readBitArrayDec[2];
    print(bgTrimCode, ptatUpTrimCode);
    
    #F_repowerReadFuses(pybrd_DUT_ps, h, 3.3);        
    pybrd_DUT_ps.power_off();time.sleep(equipDelay);
    pybrd_DUT_ps.set_voltage(3.3);
    pybrd_DUT_ps.power_on();time.sleep(psDelay);    
        
    print(bgTrimCode, ptatUpTrimCode)    
    
    #%% ###########################################################################      
    # select if you want to burn fuses: True or False
    if (flavour["burn"] == True):
        burnFuseBool = True;
    elif (flavour["nburn"] == True):
        burnFuseBool = False;    
    
    #%% TBB mode funtion
    repowerBool     =  True;    

    #%% Flavor selection
    # 20A = 8mT, 30A = 12mT, 50A = 20mT
        
    if ((flavour["3v3"] == True) and (flavour["bipolar"] == True)):        
         flavorStr = '3p3VBi'; 
        
    elif ((flavour["3v3"] == True) and (flavour["unipolar"] == True)):        
         flavorStr = '3p3VUni';
         
    elif ((flavour["5v"] == True) and (flavour["bipolar"] == True)):        
         flavorStr = '5p0VBi';
         
    elif ((flavour["5v"] == True) and (flavour["unipolar"] == True)):        
         flavorStr = '5p0VUni';    
            
    h.testBits.set.general.zeroAllTestBits();
    h.trimBits.set.zeroAllTrimBits();   
    
    if flavorStr[0] == '5':
        vddDut  =  5.0;
    else:
        vddDut  =  3.3;
    
    h.trimBits.set.vddSupply(str(vddDut) + 'V');
    
    if flavorStr[4] == 'B':
        h.trimBits.set.polarFlavor('bipolar');
    else:
        h.trimBits.set.polarFlavor('unipolar');
           
    xDownTrimCode = ptatUpTrimCode;
             
    if (flavour["CT450"] == True):
        swapStrBit  =  'noSwap';
        
    elif (flavour["CT453"] == True):    
        # Orian board CT453: 'leftSwap', New board CT454: 'rightSwap'
        swapStrBit  = 'rightSwap';
    
    # determine Zero Vref value
    zeroAvRefArray  =  [1.65, 0.65, 2.5, 0.5];
    idxVRef         =  F_binToDec( str( h.trimBits.read.vddSupply()[1] ) +  str( h.trimBits.read.polarFlavor()[1] ) );
    idealZeroAvRef  =  zeroAvRefArray[idxVRef];    
        
    currMax = flavour["maxcurr"];    
    currMin = -1*currMax;
    
    ###############################################################################        
    # Trim Left Only
    def F_trimLeftOnly(deltaCode, leftCurrCode):
    
        # Current Left Code is Between 128 and 255
        if (leftCurrCode >= 128) and (leftCurrCode <= 255):
            deltaCode = -deltaCode;
            
            if (leftCurrCode + deltaCode) < 128:
                nextCode = abs( 127 - (leftCurrCode + deltaCode) );
            else:
                nextCode = leftCurrCode + deltaCode;
                if nextCode > 255:
                    nextCode = 255;
                    
        # Current Left Code is Between 0 and 127        
        else:
            
            if (leftCurrCode + deltaCode) < 0:
                nextCode = 129 + leftCurrCode + abs(deltaCode);
            else:
                nextCode = leftCurrCode + deltaCode;
                if nextCode > 127:
                    nextCode = 127;
                    
        return(nextCode);
        
    # Trim Right Only
    def F_trimRightOnly(deltaCode, rightCurrCode):
    
        # Current Right Code is Between 128 and 255
        if (rightCurrCode >= 128) and (rightCurrCode <= 255):
            deltaCode = -deltaCode;
            
            if (rightCurrCode + deltaCode) < 128:
                nextCode = abs( 127 - (rightCurrCode + deltaCode) );
            
            else:
                nextCode = rightCurrCode + deltaCode;
                
                if nextCode > 255:
                    nextCode = 255;
                
        # Current Right Code is Between 0 and 127        
        else:
            
            if (rightCurrCode + deltaCode) < 0:
                nextCode = 129 + rightCurrCode + abs(deltaCode);
                
            else:
                nextCode = rightCurrCode + deltaCode;
                
                if nextCode > 127:
                    nextCode = 127;
                    
        return(nextCode);        
    
    def F_sensorGainFbitSetting( setIdx ):
        # Marking Bit = 8
        # 4mT, 20mT                
        # 10A, 50A
        
        # Marking Bit = 10
        # 8mT, 12mT                
        # 20A,  30A
        
    ##########   8mT (20A)   #############################
        if setIdx == 1:
            h.trimBits.set.marking( 0 );
            h.trimBits.set.currentFieldScale( '8mT(20A)' );
        elif setIdx == 2:
            h.trimBits.set.marking( 4 );
            h.trimBits.set.currentFieldScale( '8mT(20A)' );
        elif setIdx == 3:
            h.trimBits.set.marking( 8 );
            h.trimBits.set.currentFieldScale( '8mT(20A)' );
        elif setIdx == 4:
            h.trimBits.set.marking( 2 );
            h.trimBits.set.currentFieldScale( '8mT(20A)' );
        elif setIdx == 5:
            h.trimBits.set.marking( 6 );
            h.trimBits.set.currentFieldScale( '8mT(20A)' );
        elif setIdx == 6:
            h.trimBits.set.marking( 10 );
            h.trimBits.set.currentFieldScale( '8mT(20A)' );
            
    ##########   12mT (30A)   #############################
        elif setIdx == 7:
            h.trimBits.set.marking( 2 );
            h.trimBits.set.currentFieldScale( '12mT(30A)' );
        elif setIdx == 8:
            h.trimBits.set.marking( 6 );
            h.trimBits.set.currentFieldScale( '12mT(30A)' );
        elif setIdx == 9:
            h.trimBits.set.marking( 10 );
            h.trimBits.set.currentFieldScale( '12mT(30A)' );
        elif setIdx == 10:
            h.trimBits.set.marking( 0 );
            h.trimBits.set.currentFieldScale( '12mT(30A)' );
        elif setIdx == 11:
            h.trimBits.set.marking( 4 );
            h.trimBits.set.currentFieldScale( '12mT(30A)' );
        elif setIdx == 12:
            h.trimBits.set.marking( 8 );
            h.trimBits.set.currentFieldScale( '12mT(30A)' );    
    
    def F_repeatCodeCheck( cCode, cArray ):           
            repeatLim = 3;
            cArray[cCode] = cArray[cCode] + 1;
            boolCheck = any(list( idx > repeatLim for idx in cArray ));
            return( boolCheck, cArray );        
    
    def F_repowerTbbm(pybrd_DUT_ps, h, vddDut):   
        if repowerBool:
            pybrd_DUT_ps.power_off();time.sleep(equipDelay);
            pybrd_DUT_ps.set_voltage(3.3);
            pybrd_DUT_ps.power_on(); time.sleep(psDelay);
        elif vddDut == 5:
            pybrd_DUT_ps.set_voltage(3.3);
            pybrd_DUT_ps.power_on(); time.sleep(psDelay);        
    
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard(); time.sleep(equipDelay);
        
        if vddDut == 5:
            pybrd_DUT_ps.set_voltage(vddDut); time.sleep(psDelay); 
               
    #%% Initialization of sensor
    if h.trimBits.read.polarFlavor()[0] == 'unipolar':
        currArray   =  [0, currMax];        
        outArray = [0, 0];
        biFactor    =  1;
    else:        
        # Kepco bipolar controller [currMin, 0, currMax];
        currArray   =  [currMin, 0, currMax];        
        outArray = [0, 0, 0];
        biFactor    =  1;
    
    #ampGainDeltaArray   =  [2/float(factor), 4/float(factor)];
    ampGainDeltaArray   =  [2, 4]; ######
    idxAmpGain          =  h.trimBits.read.vddSupply()[1];
    ampGainDelta        =  ampGainDeltaArray[idxAmpGain];    
    print('ampGainDelta = ',ampGainDelta)
    h.trimBits.set.vRefBandGapCorner(bgTrimCode);
    h.trimBits.set.vRefPtatUp(ptatUpTrimCode);
    h.trimBits.set.vRegBandGapXDown(xDownTrimCode);    
    h.trimBits.set.bridgeSwap(swapStrBit);
    h.opCodeMode.tryBeforeBuy();
    
    # Display flavor selection
    gui.send_message_to_gui('Sensor flavour selection: '+ flavorStr );    
        
    #%% Step #0: Determine Sensor Gain Bits    
    print('Step 0: Trimming Sensor Gain Bits ...');
    gui.set_progress(10)
    gui.send_message_to_gui('Step 0: Trimming Sensor Gain Bits ...');    
    
    F_sensorGainFbitSetting( 10 );
    
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
 
    for i in range (len(currArray)):  
        if(currentControlExist):
            if sys.version_info.major == 3:                    
                process = subprocess.run(["CurrentControl.bat",str(currArray[i])], shell=True, stdout=subprocess.PIPE)                
                print (process.returncode)
                #time.sleep(currDelay)                    
                print("Current set to "+ str(currArray[i]));                
                [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
                print(meanVal)
                outArray[i]=meanVal;
                if currArray[i] != 0:
                    process = subprocess.run(["CurrentControl.bat",str(0)], shell=True, stdout=subprocess.PIPE)
                    print (process.returncode)
                                                                   
            else:
                process = subprocess.check_output(["CurrentControl.bat",str(currArray[i])])
                print(process)
                print("Current set to "+ str(currArray[i]));                
                [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
                print(meanVal)
                outArray[i]=meanVal;
                if currArray[i] != 0:
                    process = subprocess.check_output(["CurrentControl.bat",str(0)]);
                    print(process);              
        else:
            gui.wait_for_next_button("Please set current to "+ str(currArray[i]) + "A then press Next button");
            gui.continue_process("");
            print("Current set to "+ str(currArray[i]));                
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
            print(meanVal)
            outArray[i]=meanVal;
            
    if(not currentControlExist):       
        gui.wait_for_next_button("Please set back current to "+ str(0) + "A then press Next button");
        gui.continue_process("");
        
    measuredSensorGain = outArray[-1] - outArray[0]
    
    perDelta_setting10 = 100*( ampGainDelta / measuredSensorGain );
    perDelta_setting11 = 100*( ampGainDelta / ( 0.85*measuredSensorGain ) );
    perDelta_setting12 = 100*( ampGainDelta / ( 1.15*measuredSensorGain ) );
    
    absDelta_setting1 = abs( 501 - perDelta_setting10 );
    absDelta_setting2 = abs( 426 - perDelta_setting10 );
    absDelta_setting3 = abs( 576 - perDelta_setting10 );
    
    absDelta_setting4 = abs( 252 - perDelta_setting10 );
    absDelta_setting5 = abs( 214 - perDelta_setting10 );
    absDelta_setting6 = abs( 289 - perDelta_setting10 );
    
    absDelta_setting7 = abs( 165 - perDelta_setting10 );
    absDelta_setting8 = abs( 140 - perDelta_setting10 );
    absDelta_setting9 = abs( 190 - perDelta_setting10 );
    
    absDelta_setting10 = abs( 100 - perDelta_setting10 );
    absDelta_setting11 = abs( 100 - perDelta_setting11 );
    absDelta_setting12 = abs( 100 - perDelta_setting12 );
    
    absDeltaArray = [absDelta_setting1, absDelta_setting2, absDelta_setting3,
                     absDelta_setting4, absDelta_setting5, absDelta_setting6,
                     absDelta_setting7, absDelta_setting8, absDelta_setting9,
                     absDelta_setting10, absDelta_setting11, absDelta_setting12];
    
    idealSetting = absDeltaArray.index( min( absDeltaArray ) ) + 1;
    print(absDeltaArray)
    print(idealSetting)
    F_sensorGainFbitSetting( idealSetting );
    gui.send_message_to_gui('Step 0 is done! ');    
        
    #%% Step #1 : Trim Zero Current Voltage Reference        
    print('Step 1: Trimming vRef...');
    gui.increase_progress(10)
    gui.send_message_to_gui('Step 1: Trimming vRef...');
    
    strTestName         =  'vRef';
    vRefCodesTested     =  [];
    measuredVref        =  [];
    
    h.testBits.set.afeMux.afeMuxToFltPin('en');
    h.testBits.set.afeMux.bufferedZeroCurrVref();
    
    codeRepeatArray = np.linspace(0, 0, 64);
    
    # Largest vRef Value
    sCode = 32;
    h.trimBits.set.vRefScaledBandGap(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);        
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    
    currCode = h.trimBits.read.vRefScaledBandGap();
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( currCode, codeRepeatArray );
    
    vRefCodesTested.append( currCode );
    measuredVref.append( meanVal );
    print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
    
    # Smallest vRef Value
    sCode = 31;
    h.trimBits.set.vRefScaledBandGap(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);        
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    
    currCode = h.trimBits.read.vRefScaledBandGap();
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( currCode, codeRepeatArray );
    
    vRefCodesTested.append( currCode );
    measuredVref.append( meanVal );
    print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
    
    sCode = 0;
    h.trimBits.set.vRefScaledBandGap(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);        
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        
    currCode = h.trimBits.read.vRefScaledBandGap();
    
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( currCode, codeRepeatArray );
    
    vRefCodesTested.append( currCode );
    measuredVref.append( meanVal );
    print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
    
    vRefStepSize = ( measuredVref[-1] - measuredVref[-2] ) / 31.0;
    deltaCode = int( round( (measuredVref[-1] - idealZeroAvRef) / vRefStepSize ) );
    
    # Check if Ideal vRef is Smaller than Code 31
    if measuredVref[-2] > idealZeroAvRef:
        h.trimBits.set.vRefScaledBandGap(31);
        deltaCode = 0;
        
    # Check if Ideal vRef is Larger than Code 32
    if measuredVref[-3] < idealZeroAvRef:
        h.trimBits.set.vRefScaledBandGap(32);
        deltaCode = 0;        
    
    while (deltaCode != 0) and (not boolCheck):        
    
        nextCode = currCode + deltaCode;
        
        if (nextCode < 0) and (currCode - 32 < 0):
            nextCode = 64 + nextCode;
    			
        if (nextCode > 63) and (currCode - 31 > 0):
            nextCode = nextCode - 64;
        
        h.trimBits.set.vRefScaledBandGap(nextCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);        
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        
        currCode = h.trimBits.read.vRefScaledBandGap();
        vRefCodesTested.append( currCode );
        measuredVref.append( meanVal );
        
        print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
        
        deltaCode = int( round( (measuredVref[-1] - idealZeroAvRef) / vRefStepSize ) );
    
        [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( currCode, codeRepeatArray );                
    
    h.testBits.set.general.zeroAllTestBits();
    gui.send_message_to_gui('Step 1 is done! ');        

    #%% Step #2 : Trim Left & Right Amp Gain Together
    print('Step 2: Trimming Left & Right Amp Gain...');
    gui.increase_progress(30)
    gui.send_message_to_gui('Step 2: Trimming Left & Right Amp Gain...');    
    
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
    
    for i in range (len(currArray)):  
        if(currentControlExist):                
            if sys.version_info.major == 3:                    
                process = subprocess.run(["CurrentControl.bat",str(currArray[i])], shell=True, stdout=subprocess.PIPE)                
                #time.sleep(currDelay)
                print (process.returncode)                    
                print("Current set to "+ str(currArray[i]));                
                [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
                print(meanVal)
                outArray[i]=meanVal;
                if currArray[i] != 0:
                    process = subprocess.run(["CurrentControl.bat",str(0)], shell=True, stdout=subprocess.PIPE)
                    print (process.returncode)
                    
            else:
                process = subprocess.check_output(["CurrentControl.bat",str(currArray[i])])
                print(process)
                print("Current set to "+ str(currArray[i]));                
                [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
                print(meanVal)
                outArray[i]=meanVal;
                if currArray[i] != 0:
                    process = subprocess.check_output(["CurrentControl.bat",str(0)]);
                    print(process);              
        else:
            gui.wait_for_next_button("Please set current to "+ str(currArray[i]) + "A then press Next button");
            gui.continue_process("");
            print("Current set to "+ str(currArray[i]));                
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
            print(meanVal)
            outArray[i]=meanVal;
            
    if(not currentControlExist):       
        gui.wait_for_next_button("Please set back current to "+ str(0) + "A then press Next button");
        gui.continue_process("");
            
    perChange = 100.0*( ( ampGainDelta / ( outArray[-1] - outArray[0] ) ) - 1 );
    if perChange > 18:
        pybrd_DUT_ps.power_off();
        pyboard_1.close();
        gui.send_successful_end_to_gui("Failed:Gain adjusment needed more than 18%, sensor doesnt get enough field!");
        
    leftPerStep_2 = 0.1769;
    rightPerStep_2 = leftPerStep_2;
    
    leftPerStep_1 = 0.1443;
    rightPerStep_1 = leftPerStep_1;    
    
    if perChange < 0:
        finalLeftGainCode = 255 + round( perChange / leftPerStep_2, 0 );
        finalRightGainCode = 255 + round( perChange / rightPerStep_2, 0 );
        leftFinalStep = leftPerStep_2;
        rightFinalStep = rightPerStep_2;
    else:
        finalLeftGainCode = 0 + round( perChange / leftPerStep_1, 0 );
        finalRightGainCode = 0 + round( perChange / rightPerStep_1, 0 );
        leftFinalStep = leftPerStep_1;
        rightFinalStep = rightPerStep_1;        
    
    strTestName     =  'leftAndRightAmpGain';
    leftCombinedAmpGainCodesTested      =  [];
    rightCombinedAmpGainCodesTested     =  [];
    measuredLeftAndRightAmpGain         =  [];
    
    h.trimBits.set.afeAmpGainLeft(finalLeftGainCode);
    h.trimBits.set.afeAmpGainRight(finalRightGainCode);
    
    currLeftCode    =  h.trimBits.read.afeAmpGainLeft();
    currRightCode   =  h.trimBits.read.afeAmpGainRight();
    
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
            
    for i in range (len(currArray)):  
        if(currentControlExist):                
            if sys.version_info.major == 3:                    
                process = subprocess.run(["CurrentControl.bat",str(currArray[i])], shell=True, stdout=subprocess.PIPE)                
                #time.sleep(currDelay)
                print (process.returncode)
                print("Current set to "+ str(currArray[i]));                
                [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
                print(meanVal)
                outArray[i]=meanVal;
                if currArray[i] != 0:
                    process = subprocess.run(["CurrentControl.bat",str(0)], shell=True, stdout=subprocess.PIPE)
                    print(process.returncode)                                              
            else:
                process = subprocess.check_output(["CurrentControl.bat",str(currArray[i])])
                print(process)
                print("Current set to "+ str(currArray[i]));                
                [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
                print(meanVal)
                outArray[i]=meanVal;
                if currArray[i] != 0:
                    process = subprocess.check_output(["CurrentControl.bat",str(0)]);
                    print(process);              
        else:
            gui.wait_for_next_button("Please set current to "+ str(currArray[i]) + "A then press Next button");
            gui.continue_process("");
            print("Current set to "+ str(currArray[i]));                
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
            print(meanVal)
            outArray[i]=meanVal;
            
    if(not currentControlExist):       
        gui.wait_for_next_button("Please set back current to "+ str(0) + "A then press Next button");
        gui.continue_process("");
        
    leftCombinedAmpGainCodesTested.append( currLeftCode );
    rightCombinedAmpGainCodesTested.append( currRightCode );
    measuredLeftAndRightAmpGain.append( outArray[-1] - outArray[0] );
    print('leftAmp @ Code ' + str(currLeftCode)  + ' & rightAmp @ Code '+ str(currRightCode) + ' = ' + str(outArray[-1] - outArray[0])  + 'V');
    leftAndRightGainInScale = (measuredLeftAndRightAmpGain[-1] <= biFactor*1.0016*ampGainDelta) and (measuredLeftAndRightAmpGain[-1] >= biFactor*0.9984*ampGainDelta);        
    
    while not leftAndRightGainInScale:
        
        leftDelta = round( 100*((ampGainDelta/measuredLeftAndRightAmpGain[-1]) - 1)/leftFinalStep, 0);
        rightDelta = round( 100*((ampGainDelta/measuredLeftAndRightAmpGain[-1]) - 1)/rightFinalStep, 0);
    
        corrDeltaLeftBool = True;
        corrDeltaRightBool = True;
        
        if currLeftCode + leftDelta > 255:
            h.trimBits.set.afeAmpGainLeft(currLeftCode + leftDelta - 256);
            corrDeltaLeftBool = False;
        
        if currRightCode + rightDelta > 255:
            h.trimBits.set.afeAmpGainRight(currRightCode + rightDelta - 256);
            corrDeltaRightBool = False;
            
        if currLeftCode + leftDelta < 0:
            h.trimBits.set.afeAmpGainLeft(currLeftCode + leftDelta + 256);
            corrDeltaLeftBool = False;
            
        if currRightCode + rightDelta < 0:
            h.trimBits.set.afeAmpGainRight(currRightCode + rightDelta + 256);
            corrDeltaRightBool = False;
            
        if corrDeltaLeftBool:
            h.trimBits.set.afeAmpGainLeft(currLeftCode + leftDelta);
            
        if corrDeltaRightBool: 
            h.trimBits.set.afeAmpGainRight(currRightCode + rightDelta);
        
        currLeftCode    =  h.trimBits.read.afeAmpGainLeft();
        currRightCode   =  h.trimBits.read.afeAmpGainRight();
        
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        
        for i in range (len(currArray)):  
            if(currentControlExist):                
                if sys.version_info.major == 3:                    
                    process = subprocess.run(["CurrentControl.bat",str(currArray[i])], shell=True, stdout=subprocess.PIPE)                
                    #time.sleep(currDelay)
                    print(process.returncode)                       
                    print("Current set to "+ str(currArray[i]));                
                    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
                    print(meanVal)
                    outArray[i]=meanVal;
                    if currArray[i] != 0:
                        process = subprocess.run(["CurrentControl.bat",str(0)], shell=True, stdout=subprocess.PIPE)
                        print(process.returncode)
                        
                else:
                    process = subprocess.check_output(["CurrentControl.bat",str(currArray[i])])
                    print(process)
                    print("Current set to "+ str(currArray[i]));                
                    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
                    print(meanVal)
                    outArray[i]=meanVal;
                    if currArray[i] != 0:
                        process = subprocess.check_output(["CurrentControl.bat",str(0)]);
                        print(process);              
            else:
                gui.wait_for_next_button("Please set current to "+ str(currArray[i]) + "A then press Next button");
                gui.continue_process("");
                print("Current set to "+ str(currArray[i]));                
                [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
                print(meanVal)
                outArray[i]=meanVal;
                
        if(not currentControlExist):       
            gui.wait_for_next_button("Please set back current to "+ str(0) + "A then press Next button");
            gui.continue_process("");
            
        leftCombinedAmpGainCodesTested.append( currLeftCode );
        rightCombinedAmpGainCodesTested.append( currRightCode );
        measuredLeftAndRightAmpGain.append( outArray[-1] - outArray[0] );        
        print(strTestName + ' @ Left & Right Code ' + str(currLeftCode)+ ' & '  + str(currRightCode)  + ' = ' + str(outArray[-1] - outArray[0])  + 'V');            
        leftAndRightGainInScale = (measuredLeftAndRightAmpGain[-1] <= 1.0216*ampGainDelta) and (measuredLeftAndRightAmpGain[-1] >= 0.98*ampGainDelta);            
        
    h.testBits.set.general.zeroAllTestBits();
    gui.increase_progress(5);
    gui.send_message_to_gui('Step 2 is done! ');             
      
    #%% Step #5 : Trim Left & Right Electrical Offset Together
    print('Step 3: Trimming Left & Right Elect Offset...');
    gui.increase_progress(20)
    gui.send_message_to_gui('Step 3: Trimming Left & Right Elect Offset...');        
    
    strTestName     =  'leftAndRightElectOffset';
    leftCombinedElectOffsetCodesTested      =  [];
    rightCombinedElectOffsetCodesTested     =  [];
    measuredLeftAndRightElectOffset         =  [];
    
    h.testBits.set.general.rightBridge('short');
    h.testBits.set.general.leftBridge('short');
    
    codeRepeatArray = np.linspace(0, 0, 256);
            
    sCode = 128;
    h.trimBits.set.afeElectOffsetLeft(sCode);
    h.trimBits.set.afeElectOffsetRight(sCode);
    
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);        
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    
    leftCurrCode = h.trimBits.read.afeElectOffsetLeft();
    rightCurrCode = h.trimBits.read.afeElectOffsetRight();
    
    leftCombinedElectOffsetCodesTested.append( leftCurrCode );
    rightCombinedElectOffsetCodesTested.append( rightCurrCode );
    measuredLeftAndRightElectOffset.append( meanVal );
    print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');

    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
    
    sCode = 255;
    h.trimBits.set.afeElectOffsetLeft(sCode);
    h.trimBits.set.afeElectOffsetRight(sCode);
    
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);        
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    leftCurrCode = h.trimBits.read.afeElectOffsetLeft();
    rightCurrCode = h.trimBits.read.afeElectOffsetRight();
    
    leftCombinedElectOffsetCodesTested.append( leftCurrCode );
    rightCombinedElectOffsetCodesTested.append( rightCurrCode );
    measuredLeftAndRightElectOffset.append( meanVal );
    print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
    
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
    
    normOffsetStep = ( measuredLeftAndRightElectOffset[-1] - measuredLeftAndRightElectOffset[-2] ) / 127.0;
            
    sCode = 0;
    h.trimBits.set.afeElectOffsetLeft(sCode);
    h.trimBits.set.afeElectOffsetRight(sCode);
    
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);        
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    leftCurrCode = h.trimBits.read.afeElectOffsetLeft();
    rightCurrCode = h.trimBits.read.afeElectOffsetRight();
    
    leftCombinedElectOffsetCodesTested.append( leftCurrCode );
    rightCombinedElectOffsetCodesTested.append( rightCurrCode );
    measuredLeftAndRightElectOffset.append( meanVal );
    print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
    
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
    
    deltaCode = int( round( (measuredLeftAndRightElectOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
    
    while (deltaCode != 0) and (not boolCheck):

        # Current Code is Between 128 and 255
        if (leftCurrCode >= 128) and (leftCurrCode <= 255):
            deltaCode = -deltaCode;
            
            if (leftCurrCode + deltaCode) < 128:
                nextCode = abs( 127 - (leftCurrCode + deltaCode) );
            
            else:
                nextCode = leftCurrCode + deltaCode;
                
                if nextCode > 255:
                    nextCode = 255;
                
        # Current Code is Between 0 and 127        
        else:
            
            if (leftCurrCode + deltaCode) < 0:
                nextCode = 129 + leftCurrCode + abs(deltaCode);
                
            else:
                nextCode = leftCurrCode + deltaCode;
                
                if nextCode > 127:
                    nextCode = 127;
            
        h.trimBits.set.afeElectOffsetLeft(nextCode); 
        h.trimBits.set.afeElectOffsetRight(nextCode); 
        
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);        
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        leftCurrCode = h.trimBits.read.afeElectOffsetLeft();
        rightCurrCode = h.trimBits.read.afeElectOffsetRight();
        
        leftCombinedElectOffsetCodesTested.append( leftCurrCode );
        rightCombinedElectOffsetCodesTested.append( rightCurrCode );
        measuredLeftAndRightElectOffset.append( meanVal );
        print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
        
        deltaCode = int( round( (measuredLeftAndRightElectOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
            
        [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
    
    h.testBits.set.general.zeroAllTestBits();
    gui.send_message_to_gui('afeElectOffsetLeft = '+ str(currLeftCode));    
    gui.send_message_to_gui('afeElectOffsetRight = '+ str(currRightCode));
    gui.send_message_to_gui('Step 3 is done! ');        
    
    #%% Step #8 : Trim Left & Right Magnetic Offset Together            
    print('Step 4: Trimming Left & Right Magnetic Offset...');
    gui.increase_progress(20)
    gui.send_message_to_gui('Step 4: Trimming Left & Right Magnetic Offset...');
    
    strTestName     =  'leftAndRightMagOffset';
    leftCombinedMagOffsetCodesTested    =  [];
    rightCombinedMagOffsetCodesTested   =  [];
    measuredLeftAndRightMagOffset       =  [];
    
    codeRepeatArray = np.linspace(0, 0, 256);
            
    sCode = 128;
    h.trimBits.set.afeMagOffsetLeft(sCode);
    h.trimBits.set.afeMagOffsetRight(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    leftCurrCode = h.trimBits.read.afeMagOffsetLeft();
    rightCurrCode = h.trimBits.read.afeMagOffsetRight();        
    
    leftCombinedMagOffsetCodesTested.append( leftCurrCode );
    rightCombinedMagOffsetCodesTested.append( rightCurrCode );
    measuredLeftAndRightMagOffset.append( meanVal );
    print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
    
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
    
    sCode = 255;
    h.trimBits.set.afeMagOffsetLeft(sCode);
    h.trimBits.set.afeMagOffsetRight(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    leftCurrCode = h.trimBits.read.afeMagOffsetLeft();
    rightCurrCode = h.trimBits.read.afeMagOffsetRight();        
    
    leftCombinedMagOffsetCodesTested.append( leftCurrCode );
    rightCombinedMagOffsetCodesTested.append( rightCurrCode );
    measuredLeftAndRightMagOffset.append( meanVal );
    print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
    
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
    
    normOffsetStep = ( measuredLeftAndRightMagOffset[-1] - measuredLeftAndRightMagOffset[-2] ) / 127.0;
    
    sCode = 0;
    h.trimBits.set.afeMagOffsetLeft(sCode);
    h.trimBits.set.afeMagOffsetRight(sCode);
    F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    
    [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
    leftCurrCode = h.trimBits.read.afeMagOffsetLeft();
    rightCurrCode = h.trimBits.read.afeMagOffsetRight();        
    
    leftCombinedMagOffsetCodesTested.append( leftCurrCode );
    rightCombinedMagOffsetCodesTested.append( rightCurrCode );
    measuredLeftAndRightMagOffset.append( meanVal );
    print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
    
    [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );
    
    # Create Finer Step Loop Here
    normOffsetStep = normOffsetStep / 2.0;
    
    deltaCode = int( round( (measuredLeftAndRightMagOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
    
    leftCodeBool = True;
    rightCodeBool = False;        
    
    while (deltaCode != 0) and (not boolCheck):
        rightCodeBool = rightCodeBool;
        # Change Left Code Only
        if leftCodeBool:                
            nextCode = F_trimLeftOnly(deltaCode, leftCurrCode);
            leftCodeBool = False;
            rightCodeBool = True;                
            h.trimBits.set.afeMagOffsetLeft(nextCode);
         
        # Change Right Code Only   
        else:
            nextCode = F_trimRightOnly(deltaCode, rightCurrCode);
            leftCodeBool = True;
            rightCodeBool = False;
            h.trimBits.set.afeMagOffsetRight(nextCode);
            
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        leftCurrCode = h.trimBits.read.afeMagOffsetLeft();
        rightCurrCode = h.trimBits.read.afeMagOffsetRight();  
        
        leftCombinedMagOffsetCodesTested.append( leftCurrCode );
        rightCombinedMagOffsetCodesTested.append( rightCurrCode );
        measuredLeftAndRightMagOffset.append( meanVal );
        print(strTestName + ' Left @ Code ' + str(leftCurrCode)  + ' & Right @ Code ' + str(rightCurrCode) + ' = ' + str(meanVal)  + 'V');
        
        deltaCode = int( round( (measuredLeftAndRightMagOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
            
        [ boolCheck, codeRepeatArray ] = F_repeatCodeCheck( leftCurrCode, codeRepeatArray );    
            
    h.testBits.set.general.zeroAllTestBits();        
    gui.send_message_to_gui('Step 4 is done! ');   
    
    

    #...
# =============================================================================
#         som = pickle.load(fileObject)
#         som.work()        
#         
# =============================================================================
    #%% Burn Fuses: For burn fuses we need to have Sdata voltage of 3.6V and first clk pulse 3.6+1.4=5V    
    if burnFuseBool:
        print('Burn Fuses according to trimming values...');
        gui.send_message_to_gui('Burn Fuses according to trimming values...');
        pybrd_DUT_ps.power_off();time.sleep(equipDelay);
        pybrd_DUT_ps.set_voltage(4);
        pybrd_DUT_ps.power_on(); time.sleep(psDelay);        
                
        # TBB Mode
        h.opCodeMode.tryBeforeBuy();time.sleep(equipDelay);        
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);        
        
        # Burn Fuse Mode
        h.opCodeMode.burnFuse();time.sleep(equipDelay);        
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);        
                
        
        pybrd_DUT_ps.power_off();time.sleep(equipDelay);
        pybrd_DUT_ps.set_voltage(3.3); 
        pybrd_DUT_ps.power_on(); time.sleep(psDelay); 
        # Read Fuse Mode
        h.opCodeMode.readFuse();time.sleep(equipDelay);                    
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);        
        
        if setBitArrayDec == readBitArrayDec:
            print('Set & Read Fuse Bits are the same!');
            gui.send_message_to_gui('Burn Fuses is done successfully! ');
            gui.increase_progress(5);
            gui.send_successful_end_to_gui("Trimming is done and Fuses Burn successfully!");                
            
        else:
            print('Set & Read Fuse Bits are NOT the same!!!!!');
            gui.send_message_to_gui('Set & Read Fuse Bits are NOT the same!!!!!');
            gui.increase_progress(5);
            gui.send_successful_end_to_gui('Set & Read Fuse Bits are NOT the same!!!!!');                
        
    else:
        gui.increase_progress(5);        
        gui.send_successful_end_to_gui("Trimming is done successfully without Burn Fuse selection!");
        
#%% Finished and Turn off all power supplies
    pybrd_DUT_ps.power_off();
    pyboard_1.close();
# =============================================================================
#     except Exception as e: # work on python 3.x
#         pybrd_DUT_ps.power_off();
#         pyboard_1.close();     
#         print('Failed: '+ str(e)) 
#         gui.send_successful_end_to_gui("Trimming failed, see log file !");
# =============================================================================
