# -*- coding: utf-8 -*-
"""
Created on Fri Mar  19 13:37:21 2021

@author: rmazrae
"""

##########   Import   #########################################################
import time, sys
from F_binToDec import F_binToDec
from halo2_1F import halo2_1F
from pyboard import PyBoard
from pyboard_dvm import Pyboard_DVM
from pyboard_dut_ps import Pyboard_DUT_PS
from gui_communication import GUI_Communication
import numpy as np

import sys
import logging
import os

from os.path import exists
from os.path import exists
import subprocess


sys.path.append('C:\\Users\\Rmazrae\\Documents\\0_Application-Docs\\0_CalibrationBox\\Pyboard_Codes\\drivers')

class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, level):
       self.logger = logger
       self.level = level
       self.linebuf = ''

    def write(self, buf):
       for line in buf.rstrip().splitlines():
          self.logger.log(self.level, line.rstrip())

    def flush(self):
        pass
#%%
def trimming_process(callbackFunc, gui_communication, flavour, com_port, wait_for_gui):
    # Check to see if Current Control Script is provided by the customer
        loggerEnabled = False
        
        if(exists("./CurrentControl.bat")):
            currentControlExist = True
        else:
            currentControlExist = False
    
        file_path = 'out.log'
  #  try:
        if os.path.isfile(file_path):
          os.remove(file_path)
          print("File has been deleted")
   # except Exception as e: # work on python 3.x
#        print('Failed: '+ str(e)) 
    #try: 
        if(loggerEnabled):         
            logging.basicConfig(
                    level=logging.DEBUG,
                    format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
                    filename='out.log',
                    filemode='a'
                    )
            log = logging.getLogger('TrimmingProcess')

            sys.stdout = StreamToLogger(log,logging.INFO)
            sys.stderr = StreamToLogger(log,logging.ERROR)
        print('Trimming Process being started. ')    
        gui = GUI_Communication(callbackFunc, gui_communication, wait_for_gui)    
        
        gui.send_message_to_gui('Initialization');
    
        pyb_com_port = com_port
        pyboard_1 = PyBoard(com_port = pyb_com_port, pcb_version='4.8')
        halo2_parms = {'device':'CTC4000', 'pyboard_object':pyboard_1}
        h = halo2_1F(halo2_parms);   
        
        gui.send_message_to_gui('Connected to CTC4000.');
        
        ##########   Modify Parameters   ##############################################    
        psDelay =  0.1;
        equipDelay  =  0.01;     #  default 0.3s
        # Each nReads read 20 sample per sec of ADC
        nReads =  2;   
        
        h.opCodeMode.tryBeforeBuy();
        h.testBits.set.general.zeroAllTestBits();
        h.trimBits.set.zeroAllTrimBits();
        h.nSclkPeriods(107);
        h.setVddMax(5.5);
        time.sleep(equipDelay);    
        
        ###  DVM #1
        pybrd_dvm = Pyboard_DVM(pyboard_1)
        time.sleep(equipDelay);    
        
        ### Calibration box power supply
        pybrd_DUT_ps = Pyboard_DUT_PS(pyboard_1)    
        
        #%%  Step #0 [Package]: Read Bandgap, PtatUp, Xdown, Marking, and Factory FBits initialization    
        h.opCodeMode.readFuse();
    
        pybrd_DUT_ps.power_off();time.sleep(equipDelay);
        pybrd_DUT_ps.set_voltage(3.3); time.sleep(equipDelay);
        pybrd_DUT_ps.power_on(); time.sleep(equipDelay);
        
        [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);    
        
        h.opCodeMode.tryBeforeBuy();
        
        ###  Needed if Parts are UnTrimmed
        if readBitArrayDec.count(0) == 17:
            bgTrimCode      =  3;
            ptatUpTrimCode  =  14;
        else:
            bgTrimCode      =  readBitArrayDec[0];
            ptatUpTrimCode  =  readBitArrayDec[2];
            print(bgTrimCode, ptatUpTrimCode)    
        
        #F_repowerReadFuses(pybrd_DUT_ps, h, 3.3);
        time.sleep(equipDelay);
        pybrd_DUT_ps.power_off()
        time.sleep(equipDelay);
        pybrd_DUT_ps.set_voltage(3.3)
        pybrd_DUT_ps.power_on()
        time.sleep(equipDelay);    
            
        print(bgTrimCode, ptatUpTrimCode)    
        
        #%% ###########################################################################      
        # select if you want to burn fuses: True or False
        if (flavour["burn"] == True):
            burnFuseBool = True;
        elif (flavour["nburn"] == True):
            burnFuseBool = False;    
        
        #%% TBB mode funtion
        repowerBool     =  True;    
    
        #%% Flavor selection
        # 20A = 8mT, 30A = 12mT, 50A = 20mT
            
        if ((flavour["3v3"] == True) and (flavour["bipolar"] == True)):        
             flavorStr = '3p3VBi'; 
            
        elif ((flavour["3v3"] == True) and (flavour["unipolar"] == True)):        
             flavorStr = '3p3VUni';
             
        elif ((flavour["5v"] == True) and (flavour["bipolar"] == True)):        
             flavorStr = '5p0VBi';
             
        elif ((flavour["5v"] == True) and (flavour["unipolar"] == True)):        
             flavorStr = '5p0VUni';    
                
        h.testBits.set.general.zeroAllTestBits();
        h.trimBits.set.zeroAllTrimBits();   
        
        if flavorStr[0] == '5':
            vddDut  =  5.0;
        else:
            vddDut  =  3.3;
        
        h.trimBits.set.vddSupply(str(vddDut) + 'V');
        
        if flavorStr[4] == 'B':
            h.trimBits.set.polarFlavor('bipolar');
        else:
            h.trimBits.set.polarFlavor('unipolar');
               
        xDownTrimCode = ptatUpTrimCode;
                 
        if (flavour["CT450"] == True):
            swapStrBit  =  'noSwap';
            
        elif (flavour["CT453"] == True):    
            # Orian board CT453: 'leftSwap' ,
            swapStrBit  = 'rightSwap';
        
        # New board CT454: 'rightSwap' ,
        #swapStrBit  = 'rightSwap';
        
        # determine Zero Vref value
        zeroAvRefArray  =  [1.65, 0.65, 2.5, 0.5];
        idxVRef         =  F_binToDec( str( h.trimBits.read.vddSupply()[1] ) +  str( h.trimBits.read.polarFlavor()[1] ) );
        idealZeroAvRef  =  zeroAvRefArray[idxVRef];    
            
        currMax = flavour["maxcurr"];    
        currMin = -1*currMax;
        
        ###############################################################################
        def F_repowerTbbm(pybrd_DUT_ps, h, vddDut):
                
                if repowerBool:            
                    pybrd_DUT_ps.power_off();time.sleep(equipDelay);
                    pybrd_DUT_ps.set_voltage(3.3); time.sleep(equipDelay);
                    pybrd_DUT_ps.power_on(); time.sleep(equipDelay);
                else:
                    pybrd_DUT_ps.set_voltage(3.3); time.sleep(equipDelay);        
                        
                [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard(); time.sleep(equipDelay);       
                
                pybrd_DUT_ps.set_voltage(vddDut); time.sleep(psDelay);
            # Table 3 (sensorAndGain Fbit Settings)
        def F_sensorGainFbitSetting( setIdx ):
            
        ##########   8mT (20A)   #############################
            if setIdx == 1:
                h.trimBits.set.marking( 0 );
                h.trimBits.set.currentFieldScale( '8mT(20A)' );
            elif setIdx == 2:
                h.trimBits.set.marking( 4 );
                h.trimBits.set.currentFieldScale( '8mT(20A)' );
            elif setIdx == 3:
                h.trimBits.set.marking( 8 );
                h.trimBits.set.currentFieldScale( '8mT(20A)' );
            elif setIdx == 4:
                h.trimBits.set.marking( 2 );
                h.trimBits.set.currentFieldScale( '8mT(20A)' );
            elif setIdx == 5:
                h.trimBits.set.marking( 6 );
                h.trimBits.set.currentFieldScale( '8mT(20A)' );
            elif setIdx == 6:
                h.trimBits.set.marking( 10 );
                h.trimBits.set.currentFieldScale( '8mT(20A)' );
                
        ##########   12mT (30A)   #############################
            elif setIdx == 7:
                h.trimBits.set.marking( 2 );
                h.trimBits.set.currentFieldScale( '12mT(30A)' );
            elif setIdx == 8:
                h.trimBits.set.marking( 6 );
                h.trimBits.set.currentFieldScale( '12mT(30A)' );
            elif setIdx == 9:
                h.trimBits.set.marking( 10 );
                h.trimBits.set.currentFieldScale( '12mT(30A)' );
            elif setIdx == 10:
                h.trimBits.set.marking( 0 );
                h.trimBits.set.currentFieldScale( '12mT(30A)' );
            elif setIdx == 11:
                h.trimBits.set.marking( 4 );
                h.trimBits.set.currentFieldScale( '12mT(30A)' );
            elif setIdx == 12:
                h.trimBits.set.marking( 8 );
                h.trimBits.set.currentFieldScale( '12mT(30A)' );    
        
        #%% Initialization of sensor
        if h.trimBits.read.polarFlavor()[0] == 'unipolar':
            currArray   =  [0, currMax];        
            outArray = [0, 0];
            biFactor    =  1;
        else:        
            # Kepco bipolar controller [currMin, 0, currMax];
            currArray   =  [currMin, 0, currMax];        
            outArray = [0, 0, 0];
            biFactor    =  1;
        
        #ampGainDeltaArray   =  [2/float(factor), 4/float(factor)];
        ampGainDeltaArray   =  [2, 4]; ######
        idxAmpGain          =  h.trimBits.read.vddSupply()[1];
        ampGainDelta        =  ampGainDeltaArray[idxAmpGain];    
        
        h.trimBits.set.vRefBandGapCorner(bgTrimCode);
        h.trimBits.set.vRefPtatUp(ptatUpTrimCode);
        h.trimBits.set.vRegBandGapXDown(xDownTrimCode);    
        h.trimBits.set.bridgeSwap(swapStrBit);
        h.opCodeMode.tryBeforeBuy();
        
        # Display flavor selection
        gui.send_message_to_gui('Sensor flavour selection: '+ flavorStr );    
            
        #%% Step #0: Determine Sensor Gain Bits    
        print('Step 0: Trimming Sensor Gain Bits ...');
        gui.set_progress(10)
        gui.send_message_to_gui('Step 0: Trimming Sensor Gain Bits ...');    
        
        F_sensorGainFbitSetting( 10 );
        
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
            
        for i in range (len(currArray)):  
            if(currentControlExist):
                #process = subprocess.run(["python","CurrentControl.py",str(currArray[i])], shell=True, stdout=subprocess.PIPE)
                process = subprocess.run(["CurrentControl.bat",str(currArray[i])], shell=True, stdout=subprocess.PIPE)
                #process.wait()
                print(process.returncode)
                print(process.stdout)            
            else:
                gui.wait_for_next_button("Please set current to "+ str(currArray[i]) + "A then press Next button");
            print("Current set to "+ str(currArray[i]))                
            #os.system("echo Hello from the other side!"+str(currArray[i])+">>output.text")            
            gui.continue_process("");
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
            print(meanVal)
            outArray[i]=meanVal;
        
        measuredSensorGain = outArray[-1] - outArray[0];
        
        perDelta_setting10 = 100*( ampGainDelta / measuredSensorGain );
        perDelta_setting11 = 100*( ampGainDelta / ( 0.85*measuredSensorGain ) );
        perDelta_setting12 = 100*( ampGainDelta / ( 1.15*measuredSensorGain ) );
        
        absDelta_setting1 = abs( 501 - perDelta_setting10 );
        absDelta_setting2 = abs( 426 - perDelta_setting10 );
        absDelta_setting3 = abs( 576 - perDelta_setting10 );
        
        absDelta_setting4 = abs( 252 - perDelta_setting10 );
        absDelta_setting5 = abs( 214 - perDelta_setting10 );
        absDelta_setting6 = abs( 289 - perDelta_setting10 );
        
        absDelta_setting7 = abs( 165 - perDelta_setting10 );
        absDelta_setting8 = abs( 140 - perDelta_setting10 );
        absDelta_setting9 = abs( 190 - perDelta_setting10 );
        
        absDelta_setting10 = abs( 100 - perDelta_setting10 );
        absDelta_setting11 = abs( 100 - perDelta_setting11 );
        absDelta_setting12 = abs( 100 - perDelta_setting12 );
        
        absDeltaArray = [absDelta_setting1, absDelta_setting2, absDelta_setting3,
                         absDelta_setting4, absDelta_setting5, absDelta_setting6,
                         absDelta_setting7, absDelta_setting8, absDelta_setting9,
                         absDelta_setting10, absDelta_setting11, absDelta_setting12];
        
        idealSetting = absDeltaArray.index( min( absDeltaArray ) ) + 1;
        
        F_sensorGainFbitSetting( idealSetting );
        gui.send_message_to_gui('Step 0 is done! ');    
            
        #%% Step #1 : Trim Zero Current Voltage Reference
        
        # delAllVarClrConsole('cls');
        
        print('Step 1: Trimming vRef...');
        gui.set_progress(10)
        gui.send_message_to_gui('Step 1: Trimming vRef...');
        
        strTestName         =  'vRef';
        vRefCodesTested     =  [];
        measuredVref        =  [];
        
        h.testBits.set.afeMux.afeMuxToFltPin('en');
        h.testBits.set.afeMux.bufferedZeroCurrVref();          
        
        # Dynamic Step size
        sCode = 0;
        mxCode = 32;
        miCode = 31;
        ssCode = 63;
        
        h.trimBits.set.vRefScaledBandGap(sCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        sVref=meanVal;
        h.trimBits.set.vRefScaledBandGap(miCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        miVref=meanVal;
        
        h.trimBits.set.vRefScaledBandGap(ssCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        ssVref=meanVal;
        h.trimBits.set.vRefScaledBandGap(mxCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        mxVref=meanVal;
        
        if ((miVref > idealZeroAvRef) or (mxVref < idealZeroAvRef)):
            print('idealZeroAvRef is not on the range?');
            gui.send_error_to_gui('Error 01: please check connection wires to sensor, close window and restart')        
        
        # Dynamic normOffsetStep calculation    
        vRefStepSize = max(abs((sVref - miVref)/32), abs((mxVref - ssVref)/32));
        vRefStepSize = round(vRefStepSize + 0.000001, 6);    
        
        h.trimBits.set.vRefScaledBandGap(sCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);         
            
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads)
        print([minVal, meanVal, maxVal])    
        
        currCode = h.trimBits.read.vRefScaledBandGap();
        print(currCode)    
        
        vRefCodesTested.append( currCode );
        measuredVref.append( meanVal );
        print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
        #gui.send_message_to_gui(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
         
        deltaCode = int( round( (measuredVref[-1] - idealZeroAvRef) / vRefStepSize ) );
        print(deltaCode)    
        
        if deltaCode < 0:
            deltaCode = 63 + deltaCode;
            
        nTime = 0
        while deltaCode != 0:
            nextCode = currCode + deltaCode;
            print(nextCode)        
            
            # Check if nextcode jump out of range
            if currCode > 63:
                deltaCode = -round(deltaCode/2);
            
            nextCode = currCode + deltaCode;
            
            if nextCode < 0:
                nextCode = 63 + nextCode;    
            
            # Check if nextCode repeated        
            if (nextCode == currCode + 1) or (nextCode == currCode - 1) or (nextCode == currCode):
                nTime += 1
                print(nTime)
                if(nTime>2):
                    break           
            
            h.trimBits.set.vRefScaledBandGap(nextCode);
            F_repowerTbbm(pybrd_DUT_ps, h, vddDut);        
                   
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads)
            print([minVal, meanVal, maxVal])                
            
            currCode = h.trimBits.read.vRefScaledBandGap();
            vRefCodesTested.append( currCode );
            measuredVref.append( meanVal );
            
            print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
            #gui.send_message_to_gui(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
            
            deltaCode = int( round( (measuredVref[-1] - idealZeroAvRef) / vRefStepSize ) );    
           
        h.testBits.set.general.zeroAllTestBits();
        gui.send_message_to_gui('Step 1 is done! ');           
        
        #%% Step #2 : Trim Left Amp Gain    
           
        #%% Step #3 : Trim Right Amp Gain
       
        #%% Step #2 : Trim Left & Right Amp Gain Together
        print('Step 2: Trimming Left & Right Amp Gain...');
        gui.increase_progress(15)
        gui.send_message_to_gui('Step 2: Trimming Left & Right Amp Gain...');    
        
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        
        for i in range (len(currArray)): 
            if(currentControlExist):
               #process = subprocess.run(["python","CurrentControl.py",str(currArray[i])], shell=True, stdout=subprocess.PIPE)
                process = subprocess.run(["CurrentControl.bat",str(currArray[i])], shell=True, stdout=subprocess.PIPE)
                
                #process.wait()
                print(process.returncode)
                print(process.stdout)            
            else:
                gui.wait_for_next_button("Please set current to "+ str(currArray[i]) + "A then press Next button");            

            print("Current set to "+ str(currArray[i]))                
            gui.continue_process("");
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);        
            print(meanVal)
            outArray[i] = meanVal;
                
        perChange = 100.0*( ( ampGainDelta / ( outArray[-1] - outArray[0] ) ) - 1 );
        
        leftPerStep_2 = 0.1769;
        rightPerStep_2 = leftPerStep_2;
        
        leftPerStep_1 = 0.1443;
        rightPerStep_1 = leftPerStep_1;    
        
        if perChange < 0:
            finalLeftGainCode = 255 + round( perChange / leftPerStep_2, 0 );
            finalRightGainCode = 255 + round( perChange / rightPerStep_2, 0 );
            leftFinalStep = leftPerStep_2;
            rightFinalStep = rightPerStep_2;
        else:
            finalLeftGainCode = 0 + round( perChange / leftPerStep_1, 0 );
            finalRightGainCode = 0 + round( perChange / rightPerStep_1, 0 );
            leftFinalStep = leftPerStep_1;
            rightFinalStep = rightPerStep_1;
        
        
        strTestName     =  'leftAndRightAmpGain';
        leftCombinedAmpGainCodesTested      =  [];
        rightCombinedAmpGainCodesTested     =  [];
        measuredLeftAndRightAmpGain         =  [];
        
        h.trimBits.set.afeAmpGainLeft(finalLeftGainCode);
        h.trimBits.set.afeAmpGainRight(finalRightGainCode);
        
        currLeftCode    =  h.trimBits.read.afeAmpGainLeft();
        currRightCode   =  h.trimBits.read.afeAmpGainRight();
        
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
                
        for i in range (len(currArray)):        
            if(currentControlExist):
#                process = subprocess.run(["python","CurrentControl.py",str(currArray[i])], shell=True, stdout=subprocess.PIPE)
                process = subprocess.run(["CurrentControl.bat",str(currArray[i])], shell=True, stdout=subprocess.PIPE)
                
                #process.wait()
                print(process.returncode)
                print(process.stdout)            
            else:
                gui.wait_for_next_button("Please set current to "+ str(currArray[i]) + "A then press Next button");
            print("Current set to "+ str(currArray[i]))                
            gui.continue_process("");
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
            print(meanVal)
            outArray[i] = meanVal;    
        
        leftCombinedAmpGainCodesTested.append( currLeftCode );
        rightCombinedAmpGainCodesTested.append( currRightCode );
        measuredLeftAndRightAmpGain.append( outArray[-1] - outArray[0] );
        print(strTestName + ' @ Left & Right Code ' + str(currLeftCode)  + ' & '+ str(currRightCode) + ' = ' + str(outArray[-1] - outArray[0])  + 'V');
        #gui.send_message_to_gui(strTestName + ' @ Left & Right Code ' + str(currLeftCode)  + ' & '+ str(currRightCode) + ' = ' + str(outArray[-1] - outArray[0])  + 'V');
        leftAndRightGainInScale = (measuredLeftAndRightAmpGain[-1] <= biFactor*1.0016*ampGainDelta) and (measuredLeftAndRightAmpGain[-1] >= biFactor*0.9984*ampGainDelta); 
        nTime = 0
        while not leftAndRightGainInScale:
            
            leftDelta = round( 100*((ampGainDelta/measuredLeftAndRightAmpGain[-1]) - 1)/leftFinalStep, 0);
            rightDelta = round( 100*((ampGainDelta/measuredLeftAndRightAmpGain[-1]) - 1)/rightFinalStep, 0);
        
            h.trimBits.set.afeAmpGainLeft(currLeftCode + leftDelta);
            h.trimBits.set.afeAmpGainRight(currRightCode + rightDelta);
            
            currLeftCode    =  h.trimBits.read.afeAmpGainLeft();
            currRightCode   =  h.trimBits.read.afeAmpGainRight();
            print('leftDelta,rightDelta ',leftDelta,rightDelta)
            F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
            
            for i in range (len(currArray)):        
                if(currentControlExist):
                #    process = subprocess.run(["python","CurrentControl.py",str(currArray[i])], shell=True, stdout=subprocess.PIPE)
                    process = subprocess.run(["CurrentControl.bat",str(currArray[i])], shell=True, stdout=subprocess.PIPE)
                        
                    #process.wait()
                    print(process.returncode)
                    print(process.stdout)            
                else:
                    gui.wait_for_next_button("Please set current to "+ str(currArray[i]) + "A then press Next button");
                    print("Current set to "+ str(currArray[i]))                
                gui.continue_process("");
                [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);        
                print(meanVal)
                outArray[i] = meanVal;
            
            leftCombinedAmpGainCodesTested.append( currLeftCode );
            rightCombinedAmpGainCodesTested.append( currRightCode );
            measuredLeftAndRightAmpGain.append( outArray[-1] - outArray[0] );
            #Check if nextCode repeated        
            if (currLeftCode == currLeftCode + leftDelta) and (currRightCode == currRightCode + rightDelta):
                nTime += 1
                print(nTime)
                if(nTime>2):
                    break  
            print(strTestName + ' @ Left & Right Code ' + str(currLeftCode)+ ' & '  + str(currRightCode)  + ' = ' + str(outArray[-1] - outArray[0])  + 'V');
            #gui.send_message_to_gui(strTestName + ' @ Left & Right Code ' + str(currLeftCode)  + ' & '+ str(currRightCode)+ ' = ' + str(outArray[-1] - outArray[0])  + 'V');
            leftAndRightGainInScale = (measuredLeftAndRightAmpGain[-1] <= biFactor*1.02*ampGainDelta) and (measuredLeftAndRightAmpGain[-1] >= biFactor*0.98*ampGainDelta);
            print('leftAndRightGainInScale',leftAndRightGainInScale)
        h.testBits.set.general.zeroAllTestBits();
        gui.increase_progress(5);
        gui.send_message_to_gui('Step 2 is done! '); 
        
    
    
        #%% Step #3 : Trim Left Electrical Offset
        print('Step 3: Trimming Left Elect Offset...');
        gui.increase_progress(10)
        gui.send_message_to_gui('Step 3: Trimming Left Elect Offset...');
        
        strTestName = 'leftElectOffset';
        leftElectOffsetCodesTested      =  [];
        measuredLeftElectOffset         =  [];
        
        h.testBits.set.general.leftBridge('short');
        h.testBits.set.general.rightBridge('short');
        h.testBits.set.general.rightAmp('dis');
        h.trimBits.set.afeElectOffsetRight(0);
        
        # Dynamic Step size
        sCode = 0;
        mxCode=255;
        miCode=127;
        ssCode=128;
        
        h.trimBits.set.afeElectOffsetLeft(sCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        sVref=meanVal;
        h.trimBits.set.afeElectOffsetLeft(miCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        miVref=meanVal;
        
        h.trimBits.set.afeElectOffsetLeft(ssCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        ssVref=meanVal;
        h.trimBits.set.afeElectOffsetLeft(mxCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        mxVref=meanVal;
        
        if ((miVref > idealZeroAvRef) or (mxVref < idealZeroAvRef)):
            print('idealZeroAvRef is not on the range?');        
        
        # Dynamic normOffsetStep calculation    
        normOffsetStep = max(abs((sVref-miVref)/128), abs((ssVref-mxVref)/128));
        normOffsetStep = round(normOffsetStep + 0.000001, 6);
        
        h.trimBits.set.afeElectOffsetLeft(sCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
        
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        currCode = h.trimBits.read.afeElectOffsetLeft();
        
        leftElectOffsetCodesTested.append( currCode );
        measuredLeftElectOffset.append( meanVal );
        print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
        #gui.send_message_to_gui(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
        
        if meanVal > vddDut - 0.5:
            deltaCode = 5;
        else:
            deltaCode = int( round( (measuredLeftElectOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
        
        if deltaCode < 0:
            deltaCode = 127 + abs(deltaCode);
        
        nTime = 0
        while deltaCode != 0:
            
            if currCode >= 128 and currCode <= 255:
                deltaCode = -deltaCode;
            
            # Check if nextcode jump out of range
            if currCode > 255:
                deltaCode = -round(deltaCode/2);
            
            nextCode = currCode + deltaCode;
            
            if nextCode < 0:
                nextCode = 127 + abs(nextCode);                
            
            #Check if nextCode repeated        
            if (nextCode == currCode + 1) or (nextCode == currCode - 1) or (nextCode == currCode):
                nTime += 1
                print(nTime)
                if(nTime>2):
                    break       
            
            h.trimBits.set.afeElectOffsetLeft(nextCode);
            F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
                
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
            
            currCode = h.trimBits.read.afeElectOffsetLeft();
            leftElectOffsetCodesTested.append( currCode );
            measuredLeftElectOffset.append( meanVal );
            
            print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
            #gui.send_message_to_gui(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
            
            if meanVal > vddDut - 0.5:
                deltaCode = 5;
            else:
                deltaCode = int( round( (measuredLeftElectOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
        
        h.testBits.set.general.zeroAllTestBits();
        gui.send_message_to_gui('Step 3 is done! ');
        
        #%% Step #4 : Trim Right Electrical Offset    
        
        print('Step 4: Trimming Right Elect Offset...');
        gui.increase_progress(10)
        gui.send_message_to_gui('Step 4: Trimming Right Elect Offset...');
        
        strTestName     =  'rightElectOffset';
        rightElectOffsetCodesTested     =  [];
        measuredRightElectOffset        =  [];
        
        h.testBits.set.general.rightBridge('short');
        h.testBits.set.general.leftBridge('short');
        h.testBits.set.general.leftAmp('dis');    
        h.trimBits.set.afeElectOffsetLeft(0);
        
        # Dynamic Step size
        sCode = 0;
        mxCode=255;
        miCode=127;
        ssCode=128;
        
        h.trimBits.set.afeElectOffsetRight(sCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        sVref=meanVal;
        h.trimBits.set.afeElectOffsetRight(miCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        miVref=meanVal;
           
        h.trimBits.set.afeElectOffsetRight(ssCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        ssVref=meanVal;
        h.trimBits.set.afeElectOffsetRight(mxCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        mxVref=meanVal;
        
        if ((miVref > idealZeroAvRef) or (mxVref < idealZeroAvRef)):
            print('idealZeroAvRef is not on the range?')        
        
        # Dynamic normOffsetStep calculation
        normOffsetStep = max(abs((sVref-miVref)/128), abs((ssVref-mxVref)/128));
        normOffsetStep = round(normOffsetStep + 0.000001, 6);
        
        h.trimBits.set.afeElectOffsetRight(sCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
        
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        currCode = h.trimBits.read.afeElectOffsetRight();
        
        rightElectOffsetCodesTested.append( currCode );
        measuredRightElectOffset.append( meanVal );
        print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
        #gui.send_message_to_gui(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
        
        if meanVal > vddDut - 0.5:
            deltaCode = 5;
        else:
            deltaCode = int( round( (measuredRightElectOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
        
        if deltaCode < 0:
            deltaCode = 127 + abs(deltaCode);
        
        nTime=0;
        while deltaCode != 0:
        
            if currCode >= 128 and currCode <= 255:
                deltaCode = -deltaCode;
            
            # Check if nextcode jump out of range
            if currCode > 255:
                deltaCode = -round(deltaCode/2);
            
            nextCode = currCode + deltaCode;
            
            if nextCode < 0:
                nextCode = 127 + abs(nextCode);
            
            # Check if nextCode repeated        
            if (nextCode == currCode + 1) or (nextCode == currCode - 1) or (nextCode == currCode):
                nTime += 1
                print(nTime)
                if(nTime>2):
                    break
            
            h.trimBits.set.afeElectOffsetRight(nextCode);
            F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
            
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
            
            currCode = h.trimBits.read.afeElectOffsetRight();
            rightElectOffsetCodesTested.append( currCode );
            measuredRightElectOffset.append( meanVal );
            
            print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
            #gui.send_message_to_gui(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
            
            if meanVal > vddDut - 0.5:
                deltaCode = 5;
            else:
                deltaCode = int( round( (measuredRightElectOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
        
        h.testBits.set.general.zeroAllTestBits();
        gui.send_message_to_gui('Step 4 is done! ');    
          
        #%% Step #5 : Trim Left & Right Electrical Offset Together
        print('Step 5: Trimming Left & Right Elect Offset...');
        gui.increase_progress(10)
        gui.send_message_to_gui('Step 5: Trimming Left & Right Elect Offset...');
        
        strTestName     =  'leftAndRightElectOffset';
        leftCombinedElectOffsetCodesTested      =  [];
        rightCombinedElectOffsetCodesTested     =  [];
        measuredLeftAndRightElectOffset         =  [];
        
        h.testBits.set.general.rightBridge('short');
        h.testBits.set.general.leftBridge('short');
        
        currLeftCode    =  h.trimBits.read.afeElectOffsetLeft();
        currRightCode   =  h.trimBits.read.afeElectOffsetRight();
        
        h.trimBits.set.afeElectOffsetLeft(currLeftCode);
        h.trimBits.set.afeElectOffsetRight(currRightCode);
        
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
        
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        
        leftCombinedElectOffsetCodesTested.append( currLeftCode );
        rightCombinedElectOffsetCodesTested.append( currRightCode );
        measuredLeftAndRightElectOffset.append( meanVal );
        
        if meanVal > vddDut - 0.5:
            deltaCode = 5;
        else:
            deltaCode = int( round( (measuredLeftAndRightElectOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
        
        trimChannel = ['Left', 'Right'];
        trimIdx = 0;
        
        lnTime = 0;
        rnTime = 0;
        while deltaCode != 0:
            
            if trimChannel[trimIdx] == 'Left':
                
                if currLeftCode >= 128:
                    deltaCode = -deltaCode;
                
                nextCode = currLeftCode + deltaCode;
                
                if nextCode < 0:
                    nextCode = 127 + abs(nextCode);
                # Check if nextCode repeated        
                if (nextCode == currLeftCode + 1) or (nextCode == currLeftCode - 1) or (nextCode == currLeftCode):
                    lnTime += 1
                    print(lnTime)
                    if(lnTime>3):
                        break    
                h.trimBits.set.afeElectOffsetLeft(nextCode);  
                
            else:
                
                if currRightCode >= 128:
                    deltaCode = -deltaCode;
                
                nextCode = currRightCode + deltaCode;
                
                if nextCode < 0:
                    nextCode = 127 + abs(nextCode);
                
                # Check if nextCode repeated        
                if (nextCode == currRightCode + 1) or (nextCode == currRightCode - 1) or (nextCode == currRightCode):
                    rnTime += 1
                    print(rnTime)
                    if(rnTime>3):
                        break
                h.trimBits.set.afeElectOffsetRight(nextCode);
            
            F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
            
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
            
            currLeftCode    =  h.trimBits.read.afeElectOffsetLeft();
            currRightCode   =  h.trimBits.read.afeElectOffsetRight();
            
            leftCombinedElectOffsetCodesTested.append( currLeftCode );
            rightCombinedElectOffsetCodesTested.append( currRightCode );
            measuredLeftAndRightElectOffset.append( meanVal );
        
            if meanVal > vddDut - 0.5:
                deltaCode = 5;
            else:
                deltaCode = int( round( (measuredLeftAndRightElectOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
            
            trimIdx = abs(trimIdx - 1);
            
        h.testBits.set.general.zeroAllTestBits();
        gui.send_message_to_gui('afeElectOffsetLeft = '+ str(currLeftCode));    
        gui.send_message_to_gui('afeElectOffsetRight = '+ str(currRightCode));
        gui.send_message_to_gui('Step 5 is done! ');    
        
        
        #%% Step #6 : Trim Left Magnetic Offset
        
        # delAllVarClrConsole('cls');
        
        print('Step 6: Trimming Left Magnetic Offset...');
        gui.increase_progress(10)
        gui.send_message_to_gui('Step 6: Trimming Left Magnetic Offset...');
        
        
        strTestName     =  'leftMagOffset';
        leftMagOffsetCodesTested    =  [];
        measuredLeftMagOffset       =  [];
        
        h.testBits.set.general.rightBridge('short');
        h.trimBits.set.afeMagOffsetRight(0);
        
        # Dynamic Step size
        sCode = 0;
        mxCode=255;
        miCode=127;
        ssCode=128;
        
        h.trimBits.set.afeMagOffsetLeft(sCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        sVref=meanVal;
        h.trimBits.set.afeMagOffsetLeft(miCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        miVref=meanVal;
        
        h.trimBits.set.afeMagOffsetLeft(ssCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        ssVref=meanVal;
        h.trimBits.set.afeMagOffsetLeft(mxCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        mxVref=meanVal;
        
        if ((miVref > idealZeroAvRef) or (mxVref < idealZeroAvRef)):
            print('idealZeroAvRef is not on the range?')
        
        # Dynamic normOffsetStep calculation
        normOffsetStep = max(abs((sVref-miVref)/128), abs((ssVref-mxVref)/128));
        normOffsetStep = round(normOffsetStep + 0.000001, 6);
        
        h.trimBits.set.afeMagOffsetLeft(sCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
        
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        currCode = h.trimBits.read.afeMagOffsetLeft();
        
        leftMagOffsetCodesTested.append( currCode );
        measuredLeftMagOffset.append( meanVal );
        print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
        #gui.send_message_to_gui(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
        
        if meanVal > vddDut - 0.5:
            deltaCode = 5;
        else:
            deltaCode = int( round( (measuredLeftMagOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
        
        if deltaCode < 0:
            deltaCode = 127 + abs(deltaCode);
        
        nTime=0;
        while deltaCode != 0:
        
            if currCode >= 128 and currCode <= 255:
                deltaCode = -deltaCode;
            
            # Check if nextcode jump out of range
            if currCode > 255:
                deltaCode = -round(deltaCode/2);
                
            nextCode = currCode + deltaCode;
            
            if nextCode < 0:
                nextCode = 127 + abs(nextCode);
            
            # Check if nextCode repeated        
            if (nextCode == currCode + 1) or (nextCode == currCode - 1) or (nextCode == currCode):
                nTime += 1
                print(nTime)
                if(nTime>2):
                    break    
            
            h.trimBits.set.afeMagOffsetLeft(nextCode);
            F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
            
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
            
            currCode = h.trimBits.read.afeMagOffsetLeft();
            leftMagOffsetCodesTested.append( currCode );
            measuredLeftMagOffset.append( meanVal );
            
            print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
            #gui.send_message_to_gui(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
            
            if meanVal > vddDut - 0.5:
                deltaCode = 5;
            else:
                deltaCode = int( round( (measuredLeftMagOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
        
        h.testBits.set.general.zeroAllTestBits();
        gui.send_message_to_gui('Step 6 is done! ');
        
        
        #%% Step #7 : Trim Right Magnetic Offset
        print('Step 7: Trimming Right Magnetic Offset...');
        gui.increase_progress(10)
        gui.send_message_to_gui('Step 7: Trimming Right Magnetic Offset...');
        
        strTestName = 'rightMagOffset';
        rightMagOffsetCodesTested   =  [];
        measuredRightMagOffset      =  [];
        
        # Short left bridge and set mag offset of left to 0
        h.testBits.set.general.leftBridge('short');
        h.trimBits.set.afeMagOffsetLeft(0);
        
        sCode = 0;
        mxCode=255;
        miCode=127;
        ssCode=128;
        
        h.trimBits.set.afeMagOffsetRight(sCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        sVref=meanVal;
        
        h.trimBits.set.afeMagOffsetRight(miCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        miVref=meanVal;
        
        h.trimBits.set.afeMagOffsetRight(ssCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        ssVref=meanVal;
        
        h.trimBits.set.afeMagOffsetRight(mxCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);    
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        mxVref=meanVal;
        
        if ( (miVref > idealZeroAvRef) or (mxVref < idealZeroAvRef) ):
            print('idealZeroAvRef is not on the range?')        
        
        normOffsetStep = max(abs((sVref-miVref)/128), abs((ssVref-mxVref)/128));
        normOffsetStep = round(normOffsetStep + 0.000001, 6);    
        
        h.trimBits.set.afeMagOffsetRight(sCode);
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
        
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        currCode = h.trimBits.read.afeMagOffsetRight();
        
        rightMagOffsetCodesTested.append( currCode );
        measuredRightMagOffset.append( meanVal );
        print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
        #gui.send_message_to_gui(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
        
        if meanVal > vddDut - 0.5:
            deltaCode = 5;
        else:
            deltaCode = int( round( (measuredRightMagOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
        
        if deltaCode < 0:
            deltaCode = 127 + abs(deltaCode);
        
        nTime=0;
        while deltaCode != 0:    
            
            if currCode >= 128 and currCode <= 255:
                deltaCode = -deltaCode;
            
            # Check if nextcode jump out of range
            if currCode > 255:
                deltaCode = -round(deltaCode/2);
                
            nextCode = currCode + deltaCode;
            
            if nextCode < 0:
                nextCode = 127 + abs(nextCode);            
                    
            # Check if nextCode repeated        
            if (nextCode == currCode + 1) or (nextCode == currCode - 1) or (nextCode == currCode):
                nTime += 1
                print(nTime)
                if(nTime>2):
                    break                        
            
            h.trimBits.set.afeMagOffsetRight(nextCode);
            F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
            
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
            
            currCode = h.trimBits.read.afeMagOffsetRight();
            rightMagOffsetCodesTested.append( currCode );
            measuredRightMagOffset.append( meanVal );
            
            print(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
            #gui.send_message_to_gui(strTestName + ' @ Code ' + str(currCode)  + ' = ' + str(meanVal)  + 'V');
            
            if meanVal > vddDut - 0.5:
                deltaCode = 5;
            else:
                deltaCode = int( round( (measuredRightMagOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
        
        h.testBits.set.general.zeroAllTestBits();
        gui.send_message_to_gui('Step 7 is done! ');    
        
        #%% Step #8 : Trim Left & Right Magnetic Offset Together
            
        print('Step 8: Trimming Left & Right Magnetic Offset...');
        gui.increase_progress(10)
        gui.send_message_to_gui('Step 8: Trimming Left & Right Magnetic Offset...');
        
        strTestName     =  'leftAndRightMagOffset';
        leftCombinedMagOffsetCodesTested    =  [];
        rightCombinedMagOffsetCodesTested   =  [];
        measuredLeftAndRightMagOffset       =  [];
        
        currLeftCode    =  h.trimBits.read.afeMagOffsetLeft();
        currRightCode   =  h.trimBits.read.afeMagOffsetRight();
        
        h.trimBits.set.afeMagOffsetLeft(currLeftCode);
        h.trimBits.set.afeMagOffsetRight(currRightCode);
        
        F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
        
        [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
        
        leftCombinedMagOffsetCodesTested.append( currLeftCode );
        rightCombinedMagOffsetCodesTested.append( currRightCode );
        measuredLeftAndRightMagOffset.append( meanVal );
        
        if meanVal > vddDut - 0.5:
            deltaCode = 5;
        else:
            deltaCode = int( round( (measuredLeftAndRightMagOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
        
        trimChannel = ['Left', 'Right'];
        
        trimIdx = 0;
        
        lnTime = 0;
        rnTime = 0;
        while deltaCode != 0:
            
            if trimChannel[trimIdx] == 'Left':
                
                if currLeftCode >= 128:
                    deltaCode   = -deltaCode;
                
                nextCode = currLeftCode + deltaCode;
                
                if nextCode < 0:
                    nextCode = 127 + abs(nextCode);
                
                # Check if nextCode repeated        
                if (nextCode == currLeftCode + 1) or (nextCode == currLeftCode - 1) or (nextCode == currLeftCode):
                    lnTime += 1
                    print(lnTime)
                    if(lnTime>3):
                        break                        
            
                h.trimBits.set.afeMagOffsetLeft(nextCode);
                
            else:
        
                if currRightCode >= 128:
                    deltaCode   = -deltaCode;
                
                nextCode = currRightCode + deltaCode;
                
                if nextCode < 0:
                    nextCode = 127 + abs(nextCode);
                    
                # Check if nextCode repeated        
                if (nextCode == currRightCode + 1) or (nextCode == currRightCode - 1) or (nextCode == currRightCode):
                    rnTime += 1
                    print(rnTime)
                    if(rnTime>3):
                        break    
                
                h.trimBits.set.afeMagOffsetRight(nextCode);
                    
            
            F_repowerTbbm(pybrd_DUT_ps, h, vddDut);
            
            [minVal, meanVal, maxVal] = pybrd_dvm.averageDvm(nReads);
            
            currLeftCode    =  h.trimBits.read.afeMagOffsetLeft();
            currRightCode   =  h.trimBits.read.afeMagOffsetRight();
            
            leftCombinedMagOffsetCodesTested.append( currLeftCode );
            rightCombinedMagOffsetCodesTested.append( currRightCode );
            measuredLeftAndRightMagOffset.append( meanVal );
            
            if meanVal > vddDut - 0.5:
                deltaCode = 5;
            else:
                deltaCode = int( round( (measuredLeftAndRightMagOffset[-1] - idealZeroAvRef) / normOffsetStep ) );
            
            trimIdx = abs(trimIdx - 1);
            
        h.testBits.set.general.zeroAllTestBits();
        #gui.send_message_to_gui('afeElectOffsetLeft = '+ str(currLeftCode));    
        #gui.send_message_to_gui('afeElectOffsetRight = '+ str(currRightCode));
        gui.send_message_to_gui('Step 8 is done! ');
        
        
        #%% Final result      
        # #####  See All Trim Bits 
        # fullTrimArray = [h.trimBits.read.marking(),
        #                  h.trimBits.read.polarFlavor()[0],
        #                  h.trimBits.read.vddSupply()[0],
        #                  setFieldVal,
        #                  h.trimBits.read.vRefBandGapCorner(),
        #                  h.trimBits.read.vRefPtatUp(),
        #                  h.trimBits.read.vRegBandGapXDown(),
        #                  h.trimBits.read.vRefScaledBandGap(),
        #                  h.trimBits.read.afeAmpGainLeft(),
        #                  h.trimBits.read.afeAmpGainRight(),
        #                  h.trimBits.read.afeElectOffsetLeft(),
        #                  h.trimBits.read.afeElectOffsetRight(),
        #                  h.trimBits.read.afeMagOffsetLeft(),
        #                  h.trimBits.read.afeMagOffsetRight(),
        #                  h.trimBits.read.bridgeSwap()[0]];
        
        # gui.send_message_to_gui('Final results: '+
        #       'Marking Setting Code     =  ' + str(fullTrimArray[0]) + '\n' +
        #       'Polar Setting Code       =  ' + str(fullTrimArray[1]) + '\n' +
        #       'Vdd Setting Code         =  ' + str(fullTrimArray[2]) + '\n' +
        #       'Curr Scale Setting Code  =  ' + str(fullTrimArray[3]) + '\n' +
        #       'BgCorner Setting Code    =  ' + str(fullTrimArray[4]) + '\n' +
        #       'PtatUp Setting Code      =  ' + str(fullTrimArray[5]) + '\n' +
        #       'XDown Setting Code       =  ' + str(fullTrimArray[6]) + '\n' +
        #       'VrefScaled Setting Code      =  ' + str(fullTrimArray[7]) + '\n' +
        #       'Left Amp Gain Setting Code   =  ' + str(fullTrimArray[8]) + '\n' +
        #       'Right Amp Gain Setting Code      =  ' + str(fullTrimArray[9]) + '\n' +
        #       'Left Elect Offset Setting Code   =  ' + str(fullTrimArray[10]) + '\n' +
        #       'Right Elect Offset Setting Code  =  ' + str(fullTrimArray[11]) + '\n' +
        #       'Left Mag Offset Setting Code     =  ' + str(fullTrimArray[12]) + '\n' +
        #       'Right Mag Offset Setting Code    =  ' + str(fullTrimArray[13]) + '\n' +
        #       'Bridge Swap Code         =  ' + fullTrimArray[14]);    
        
        # print(flavorStr);    
        
        #%% Burn Fuses: For burn fuses we need to have Sdata voltage of 3.6V and first clk pulse 3.6+1.4=5V    
        if burnFuseBool:
            print('Burn Fuses according to trimming values...');
            gui.send_message_to_gui('Burn Fuses according to trimming values...');
            pybrd_DUT_ps.power_off();time.sleep(equipDelay);
            pybrd_DUT_ps.set_voltage(4); time.sleep(equipDelay);
            pybrd_DUT_ps.power_on(); time.sleep(equipDelay);        
                    
            # TBB Mode
            h.opCodeMode.tryBeforeBuy();time.sleep(equipDelay);        
            [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);        
            
            # Burn Fuse Mode
            h.opCodeMode.burnFuse();time.sleep(equipDelay);        
            [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);        
                    
            
            pybrd_DUT_ps.power_off();time.sleep(equipDelay);
            pybrd_DUT_ps.set_voltage(3.3); time.sleep(equipDelay);
            pybrd_DUT_ps.power_on(); time.sleep(equipDelay); 
                    
            # Read Fuse Mode
            h.opCodeMode.readFuse();time.sleep(equipDelay);
                    
            [setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(equipDelay);        
            
            if setBitArrayDec == readBitArrayDec:
                print('Set & Read Fuse Bits are the same!');
                gui.send_message_to_gui('Burn Fuses is done successfully! ');
            else:
                print('Set & Read Fuse Bits are NOT the same!!!!!');
                gui.send_message_to_gui('Burn Fuses is failed...');
            
            # print(setBitArrayDec);
            # print(readBitArrayDec);
            
        #%% Finished and Turn off all power supplies
        gui.increase_progress(10);
        gui.send_successful_end_to_gui("Trimming is done successfully!"); 
        time.sleep(1)
        pybrd_DUT_ps.power_off();
        pyboard_1.close(); 
    #except Exception as e: # work on python 3.x
# =============================================================================
#         pybrd_DUT_ps.power_off();
#         pyboard_1.close();     
#         print('Failed: '+ str(e)) 
#         gui.send_successful_end_to_gui("Trimming failed, see log file !");         
#             
# =============================================================================
