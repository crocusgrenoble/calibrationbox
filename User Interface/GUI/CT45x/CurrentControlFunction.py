# -*- coding: utf-8 -*-
"""
Created on Fri Aug 12 15:28:33 2022

@author: eaerabi
"""
import sys, pyvisa
import time
sys.path.append('C:\\Users\\eaerabi\\Git\\UniversalTestFramework')
sys.path.append('C:\\Users\\eaerabi\\Git\\UniversalTestFramework\\Devices\\DCSs')

from Devices.DCSs.dcs_keysight_e3633 import DCS_Keysight_E3633

#curr = dcs.setCurrent(1)
def SetCurrent(c):
    currentToSet = float(c)
    gpib_addr='GPIB0::5::INSTR'
    dcs = DCS_Keysight_E3633(gpib_addr)
    #dcs.output_OFF()
    dcs.setCurrent(currentToSet)
    print('current set successful')
    time.sleep(0.1)
    #dcs.output_ON()
    return float(c)
    ##################### Customer writes a code here to control their current supply #########
    ######### The Current value is in "currentToSet" variable 
    # -*- coding: utf-8 -*-
    ##Connect the serial port
    # -*- coding: utf-8 -*-
    
    rm = pyvisa.ResourceManager()    
    current10 = currentToSet    
    hh_const = 2.5
    
    #######################################################################
    #                            FUNCTIONS                                #
    #######################################################################
    
    # hpmeter = rm.open_resource('GPIB0::28::INSTR')
    # #hpmeter = rm.open_resource('GPIB0::28::INSTR')
    # hpmeter.write("*RST")                                                       
    # hpmeter.write("*CLS")
    # hpmeter.write("configure:voltage:dc")
    # hpmeter.write("system:beeper:state off")
    # hpmeter.write("calculate:function average")
    # hpmeter.write("trigger:source immediate")
    # hpmeter.write("trigger:delay minimum")
    # hpmeter.write("sample:count 10")
    #hpmeter.timeout = 250
        
    kepco = rm.open_resource('GPIB0::6::INSTR')
    kepco.write("*RST")
    kepco.write("*CLS")
    kepco.write("FUNC:MODE CURR;:CURR 0;VOLT MAX")
    kepco.write("OUTP OFF")      
    kepco.write("CURR 0")
    
    #-- Start the meters
    # hpmeter.write("calculate:state on")
    
    current = current10 / 100.0                    
    #-- Set the current
    kepco.write("CURR "+str(current))
    time.sleep(.5)            
    #-- Start the meters
    # hpmeter.write("calculate:state on")        
                
    # hpmeter.query("read?") 
    # out_v = hpmeter.query("calculate:average:average?") 
    # hpmeter.write("calculate:state off")
    print(str(current))   # +"\t"+out_v)
    
    # hpmeter.write("system:beeper")    
    
    return float(c)

# SetCurrent(400)
# time.sleep(1)
# SetCurrent(-400)
# time.sleep(1)
# SetCurrent(0)

if __name__ == "__main__":
    r = SetCurrent(sys.argv[1])        
    sys.exit(int(round(r)))