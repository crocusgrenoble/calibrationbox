# -*- coding: utf-8 -*-
"""
Created on Fri Jun  01 13:37:21 2021

@author: rmazrae
"""
import IPython
from IPython import get_ipython
import sys
import numpy as np

sys.path.append(".\\Lib\\")
sys.path.append(".\\Lib\\pyboard\\")
sys.path.append(".\\Lib\\Halo2_1fClassPy3\\")

#from PyQt5.QtWidgets import QApplication, QLabel
from PyQt5 import QtWidgets,uic,QtCore
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QLineEdit
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5 import QtGui
import os                                                        
import threading

import serial
from serial.tools import list_ports
import time
import ctypes            
#import multiprocessing        
class Communicate(QtCore.QObject):
    myGUI_signal = QtCore.pyqtSignal(dict)

''' End class '''
#from Step1_Final import Step1
from Gui_AllTrim_CT45x_v10 import trimming_process  

class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__() # Call the inherited classes __init__ method
        uic.loadUi('CTC4000_GUI_CT45x_v10.ui', self) # Load the .ui file
        # self.runButton.setText("Submit")
        self.runButton.clicked.connect(self.runButton_clicked) # Remember to pass the definition/method, not the return value!
        self.show() # Show the GUI
        self.gui_communication = Communicate() # Communication channel to send messages from BE to GUI
        self.wait_for_gui = threading.Event()  # Event too synch GUI and the backend
        self.trimming_thread_started = False
        # self.step2_resume = threading.Event()
        self.otherCOMs =[]
        # Start finding pyboards in a thread
        self.pyboards = []
        self.flavour={}
        self.stop_searching_pyboard = False
        self.search_for_pyboard_thread = threading.Thread(name = 'search_for_CTC4000', target = self.search_for_pyboards, args = ())
        self.search_for_pyboard_thread.start()
        # self.label_bg.setStyleSheet("background-image: url(:C:/Users/eaerabi/Git/CalibrationBox-BitBucket/GUI/images/logonew1.jpg);");
        
        # Set Crocus Lego
        pixmap = QPixmap('logoct45x.jpg')
        self.label_bg.setPixmap(pixmap)
        
        # Set Crocus Icon
        self.setWindowIcon(QtGui.QIcon('favicon-66x66.jpg'))
        myappid = 'mycompany.myproduct.subproduct.version' # arbitrary string
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

        # Fix main windows
        self.setFixedSize(780, 650)                                                                                                                                
        time.sleep(3)        
        # input maximum magnetic field
        # self.line = QLineEdit(self)
        # maxfield = self.line
        #Update Device Info on GUI
        # self.find_pyboard()
        if len(self.pyboards)>0 : 
            print("set current text "+self.pybListComboBox.currentText())
            self.pybListComboBox.setCurrentText(self.pyboards[0])            
            self.update_device_info()
            self.pybListComboBox.currentIndexChanged.connect(self.update_device_info)
            self.runButton.setEnabled(True)
        else:
            self.deviceInfoLogBox.setText("No CTC4000 found !!")            

        #
        self.trimming_thread = []       
    
    def search_for_pyboards(self):
        while(1):
            self.find_pyboard()
            if self.stop_searching_pyboard:
                return
               
            time.sleep(0.5)
 
    def update_device_info(self):
        # 
        com_port = str(self.pybListComboBox.currentText())
        print("com port"+com_port)
        try:
            serialport = serial.Serial(com_port, baudrate=115200, timeout=1)
            serialport.isOpen()
        except :
            print("Failed to connect to CTC4000 at "+ com_port)
            self.deviceInfoLogBox.setText("Failed to connect to CTC4000 at "+ com_port)            
        try:
            serialport.write("print(firmware)\r\n".encode())
        
            serialport.readline() 
            response = serialport.readline() 
            # print("firmware "+ response.decode())
            self.deviceInfoLogBox.setText("CTC4000 on port {} \r\nFirmware: {}"\
                                          .format(com_port, response.decode()))
            serialport.close()    

        except :
            print("Failed read firmware version at "+ com_port)
            self.deviceInfoLogBox.setText("Failed read firmware version at  "+ com_port)         


    def find_pyboard(self):

        ports= list_ports.comports()    
        for port, desc, hwid in sorted(ports):
            if(port in self.otherCOMs):
                continue
            #print(self.otherCOMs)
            #print("\r\n{}: {} [{}]".format(port, desc, hwid))
            if("Bluetooth" in desc):
                continue
            if port in self.pyboards:
                continue
            try:
                serialport = serial.Serial(port, baudrate=115200, timeout=0.5)
            except :
               # self.logBox.append("Failed to connect to "+ port)
                print("Failed to connect to "+ port)
                self.otherCOMs.append(port)
                continue
            serialport.isOpen()
            serialport.write("help()\r\n".encode())

            serialport.readline()
            response = serialport.readline()
            #print(response)
            if response == b'Welcome to MicroPython!\r\n':
                print("Found a CTC4000 on " + port)
                self.pyboards.append(port)
                self.pybListComboBox.addItems([port])
                if(len(self.pyboards) == 1):
                    self.pybListComboBox.setCurrentText(self.pyboards[0])
            serialport.close()

    def runButton_clicked(self):
        maxcurr_str = self.Textbox_MaxCurr.text()
        try:
            self.flavour["maxcurr"] = float(maxcurr_str)
            self.backendMsgBox.setText("")         
            
        except:
             self.backendMsgBox.setText("Enter a a correct number in \"Max Current\" text box and click on \"RUN\"")
             return
        # print(self.flavour["maxfield"])
        
        self.stop_searching_pyboard = True
        self.runButton.setEnabled(False)
        time.sleep(0.5)

        if self.CT450RadioButton.isChecked():
            self.flavour["CT450"] = True
            self.flavour["CT453"] = False
            self.flavour["CT43x"] = False
        elif self.CT453RadioButton.isChecked():
            self.flavour["CT453"] = True
            self.flavour["CT450"] = False
            self.flavour["CT43x"] = False
        elif self.CT43xRadioButton.isChecked():
            self.flavour["CT43x"] = True
            self.flavour["CT453"] = False
            self.flavour["CT450"] = False 
        
        if self.unipolarRadioButton.isChecked():
            self.flavour["unipolar"] = True
            self.flavour["bipolar"] = False
        elif self.bipolarRadioButton.isChecked():
            self.flavour["bipolar"] = True
            self.flavour["unipolar"] = False
        
        if self.supply5vRadioButton.isChecked():
            self.flavour["5v"] = True
            self.flavour["3v3"] = False
        elif self.supply3v3RadioButton.isChecked():
            self.flavour["3v3"] = True
            self.flavour["5v"] = False        
        
        if self.HalfScale.isChecked():
            self.flavour["HalfScale"] = True            
        else:            
            self.flavour["HalfScale"] = False
        
        if self.burntrueButton.isChecked():
            self.flavour["burn"] = True
            self.flavour["nburn"] = False
        elif self.burnfalseButton.isChecked():
            self.flavour["nburn"] = True
            self.flavour["burn"] = False

        com_port = str(self.pybListComboBox.currentText())

        # If trimming has not started yet,
        # this implies that "Start" button is pressed, start triming event ...
        if (self.trimming_thread_started is False):
            self.trimming_thread = threading.Thread(name='trimming_thread',
                                                    #target=trimming_backend_procedure,
                                                    target=trimming_process,
                                                    args=(self.theCallbackFunc,
                                                    self.gui_communication, 
                                                    self.flavour, com_port,
                                                    self.wait_for_gui))              
            self.trimming_thread.start() 
            self.trimming_thread_started=True
            self.groupBox_Supply.setEnabled(False)
            self.groupBox_SensorSelection.setEnabled(False)
            self.groupBox_Polarity.setEnabled(False)
            self.groupBox_Current.setEnabled(False)
            self.groupBox_BurnFuse.setEnabled(False)
        
        # If trimming has already started,
        # then the "Next" button has been pressed, 
        # then signal BE for this actin.
        else:
            self.wait_for_gui.set()    
        
    # Callback to process msg received from BE
    def theCallbackFunc(self, rcvd):
        if "msg" in rcvd:
            # print('the thread has sent this message to the GUI:')
            print(rcvd["msg"])
            print('---------')
            self.logBox.append("\r\n" + rcvd["msg"])

        if "progress" in rcvd:
            self.progressBar.setValue(rcvd["progress"])
            # print('the thread has sent this message to the GUI:')
            print(rcvd["progress"])

        if "error" in rcvd:
            self.backendMsgBox.setText(rcvd["error"])
            self.logBox.append("ERROR:\r\n" + rcvd["error"])
            self.runButton.setEnabled(False)
            self.trimming_thread.join()
            self.runButton.setText("Start")
            self.trimming_thread_started = False
            self.groupBox_Supply.setEnabled(False)
            self.groupBox_SensorSelection.setEnabled(False)
            self.groupBox_Polarity.setEnabled(False)
            self.groupBox_Current.setEnabled(False)
            self.groupBox_BurnFuse.setEnabled(False)        
            
        if "success" in rcvd:
            self.backendMsgBox.setText(rcvd["success"])
            self.logBox.append("Finished:\r\n" + rcvd["success"])
            self.runButton.setEnabled(True)
            self.trimming_thread.join()
            self.runButton.setText("Start")
            self.trimming_thread_started = False
            self.groupBox_SensorSelection.setEnabled(True)
            self.groupBox_Supply.setEnabled(True)            
            self.groupBox_Polarity.setEnabled(True)
            self.groupBox_Current.setEnabled(True)
            self.groupBox_BurnFuse.setEnabled(True)            

        # Upon receving "next" messages,
        # the BE is waiting for somthing from GUI(user), 
        # We show the msg and turn Start label to Next
        if "next" in rcvd:
            self.runButton.setEnabled(True)
            self.runButton.setText("Next")
            self.backendMsgBox.setText(rcvd["next"])            
            
        if "continue" in rcvd: 
            self.runButton.setEnabled(False)
            self.runButton.setText("Next")
            self.backendMsgBox.setText(rcvd["continue"])
            
if __name__ == "__main__":
               
    app=QtWidgets.QApplication(sys.argv) # Create an instance of QtWidgets.QApplication
    window=Ui() # Create an instance of our class
    ret = app.exec_() # Start the application
    sys.exit(ret)