# -*- coding: utf-8 -*-
"""
Created on Tue Oct 25 17:44:37 2022

@author: eaerabi
"""

import time
from F_binToDec import F_binToDec
from halo2_1F import halo2_1F
from pyboard import PyBoard
from pyboard_dvm import Pyboard_DVM
from pyboard_dut_ps import Pyboard_DUT_PS
from gui_communication import GUI_Communication
import numpy as np
import pickle
        

import sys
import logging
import os
from os.path import exists
import subprocess



from  global_constants import *

from pyboard import PyBoard
# from f_pyboard2halo import F_pyBoard2Halo
from pyboard2halo import Pyboard2Halo
    
from opCodeModeHalo import opCodeModeHalo
from F_binToDec import F_binToDec
from F_reverseLrStr import F_reverseLrStr
import sys, numpy as np

from F_decToBin import F_decToBin


pyboard_1 = PyBoard(com_port = 'COM3', pcb_version='4.8')
#pyboard_1.close()
halo2_parms = {'device':'CTC4000', 'pyboard_object':pyboard_1}
__pyb_halo2__ = Pyboard2Halo(halo2_parms['pyboard_object'] )

h = halo2_1F(halo2_parms);
pybrd_DUT_ps = Pyboard_DUT_PS(pyboard_1)
#__pyb_halo2__.read_bits_to_halo2()
#h.__nTotalBits__ = 1234

params = h.__pyBoardInputParameters__()
print('params:'+ repr(params))
pybrd_DUT_ps.power_off();time.sleep(0.1);
pybrd_DUT_ps.set_voltage(3.3); 
pybrd_DUT_ps.power_on(); time.sleep(0.1); 

# =============================================================================
# bits= [1, 1, 1, 1, 0, 0, 1, 0,\
# 1, 0,\
# 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, \
# 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0]
# =============================================================================
#out = __pyb_halo2__.F_pyBoard2Halo(params)
#h.opCodeMode.tryBeforeBuy()
#[setBitArrayDec, readBitArrayDec] = h.sendArbitraryBitsToPyboard(np.array(bits));time.sleep(0.5); 
#[setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(0.5); 

#pybrd_DUT_ps.power_off();time.sleep(0.1);
#pybrd_DUT_ps.set_voltage(3.3); 
#pybrd_DUT_ps.power_on(); time.sleep(0.5); 

h.opCodeMode.readFuse()
#out = __pyb_halo2__.F_pyBoard2Halo(params)
[setBitArrayDec, readBitArrayDec] = h.sendBitsToPyBoard();time.sleep(0.1); 
print([setBitArrayDec, readBitArrayDec])
pybrd_DUT_ps.power_off();time.sleep(0.1);
pybrd_DUT_ps.set_voltage(3.3);

##########         Save
# =============================================================================
# with open(f'halo.pickle', 'wb') as file:
#     pickle.dump(params[:-1], file)
# 
# 
# with open(f'halo.pickle', 'rb') as file2:
#     s1_new = pickle.load(file2)    
# =============================================================================
pyboard_1.close()