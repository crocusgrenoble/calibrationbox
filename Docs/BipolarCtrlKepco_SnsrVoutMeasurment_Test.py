# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 13:02:17 2020

@author: Rmazrae
"""
from delAllVarClrConsole import delAllVarClrConsole
delAllVarClrConsole('reset');
from delAllVarClrConsole import delAllVarClrConsole
delAllVarClrConsole('cls');

from time import sleep
from connect import connect
import serial
# from agilent34410a import agilent34410a # multimeter
# from keysightN5748A import keysightN5748A

# #LAB setup
# psDutAddr = 'GPIB0::7::INSTR';
# dvmAddr1 = 'GPIB0::23::INSTR'; # vOut measurment


# # Kepco power supply limits
# #sensorname = 'CT425bi5v12n2test'
# sensorname = 'CT5405p0B6mT'
# maxCurr = 4;
# maxVoltage = 12.5;

# # power supply for relays
# currLimit = 3;
# vddDut = 17.0;
# # DUT PS
# psDutPort = connect(psDutAddr);
# psDut = keysightN5748A(psDutPort);
# # vOut measurment
# dvmPort1 = connect(dvmAddr1);
# dvm1 = agilent34410a(dvmPort1);


#%%
"""
IMPORTANT: Please set the serial port attributed to STM32
    On Windows: COMx
    On Unix:    /dev/x
"""
##Connect the serial port
ser=serial.Serial ('COM3', 115200, timeout=1) #connect com14, baud rate is 115200
ser.flushInput()
ser.flushOutput()

try:
    ser.isOpen()
    print('port is opened!')
except IOError:
    ser.close()
    ser.open()
    print('port was closed and opened!')    

inputCurr = [];
vOut = [];
vRef = [];
finalCurrArray = [];

nReads = 20;
nLoops = 1;
currSetDelay = 1; # Set time that how long you need current to be on +- 3sec for trimming
equipDelay = 0.1; # sec
loopDelay = 3; # sec
psDelay = 2; #current power supply apply time

# # Power On DUT
# psDut.reset();
# psDut.output.off();
# psDut.source.set.voltage(vddDut);
# psDut.source.set.current(currLimit);


# #DVM initialization
# dvm1.clrError();
# dvm1.reset();
# dvm1.configure.voltageDc();
# dvm1.inputImpedance.tenGigaOhm();
# dvm1.rawWrite('VOLT:DC:NPLCycles 1');
# sleep(equipDelay);

###############################################################################
#%%
"""
@CmdContact Function that command Stm32:
    1: Read ADC value as integer in mV
    2: Write Forward Switching cmd: Sw1,2 set and Sw3,4 Reset, 6000
    3: Write Backward Switching cmd: Sw3,4 set and Sw1,2 Reset, 7000
"""
ff = 0;

def CmdContact(input0):
    # 0=x30, 9=0x39, A=x41, B=x42, F=x46    
    flg = 0;
    in0 = input0
    ret_array = []         
    if in0 == 0:
        val = b'\x30\x0A'        
    elif in0 == 1:
        val = b'\x31\x0A'
    elif in0 == 2:
        val = b'\x32\x0A'
    elif in0 == 3:
        val = b'\x33\x0A'
    elif in0 == 4:
        val = b'\x34\x0A'
    elif in0 == 5:
        val = b'\x35\x0A'
    elif in0 == 6:
        val = b'\x36\x0A'
    elif in0 == 7:
        val = b'\x37\x0A'
    elif in0 == 8:
        val = b'\x38\x0A'
    elif in0 == 9:
        val = b'\x39\x0A'
    else:
        val=b'\x45'
        print('wrong code')
        return -1
    
    ser.write(val);
    #print('sent:', val)
    ret = ser.readline().decode('utf-8').rstrip().split(" ")
    
    if (ret[0] != "00") and (ret[0] != "01") and (ret[0] != "02") and (ret[0] != "03") and (ret[0] != "04") and (ret[0] != "05") and (ret[0] != "06") and (ret[0] != "07") and (ret[0] != "08") and (ret[0] != "09"):
        return -1           
    elif (ret[0] == "00"):
        ret_array = ret[1]
        raw = int(ret_array, 16)            
        flg = 0;        
    elif (ret[0] == "01"):    
       ret_array = ret[1]
       raw = int(ret_array, 16)     
       flg = 1;
    elif (ret[0] == "02"):    
       ret_array = ret[1]
       raw = int(ret_array, 16)     
       flg = 2;    
    elif (ret[0] == "03"):
        ret_array = ret[1]
        raw = int(ret_array, 16)            
        flg = 3;        
    elif (ret[0] == "04"):    
       ret_array = ret[1]
       raw = int(ret_array, 16)     
       flg = 4;
    elif (ret[0] == "05"):    
       ret_array = ret[1]
       raw = int(ret_array, 16)     
       flg = 5;    
    elif (ret[0] == "06"):
        ret_array = ret[1]
        raw = int(ret_array, 16)            
        flg = 6; 
    elif (ret[0] == "07"):    
       ret_array = ret[1]
       raw = int(ret_array, 16)     
       flg = 7;
    elif (ret[0] == "08"):    
       ret_array = ret[1]
       raw = int(ret_array, 16)     
       flg = 8;
    elif (ret[0] == "09"):    
       ret_array = ret[1]
       raw = int(ret_array, 16)     
       flg = 9;
    sleep(0.001)  
    return raw, flg

def forward():        
    CmdContact(7); # SW3 off
    sleep(0.5);
    CmdContact(8); # SW4 off
    sleep(0.5);
    (raw, flag) = CmdContact(1); # SW1 on, flag = 1, 
    ff = flag;
    sleep(0.5);
    (raw, flag) = CmdContact(2); # SW2 on, flag = 2, 
    ff = ff + flag; # ff = 3 for forward
    
def backward():        
    CmdContact(5); # SW1 off
    sleep(0.5);
    CmdContact(6); # SW2 off
    sleep(0.5);
    (raw, flag) = CmdContact(3); # SW3 on, flag = 3, 
    ff = flag;
    sleep(0.5);
    (raw, flag) = CmdContact(4); # SW4 on, flag = 4, 
    ff = ff + flag; # ff = 7 for backward

def relaysOff():
    CmdContact(5); # SW1 off
    sleep(0.5);
    CmdContact(6); # SW2 off
    sleep(0.5);
    CmdContact(7); # SW3 off
    sleep(0.5);
    CmdContact(8); # SW4 off
    sleep(1);
    (raw, flag) = CmdContact(0);

###############################################################################
#%% Run test scripts for relays
#Turn on 15 v power supply    
# psDut.output.on();
# sleep(equipDelay);
# #Turn Kepco power supply
# sleep(equipDelay);
# relaysOff();

# sleep(0.5);
# backward();
# [minRead1, meanRead1, maxRead1] = dvm1.measure.voltage.dc.averageDvm(nReads);
# sleep(2.5);            
# mosCurr = psDut.measure.outputCurrent()/2;
# print('bk0', mosCurr,meanRead1);
 
# sleep(0.5);
# forward();
# [minRead1, meanRead1, maxRead1] = dvm1.measure.voltage.dc.averageDvm(nReads);
# sleep(2.5);            
# mosCurr = psDut.measure.outputCurrent()/2;
# print('Fw0', mosCurr,meanRead1);
    
CmdContact(0);

#%%  Run bipolar sweep test 
####################### Turn Off Current power supply #########################
# psDut.output.off();

# Turn off Kepco

###################Close the serial port###################################
ser.close();
if ser.isOpen():
    print ("Serial port is not closed")
else:
    print ("Serial port is closed")