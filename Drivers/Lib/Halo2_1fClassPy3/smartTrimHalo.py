# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

from F_analogDiscovery2 import F_analogDiscovery2
import sys, time, numpy as np

class smartTrimHalo():
  
    def __init__(self, testBits, trimBits, opCodeMode):
        
        self.__testBits__ = testBits;
        self.__trimBits__ = trimBits;
        self.__opCodeMode__ = opCodeMode;
        
        self.__vddMax__ = 5;
        self.__measDelay__ = 0.1;
        self.__minStartupCurrent__ = 2;
        self.__maxStartupCurrent__ = 30;
        
        #self.__coilResistance__ = 4; # ohms
        #self.__avgReads__ = 10;
        
        self.__nDummyBits__ = 1;
        self.__nKeyCodeBits__ = 8;
        self.__nOpCodeBits__ = 2;
        self.__nTestModeBits__ = 16;
        self.__nProgrammingBits__ = 80;
        
        self.__nTotalBits__ = self.__nDummyBits__ + self.__nKeyCodeBits__ + self.__nOpCodeBits__ + self.__nTestModeBits__ + self.__nProgrammingBits__;

        self.__keyCodeArray__ = [1, 0, 1, 1, 0, 0, 1, 0]; # LSB first of "M" (for Murata) in ASICII
        self.__opCodeArray__ = [1, 0]; # code 1, but LSB first!
        
        self.__nSamplesPerHalfPeriod__ = 100;
        self.__mainClkFreq__ = 100e3;
        self.__clkChannel__ = 1;
        self.__vddMax__ = 4.75;
        self.__sDataChannel__ = 2;
        self.__clkEdgeStr__ = 'R';
        self.__vRef___ = 1.65;
        
        self.__psDelay__ = 3; # 3 seconds
        self.__equipDelay__ = 0.25; 
        self.__avgReads__ = 5;
    
    def bufferedVrefScaled(self, psDutClass, dvmClass, *dvmRelayClass):
    
        # Check if DVM Relay is Used
        relayCheck = len(dvmRelayClass) > 0;
        if relayCheck:
            dvmRelayClass.input2();
    
        self.__testBits__.set.afeMux.afeMuxToFltPin('en');
        self.__testBits__.set.afeMux.bufferedZeroCurrVref();
    
        vRefVoltArray = [0.5,   # '5.0V' & 'Unipolar'
                         0.65,  # '3.3V' & 'Unipolar'
                         2.5,   # '5.0V' & 'Bipolar'
                         1.65]; # '3.3V' & 'Bipolar'
                         
        supplyArray = ['5.0V', '3.3V', '5.0V', '3.3V'];
        polarArray = ['Unipolar', 'Unipolar', 'Bipolar', 'Bipolar'];
        
        currSupplySetting = self.__trimBits__.read.vddSupply()[0];
        currPolarSetting = self.__trimBits__.read.polarFlavor()[0];
        
        supplyIdx = [i for i, x in enumerate(supplyArray) if x == currSupplySetting];
        supplyIdx.sort();
        
        polarIdx = [i for i, x in enumerate(polarArray) if x == currPolarSetting];
        polarIdx.sort();
        
        # Check if First or Second Element is the same
        firstElementCheck = supplyIdx[0] == polarIdx[0];
    
        if firstElementCheck:
            idxPick = 0;
        else:
            idxPick = 1;
        
        vRefIdx = polarIdx[idxPick];
        vRefVal = vRefVoltArray( vRefIdx );
    
        testCodes = [];
        measVrefArray = [];
    
        # Check if measuredVrefVal is less than vRefVal @ Code 31
        self.__trimBits__.set.vRefScaledBandGap(31);
        testCodes.append(31);
    
        time.sleep(self.__equipDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        measVrefArray.append(meanVal);
        time.sleep(self.__equipDelay__);
    
        maxCodeMeasBool = meanVal > vRefVal;
        
        while maxCodeMeasBool:
            
            meanVal = meanVal + 0.01;
            maxCodeMeasBool = meanVal > vRefVal;

        if measVrefArray[-1] > vRefVal:
            vRefVal = meanVal;

        self.__vRef___ = vRefVal;

        self.__trimBits__.set.vRefScaledBandGap(0);
        testCodes.append(0);
    
        time.sleep(self.__equipDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        measVrefArray.append(meanVal);
        time.sleep(self.__equipDelay__);
        
        nextCode = round( (measVrefArray[-1]  - vRefVal)/0.00174 );
        
        self.__trimBits__.set.vRefScaledBandGap(nextCode);
        testCodes.append(nextCode);
        
        time.sleep(self.__equipDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        measVrefArray.append(meanVal);
        time.sleep(self.__equipDelay__);
        
        correctionCode = round( (measVrefArray[-1]  - vRefVal)/0.00174 );
        
        while abs(correctionCode) > 0:
        
            nextCode = nextCode + correctionCode;
            
            self.__trimBits__.set.vRefScaledBandGap(nextCode);
            testCodes.append(nextCode);
        
            time.sleep(self.__equipDelay__);
            [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
            measVrefArray.append(meanVal);
            time.sleep(self.__equipDelay__);
        
            correctionCode = round( (measVrefArray[-1]  - vRefVal)/0.00174 );
    
        self.__testBits__.set.general.zeroAllTestBits();
    
        return( [testCodes, measVrefArray] );
    
    def xDown(self, psDutClass, dvmClass, *dvmRelayClass):
    
        # Check if DVM Relay is Used
        relayCheck = len(dvmRelayClass) > 0;
        if relayCheck:
            dvmRelayClass.input2();
            
        self.__testBits__.set.isbMux.isbMuxToFltPin('en');
        self.__testBits__.set.isbMux.bridgeRegVolt();
        
        idealBridgeRegVolt = 2.6;
        
        testCodes = [];
        xDownArray = [];
        
        self.__trimBits__.set.vRegBandGapXDown(0);
        testCodes.append(0);
    
        time.sleep(self.__equipDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        xDownArray.append(meanVal);
        time.sleep(self.__equipDelay__);
        
        nextCode = round( (xDownArray[-1]  - idealBridgeRegVolt)/0.022 );
        
        self.__trimBits__.set.vRegBandGapXDown(nextCode);
        testCodes.append(nextCode);
        
        time.sleep(self.__equipDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        xDownArray.append(meanVal);
        time.sleep(self.__equipDelay__);
        
        correctionCode = round( (xDownArray[-1] - idealBridgeRegVolt)/0.022 );
        
        while abs(correctionCode) > 0:
        
            nextCode = nextCode + correctionCode;
            
            self.__trimBits__.set.vRegBandGapXDown(nextCode);
            testCodes.append(nextCode);
        
            time.sleep(self.__equipDelay__);
            [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
            xDownArray.append(meanVal);
            time.sleep(self.__equipDelay__);
        
            correctionCode = round( (xDownArray[-1] - idealBridgeRegVolt)/0.022 );
    
        self.__testBits__.set.general.zeroAllTestBits();
        
        return( [testCodes, xDownArray] );
        
    
    def gain(self, psDutClass, dvmClass, psCoilClass, minFieldAsCurr, maxFieldAsCurr):
        
        self.opCodeMode.tryBeforeBuy();
        
        psCoilClass.source.set.current(0);
        psCoilClass.output.on();
        
        midFieldAsCurr = (minFieldAsCurr + maxFieldAsCurr)/2.0;
        gainCodeArray = np.linspace(-128, 127, 256);
        gainOutArray = -0.0006343633455431585*gainCodeArray + 0.49992029757212425;
        
        self.__testBits__.set.general.rightAmp('dis');
        self.__testBits__.set.general.rightBridge('short');
        
        leftCodesTestedArray = [];
        leftMinFieldOutArray = [];
        leftMidFieldOutArray = [];
        leftMaxFieldOutArray = [];
        
        sIdx = 0;
        currCode = 0;
        
        self.__trimBits__.set.afeAmpGainLeft(currCode);
        leftCodesTestedArray.append(currCode);
        
        # Read OUT at Min Field
        psCoilClass.source.set.current(minFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        leftMinFieldOutArray.append(meanVal);
        
        # Read OUT at Mid Field
        psCoilClass.source.set.current(midFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        leftMidFieldOutArray.append(meanVal);
        
        # Read OUT at Max Field
        psCoilClass.source.set.current(midFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        leftMaxFieldOutArray.append(meanVal);
        
        midGainVal = 0.5*( abs(leftMinFieldOutArray[sIdx] - leftMidFieldOutArray[sIdx]) + abs(leftMaxFieldOutArray[sIdx] - leftMidFieldOutArray[sIdx]) );
        
        deltaGain = [np.abs(val - midGainVal) for val in gainOutArray];
        minDelta = np.min(deltaGain);
        idxMin = deltaGain.index(minDelta);
        
        sIdx = sIdx + 1;
        currCode = idxMin - 128;
        
        self.__trimBits__.set.afeAmpGainLeft(currCode);
        leftCodesTestedArray.append(currCode);
        
        # Read OUT at Min Field
        psCoilClass.source.set.current(minFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        leftMinFieldOutArray.append(meanVal);
        
        # Read OUT at Mid Field
        psCoilClass.source.set.current(midFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        leftMidFieldOutArray.append(meanVal);
        
        # Read OUT at Max Field
        psCoilClass.source.set.current(midFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        leftMaxFieldOutArray.append(meanVal);
        
        midGainVal = 0.5*( abs(leftMinFieldOutArray[sIdx] - leftMidFieldOutArray[sIdx]) + abs(leftMaxFieldOutArray[sIdx] - leftMidFieldOutArray[sIdx]) );
        
        correctionCode = int( round((0.49992029757212425 - midGainVal) / 0.0006343633455431585) );
        
        correctGain = abs(correctionCode) >= 1;
        
        while correctGain:
        
            currCode = currCode + correctionCode;
            
            self.__trimBits__.set.afeAmpGainLeft(currCode);
            leftCodesTestedArray.append(currCode);
        
            # Read OUT at Min Field
            psCoilClass.source.set.current(minFieldAsCurr);
            time.sleep(self.__psDelay__);
            [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
            leftMinFieldOutArray.append(meanVal);
        
            # Read OUT at Mid Field
            psCoilClass.source.set.current(midFieldAsCurr);
            time.sleep(self.__psDelay__);
            [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
            leftMidFieldOutArray.append(meanVal);
        
            # Read OUT at Max Field
            psCoilClass.source.set.current(midFieldAsCurr);
            time.sleep(self.__psDelay__);
            [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
            leftMaxFieldOutArray.append(meanVal);
        
            midGainVal = 0.5*( abs(leftMinFieldOutArray[sIdx] - leftMidFieldOutArray[sIdx]) + abs(leftMaxFieldOutArray[sIdx] - leftMidFieldOutArray[sIdx]) );
        
            correctionCode = int( round((0.49992029757212425 - midGainVal) / 0.0006343633455431585) );
            
            correctGain = abs(correctionCode) >= 1;
            
        self.__testBits__.set.general.rightAmp('en');
        self.__testBits__.set.general.rightBridge('unshort');

        self.__testBits__.set.general.leftAmp('dis');
        self.__testBits__.set.general.leftBridge('short');

        rightCodesTestedArray = [];
        rightMinFieldOutArray = [];
        rightMidFieldOutArray = [];
        rightMaxFieldOutArray = [];
        
        sIdx = 0;
        currCode = 0;
        
        self.__trimBits__.set.afeAmpGainLeft(currCode);
        rightCodesTestedArray.append(currCode);
        
        # Read OUT at Min Field
        psCoilClass.source.set.current(minFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        rightMinFieldOutArray.append(meanVal);
        
        # Read OUT at Mid Field
        psCoilClass.source.set.current(midFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        rightMidFieldOutArray.append(meanVal);
        
        # Read OUT at Max Field
        psCoilClass.source.set.current(midFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        rightMaxFieldOutArray.append(meanVal);
        
        midGainVal = 0.5*( abs(rightMinFieldOutArray[sIdx] - rightMidFieldOutArray[sIdx]) + abs(rightMaxFieldOutArray[sIdx] - rightMidFieldOutArray[sIdx]) );
        
        deltaGain = [np.abs(val - midGainVal) for val in gainOutArray];
        minDelta = np.min(deltaGain);
        idxMin = deltaGain.index(minDelta);
        
        sIdx = sIdx + 1;
        currCode = idxMin - 128;
        
        self.__trimBits__.set.afeAmpGainLeft(currCode);
        rightCodesTestedArray.append(currCode);
        
        # Read OUT at Min Field
        psCoilClass.source.set.current(minFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        rightMinFieldOutArray.append(meanVal);
        
        # Read OUT at Mid Field
        psCoilClass.source.set.current(midFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        rightMidFieldOutArray.append(meanVal);
        
        # Read OUT at Max Field
        psCoilClass.source.set.current(midFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        rightMaxFieldOutArray.append(meanVal);
        
        midGainVal = 0.5*( abs(leftMinFieldOutArray[sIdx] - leftMidFieldOutArray[sIdx]) + abs(leftMaxFieldOutArray[sIdx] - leftMidFieldOutArray[sIdx]) );
        
        correctionCode = int( round((0.49992029757212425 - midGainVal) / 0.0006343633455431585) );
        
        correctGain = abs(correctionCode) >= 1;
        
        while correctGain:
        
            currCode = currCode + correctionCode;
            
            self.__trimBits__.set.afeAmpGainLeft(currCode);
            rightCodesTestedArray.append(currCode);
        
            # Read OUT at Min Field
            psCoilClass.source.set.current(minFieldAsCurr);
            time.sleep(self.__psDelay__);
            [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
            rightMinFieldOutArray.append(meanVal);
        
            # Read OUT at Mid Field
            psCoilClass.source.set.current(midFieldAsCurr);
            time.sleep(self.__psDelay__);
            [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
            rightMidFieldOutArray.append(meanVal);
        
            # Read OUT at Max Field
            psCoilClass.source.set.current(midFieldAsCurr);
            time.sleep(self.__psDelay__);
            [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
            rightMaxFieldOutArray.append(meanVal);
        
            midGainVal = 0.5*( abs(rightMinFieldOutArray[sIdx] - rightMidFieldOutArray[sIdx]) + abs(rightMaxFieldOutArray[sIdx] - rightMidFieldOutArray[sIdx]) );
        
            correctionCode = int( round((0.49992029757212425 - midGainVal) / 0.0006343633455431585) );
            
            correctGain = abs(correctionCode) >= 1;

        self.__testBits__.set.general.leftAmp('en');
        self.__testBits__.set.general.leftBridge('unshort');

        leftRightMinFieldOutArray = [];
        leftRightMidFieldOutArray = [];
        leftRightMaxFieldOutArray = [];
        
        # Read OUT at Min Field
        psCoilClass.source.set.current(minFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        leftRightMinFieldOutArray.append(meanVal);
        
        # Read OUT at Mid Field
        psCoilClass.source.set.current(midFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        leftRightMidFieldOutArray.append(meanVal);
        
        # Read OUT at Max Field
        psCoilClass.source.set.current(midFieldAsCurr);
        time.sleep(self.__psDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        leftRightMaxFieldOutArray.append(meanVal);

        finalMidGainVal = 0.5*( abs(leftRightMinFieldOutArray[0] - leftRightMidFieldOutArray[0]) + abs(leftRightMaxFieldOutArray[0] - leftRightMidFieldOutArray[0]) );

        finalGainVal = leftRightMaxFieldOutArray[0] - leftRightMinFieldOutArray[0];

        psCoilClass.source.set.current(0);
        psCoilClass.output.off();

        return([leftCodesTestedArray, leftMinFieldOutArray, leftMidFieldOutArray, leftMaxFieldOutArray,
                rightCodesTestedArray, rightMinFieldOutArray, rightMidFieldOutArray, rightMaxFieldOutArray,
                leftRightMinFieldOutArray, leftRightMidFieldOutArray, leftRightMaxFieldOutArray,
                finalMidGainVal, finalGainVal]);
        
        
    def electricalOffset(self, psDutClass, dvmClass):
        
        testedCodes = [];
        leftElecOffset = [];
        
        self.opCodeMode.tryBeforeBuy();
    
        # Trim Electrical Offset Left
        self.__testBits__.set.general.leftBridge('short');
        self.__testBits__.set.general.rightBridge('short');
        self.__testBits__.set.general.rightAmp('dis');
        
        currCode = 0;
        
        self.__trimBits__.set.afeElectOffsetLeft(currCode);
        testedCodes.append(currCode);
        
        self.connectSclkNode(); time.sleep(self.__measDelay__);
        self.connectSdataNode(); time.sleep(self.__measDelay__);

        [setBitArrayDec, readBitArrayDec] = self.sendBitsToAd2();

        time.sleep(self.__measDelay__);

        # Disconnect AD2 SCLK & SDATA
        self.disconnectSclkNode(); time.sleep(self.__measDelay__);
        self.disconnectSdataNode(); time.sleep(self.__measDelay__);
    
        time.sleep(self.__measDelay__);
        [minVal, meanVal, maxVal] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
        leftElecOffset.append(meanVal);
        time.sleep(self.__measDelay__);    
        
         = round( 2*(leftElecOffset[-1] - self.__vRef___) / 0.0011 );
        
        
        # Check if DUT is sourcing current > startupCurrent and can output osc test mode!
        self.connectionAndOscTmCheck(psDutClass, dvmClass);
            
        time.sleep(self.__measDelay__);
        
        dvmClass.reset();
        dvmClass.configure.frequency(); # dummy measurement
        dvmClass.measure.frequency.single();
        
        maxOscVal = 2**self.__trimBits__.__bits__.trim.T11_Osc_frequency_trim.bitSize - 1;
    
        oscHz = np.zeros(2*maxOscVal + 2);
        
        codesTested = [];
    
        for i in range(2*maxOscVal + 2): # python's crappy end indexing
      
            # Repower DUT
            self.repower(psDutClass);
            
            # Enable Osc Clk 100k Test Mode
            self.__testBits__.set.oscClk100k('en');
            
            if i <= maxOscVal:
                self.__trimBits__.set.oscSign('-');
                self.__trimBits__.set.osc(i);
                codesTested.append(-1*i);
            else:
                self.__trimBits__.set.oscSign('+');
                self.__trimBits__.set.osc(int(i - maxOscVal - 1));
                codesTested.append(int(i - maxOscVal - 1));
            
            dataBits = self.__trimBits__.__bits__.finalBitStream('analogDiscovery2');
            inputParameters = [self.__nDummyBits__, self.__nProgrammingBits__, self.__nTestModeBits__,
                               self.__burnMode__, self.__mainClkFreq__, self.__clkPolarity__, 
                               self.__edgePol__, dataBits];
            F_analogDiscovery2(inputParameters);
            
            time.sleep(self.__measDelay__);
            [a, b, c] = dvmClass.measure.frequency.averageDvm(self.__avgReads__);
            oscHz[i] = b;   
            time.sleep(self.__measDelay__);
      
        deltaOsc = [np.abs(val - self.__mainClkFreq__) for val in oscHz];
        minDelta = np.min(deltaOsc);
        idxMin = deltaOsc.index(minDelta);
    
        if idxMin <= maxOscVal:
            bestOscPol = '-';
            idxOffset = 0;
        else:
            bestOscPol = '+';
            idxOffset = maxOscVal + 1;
    
        self.__trimBits__.set.oscSign(bestOscPol);
        self.__trimBits__.set.osc(int(idxMin - idxOffset)); 
        dataBits = self.__trimBits__.__bits__.finalBitStream('analogDiscovery2');
        inputParameters = [self.__nDummyBits__, self.__nProgrammingBits__, self.__nTestModeBits__, 
                           self.__burnMode__, self.__mainClkFreq__, self.__clkPolarity__,
                           self.__edgePol__, dataBits];
        F_analogDiscovery2(inputParameters);  
    
        bestOscTrim = [self.__trimBits__.read.oscSign(), self.__trimBits__.read.osc(), oscHz, idxOffset, codesTested];
        return(bestOscTrim);
  
    def ampGain(self, psDutClass, dvmClass, psCoilClass, currArray, icccRange):
        
        rangeBol = icccRange == '-50~+50A' or icccRange == '0~+50A';
        
        if not rangeBol:
            sys.exit("ICCC Range must be a str of '-50~+50A' or '0~+50A'");
            
        self.__trimBits__.set.icccRange(icccRange);
        
        # Determine Ideal Sensitivity (40mV/A or 80mV/A)
        if icccRange == '-50~+50A':
            idealSens = 40;
        else:
            idealSens = 80;
        
        nInputs = len(currArray);
        
        signBool = all(item >= 0 for item in currArray) or all(item <= 0 for item in currArray);
        
        if not signBool:
            sys.exit('All current values need to be the same sign!');
        
        # Check if DUT is sourcing current > startupCurrent and can output osc test mode!
        self.connectionAndOscTmCheck(psDutClass, dvmClass);
            
        time.sleep(self.__measDelay__);
        dvmClass.reset();
        dvmClass.configure.voltageDc();
        dvmClass.measure.voltage.dc.single(); # dummy measurement
        time.sleep(self.__measDelay__);
        
        maxAmpGainVal = 2**self.__trimBits__.__bits__.trim.T13_Output_amplifier_gain.bitSize - 1;
    
        ampGainOutput = np.zeros([nInputs, 2*maxAmpGainVal + 2]);
        measCurr = np.zeros([nInputs, 2*maxAmpGainVal + 2]);
        
        # Turn Coil Off
        psCoilClass.output.off();
        
        psCoilClass.source.set.voltage(10);
        
        codesTested = [];
        
        for j in range(2*maxAmpGainVal + 2): # python's crappy end indexing
          
            # Turn Coil Off
            psCoilClass.output.off();
                
            time.sleep(self.__measDelay__);
                
            # Repower DUT
            self.repower(psDutClass);
                
            if j <= maxAmpGainVal:
                self.__trimBits__.set.ampGainSign('-');
                self.__trimBits__.set.ampGain(j);
                codesTested.append(-1*j);
            else:
                self.__trimBits__.set.ampGainSign('+');
                self.__trimBits__.set.ampGain(int(j - maxAmpGainVal - 1));
                codesTested.append(int(j - maxAmpGainVal - 1));
                
            #print('----------------');
            #print(self.__trimBits__.read.ampGainSign());
            #print(self.__trimBits__.read.ampGain());
            
            dataBits = self.__trimBits__.__bits__.finalBitStream('analogDiscovery2');
            inputParameters = [self.__nDummyBits__, self.__nProgrammingBits__, self.__nTestModeBits__, 
                               self.__burnMode__, self.__mainClkFreq__, self.__clkPolarity__,
                               self.__edgePol__, dataBits];
            F_analogDiscovery2(inputParameters);
            
            # Turn Coil On
            time.sleep(self.__measDelay__);
            psCoilClass.output.on();
                
            for i in range(nInputs):
            
                psCoilClass.source.set.current(currArray[i]);
                time.sleep(2);
                [a, b, c] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
                ampGainOutput[i, j] = b;  
                time.sleep(self.__measDelay__);
                measCurr[i, j] = psCoilClass.measure.outputCurrent();
                time.sleep(self.__measDelay__);
        
        # Turn Coil Off
        psCoilClass.output.off();
        
        outputSens = np.zeros(2*maxAmpGainVal + 2);
        
        for k in range(2*maxAmpGainVal + 2): # python's crappy end indexing
            
            tempDeltaOutput = np.diff(ampGainOutput[:, k]);
            tempCurr = np.diff(measCurr[:, k]);
            tempOutputSens = np.divide(tempDeltaOutput, tempCurr);
            tempOutputSensMv = [1000*x for x in tempOutputSens];
            outputSens[k] = np.mean(tempOutputSensMv);
            
        deltaSens = [np.abs(np.abs(y) - idealSens) for y in outputSens];
        minDelta = np.min(deltaSens);
        idxMin = deltaSens.index(minDelta);
    
        if idxMin <= maxAmpGainVal:
            bestAmpGainPol = '-';
            idxOffset = 0;
        else:
            bestAmpGainPol = '+';
            idxOffset = maxAmpGainVal + 1;
    
        self.__trimBits__.set.ampGainSign(bestAmpGainPol);
        self.__trimBits__.set.ampGain(int(idxMin - idxOffset));  
        dataBits = self.__trimBits__.__bits__.finalBitStream('analogDiscovery2');
        inputParameters = [self.__nDummyBits__, self.__nProgrammingBits__, self.__nTestModeBits__, 
                           self.__burnMode__, self.__mainClkFreq__, self.__clkPolarity__,
                           self.__edgePol__, dataBits];
        F_analogDiscovery2(inputParameters);  
    
        bestAmpGainTrim = [self.__trimBits__.read.ampGainSign(), self.__trimBits__.read.ampGain(),  
                           measCurr, ampGainOutput, outputSens, idxOffset, codesTested];
        return(bestAmpGainTrim);
          
    def flOffsetCurrent(self, psDutClass, dvmClass):
            
        time.sleep(self.__measDelay__);
        dvmClass.reset();
        dvmClass.configure.voltageDc();
        dvmClass.measure.voltage.dc.single(); # dummy measurement
        time.sleep(self.__measDelay__);
        
        maxOffsetCurrVal = 2**self.__trimBits__.__bits__.trim.T14_AFE_current_offset.bitSize - 1;
    
        offsetCurrOutput = np.zeros(2*maxOffsetCurrVal + 2);
            
        codesTested = [];
        
        for i in range(2*maxOffsetCurrVal + 2): # python's crappy end indexing
                
            # Repower DUT
            self.repower(psDutClass);
                
            if i <= maxOffsetCurrVal:
                self.__trimBits__.set.flOffsetCurrentSign('-');
                self.__trimBits__.set.flOffsetCurrent(i);
                codesTested.append(-1*i);
            else:
                self.__trimBits__.set.flOffsetCurrentSign('+');
                self.__trimBits__.set.flOffsetCurrent(int(i - maxOffsetCurrVal - 1));
                codesTested.append(int(i - maxOffsetCurrVal - 1));
                
            #print('----------------');
            #print(self.__trimBits__.read.flOffsetCurrentSign());
            #print(self.__trimBits__.read.flOffsetCurrent());
            
            dataBits = self.__trimBits__.__bits__.finalBitStream('analogDiscovery2');
            inputParameters = [self.__nDummyBits__, self.__nProgrammingBits__, self.__nTestModeBits__, 
                               self.__burnMode__, self.__mainClkFreq__, self.__clkPolarity__,
                               self.__edgePol__, dataBits];
            F_analogDiscovery2(inputParameters);
            
            time.sleep(self.__measDelay__);
            [a, b, c] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
            offsetCurrOutput[i] = b;  
            time.sleep(self.__measDelay__);
            
        absOffset = [np.abs(y) for y in offsetCurrOutput];
        minOffset = np.min(absOffset);
        idxMin = absOffset.index(minOffset);
    
        if idxMin <= maxOffsetCurrVal:
            bestOffsetCurrPol = '-';
            idxOffset = 0;
        else:
            bestOffsetCurrPol = '+';
            idxOffset = maxOffsetCurrVal + 1;
    
        self.__trimBits__.set.flOffsetCurrentSign(bestOffsetCurrPol);
        self.__trimBits__.set.flOffsetCurrent(int(idxMin - idxOffset));  
        dataBits = self.__trimBits__.__bits__.finalBitStream('analogDiscovery2');
        inputParameters = [self.__nDummyBits__, self.__nProgrammingBits__, self.__nTestModeBits__, 
                           self.__burnMode__, self.__mainClkFreq__, self.__clkPolarity__,
                           self.__edgePol__, dataBits];
        F_analogDiscovery2(inputParameters);  
    
        bestOffsetCurrTrim = [self.__trimBits__.read.flOffsetCurrentSign(), self.__trimBits__.read.flOffsetCurrent(), 
                              offsetCurrOutput, idxOffset, codesTested];
        return(bestOffsetCurrTrim);
    
    def ampOffset(self, psDutClass, dvmClass, icccRange):
        
        rangeBol = icccRange == '-50~+50A' or icccRange == '0~+50A';
        
        if not rangeBol:
            sys.exit("ICCC Range must be a str of '-50~+50A' or '0~+50A'");
            
        self.__trimBits__.set.icccRange(icccRange);
       
        # Check type of PS being used and measure voltage
        idnStr = psDutClass.rawRead('*IDN?');
        dualPsCheck = 'E36' in idnStr;
        if dualPsCheck:
            measVolt = psDutClass.ch1.measure.outputVoltage();
        else:
            measVolt = psDutClass.measure.outputVoltage();
        time.sleep(self.__measDelay__);
        
        # Determine % of VDD
        if icccRange == '-50~+50A':
            idealOffset = 0.5*measVolt;
        else:
            idealOffset = 0.1*measVolt;
        
        # Check if DUT is sourcing current > startupCurrent and can output osc test mode!
        self.connectionAndOscTmCheck(psDutClass, dvmClass);
            
        time.sleep(self.__measDelay__);
        
        dvmClass.reset();
        dvmClass.configure.voltageDc();
        dvmClass.measure.voltage.dc.single(); # dummy measurement 
        time.sleep(self.__measDelay__);
        
        maxAmpOffsetVal = 2**self.__trimBits__.__bits__.trim.T17_Output_amplifier_offset.bitSize - 1;
    
        ampOffsetOutput = np.zeros(2*maxAmpOffsetVal + 2);
        
        codesTested = [];
        
        for i in range(2*maxAmpOffsetVal + 2): # python's crappy end indexing
                
            # Repower DUT
            self.repower(psDutClass);
                
            if i <= maxAmpOffsetVal:
                self.__trimBits__.set.ampOffsetSign('-');
                self.__trimBits__.set.ampOffset(i);
                codesTested.append(-1*i);
            else:
                self.__trimBits__.set.ampOffsetSign('+');
                self.__trimBits__.set.ampOffset(int(i - maxAmpOffsetVal - 1));
                codesTested.append(int(i - maxAmpOffsetVal - 1));
                
            #print('----------------');
            #print(self.__trimBits__.read.ampOffsetSign());
            #print(self.__trimBits__.read.ampOffset());
            
            dataBits = self.__trimBits__.__bits__.finalBitStream('analogDiscovery2');
            inputParameters = [self.__nDummyBits__, self.__nProgrammingBits__, self.__nTestModeBits__, 
                               self.__burnMode__, self.__mainClkFreq__, self.__clkPolarity__,
                               self.__edgePol__, dataBits];
            F_analogDiscovery2(inputParameters);
            
            time.sleep(self.__measDelay__);
            [a, b, c] = dvmClass.measure.voltage.dc.averageDvm(self.__avgReads__);
            ampOffsetOutput[i] = b;  
            time.sleep(self.__measDelay__);
            
        absOffset = [np.abs(y - idealOffset) for y in ampOffsetOutput];
        minOffset = np.min(absOffset);
        idxMin = absOffset.index(minOffset);
    
        if idxMin <= maxAmpOffsetVal:
            bestAmpOffsetPol = '-';
            idxOffset = 0;
        else:
            bestAmpOffsetPol = '+';
            idxOffset = maxAmpOffsetVal + 1;
    
        self.__trimBits__.set.ampOffsetSign(bestAmpOffsetPol);
        self.__trimBits__.set.ampOffset(int(idxMin - idxOffset));  
        dataBits = self.__trimBits__.__bits__.finalBitStream('analogDiscovery2');
        inputParameters = [self.__nDummyBits__, self.__nProgrammingBits__, self.__nTestModeBits__, 
                           self.__burnMode__, self.__mainClkFreq__, self.__clkPolarity__,
                           self.__edgePol__, dataBits];
        F_analogDiscovery2(inputParameters);  
    
        bestAmpOffsetTrim = [self.__trimBits__.read.ampOffsetSign(), self.__trimBits__.read.ampOffset(), 
                              ampOffsetOutput, idxOffset, codesTested];
        return(bestAmpOffsetTrim);
    
    def setVddMax(self, val):
        
        vddValCheck = val > 0 and val <= 5;
        if not vddValCheck:
            sys.exit('Max Vdd needs to be between 0 (not inclusive) and 5V (inclusive)!');
            
        self.__vddMax__ = val;
    
    def connectionCheck(self, psDutClass):
        
        # Repower DUT
        self.repower(psDutClass, self.__vddMax__ - 1);
        
        idnStr = psDutClass.rawRead('*IDN?');
        dualPsCheck = 'E36' in idnStr;
        
        if dualPsCheck:
            mCurr = 1000*psDutClass.ch1.measure.outputCurrent();
        else:
            mCurr = 1000*psDutClass.measure.outputCurrent();
        
        fCheck = mCurr < self.__minStartupCurrent__ or mCurr >= self.__maxStartupCurrent__;
        
        if fCheck:
            sys.exit('Startup current was < ' + str(self.__minStartupCurrent__) + 'mA or > ' + str(self.__maxStartupCurrent__) + 'mA!');
    
    def repower(self, psDutClass, *voltVal):
        
        errorCheck = False;
        nInput = len(voltVal);
        if nInput == 0:
            psDutVolt = self.__setVolt__;
        elif nInput == 1:
            if type(voltVal) == float or type(voltVal) == int:
                psDutVolt = voltVal;
            else:
                errorCheck = True;
        else:
            errorCheck = True;
            
        if errorCheck:
            sys.exit("The # of inputs isn't 1 and/or the set voltage input is not an integer or float!");
        
        # Check type of PS being used
        idnStr = psDutClass.rawRead('*IDN?');
        dualPsCheck = 'E36' in idnStr;
        psDutClass.output.off();
        time.sleep(self.__measDelay__);
        if dualPsCheck:
            psDutClass.ch1.source.set.voltage(psDutVolt);
        else:
            psDutClass.source.set.voltage(psDutVolt);
        psDutClass.output.on();
        time.sleep(self.__measDelay__);
    
    def __outputSetBits__(self):
        
        dataBits = self.__bits__.finalBitStream();
        
        if self.opCodeMode.__opCodeArray__ == [1, 1] or self.opCodeMode.__opCodeArray__ == [0, 1]:
            
            nFuseBits = 80;
                    
            dataBits[-nFuseBits : ] = np.linspace(0, 0, nFuseBits);
            
        setAd2Bits = np.concatenate( (self.__keyCodeArray__, self.opCodeMode.__opCodeArray__, dataBits) );
        
        return(setAd2Bits);
    
    def __ad2InputParameters__(self):
        
        dataBits = self.__outputSetBits__();
        
        #modeArray = self.trimBits.read.crocusMurataHaloMode();
        #if modeArray[0] == 'murata':
        #    self.__nProgrammingBits__ = 77;
        #else:
        #    self.__nProgrammingBits__ = 80;    
        #self.__nTotalBits__ = self.__nDummyBits__ + self.__nKeyCodeBits__ + self.__nOpCodeBits__ + self.__nTestModeBits__ + self.__nProgrammingBits__;
        
        inputParameters = [self.__nTotalBits__, self.__nSamplesPerHalfPeriod__, self.__mainClkFreq__, self.__clkChannel__,
                           self.__vddMax__, self.__sDataChannel__, self.__clkEdgeStr__, dataBits, self.opCodeMode.__opCodeArray__];
                
        return(inputParameters);
    
    def sendBitsToAd2(self):
        
        inputParameters = self.__ad2InputParameters__();
        readBitArrayBin = F_analogDiscovery2Halo(inputParameters);
        nFuseElements = len(readBitArrayBin);
        readBitArrayDec = [];
        
        if nFuseElements > 0:
            
            # Check the Number of Fuses to be Read back!
            murataCrocusMode = inputParameters[7][77] == 1;
            #murataCrocusMode = False;
            
            if murataCrocusMode:
                fuseStartIdx = [0, 3, 8, 14, 20, 28, 36, 44, 52, 60, 68, 69, 70, 71, 76];
                fuseEndIdx = [3, 8, 14, 20, 28, 36, 44, 52, 60, 68, 69, 70, 71, 76, 77]; # don't forget python indexing
                
            else:
                fuseStartIdx = [0, 3, 8, 14, 20, 28, 36, 44, 52, 60, 68, 69, 70, 71, 76, 77, 78]; 
                fuseEndIdx = [3, 8, 14, 20, 28, 36, 44, 52, 60, 68, 69, 70, 71, 76, 77, 78, 80]; # don't forget python indexing
            
            readBitArrayBinStr = ''.join(str(x) for x in readBitArrayBin);            
            nFuseIdx = len(fuseStartIdx);            
            negFuseArrayIdx = [4, 5, 6, 7, 8, 9];
            
            for i in range(nFuseIdx):
                
                binStrBitsLsbFirst = readBitArrayBinStr[fuseStartIdx[i] : fuseEndIdx[i]];
                binStrBitsMsbFirst = F_reverseLrStr(binStrBitsLsbFirst);
                decVal = F_binToDec(binStrBitsMsbFirst);
                
                if i in negFuseArrayIdx:
                    
                    negCheck = binStrBitsMsbFirst[0] == '1';                       
                    if negCheck:
                        decVal = -1*F_binToDec( F_twosComplement( binStrBitsMsbFirst ) );
                
                readBitArrayDec.append(decVal);
        
        setBitArrayDec = self.trimBits.read.allTrimBitsLsbToMsb();
        
        return([setBitArrayDec, readBitArrayDec]);
        
  
    def __del__(self):
        
        del(self);