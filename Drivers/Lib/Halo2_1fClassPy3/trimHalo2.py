# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

class trimHalo2():
  
    def __init__(self, haloBits):

        from setBitsTrimHalo2 import setBitsTrimHalo2
        from readBitsTrimHalo import readBitsTrimHalo
        from explainBitsTrimHalo import explainBitsTrimHalo
        from maxValBitsTrimHalo import maxValBitsTrimHalo
        
        self.__bits__ = haloBits;
        
        self.set = setBitsTrimHalo2(self.__bits__);
        self.read = readBitsTrimHalo(self.set);
        self.explain = explainBitsTrimHalo(self.__bits__);
        self.maxVal = maxValBitsTrimHalo(self.__bits__);