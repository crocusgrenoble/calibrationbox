# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""


class isbMuxReadBitsTestHalo():
  
    def __init__(self, setBits):
        
        self.__bits__ = setBits;
    
    def disMux(self):
        
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 0:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def porTest(self):
        
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 1:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def overTempTestAtRoomTemp(self):
        
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 2:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def overTempFaultMonitorAtHighTemp(self):
    
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 3:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
    
    def bridgeRegVolt(self):
    
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 4:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
    
    def afeLowVoltReg(self):
        
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 5:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
    
    def rawBgRef(self):
        
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 6:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
    
    def clk(self):
    
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 7:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def neg110PerOverCurrRef(self):
        
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 8:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);       

    def neg90PerOverCurrRef(self):

        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 9:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def zeroCurrVref(self):
        
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 10:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);       
        
    def pos90PerOverCurrRef(self):
            
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 11:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def pos110PerOverCurrRef(self):
    
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 12:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);       
        
    def bridgeRegPtat(self):
    
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 13:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def overTempRefCheck(self):
        
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 14:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);      
        
    def zeroCurrTempCoFromBridgeReg(self):
        
        decVal = self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val;
        
        if decVal == 15:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def isbMuxToFltPin(self):
        
        decVal = self.__bits__.__bits__.test.TEST08_testMuxInfraEn.base10Val;
        
        if decVal == 1:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
    
    
