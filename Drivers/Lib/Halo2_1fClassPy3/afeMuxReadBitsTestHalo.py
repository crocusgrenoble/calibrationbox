# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""


class afeMuxReadBitsTestHalo():
  
    def __init__(self, setBits):
        
        self.__bits__ = setBits;
    
    def disMux(self):
    
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 0:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def negInputRightBridge(self):
        
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 1:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def posInputRightBridge(self):
    
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 2:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def negInputLeftBridge(self):
        
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 3:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def posInputLeftBridge(self):
    
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 4:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def negOutLeftAmp(self):
        
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 5:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def posOutLeftAmp(self):
        
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 6:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);

    def negOutRightAmp(self):
        
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 7:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def posOutRightAmp(self):

        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 8:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def normalized(self):
        
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 9:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def bufferedZeroCurrVref(self):
    
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 10:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);

    def overNegCurrDigtialFlagComparator(self):
    
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 11:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);

    def overPosCurrDigtialFlagComparator(self):
    
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 12:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);

    def overPosNegCurrLogicOR(self):
    
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 13:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
            
    def overNegCurrVref(self):
    
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 14:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
    
    def overPosCurrVref(self):
        
        decVal = self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val;
        
        if decVal == 15:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
        
    def afeMuxToFltPin(self):
    
        decVal = self.__bits__.__bits__.test.TEST09_testMuxAfeEn.base10Val;
        
        if decVal == 1:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
    
    
