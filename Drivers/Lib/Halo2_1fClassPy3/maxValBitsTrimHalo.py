# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

class maxValBitsTrimHalo():
  
    def __init__(self, haloBits):
        
        self.__bits__ = haloBits;
    
    def vRefBgCornerTrim(self):
    
        return(2**self.__bits__.trim.TRIM00_vRefBgCornerTrim.bitSize - 1);
    
    def vRefScaledBgTrim(self):
        
        return(2**self.__bits__.trim.TRIM01_vRefScaledBgTrim.bitSize - 1);

    def vRegPtatUp(self):
        
        return(2**self.__bits__.trim.TRIM02_vRegPtatUp.bitSize - 1);

    def vRegBgXDown(self):

        return(2**self.__bits__.trim.TRIM03_vRegBgXDown.bitSize - 1);

    def vosMagLeft(self):

        return(2**(self.__bits__.trim.TRIM04_vosMagLeft.bitSize) - 1);

    def vosMagRight(self):
        
        return(2**(self.__bits__.trim.TRIM05_vosMagRight.bitSize) - 1);

    def vosElectLeft(self):
        
        return(2**(self.__bits__.trim.TRIM06_vosElectLeft.bitSize) - 1);

    def vosElectRight(self):
        
        print('sdsd');
        return(2**(self.__bits__.trim.TRIM07_vosElectRight.bitSize) - 1);

    def gainInstrAmpLeft(self):
        
        return(2**(self.__bits__.trim.TRIM08_gainInstrAmpLeft.bitSize) - 1);

    def gainInstrAmpRight(self):
        
        return(2**(self.__bits__.trim.TRIM09_gainInstrAmpRight.bitSize) - 1);
    
    def gainUnipolar(self):
        
        return(2**self.__bits__.trim.TRIM10_gainUnipolar.bitSize - 1);

    def gainHiSupply(self):
        
        return(2**self.__bits__.trim.TRIM11_gainHiSupply.bitSize - 1);
    
    def gainFor12mT(self):
        
        return(2**self.__bits__.trim.TRIM12_gainFor12mT.bitSize - 1);
    
    def markingBits(self):
        
        return(2**self.__bits__.trim.TRIM13_markingBits.bitSize - 1);
        
    def blockTryBeforeBuy(self):

        return(2**self.__bits__.trim.TRIM14_blockTryBeforeBuy.bitSize - 1);
    
    def crousMurataModes(self):
        
        return(2**self.__bits__.trim.TRIM15_crousMurataModes.bitSize - 1);
    
    def bridgeSwaps(self):
        
        return(2**self.__bits__.trim.TRIM16_bridgeSwaps.bitSize - 1);