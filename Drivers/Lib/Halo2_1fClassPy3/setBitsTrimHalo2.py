# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

import sys, numpy as np

from F_decToBin import F_decToBin
from F_binToDec import F_binToDec
from F_twosComplement import F_twosComplement

class setBitsTrimHalo2():
  
    def __init__(self, haloBits):
        
        self.__bits__ = haloBits;
    
    def zeroAllTrimBits(self):
        
        self.__bits__.trim.TRIM00_vRefBgCornerTrim.base10Val = 0; self.__bits__.trim.TRIM01_vRefScaledBgTrim.base10Val = 0;
        self.__bits__.trim.TRIM02_vRegPtatUp.base10Val = 0; self.__bits__.trim.TRIM03_vRegBgXDown.base10Val = 0;
        self.__bits__.trim.TRIM04_vosMagLeft.base10Val = 0; self.__bits__.trim.TRIM05_vosMagRight.base10Val = 0;
        self.__bits__.trim.TRIM06_vosElectLeft.base10Val = 0; self.__bits__.trim.TRIM07_vosElectRight.base10Val = 0;
        self.__bits__.trim.TRIM08_gainInstrAmpLeft.base10Val = 0; self.__bits__.trim.TRIM09_gainInstrAmpRight.base10Val = 0;
        self.__bits__.trim.TRIM10_gainUnipolar.base10Val = 0; self.__bits__.trim.TRIM11_gainHiSupply.base10Val = 0;        
        self.__bits__.trim.TRIM12_gainFor12mT.base10Val = 0; self.__bits__.trim.TRIM13_markingBits.base10Val = 0;        
        self.__bits__.trim.TRIM14_blockTryBeforeBuy.base10Val = 0; self.__bits__.trim.TRIM15_crousMurataModes.base10Val = 0;        
        self.__bits__.trim.TRIM16_bridgeSwaps.base10Val = 0;
    
    def oneAllTrimBits(self):
        
        self.__bits__.trim.TRIM00_vRefBgCornerTrim.base10Val = 7; self.__bits__.trim.TRIM01_vRefScaledBgTrim.base10Val = 63;
        self.__bits__.trim.TRIM02_vRegPtatUp.base10Val = 63; self.__bits__.trim.TRIM03_vRegBgXDown.base10Val = 63;
        self.__bits__.trim.TRIM04_vosMagLeft.base10Val = 255; self.__bits__.trim.TRIM05_vosMagRight.base10Val = 255;
        self.__bits__.trim.TRIM06_vosElectLeft.base10Val = 255; self.__bits__.trim.TRIM07_vosElectRight.base10Val = 255;
        self.__bits__.trim.TRIM08_gainInstrAmpLeft.base10Val = 255; self.__bits__.trim.TRIM09_gainInstrAmpRight.base10Val = 255;
        self.__bits__.trim.TRIM10_gainUnipolar.base10Val = 1; self.__bits__.trim.TRIM11_gainHiSupply.base10Val = 1;        
        self.__bits__.trim.TRIM12_gainFor12mT.base10Val = 1; self.__bits__.trim.TRIM13_markingBits.base10Val = 15;        
        self.__bits__.trim.TRIM14_blockTryBeforeBuy.base10Val = 1; self.__bits__.trim.TRIM15_crousMurataModes.base10Val = 1;        
        self.__bits__.trim.TRIM16_bridgeSwaps.base10Val = 3;
    
    def vRefBandGapCorner(self, val):
        
        maxVal = 2**self.__bits__.trim.TRIM00_vRefBgCornerTrim.bitSize - 1;
        valCheck = val >= 0 and val <= maxVal;
        if not valCheck:
            return('The val needs to be between 0 and ' + str(maxVal))
            # sys.exit('The val needs to be between 0 and ' + str(maxVal));
    
        self.__bits__.trim.TRIM00_vRefBgCornerTrim.base10Val = int(val);
        return False
    
    def vRefScaledBandGap(self, val):
        
        maxVal = 2**self.__bits__.trim.TRIM01_vRefScaledBgTrim.bitSize - 1;
        valCheck = val >= 0 and val <= maxVal;
        if not valCheck:
            # return('The val needs to be between 0 and ' + str(maxVal))
            sys.exit('The val needs to be between 0 and ' + str(maxVal));
    
        self.__bits__.trim.TRIM01_vRefScaledBgTrim.base10Val = int(val);
        return False
    
    def vRefPtatUp(self, val):
        
        maxVal = 2**self.__bits__.trim.TRIM02_vRegPtatUp.bitSize - 1;
        valCheck = val >= 0 and val <= maxVal;
        if not valCheck:
            return('The val needs to be between 0 and ' + str(maxVal))
            # sys.exit('The val needs to be between 0 and ' + str(maxVal));
    
        self.__bits__.trim.TRIM02_vRegPtatUp.base10Val = int(val);
        return False
    
    def vRegBandGapXDown(self, val):
        
        maxVal = 2**self.__bits__.trim.TRIM03_vRegBgXDown.bitSize - 1;
        valCheck = val >= 0 and val <= maxVal;
        if not valCheck:
            return('The val needs to be between 0 and ' + str(maxVal))

            #return('The val needs to be between 0 and ' + str(maxVal));
    
        self.__bits__.trim.TRIM03_vRegBgXDown.base10Val = int(val);    
        return False
    
    def afeMagOffsetLeft(self, val):
        
        maxVal = 2**self.__bits__.trim.TRIM04_vosMagLeft.bitSize - 1;
        valCheck = val >= 0 and val <= maxVal;
        if not valCheck:
            return('The val needs to be between 0 and ' + str(maxVal))
            # return('The val needs to be between 0 and ' + str(maxVal))
    
        self.__bits__.trim.TRIM04_vosMagLeft.base10Val = int(val);
        return False        
    
    def afeMagOffsetRight(self, val):
        
        maxVal = 2**self.__bits__.trim.TRIM05_vosMagRight.bitSize - 1;
        valCheck = val >= 0 and val <= maxVal;
        if not valCheck:
            return('The val needs to be between 0 and ' + str(maxVal))
    
        self.__bits__.trim.TRIM05_vosMagRight.base10Val = int(val);
        return False   
    
    def afeElectOffsetLeft(self, val):
        
        maxVal = 2**self.__bits__.trim.TRIM06_vosElectLeft.bitSize - 1;
        valCheck = val >= 0 and val <= maxVal;
        if not valCheck:
            return('The val needs to be between 0 and ' + str(maxVal))
    
        self.__bits__.trim.TRIM06_vosElectLeft.base10Val = int(val);  
        return False
    
    def afeElectOffsetRight(self, val):
        
        maxVal = 2**self.__bits__.trim.TRIM07_vosElectRight.bitSize - 1;
        valCheck = val >= 0 and val <= maxVal;
        if not valCheck:
            return('The val needs to be between 0 and ' + str(maxVal))
    
        self.__bits__.trim.TRIM07_vosElectRight.base10Val = int(val); 
        return False
    
    def afeAmpGainLeft(self, val):
        
        maxVal = 2**self.__bits__.trim.TRIM08_gainInstrAmpLeft.bitSize - 1;
        valCheck = val >= 0 and val <= maxVal;
        if not valCheck:
            return('The val needs to be between 0 and ' + str(maxVal))
    
        self.__bits__.trim.TRIM08_gainInstrAmpLeft.base10Val = int(val); 
        return False
    
    def afeAmpGainRight(self, val):
        
        maxVal = 2**self.__bits__.trim.TRIM09_gainInstrAmpRight.bitSize - 1;
        valCheck = val >= 0 and val <= maxVal;
        if not valCheck:
            return('The val needs to be between 0 and ' + str(maxVal))
    
        self.__bits__.trim.TRIM09_gainInstrAmpRight.base10Val = int(val);     
        return False
    
    def polarFlavor(self, polarStr):
        
        polarStrArray = ['unipolar', 'bipolar'];
        polarStrCheck = polarStr in polarStrArray;
        if not polarStrCheck:
           return('The input needs to be a str "unipolar" or "bipolar"!');
        
        if polarStr == 'unipolar':
            self.__bits__.trim.TRIM10_gainUnipolar.base10Val = 1;
        else:
            self.__bits__.trim.TRIM10_gainUnipolar.base10Val = 0;
        return False
    
    def vddSupply(self, vddStr):
        
        vddStrArray = ['3.3V', '5.0V'];
        vddStrCheck = vddStr in vddStrArray;
        if not vddStrCheck:
           return('The input needs to be a str "3.3V" or "5.0V"!');
        
        if vddStr == '5.0V':
            self.__bits__.trim.TRIM11_gainHiSupply.base10Val = 1;
        else:
            self.__bits__.trim.TRIM11_gainHiSupply.base10Val = 0;
        return False
     
    def currentFieldScale(self, currFieldStr):
        
        currFieldStrArray = ['8mT(20A)', '12mT(30A)'];
        currFieldStrCheck = currFieldStr in currFieldStrArray;
        if not currFieldStrCheck:
           return('The input needs to be a str "8mT(20A)" or "12mT(30A)"!');
        
        if currFieldStr == '12mT(30A)':
            self.__bits__.trim.TRIM12_gainFor12mT.base10Val = 1;
        else:
            self.__bits__.trim.TRIM12_gainFor12mT.base10Val = 0;
        return False
    
    def marking(self, val):
        
        maxVal = 2**self.__bits__.trim.TRIM13_markingBits.bitSize - 1;
        valCheck = val >= 0 and val <= maxVal;
        if not valCheck:
            return('The val needs to be between 0 and ' + str(maxVal))
    
        self.__bits__.trim.TRIM13_markingBits.base10Val = int(val);  
        return False
    
    def tryBeforeBuyFeature(self, enDisStr):
        
        strCheck = enDisStr == 'en' or enDisStr == 'dis';
        if not strCheck:
           return('The input to this function needs to be a str of "en" or "dis"!');
        
        if enDisStr == 'en':
            self.__bits__.trim.TRIM14_blockTryBeforeBuy.base10Val = 0;
        else:
            self.__bits__.trim.TRIM14_blockTryBeforeBuy.base10Val = 1;
        return False
    
    def crocusMurataHaloMode(self, modeStr):
        
        modeStrArray = ['crocus', 'murata'];
        modeStrCheck = modeStr in modeStrArray;
        if not modeStrCheck:
           return('The input needs to be a str "crocus" or "murata"!');

        if modeStr == 'murata':
            self.__bits__.trim.TRIM15_crousMurataModes.base10Val = 1;
        else:
            self.__bits__.trim.TRIM15_crousMurataModes.base10Val = 0;
        return False
    
    def bridgeSwap(self, swapStr):
        
        swapStrArray = ['noSwap', 'leftSwap', 'rightSwap', 'leftAndRightSwap'];
        swapStrCheck = swapStr in swapStrArray;
        if not swapStrCheck:
           return('The input to this function needs to be a str of "noSwap", "leftSwap", "rightSwap", or "leftAndRightSwap"!');
        
        decArray = [0, 1, 2, 3];
        idx = swapStrArray.index(swapStr);
        
        self.__bits__.trim.TRIM16_bridgeSwaps.base10Val = decArray[idx];
        return False        