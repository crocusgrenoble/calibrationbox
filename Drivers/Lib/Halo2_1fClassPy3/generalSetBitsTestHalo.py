# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

import sys

class generalSetBitsTestHalo():
  
    def __init__(self, haloBits):
        
        self.__bits__ = haloBits;    
    
    def zeroAllTestBits(self):
        
        self.__bits__.test.TEST00_leftAmpDis.base10Val = 0; self.__bits__.test.TEST01_leftBridgeShort.base10Val = 0;
        self.__bits__.test.TEST02_rightAmpDis.base10Val = 0; self.__bits__.test.TEST03_rightBridgeShort.base10Val = 0;
        self.__bits__.test.TEST04_overTempTermalTest.base10Val = 0; self.__bits__.test.TEST05_disAfe.base10Val = 0;
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 0; self.__bits__.test.TEST07_testMuxAfe.base10Val = 0;
        self.__bits__.test.TEST08_testMuxInfraEn.base10Val = 0; self.__bits__.test.TEST09_testMuxAfeEn.base10Val = 0;       
    
    def leftAmp(self, enDisStr):

        strCheck = enDisStr == 'en' or enDisStr == 'dis';
        if not strCheck:
            sys.exit('The input to this function needs to be a str of "en" or "dis"!');
        
        if enDisStr == 'en':
            self.__bits__.test.TEST00_leftAmpDis.base10Val = 0;
        else:
            self.__bits__.test.TEST00_leftAmpDis.base10Val = 1;
    
    def leftBridge(self, shortUnshortStr):
        
        strCheck = shortUnshortStr == 'short' or shortUnshortStr == 'unshort';
        if not strCheck:
            sys.exit('The input to this function needs to be a str of "short" or "unshort"!');
        
        if shortUnshortStr == 'unshort':
            self.__bits__.test.TEST01_leftBridgeShort.base10Val = 0;
        else:
            self.__bits__.test.TEST01_leftBridgeShort.base10Val = 1;
        
    def rightAmp(self, enDisStr):
        
        strCheck = enDisStr == 'en' or enDisStr == 'dis';
        if not strCheck:
            sys.exit('The input to this function needs to be a str of "en" or "dis"!');
        
        if enDisStr == 'en':
            self.__bits__.test.TEST02_rightAmpDis.base10Val = 0;
        else:
            self.__bits__.test.TEST02_rightAmpDis.base10Val = 1;
     
    def rightBridge(self, shortUnshortStr):
        
        strCheck = shortUnshortStr == 'short' or shortUnshortStr == 'unshort';
        if not strCheck:
            sys.exit('The input to this function needs to be a str of "short" or "unshort"!');
        
        if shortUnshortStr == 'unshort':
            self.__bits__.test.TEST03_rightBridgeShort.base10Val = 0;
        else:
            self.__bits__.test.TEST03_rightBridgeShort.base10Val = 1;
        
    def overTempThermalTest(self, enDisStr):
        
        strCheck = enDisStr == 'en' or enDisStr == 'dis';
        if not strCheck:
            sys.exit('The input to this function needs to be a str of "en" or "dis"!');
        
        if enDisStr == 'en':
            self.__bits__.test.TEST04_overTempTermalTest.base10Val = 1;
        else:
            self.__bits__.test.TEST04_overTempTermalTest.base10Val = 0;
    
    def afe(self, enDisStr):
        
        strCheck = enDisStr == 'en' or enDisStr == 'dis';
        if not strCheck:
            sys.exit('The input to this function needs to be a str of "en" or "dis"!');
        
        if enDisStr == 'en':
            self.__bits__.test.TEST05_disAfe.base10Val = 0;
        else:
            self.__bits__.test.TEST05_disAfe.base10Val = 1;


        