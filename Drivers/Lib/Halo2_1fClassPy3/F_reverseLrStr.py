# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 14:02:11 2018

@author: alongen
"""

import sys

def F_reverseLrStr(origStr):
    
    if type(origStr) != str:
        sys.exit('Input needs to be a string type!');
    
    newStr = origStr[ :: - 1];
    
    return newStr