# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

class generalReadBitsTestHalo():
  
    def __init__(self, setBits):
        
        self.__bits__ = setBits;
        
    def allTestBitsLsbToMsb(self):
    
        readOut = [self.__bits__.__bits__.test.TEST00_leftAmpDis.base10Val, self.__bits__.__bits__.test.TEST01_leftBridgeShort.base10Val,
                   self.__bits__.__bits__.test.TEST02_rightAmpDis.base10Val, self.__bits__.__bits__.test.TEST03_rightBridgeShort.base10Val,
                   self.__bits__.__bits__.test.TEST04_overTempTermalTest.base10Val, self.__bits__.__bits__.test.TEST05_disAfe.base10Val,
                   self.__bits__.__bits__.test.TEST06_testMuxInfra.base10Val, self.__bits__.__bits__.test.TEST07_testMuxAfe.base10Val,
                   self.__bits__.__bits__.test.TEST08_testMuxInfraEn.base10Val, self.__bits__.__bits__.test.TEST09_testMuxAfeEn.base10Val]
    
        return(readOut);    
        
    def leftAmp(self):
        
        decVal = self.__bits__.__bits__.test.TEST00_leftAmpDis.base10Val;
        
        if decVal == 0:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
    
        return([returnStr, decVal]);
    
    def leftBridge(self):
        
        decVal = self.__bits__.__bits__.test.TEST01_leftBridgeShort.base10Val;
        
        if decVal == 0:
            returnStr  = 'unshort';
        else:
            returnStr  = 'short';
    
        return([returnStr, decVal]);
        
    def rightAmp(self):
        
        decVal = self.__bits__.__bits__.test.TEST02_rightAmpDis.base10Val;
        
        if decVal == 0:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
     
        return([returnStr, decVal]);
        
    def rightBridge(self):
        
        decVal = self.__bits__.__bits__.test.TEST03_rightBridgeShort.base10Val;
        
        if decVal == 0:
            returnStr  = 'unshort';
        else:
            returnStr  = 'short';
    
        return([returnStr, decVal]);
        
    def overTempThermalTest(self):
        
        decVal = self.__bits__.__bits__.test.TEST04_overTempTermalTest.base10Val;
        
        if decVal == 1:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
     
        return([returnStr, decVal]);
    
    def afe(self):
        
        decVal = self.__bits__.__bits__.test.TEST05_disAfe.base10Val;
        
        if decVal == 0:
            returnStr  = 'en';
        else:
            returnStr  = 'dis';
     
        return([returnStr, decVal]);
    
    
    
