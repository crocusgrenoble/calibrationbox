# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 16:55:26 2018

@author: alongen
"""

class haloBitNDV():
    
    def __init__(self, varDesc, defaultBit, numberBits):
        
        self.__vDesc__ = varDesc;
        self.__dBit__ = defaultBit;
        self.__nBits__ = numberBits;
        
        exec('self.' + 'description' + '=' + 'self.' + '__vDesc__');    
        exec('self.' + 'base10Val' + '=' + 'self.' + '__dBit__');
        exec('self.' + 'bitSize' + '=' + 'self.' + '__nBits__');
        
    def __del__(self):
        
        del(self); 