# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 15:44:11 2018

@author: alongen
"""

import sys

def F_decToBin(decVal, binBits):
    
    if type(decVal) != int:
        sys.exit('The input needs to be type integer!');
        
    if type(binBits) != int or binBits < 0:
        sys.exit('The input needs to be type integer and/or positive!');        
      
    binVal = '';
    formatPart = '{0:0' + str(binBits) + 'b}';
    d = {"binVal":binVal,"decVal":decVal,"formatPart":formatPart}
    exec('binVal = formatPart.format(decVal)',d);
    
    return d["binVal"]