# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

class explainBitsTrimHalo():
  
    def __init__(self, haloBits):
        
        self.__bits__ = haloBits;
    
    def vRefBandGapCorner(self):
    
        print(self.__bits__.trim.TRIM00_vRefBgCornerTrim.description);        
    
    def vRefScaledBandGap(self):
    
        print(self.__bits__.trim.TRIM01_vRefScaledBgTrim.description);
    
    def vRefPtatUp(self):
    
        print(self.__bits__.trim.TRIM02_vRegPtatUp.description);
        
    def vRegBandGapXDown(self):
    
        print(self.__bits__.trim.TRIM03_vRegBgXDown.description); 
    
    def afeMagOffsetLeft(self):
        
        print(self.__bits__.trim.TRIM04_vosMagLeft.description); 
        
    def afeMagOffsetRight(self):
        
        print(self.__bits__.trim.TRIM05_vosMagRight.description);
        
    def afeElectOffsetLeft(self):
        
        print(self.__bits__.trim.TRIM06_vosElectLeft.description);
    
    def afeElectOffsetRight(self):
        
        print(self.__bits__.trim.TRIM07_vosElectRight.description);
    
    def afeAmpGainLeft(self):
        
        print(self.__bits__.trim.TRIM08_gainInstrAmpLeft.description);
    
    def afeAmpGainRight(self):
        
        print(self.__bits__.trim.TRIM09_gainInstrAmpRight.description);
    
    def polarFlavor(self):
        
        print(self.__bits__.trim.TRIM10_gainUnipolar.description);
    
    def vddSupply(self):
        
        print(self.__bits__.trim.TRIM11_gainHiSupply.description);
    
    def currentFieldScale(self):
        
        print(self.__bits__.trim.TRIM12_gainFor12mT.description);
    
    def marking(self):
        
        print(self.__bits__.trim.TRIM13_markingBits.description);

    def tryBeforeBuyFeature(self):
            
        print(self.__bits__.trim.TRIM14_blockTryBeforeBuy.description);

    def crocusMurataHaloMode(self):
        
        print(self.__bits__.trim.TRIM15_crousMurataModes.description);

    def bridgeSwap(self):
        
        print(self.__bits__.trim.TRIM16_bridgeSwaps.description);
        
        
        
        