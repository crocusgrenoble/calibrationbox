# -*- coding: utf-8 -*-
"""
Created on Fri Oct 19 10:19:05 2018

@author: alongen
"""

import socket, serial, sys, visa

class connect():
        
    def __init__(self, *arg):
        
        self.__closeType__ = 0;
        self.__nInputs__ = len(arg);        
        gpibUsbCondition = ( ('GPIB' in arg[0]) or ('USB' in arg[0]) ) and (self.__nInputs__ == 1); 
        lanCondition = ('.' in arg[0]) and (self.__nInputs__ == 2);       
        serialCondition = ('COM' in arg[0]) and (self.__nInputs__ == 2);      
        errorCheck = not (gpibUsbCondition or lanCondition or serialCondition);       
        if errorCheck:
            sys.exit('Input Parameters to the "connect" class are not suitable for USB/GPIB/SERIAL/LAN protocol');
        
        # GPIB or USB Equipment
        if gpibUsbCondition:
            self.__closeType__ = 1;
            rm = visa.ResourceManager();
            self.__address__ = arg[0]; 
            self.session = rm.open_resource(self.__address__);
        
        # LAN Equipment [FLEX TC ONLY!!!!]
        if lanCondition:
            self.__closeType__ = 2;
            self.__s__ = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
            self.ip = arg[0];
            self.tcp = arg[1];
            self.to = 5.5;
            self.__baseCommand__ = "m";
            self.__rxbufsize__ = 1024;
            self.__read_all_at_once__ = True;
            self.__s__.setblocking(0);
            self.__s__.settimeout(self.to);
            self.__s__.connect((self.ip,self.tcp));
            
        if serialCondition:
            self.__closeType__ = 3;
            self.__com__ = arg[0];
            self.__baud__ = arg[1];
            self.__timeout__ = 0.1;
            self.serial = serial.Serial(self.__com__, self.__baud__, timeout = self.__timeout__);
       
    def __del__(self):
        
        if self.__closeType__ == 1:
            self.session.close();
        elif self.__closeType__ == 2:
            self.__s__.close();
        elif self.__closeType__ == 3:
            self.serial.close();
        del(self);
        

