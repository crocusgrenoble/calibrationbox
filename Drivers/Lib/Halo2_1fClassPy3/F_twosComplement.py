# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 15:17:42 2019

@author: alongen
"""

from F_binToDec import F_binToDec
from F_decToBin import F_decToBin

def F_twosComplement(binStr):
    
    nBits = len(binStr);
    decVal = F_binToDec(binStr);
    twoCompDecVal = 2**nBits - decVal;
    twoCompBinVal = F_decToBin(twoCompDecVal, nBits);
    
    return(twoCompBinVal);
    