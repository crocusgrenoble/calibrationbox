# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

from generalExplainBitsTestHalo import generalExplainBitsTestHalo
from isbMuxExplainBitsTestHalo import isbMuxExplainBitsTestHalo
from afeMuxExplainBitsTestHalo import afeMuxExplainBitsTestHalo

class explainBitsTestHalo():
  
    def __init__(self, haloBits):
        
        self.__bits__ = haloBits;
        
        self.general = generalExplainBitsTestHalo(self.__bits__);
        self.isbMux = isbMuxExplainBitsTestHalo(self.__bits__);
        self.afeMux = afeMuxExplainBitsTestHalo(self.__bits__);