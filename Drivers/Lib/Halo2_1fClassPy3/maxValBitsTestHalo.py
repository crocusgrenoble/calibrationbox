# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

class maxValBitsTestHalo():
  
    def __init__(self, haloBits):
        
        self.__bits__ = haloBits;

    def leftAmp(self):
        
        return(2**self.__bits__.test.TEST00_leftAmpDis.bitSize - 1);
     
    def leftBridge(self):
        
        return(2**self.__bits__.test.TEST01_leftBridgeShort.bitSize - 1);
     
    def rightAmp(self):
        
        return(2**self.__bits__.test.TEST02_rightAmpDis.bitSize - 1);
     
    def rightBridge(self):
        
        return(2**self.__bits__.test.TEST03_rightBridgeShort.bitSize - 1);
        
    def overTempTermalTest(self):
        
        return(2**self.__bits__.test.TEST04_overTempTermalTest.bitSize - 1);
    
    def disAfe(self):
        
        return(2**self.__bits__.test.TEST05_disAfe.bitSize - 1);
    
    def testMuxInfra(self):
        
        return(2**self.__bits__.test.TEST06_testMuxInfra.bitSize - 1);    
    
    def testMuxAfe(self):
        
        return(2**self.__bits__.test.TEST07_testMuxAfe.bitSize - 1);      
   
    def testMuxInfraEn(self):
        
        return(2**self.__bits__.test.TEST08_testMuxInfraEn.bitSize - 1);

    def testMuxAfeEn(self):
        
        return(2**self.__bits__.test.TEST09_testMuxAfeEn.bitSize - 1);