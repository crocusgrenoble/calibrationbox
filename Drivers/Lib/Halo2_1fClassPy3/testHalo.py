# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

class testHalo():
  
    def __init__(self, hmBits):

        from setBitsTestHalo import setBitsTestHalo
        from readBitsTestHalo import readBitsTestHalo
        from explainBitsTestHalo import explainBitsTestHalo
        from maxValBitsTestHalo import maxValBitsTestHalo
        
        self.__bits__ = hmBits;
        
        self.set = setBitsTestHalo(self.__bits__);
        self.read = readBitsTestHalo(self.set);
        self.explain = explainBitsTestHalo(self.__bits__);
        self.maxVal = maxValBitsTestHalo(self.__bits__);
    
    def __del__(self):
        
        del(self);