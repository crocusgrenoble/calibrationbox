# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

class opCodeModeHalo():
  
    def __init__(self, opCodeArray):
        
        self.__opCodeArray__ = opCodeArray;
        self.normalOperation();
    
    def normalOperation(self):
        
        self.__opCodeArray__ = [0, 0]; # code 1, but LSB first!
    
    def tryBeforeBuy(self):
        
        self.__opCodeArray__ = [1, 0]; # code 1, but LSB first!
        
    def burnFuse(self):
        
        self.__opCodeArray__ = [0, 1]; # code 2, but LSB first!
    
    def readFuse(self):
        
        self.__opCodeArray__ = [1, 1]; # code 3, but LSB first!