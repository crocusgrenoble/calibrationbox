# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 00:09:56 2018

@author: alongen
"""

from F_reverseLrStr import F_reverseLrStr
from haloBitNDV import haloBitNDV
import numpy as np
from F_decToBin import F_decToBin
from F_twosComplement import F_twosComplement

class trimHalo2Bits():
    
    def __init__(self):
        
        # Halo Trim Bit Names
        self.__trimVarNameArray__ = ['TRIM00_vRefBgCornerTrim', 'TRIM01_vRefScaledBgTrim', 'TRIM02_vRegPtatUp', 'TRIM03_vRegBgXDown', 'TRIM04_vosMagLeft',
                                     'TRIM05_vosMagRight', 'TRIM06_vosElectLeft', 'TRIM07_vosElectRight', 'TRIM08_gainInstrAmpLeft', 'TRIM09_gainInstrAmpRight', 
                                     'TRIM10_gainUnipolar', 'TRIM11_gainHiSupply', 'TRIM12_gainFor12mT', 'TRIM13_markingBits', 'TRIM14_blockTryBeforeBuy',
                                     'TRIM15_crousMurataModes', 'TRIM16_bridgeSwaps'];
        
        # Halo Trim Bit Descriptions
        self.__trimVarDescArray__ = ['[CROCUS FACTORY]\nBandgap Curvature Trim: All chips on a wafer will have the same trim code. Default = 0. May need trim to compensate for process variations',
                                     'Trims the voltage reference VrefOut which determines the ZeroCurrentVoltage at the output. This is verified using the test mux. Default = 0 will cause a small error that needs trimming',
                                     '[CROCUS FACTORY]\nTrims the ptat component of the bridge regulator voltage. Increasing codes result in stronger ptat or a stronger temperature compensation. Default = 0 will have no temp compensation slope',
                                     '[CROCUS FACTORY]\nTrims the fixed voltage component of the bridge regulator. Increasing codes reduce the fixed voltage component of the bridge regulator voltage. Fixed voltage means no change with temperature. Default = 0 is a fixed voltage',
                                     'Trims the Magnetic offset of the Left AFE up/down',
                                     'Trims the Magnetic offset of the Right AFE up/down',
                                     'Trims the Electronic offset of the Left AFE up/down',
                                     'Trims the Electronic offset of the Right AFE up/down',
                                     'Trims the gain of the Left AFE up/down',
                                     'Trims the gain of the Right AFE up/down',
                                     '(Code 0) BiPolar\n(Code 1) UniPolar',
                                     '(Code 0) 3.3V\n(Code 1) 5.0V',
                                     '(Code 0) 8 mT (20A Abs Max)\n(Code 1) 12mT (30A Abs Max)',
                                     'No electrical effect',
                                     '[Anti-Hack Feature]\n(Code 0) Allows access to Try-Before-Buy\n(Code 1) Prevents access to Try-Before-Buy',
                                     '[Crocus Vs Murata Chip Mode]\n(Code 0) Crocus Chip Mode - Will NOT ignore "TRIM15_crousMurataModes" and "TRIM16_bridgeSwaps"\n(Code 1) Murata Chip Mode - Will ignore "TRIM15_crousMurataModes" and "TRIM16_bridgeSwaps"',
                                     '(Code 0) Left Bridge Default & Right Bridge Default\n(Code 1) Left Bridge Flipped & Right Bridge Default\n(Code 2) Left Bridge Default & Right Bridge Flipped\n(Code 3) Left Bridge Flipped & Right Bridge Flipped'];
                            
        # Halo Trim Default Bit Values                       
        self.__trimDefaultBitArray__ = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        
        # Halo Trim Bit Sizes
        self.__trimBitSizes__ = [3, 6, 6, 6, 8, 8, 8, 8, 8, 8, 1, 1, 1, 4, 1, 1, 2];
        
        # Number of Variable Names
        self.__nTrimNames__ = len(self.__trimVarNameArray__);
    
        for i in range(self.__nTrimNames__):
            
            self.tempClass = haloBitNDV(self.__trimVarDescArray__[i], self.__trimDefaultBitArray__[i], self.__trimBitSizes__[i]);
            exec('self.' + self.__trimVarNameArray__[i] + '=' + 'self.tempClass');

    def createBitStream(self):
        
        finalBitStream = [];
        
        for i in range(self.__nTrimNames__):
            
            exec('self.tempDec' + '=' + 'int(self.' + self.__trimVarNameArray__[i] + '.base10Val)');
            exec('self.tempBitSize' + '=' + 'int(self.' + self.__trimVarNameArray__[i] + '.bitSize)');   

            if np.sign(self.tempDec) == -1:
                binVal = F_twosComplement( F_decToBin(self.tempDec, self.tempBitSize) );
            else:
                binVal = F_decToBin(self.tempDec, self.tempBitSize);
            
            lsbToMsb = F_reverseLrStr(binVal);
            
            nBits = len(lsbToMsb);
            
            for j in range(nBits):
                finalBitStream.append( int(lsbToMsb[j]) );
       
        return(finalBitStream);