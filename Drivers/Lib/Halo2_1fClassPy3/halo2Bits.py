# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 14:22:50 2018

@author: alongen
"""

import numpy as np

class halo2Bits():
    
    def __init__(self):
        
        from testHaloBits import testHaloBits
        from trimHalo2Bits import trimHalo2Bits
        
        self.test = testHaloBits();
        self.trim = trimHalo2Bits();
        
    def finalBitStream(self):
        
        finalTestBits = self.test.createBitStream();
        finalTrimBits = self.trim.createBitStream();
        
        finalBits = np.concatenate( (finalTestBits, finalTrimBits) );
        # print(repr(finalBits))
        return(finalBits);