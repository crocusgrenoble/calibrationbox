# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

import sys

class afeMuxSetBitsTestHalo():
  
    def __init__(self, haloBits):
        
        self.__bits__ = haloBits;               
        
    def disMux(self):
    
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 0;
        
    def negInputRightBridge(self):
    
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 1;
        
    def posInputRightBridge(self):
    
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 2;

    def negInputLeftBridge(self):
        
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 3;
        
    def posInputLeftBridge(self):
    
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 4;
        
    def negOutLeftAmp(self):
        
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 5;
        
    def posOutLeftAmp(self):
        
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 6;

    def negOutRightAmp(self):
        
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 7;
        
    def posOutRightAmp(self):

        self.__bits__.test.TEST07_testMuxAfe.base10Val = 8;
        
    def normalized(self):
        
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 9;
        
    def bufferedZeroCurrVref(self):
    
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 10;

    def overNegCurrDigtialFlagComparator(self):
    
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 11;

    def overPosCurrDigtialFlagComparator(self):
    
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 12;

    def overPosNegCurrLogicOR(self):
    
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 13;
            
    def overNegCurrVref(self):
    
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 14;
    
    def overPosCurrVref(self):
        
        self.__bits__.test.TEST07_testMuxAfe.base10Val = 15;
        
    def afeMuxToFltPin(self, enDisStr):
    
        strCheck = enDisStr == 'en' or enDisStr == 'dis';
        if not strCheck:
            sys.exit('The input to this function needs to be a str of "en" or "dis"!');
        
        if enDisStr == 'en':
            self.__bits__.test.TEST09_testMuxAfeEn.base10Val = 1;
            self.__bits__.test.TEST08_testMuxInfraEn.base10Val = 0; # disable the isbMux to Fault Pin
        else:
            self.__bits__.test.TEST09_testMuxAfeEn.base10Val = 0;
        

