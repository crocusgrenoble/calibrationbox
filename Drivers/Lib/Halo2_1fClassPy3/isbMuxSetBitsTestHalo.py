# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

import sys

class isbMuxSetBitsTestHalo():
  
    def __init__(self, haloBits):
        
        self.__bits__ = haloBits;
    
    def disMux(self):
        
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 0;
        
    def porTest(self):
        
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 1;
        
    def overTempTestAtRoomTemp(self):
        
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 2;
        
    def overTempFaultMonitorAtHighTemp(self):
    
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 3;
    
    def bridgeRegVolt(self):
        
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 4;
    
    def afeLowVoltReg(self):

        self.__bits__.test.TEST06_testMuxInfra.base10Val = 5;
    
    def rawBgRef(self):
        
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 6;
    
    def clk(self):
    
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 7;
        
    def neg110PerOverCurrRef(self):
        
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 8;        

    def neg90PerOverCurrRef(self):

        self.__bits__.test.TEST06_testMuxInfra.base10Val = 9;
        
    def zeroCurrVref(self):
        
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 10;        
        
    def pos90PerOverCurrRef(self):
            
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 11;
        
    def pos110PerOverCurrRef(self):
    
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 12;        
        
    def bridgeRegPtat(self):
    
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 13;
        
    def overTempRefCheck(self):
        
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 14;        
        
    def zeroCurrTempCoFromBridgeReg(self):
        
        self.__bits__.test.TEST06_testMuxInfra.base10Val = 15;
        
    def isbMuxToFltPin(self, enDisStr):
        
        strCheck = enDisStr == 'en' or enDisStr == 'dis';
        if not strCheck:
            sys.exit('The input to this function needs to be a str of "en" or "dis"!');
        
        if enDisStr == 'en':
            self.__bits__.test.TEST08_testMuxInfraEn.base10Val = 1;
            self.__bits__.test.TEST09_testMuxAfeEn.base10Val = 0; # disable the afeMux to Fault Pin
        else:
            self.__bits__.test.TEST08_testMuxInfraEn.base10Val = 0;
            
            