# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

class generalExplainBitsTestHalo():
  
    def __init__(self, haloBits):
        
        self.__bits__ = haloBits;

    def leftAmp(self):

        print(self.__bits__.test.TEST00_leftAmpDis.description);
    
    def leftBridge(self):
            
        print(self.__bits__.test.TEST01_leftBridgeShort.description);
        
    def rightAmp(self):
        
        print(self.__bits__.test.TEST02_rightAmpDis.description);
     
    def rightBridge(self):
        
        print(self.__bits__.test.TEST03_rightBridgeShort.description);
        
    def overTempThermalTest(self):
    
        print(self.__bits__.test.TEST04_overTempTermalTest.description);
    
    def afe(self):
    
        print(self.__bits__.test.TEST05_disAfe.description);