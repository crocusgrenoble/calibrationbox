# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

class afeMuxExplainBitsTestHalo():
  
    def __init__(self, haloBits):
        
        self.__bits__ = haloBits;

    def disMux(self):
    
        print(self.__bits__.test.TEST07_testMuxAfe.description);
        
    def negInputRightBridge(self):
        
        print(self.__bits__.test.TEST07_testMuxAfe.description);
        
    def posInputRightBridge(self):
    
        print(self.__bits__.test.TEST07_testMuxAfe.description);
        
    def negInputLeftBridge(self):
        
        print(self.__bits__.test.TEST07_testMuxAfe.description);
        
    def posInputLeftBridge(self):
    
        print(self.__bits__.test.TEST07_testMuxAfe.description);
        
    def negOutLeftAmp(self):
        
        print(self.__bits__.test.TEST07_testMuxAfe.description);
        
    def posOutLeftAmp(self):
        
        print(self.__bits__.test.TEST07_testMuxAfe.description);

    def negOutRightAmp(self):
    
        print(self.__bits__.test.TEST07_testMuxAfe.description);
        
    def posOutRightAmp(self):
    
        print(self.__bits__.test.TEST07_testMuxAfe.description);
        
    def normalized(self):
    
        print(self.__bits__.test.TEST07_testMuxAfe.description);
        
    def bufferedZeroCurrVref(self):
    
        print(self.__bits__.test.TEST07_testMuxAfe.description);

    def overNegCurrDigtialFlagComparator(self):
    
        print(self.__bits__.test.TEST07_testMuxAfe.description);

    def overPosCurrDigtialFlagComparator(self):
    
        print(self.__bits__.test.TEST07_testMuxAfe.description);

    def overPosNegCurrLogicOR(self):
    
        print(self.__bits__.test.TEST07_testMuxAfe.description);
            
    def overNegCurrVref(self):
    
        print(self.__bits__.test.TEST07_testMuxAfe.description);
    
    def overPosCurrVref(self):
    
        print(self.__bits__.test.TEST07_testMuxAfe.description);
        
    def afeMuxToFltPin(self):
    
        print(self.__bits__.test.TEST09_testMuxAfeEn.description);