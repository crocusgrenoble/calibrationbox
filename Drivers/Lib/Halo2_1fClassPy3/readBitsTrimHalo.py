# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

from F_decToBin import F_decToBin
from F_binToDec import F_binToDec
from F_twosComplement import F_twosComplement

class readBitsTrimHalo():
  
    def __init__(self, haloBits):
        
        self.__bits__ = haloBits;
    
    def allTrimBitsLsbToMsb(self):
        
        readOut = [self.__bits__.__bits__.trim.TRIM00_vRefBgCornerTrim.base10Val, self.__bits__.__bits__.trim.TRIM01_vRefScaledBgTrim.base10Val,
                   self.__bits__.__bits__.trim.TRIM02_vRegPtatUp.base10Val, self.__bits__.__bits__.trim.TRIM03_vRegBgXDown.base10Val,
                   self.__bits__.__bits__.trim.TRIM04_vosMagLeft.base10Val, self.__bits__.__bits__.trim.TRIM05_vosMagRight.base10Val,
                   self.__bits__.__bits__.trim.TRIM06_vosElectLeft.base10Val, self.__bits__.__bits__.trim.TRIM07_vosElectRight.base10Val,
                   self.__bits__.__bits__.trim.TRIM08_gainInstrAmpLeft.base10Val, self.__bits__.__bits__.trim.TRIM09_gainInstrAmpRight.base10Val,
                   self.__bits__.__bits__.trim.TRIM10_gainUnipolar.base10Val, self.__bits__.__bits__.trim.TRIM11_gainHiSupply.base10Val,   
                   self.__bits__.__bits__.trim.TRIM12_gainFor12mT.base10Val, self.__bits__.__bits__.trim.TRIM13_markingBits.base10Val,        
                   self.__bits__.__bits__.trim.TRIM14_blockTryBeforeBuy.base10Val, self.__bits__.__bits__.trim.TRIM15_crousMurataModes.base10Val,        
                   self.__bits__.__bits__.trim.TRIM16_bridgeSwaps.base10Val];
        
        return(readOut);
        
    def vRefBandGapCorner(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM00_vRefBgCornerTrim.base10Val;
    
        return(decVal);        
    
    def vRefScaledBandGap(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM01_vRefScaledBgTrim.base10Val;
    
        return(decVal);
    
    def vRefPtatUp(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM02_vRegPtatUp.base10Val;
    
        return(decVal);
        
    def vRegBandGapXDown(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM03_vRegBgXDown.base10Val;
    
        return(decVal); 
    
    def afeMagOffsetLeft(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM04_vosMagLeft.base10Val;
        
        return(decVal); 
        
    def afeMagOffsetRight(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM05_vosMagRight.base10Val;
        
        return(decVal);
        
    def afeElectOffsetLeft(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM06_vosElectLeft.base10Val;
        
        return(decVal);
    
    def afeElectOffsetRight(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM07_vosElectRight.base10Val;
        
        return(decVal);
    
    def afeAmpGainLeft(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM08_gainInstrAmpLeft.base10Val;
        
        return(decVal);
    
    def afeAmpGainRight(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM09_gainInstrAmpRight.base10Val;
        
        return(decVal);
    
    def polarFlavor(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM10_gainUnipolar.base10Val;
        
        if decVal == 1:
            polarStr = 'unipolar';
        else:
            polarStr = 'bipolar';
        
        return([polarStr, decVal]);
    
    def vddSupply(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM11_gainHiSupply.base10Val;
        
        if decVal == 1:
            vddStr = '5.0V';
        else:
            vddStr = '3.3V';
        
        return([vddStr, decVal]);
    
    def currentFieldScale(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM12_gainFor12mT.base10Val;
        
        if decVal == 1:
            scaleStr = '12mT(30A)';
        else:
            scaleStr = '8mT(20A)';
        
        return([scaleStr, decVal]);
    
    def marking(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM13_markingBits.base10Val;
        
        return(decVal);

    def tryBeforeBuyFeature(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM14_blockTryBeforeBuy.base10Val;
        
        if decVal == 1:
            tryStr = 'dis';
        else:
            tryStr = 'en';
            
        return([tryStr, decVal]);

    def crocusMurataHaloMode(self):

        decVal = self.__bits__.__bits__.trim.TRIM15_crousMurataModes.base10Val;
        
        if decVal == 1:
            modeStr = 'murata';
        else:
            modeStr = 'crocus';
        
        return([modeStr, decVal]);

    def bridgeSwap(self):
        
        decVal = self.__bits__.__bits__.trim.TRIM16_bridgeSwaps.base10Val;
        
        swapStrArray = ['noSwap', 'leftSwap', 'rightSwap', 'leftAndRightSwap'];
        
        return([swapStrArray[decVal], decVal]);
        
        
        
        