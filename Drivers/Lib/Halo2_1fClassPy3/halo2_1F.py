# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""
from  global_constants import *
if(ad2_present):
    from F_analogDiscovery2Halo import F_analogDiscovery2Halo
else: 
    from pyboard import PyBoard
    # from f_pyboard2halo import F_pyBoard2Halo
    from pyboard2halo import Pyboard2Halo
    
from opCodeModeHalo import opCodeModeHalo
from F_binToDec import F_binToDec
from F_reverseLrStr import F_reverseLrStr
import sys, numpy as np
if(ad2_present):
    from analogDiscovery2Halo import analogDiscovery2Halo
from F_decToBin import F_decToBin

class halo2_1F():
  
    def __init__(self, strInput):

        from halo2Bits import halo2Bits
        from testHalo import testHalo
        from trimHalo2 import trimHalo2
        
        self.__bits__ = halo2Bits();
        
        self.testBits = testHalo(self.__bits__);
        self.trimBits = trimHalo2(self.__bits__);
  
        self.__nDummyBits__ = 1;
        self.__nKeyCodeBits__ = 8;
        self.__nOpCodeBits__ = 2;
        self.__nTestModeBits__ = 16;
        
        self.nSclkPeriods(107);

        self.__keyCodeArray__ = [1, 1, 1, 1, 0, 0, 1, 0]; # LSB first (change for Halo2 Rev 1F) 
        self.__opCodeArray__ = [1, 0]; # code 1, but LSB first!
        
        self.__nSamplesPerHalfPeriod__ = 100;
        
        #self.__mainClkFreq__ = 100e3;
        self.__mainClkFreq__ = 25e3;
        
        self.__clkChannel__ = 1;
        self.__vddMax__ = 4.75;
        self.__sDataChannel__ = 2;
        self.__clkEdgeStr__ = 'R';
        
        self.opCodeMode = opCodeModeHalo(self.__opCodeArray__);
    
        self.__sclkNode__ = False;
        self.__sdataNode__ = False;
        #print (repr(strInput))
        if len(strInput) != 0:
            self.ad2Connect = strInput['device'] == 'ad2';
            self.pyb_connect = strInput['device'] == 'CTC4000'
            if self.ad2Connect:
               self.__ad2__ = analogDiscovery2Halo();
            else:
               self.__pyb_halo2__ = Pyboard2Halo(strInput['pyboard_object'] )
    
    def nSclkPeriods(self, val):
        
        valCheck = val == 104 or val == 107;
        if not valCheck:
            sys.exit('nSclkPeriods need to be 104 or 107!');
            
        if val == 104:
            self.__nProgrammingBits__ = 77;
        else:
            self.__nProgrammingBits__ = 80;
    
        self.__nTotalBits__ = self.__nDummyBits__ + self.__nKeyCodeBits__ + self.__nOpCodeBits__ + self.__nTestModeBits__ + self.__nProgrammingBits__;
    
    def setVddMax(self, val):
        
        vddValCheck = val > 0 and val <= 5.5;
        if not vddValCheck:
            sys.exit('Max Vdd needs to be between 0 (not inclusive) and 5.5V (inclusive)!');
            
        self.__vddMax__ = val;
    
    def setFlavorSelector(self, flavorStr):
        
        flavorStrArray = ['LLB', 'LLU', 'LHB', 'LHU', 'HLB', 'HLU', 'HHB', 'HHU'];
        
        flavorCheck = flavorStr in flavorStrArray;
        if not flavorCheck:
            sys.exit('The input str is not a valid str! The following strings are valid: \n "' + str(flavorStrArray[0]) + \
                     '", "' + str(flavorStrArray[1]) + '", "' + str(flavorStrArray[2]) + '", "' + str(flavorStrArray[3]) + \
                     '", "' + str(flavorStrArray[4]) + '", "' + str(flavorStrArray[5]) + '", "' + str(flavorStrArray[6]) + '", "' + str(flavorStrArray[7]) + '"');
        
        flavorIdx = flavorStrArray.index(flavorStr);
        
        if flavorIdx == 0:
            self.trimBits.set.currentFieldScale('8mT(20A)');
            self.trimBits.set.vddSupply('3.3V');
            self.trimBits.set.polarFlavor('bipolar');
        elif flavorIdx == 1:
            self.trimBits.set.currentFieldScale('8mT(20A)');
            self.trimBits.set.vddSupply('3.3V');
            self.trimBits.set.polarFlavor('unipolar');
        elif flavorIdx == 2:
            self.trimBits.set.currentFieldScale('8mT(20A)');
            self.trimBits.set.vddSupply('5.0V');
            self.trimBits.set.polarFlavor('bipolar');
        elif flavorIdx == 3:
            self.trimBits.set.currentFieldScale('8mT(20A)');
            self.trimBits.set.vddSupply('5.0V');
            self.trimBits.set.polarFlavor('unipolar');
        elif flavorIdx == 4:
            self.trimBits.set.currentFieldScale('12mT(30A)');
            self.trimBits.set.vddSupply('3.3V');
            self.trimBits.set.polarFlavor('bipolar');
        elif flavorIdx == 5:
            self.trimBits.set.currentFieldScale('12mT(30A)');
            self.trimBits.set.vddSupply('3.3V');
            self.trimBits.set.polarFlavor('unipolar');
        elif flavorIdx == 6:
            self.trimBits.set.currentFieldScale('12mT(30A)');
            self.trimBits.set.vddSupply('5.0V');
            self.trimBits.set.polarFlavor('bipolar');
        else:
            self.trimBits.set.currentFieldScale('12mT(30A)');
            self.trimBits.set.vddSupply('5.0V');
            self.trimBits.set.polarFlavor('unipolar');
    
    def explainFlavorSelector(self):
        
        explainStr = "\n'{Token 1}{Token 2}{Token 3}'\n\n" + \
                     "### Token 1 ###\n" + \
                     " L = 20A Scale\n" + \
                     " H = 30A Scale\n" + \
                     "###############\n\n" + \
                     "### Token 2 ###\n" + \
                     " L = 3.3V Vdd\n" + \
                     " H = 5.0V Vdd\n" + \
                     "###############\n\n" + \
                     "### Token 3 ###\n" + \
                     " B = Bipolar\n" + \
                     " U = Unipolar\n" + \
                     "###############\n\n" + \
                     "Example: 'LLB' = 20A, 3.3V, Bipolar";
                     
        print(explainStr);
    
    def __outputSetBits__(self):
        
        dataBits = self.__bits__.finalBitStream();
        
        if self.__nProgrammingBits__ == 77:   
            dataBits = dataBits[0 : -3];
        
        if self.opCodeMode.__opCodeArray__ == [1, 1] or self.opCodeMode.__opCodeArray__ == [0, 1] or self.opCodeMode.__opCodeArray__ == [0, 0]:
            # print(repr(dataBits))
            dataBits[-self.__nProgrammingBits__ : ] = np.linspace(0, 0, self.__nProgrammingBits__);
        
        # Change the Ordering of FBits (LSB first)
        sBitIdx = 16;
        
        # Collect Bg FBits <73, 2:1>
        #bgBit73 = dataBits[sBitIdx + 2];
        #bgBit2 = dataBits[sBitIdx + 1];
        #bgBit1 = dataBits[sBitIdx + 0];

        # Collect Bg FBits <2:1, 73>
        bgBit73 = dataBits[sBitIdx + 0];
        bgBit1 = dataBits[sBitIdx + 1];
        bgBit2 = dataBits[sBitIdx + 2];
        
        # Collect Vref FBits <72, 7:3>
        vRefBit72 = dataBits[sBitIdx + 8];
        vRefBit7 =  dataBits[sBitIdx + 7]; 
        vRefBit6 =  dataBits[sBitIdx + 6]; 
        vRefBit5 =  dataBits[sBitIdx + 5];
        vRefBit4 =  dataBits[sBitIdx + 4]; 
        vRefBit3 =  dataBits[sBitIdx + 3]; 
        
        # Collect Marking FBits <75, 74, 71, 0>
        markingBit75 = dataBits[sBitIdx + 75];
        markingBit74 = dataBits[sBitIdx + 74];
        markingBit71 = dataBits[sBitIdx + 73];
        markingBit0 = dataBits[sBitIdx + 72];
        
        tempBits = list( dataBits[sBitIdx + 9 : sBitIdx + 72] ); # copy by value (not by reference)!!!

        # Set Correct Bg FBits
        dataBits[sBitIdx + 1] = bgBit1;
        dataBits[sBitIdx + 2] = bgBit2;
        dataBits[sBitIdx + 73] = bgBit73;
        
        # Set Correct Vref FBits
        dataBits[sBitIdx + 3] = vRefBit3;
        dataBits[sBitIdx + 4] = vRefBit4;
        dataBits[sBitIdx + 5] = vRefBit5;
        dataBits[sBitIdx + 6] = vRefBit6;
        dataBits[sBitIdx + 7] = vRefBit7;
        dataBits[sBitIdx + 72] = vRefBit72;
        
        # Set Correct Marking FBits
        dataBits[sBitIdx + 0] = markingBit0;
        dataBits[sBitIdx + 71] = markingBit71;
        dataBits[sBitIdx + 74] = markingBit74;
        dataBits[sBitIdx + 75] = markingBit75;
        
        nLastBits = len(dataBits) - (71 + 16);
        
        finalDataBits = [];
        
        for i in range( 16 ):
            finalDataBits.append(dataBits[i]);
        
        for i in range( 8 ):
            finalDataBits.append(dataBits[sBitIdx + i]);
        
        for i in range( len(tempBits) ):
            finalDataBits.append(tempBits[i]);
        
        for i in range( 71, 71 + nLastBits ):
            finalDataBits.append(dataBits[sBitIdx + i]);
        setAd2Bits = np.concatenate( (self.__keyCodeArray__, self.opCodeMode.__opCodeArray__, finalDataBits) );
        #print(len(setAd2Bits))
        #print(setAd2Bits)
        # 01001101
        return(setAd2Bits);
    
    def __ad2InputParameters__(self):
        
        dataBits = self.__outputSetBits__();
        
        inputParameters = [self.__nTotalBits__, self.__nSamplesPerHalfPeriod__, self.__mainClkFreq__, self.__clkChannel__,
                           self.__vddMax__, self.__sDataChannel__, self.__clkEdgeStr__, dataBits, self.opCodeMode.__opCodeArray__,
                           self.__ad2__];
                           
        return(inputParameters);


    def __pyBoardInputParameters__(self):
        
        dataBits = self.__outputSetBits__();
        
        inputParameters = [self.__nTotalBits__, self.__nSamplesPerHalfPeriod__, self.__mainClkFreq__, self.__clkChannel__,
                           self.__vddMax__, self.__sDataChannel__, self.__clkEdgeStr__, dataBits, self.opCodeMode.__opCodeArray__,
                           self.__pyb_halo2__];
                           
        return(inputParameters);


    def save_params_to_file():
        inputParameters = self.__pyBoardInputParameters__();                        
        with open(f'halo.pickle', 'wb') as file:
            pickle.dump(params[:-1], file) # We don't store pyabord object (the last parameter)

    def sendBitsToPyBoardFromFile(self):
        with open(f'halo.pickle', 'rb') as file2:
            params = pickle.load(file2);         
        params[9] = self.__pyb_halo2__;
        out = self.sendBitsToPyBoard(params)
        return out
    
    def sendArbitraryBitsToPyboard(self, arbitraryBits):
        inputParameters = self.__pyBoardInputParameters__();
        inputParameters[7] = arbitraryBits
        out = self.sendBitsToPyBoard(inputParameters)
        return out
        
    def sendBitsToPyBoard(self,params=False):
        if(params == False):
            inputParameters = self.__pyBoardInputParameters__();
        else:
            inputParameters = params
        readBitArrayBin = self.__pyb_halo2__.F_pyBoard2Halo(inputParameters);
        nFuseElements = len(readBitArrayBin);
        readBitArrayDec = [];
        
        if nFuseElements > 0:
            
            if self.__nProgrammingBits__ == 77:
                fuseStartIdx = [0, 3, 8, 14, 20, 28, 36, 44, 52, 60, 68, 69, 70, 71, 76];
                fuseEndIdx = [3, 8, 14, 20, 28, 36, 44, 52, 60, 68, 69, 70, 71, 76, 77]; # don't forget python indexing
            else:
                fuseStartIdx = [0, 3, 8, 14, 20, 28, 36, 44, 52, 60, 68, 69, 70, 71, 76, 77, 78];
                fuseEndIdx = [3, 8, 14, 20, 28, 36, 44, 52, 60, 68, 69, 70, 71, 76, 77, 78, 80]; # don't forget python indexing
            
            readBitArrayBinStr = ''.join(str(x) for x in readBitArrayBin);  
            print(readBitArrayBinStr)
            nFuseIdx = len(fuseStartIdx);
            
            for i in range(nFuseIdx):
                
                # Bg FBits
                if i == 0:
                    binStrBitsLsbFirst = readBitArrayBinStr[73] + readBitArrayBinStr[1] + readBitArrayBinStr[2];
                
                # vRef FBits    
                elif i == 1:
                    binStrBitsLsbFirst = readBitArrayBinStr[3] + readBitArrayBinStr[4] + readBitArrayBinStr[5] + readBitArrayBinStr[6] + readBitArrayBinStr[7] + readBitArrayBinStr[72];
                
                # Marking FBits
                elif i == 13:
                    binStrBitsLsbFirst = readBitArrayBinStr[0] + readBitArrayBinStr[71] + readBitArrayBinStr[74] + readBitArrayBinStr[75];
                
                else:
                    binStrBitsLsbFirst = readBitArrayBinStr[ fuseStartIdx[i] : fuseEndIdx[i] ];
                
                binStrBitsMsbFirst = F_reverseLrStr(binStrBitsLsbFirst);
                decVal = F_binToDec(binStrBitsMsbFirst);
                readBitArrayDec.append(decVal);
                
            readArray = [];
            for iStr in range(len(readBitArrayBinStr)):
                readArray.append(readBitArrayBinStr[iStr]);
            
            #print(readBitArrayBinStr);
        
        setBitArrayDec = self.trimBits.read.allTrimBitsLsbToMsb();
        
        ### Convert setBitsDec to setBitsBin
        BgMSB = F_decToBin(setBitArrayDec[0], 3);
        vRefMSB = F_decToBin(setBitArrayDec[1], 6);
        PtatUpMSB = F_decToBin(setBitArrayDec[2], 6);
        XDownMSB = F_decToBin(setBitArrayDec[3], 6);
        magLeftMSB = F_decToBin(setBitArrayDec[4], 8);
        magRightMSB = F_decToBin(setBitArrayDec[5], 8);
        electLeftMSB = F_decToBin(setBitArrayDec[6], 8);
        electRightMSB = F_decToBin(setBitArrayDec[7], 8);
        gainLeftMSB = F_decToBin(setBitArrayDec[8], 8);
        gainRightMSB = F_decToBin(setBitArrayDec[9], 8);
        polarMSB = F_decToBin(setBitArrayDec[10], 1);
        supplyMSB = F_decToBin(setBitArrayDec[11], 1);
        mTmodeMSB = F_decToBin(setBitArrayDec[12], 1);
        markingMSB = F_decToBin(setBitArrayDec[13], 4);
        tbbmDisMSB = F_decToBin(setBitArrayDec[14], 1);
        murataMSB = F_decToBin(setBitArrayDec[15], 1);
        swapMSB = F_decToBin(setBitArrayDec[16], 2);
        
        bit73 = BgMSB[-1];
        
        bit0 = markingMSB[-1];
        
        bit71 = markingMSB[2];
        bit74 = markingMSB[1];
        bit75 = markingMSB[0];
        
        bit72 = vRefMSB[0];
        
        finalBinaryBits = bit0  + F_reverseLrStr(BgMSB[0:2]) + F_reverseLrStr(vRefMSB[1:]) + F_reverseLrStr(PtatUpMSB) + \
                          F_reverseLrStr(XDownMSB) + F_reverseLrStr(magLeftMSB) + F_reverseLrStr(magRightMSB) + F_reverseLrStr(electLeftMSB) + \
                          F_reverseLrStr(electRightMSB) + F_reverseLrStr(gainLeftMSB) + F_reverseLrStr(gainRightMSB) + F_reverseLrStr(polarMSB) + \
                          F_reverseLrStr(supplyMSB) + F_reverseLrStr(mTmodeMSB) + bit71 + bit72 + bit73 + bit74 + bit75 + F_reverseLrStr(tbbmDisMSB) + \
                          F_reverseLrStr(murataMSB) + F_reverseLrStr(swapMSB);
        
        setArray = [];
        for iStr in range(len(finalBinaryBits)):
            setArray.append(finalBinaryBits[iStr]);
        
        #print(finalBinaryBits);
        
        if self.__nProgrammingBits__ == 77:
            setBitArrayDec = setBitArrayDec[0 : -2];
        #print("set and read decimals:" + repr([setBitArrayDec, readBitArrayDec]))
        return([setBitArrayDec, readBitArrayDec]);

        
    def sendBitsToAd2(self):
        
        inputParameters = self.__ad2InputParameters__();
        readBitArrayBin = F_analogDiscovery2Halo(inputParameters);
        nFuseElements = len(readBitArrayBin);
        readBitArrayDec = [];
        
        if nFuseElements > 0:
            
            if self.__nProgrammingBits__ == 77:
                fuseStartIdx = [0, 3, 8, 14, 20, 28, 36, 44, 52, 60, 68, 69, 70, 71, 76];
                fuseEndIdx = [3, 8, 14, 20, 28, 36, 44, 52, 60, 68, 69, 70, 71, 76, 77]; # don't forget python indexing
            else:
                fuseStartIdx = [0, 3, 8, 14, 20, 28, 36, 44, 52, 60, 68, 69, 70, 71, 76, 77, 78];
                fuseEndIdx = [3, 8, 14, 20, 28, 36, 44, 52, 60, 68, 69, 70, 71, 76, 77, 78, 80]; # don't forget python indexing
            
            readBitArrayBinStr = ''.join(str(x) for x in readBitArrayBin);            
            nFuseIdx = len(fuseStartIdx);
            
            for i in range(nFuseIdx):
                
                # Bg FBits
                if i == 0:
                    binStrBitsLsbFirst = readBitArrayBinStr[73] + readBitArrayBinStr[1] + readBitArrayBinStr[2];
                
                # vRef FBits    
                elif i == 1:
                    binStrBitsLsbFirst = readBitArrayBinStr[3] + readBitArrayBinStr[4] + readBitArrayBinStr[5] + readBitArrayBinStr[6] + readBitArrayBinStr[7] + readBitArrayBinStr[72];
                
                # Marking FBits
                elif i == 13:
                    binStrBitsLsbFirst = readBitArrayBinStr[0] + readBitArrayBinStr[71] + readBitArrayBinStr[74] + readBitArrayBinStr[75];
                
                else:
                    binStrBitsLsbFirst = readBitArrayBinStr[ fuseStartIdx[i] : fuseEndIdx[i] ];
                
                binStrBitsMsbFirst = F_reverseLrStr(binStrBitsLsbFirst);
                decVal = F_binToDec(binStrBitsMsbFirst);
                readBitArrayDec.append(decVal);
                
            readArray = [];
            for iStr in range(len(readBitArrayBinStr)):
                readArray.append(readBitArrayBinStr[iStr]);
            
            #print(readBitArrayBinStr);
        
        setBitArrayDec = self.trimBits.read.allTrimBitsLsbToMsb();
        
        ### Convert setBitsDec to setBitsBin
        BgMSB = F_decToBin(setBitArrayDec[0], 3);
        vRefMSB = F_decToBin(setBitArrayDec[1], 6);
        PtatUpMSB = F_decToBin(setBitArrayDec[2], 6);
        XDownMSB = F_decToBin(setBitArrayDec[3], 6);
        magLeftMSB = F_decToBin(setBitArrayDec[4], 8);
        magRightMSB = F_decToBin(setBitArrayDec[5], 8);
        electLeftMSB = F_decToBin(setBitArrayDec[6], 8);
        electRightMSB = F_decToBin(setBitArrayDec[7], 8);
        gainLeftMSB = F_decToBin(setBitArrayDec[8], 8);
        gainRightMSB = F_decToBin(setBitArrayDec[9], 8);
        polarMSB = F_decToBin(setBitArrayDec[10], 1);
        supplyMSB = F_decToBin(setBitArrayDec[11], 1);
        mTmodeMSB = F_decToBin(setBitArrayDec[12], 1);
        markingMSB = F_decToBin(setBitArrayDec[13], 4);
        tbbmDisMSB = F_decToBin(setBitArrayDec[14], 1);
        murataMSB = F_decToBin(setBitArrayDec[15], 1);
        swapMSB = F_decToBin(setBitArrayDec[16], 2);
        
        bit73 = BgMSB[-1];
        
        bit0 = markingMSB[-1];
        
        bit71 = markingMSB[2];
        bit74 = markingMSB[1];
        bit75 = markingMSB[0];
        
        bit72 = vRefMSB[0];
        
        finalBinaryBits = bit0  + F_reverseLrStr(BgMSB[0:2]) + F_reverseLrStr(vRefMSB[1:]) + F_reverseLrStr(PtatUpMSB) + \
                          F_reverseLrStr(XDownMSB) + F_reverseLrStr(magLeftMSB) + F_reverseLrStr(magRightMSB) + F_reverseLrStr(electLeftMSB) + \
                          F_reverseLrStr(electRightMSB) + F_reverseLrStr(gainLeftMSB) + F_reverseLrStr(gainRightMSB) + F_reverseLrStr(polarMSB) + \
                          F_reverseLrStr(supplyMSB) + F_reverseLrStr(mTmodeMSB) + bit71 + bit72 + bit73 + bit74 + bit75 + F_reverseLrStr(tbbmDisMSB) + \
                          F_reverseLrStr(murataMSB) + F_reverseLrStr(swapMSB);
        
        setArray = [];
        for iStr in range(len(finalBinaryBits)):
            setArray.append(finalBinaryBits[iStr]);
        
        #print(finalBinaryBits);
        
        if self.__nProgrammingBits__ == 77:
            setBitArrayDec = setBitArrayDec[0 : -2];
        
        return([setBitArrayDec, readBitArrayDec]);
        #return([setArray, readArray]);
    
    def connectSclkNode(self):
        if self.ad2Connect:
            self.__ad2__.powerSupply.positive.setVoltage(5);
            self.__ad2__.powerSupply.positive.enable();
        else:
            self.__pyb_halo2__.connect_sclk()
        
            
    def connectSdataNode(self):
        if self.ad2Connect:
            self.__ad2__.powerSupply.negative.setVoltage(-5);
            self.__ad2__.powerSupply.negative.enable();
        else:
             self.__pyb_halo2__.connect_sdata()
    
    def disconnectSclkNode(self):
        if self.ad2Connect:        
            self.__ad2__.powerSupply.positive.disable();    
            self.__sclkNode__ = False;
        else:
             self.__pyb_halo2__.disconnect_sclk()            
        
    def disconnectSdataNode(self):
        if self.ad2Connect:
            self.__ad2__.powerSupply.negative.disable();        
            self.__sdataNode__ = False;
        else:
             self.__pyb_halo2__.disconnect_sdata()              
    
    def __del__(self):
        if self.ad2Connect:        
            self.__ad2__.closeAd2();
        #if self.pyb_connect:
        #    self.__pyb_halo2__.close()            
        del(self);