# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 00:07:45 2018

@author: alongen
"""

from F_reverseLrStr import F_reverseLrStr
from haloBitNDV import haloBitNDV
from F_decToBin import F_decToBin
from F_twosComplement import F_twosComplement
import numpy as np
import sys

class testHaloBits():
    
    def __init__(self):
        
        # Hammer Test Bit Names
        self.__testVarNameArray__ = ['TEST00_leftAmpDis', 'TEST01_leftBridgeShort', 'TEST02_rightAmpDis', 'TEST03_rightBridgeShort', 'TEST04_overTempTermalTest', 
                                     'TEST05_disAfe', 'TEST06_testMuxInfra', 'TEST07_testMuxAfe', 'TEST08_testMuxInfraEn', 'TEST09_testMuxAfeEn'];

        # Hammer Test Bit Descriptions
        self.__testVarDescArray__ = ['(Code 1) Disables the Left Bridge Instrumentation Amplifier. It is placed in Hi-Z mode when disables and does not affect the output\n(Code 0) Enables the Left Bridge Instrumentation Amplifier',  
                                     '(Code 1) Shorts the Left Bridge Output. Used to test electronic Offset Adjustments by placing 0v differential on input to Left AFE Amplifier\n(Code 0) Unshorts the Left Bridge Output',
                                     '(Code 1) Disables the Right Bridge Instrumentation Amplifier. It is placed in Hi-Z mode when disables and does not affect the output\n(Code 0) Enables the Right Bridge Instrumentation Amplifier', 
                                     '(Code 1) Shorts the Right Bridge Output. Used to test electronic Offset Adjustments by placing 0v differential on input to right AFE Amplifier\n(Code 0) Unshorts the Right Bridge Output',
                                     '(Code 1) Activates OverTemperature Thermal Test\n(Code 0) Deactivates OverTemperature Thermal Test',
                                     '(Code 1) Disables AFE to allow current measurement of AFE\n(Code 0) Enable AFE to NOT allow current measurement of AFE',
                                     'Controls InfraStructure TestMUX which connects to Fault Pin (16 states)\n' + \
                                     '(Code 15) VZeroTempco from Bridge Regulator\n(Code 14) Monitor Vref1050: for OTemp Ref check\n(Code 13) Monitor VtempPtat: Monitor Ptat component of bridge regulator\n' + \
                                     '(Code 12) Monitor VrefOCN110: Monitor the Positive 110% Overcurrent Reference. This is 1100 mV for OT?  in Mode00\n(Code 11) Monitor VrefOCP90: Monitor the Positive 90% Overcurrent Reference\n' + \
                                     '(Code 10) VrefZeroI.  0.50 V, 0.65 V, 1.65 V, or 2.50 V  Use to trim reference voltages\n(Code 9) Monitor VrefOCN90: Monitor the Negative 90% Overcurrent Reference\n' + \
                                     '(Code 8) Monitor VrefOCN110: Monitor the Negative 110% Overcurrent Reference. This is 1100 mV for OT?  in Mode00\n(Code 7) Monitor the clock\n(Code 6) Monitor VBandgap: Monitor the raw bandgap reference\n' + \
                                     '(Code 5) Monitor AVCCL: Monitor the low voltage regulator for AFE\n(Code 4) Monitor AVCCBR: Monitor the bridge regulator voltage\n(Code 3) Enable OTempFaulMonitor Hi Temp Test\n' + \
                                     '(Code 2) Enable Room temp OTtest\n(Code 1) Enable PORtest/disable POR function\n(Code 0) DisableMux',
                                     'Controls AFE TestMUX which connects to Fault Pin (16 states)\n' + \
                                     '(Code 15) VrefOverCurrentPlus: Voltage Ref for Positive Overcurrent Comparator\n(Code 14) VrefOverCurrentMinus: Voltage Ref for Negative Overcurrent Comparator\n' + \
                                     '(Code 13) OverCurrent: Logical OR of Minus & Plus Overcurrent Digital Flags\n(Code 12) OverCurrentPlus: Comparator Output Digital Flag\n(Code 11) OverCurrentMinus: Comparator Output Digital Flag\n' + \
                                     '(Code 10) VrefOutReg: Buffered VrefZeroI. 0.50 V, 0.65 V, 1.65 V, or 2.50 V. Use to trim reference voltages\n(Code 9) Vnormal: Normalized +0.5 V or ±0.5 V swing node. Diff to SE Output Analog Overcurrent can be measured here. Overcurrent is not trimmable\n' + \
                                     '(Code 8) InPR4x: Positive output of Right Instrumentation Amplifier\n(Code 7) InNR4x: Negative output of Right Instrumentation Amplifier\n(Code 6) InPL4x: Positive output of Left Instrumentation Amplifier\n' + \
                                     '(Code 5) InNL4x: Negative output of Left Instrumentation Amplifier\n(Code 4) VinLbrP: Positive input from Left Bridge. Bridge sensitivity is measured at these 4 points\n' + \
                                     '(Code 3) VinLbrN: Negative input from Left Bridge. Bridge sensitivity is measured at these 4 points\n(Code 2) VinRbrP: Positive input from Right Bridge. Bridge sensitivity is measured at these 4 points\n' + \
                                     '(Code 1) VinRbrN: Negative input from Right Bridge. Bridge sensitivity is measured at these 4 points\n(Code 0) DisableMux',
                                     '(Code 1) Enables the InfraStructure Test MUX connected to Fault Pin\n(Code 0) Disables the InfraStructure Test MUX connected to Fault Pin',
                                     '(Code 1) Enables the AFE Test MUX connected to Fault Pin\n(Code 0) Disables the AFE Test MUX connected to Fault Pin'];
                                     
        # Halo Test Bit Values
        self.__testDefaultBitArray__ = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        
        # Halo Test Bit Sizes
        self.__testBitSizes__ = [1, 1, 1, 1, 1, 1, 4, 4, 1, 1];
        
        # Number of Halo Test Names
        self.__nTestNames__ = len(self.__testVarNameArray__);
        
        for i in range(self.__nTestNames__):
            
            self.tempClass = haloBitNDV(self.__testVarDescArray__[i], self.__testDefaultBitArray__[i], self.__testBitSizes__[i]);
            exec('self.' + self.__testVarNameArray__[i] + '=' + 'self.tempClass');

    
    def createBitStream(self):
        
        finalBitStream = [];
        for i in range(self.__nTestNames__):
            if sys.version_info[0] < 3: # Python 2.x            
                exec('self.tempDec' + '=' + 'int(self.' + self.__testVarNameArray__[i] + '.base10Val)');
                exec('self.tempBitSize' + '=' + 'int(self.' + self.__testVarNameArray__[i] + '.bitSize)');   
            else:
                exec('self.tempDec' + '=' + 'int(self.' + self.__testVarNameArray__[i] + '.base10Val)');
                exec('self.tempBitSize' + '=' + 'int(self.' + self.__testVarNameArray__[i] + '.bitSize)');   
            if np.sign(self.tempDec) == -1:
                binVal = F_twosComplement( F_decToBin(self.tempDec, self.tempBitSize) );
            else:
                binVal = F_decToBin(self.tempDec, self.tempBitSize);
            
            lsbToMsb = F_reverseLrStr(binVal);
            
            nBits = len(lsbToMsb);
            
            for j in range(nBits):
                finalBitStream.append( int(lsbToMsb[j]) );
       
        return(finalBitStream);