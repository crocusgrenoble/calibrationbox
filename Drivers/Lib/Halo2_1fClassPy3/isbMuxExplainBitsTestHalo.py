# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

class isbMuxExplainBitsTestHalo():
  
    def __init__(self, haloBits):
        
        self.__bits__ = haloBits;
        
    def mux(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);
        
    def porTest(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);
        
    def overTempTestAtRoomTemp(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);
        
    def overTempFaultMonitorAtHighTemp(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);
    
    def bridgeRegVolt(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);
    
    def afeLowVoltReg(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);
    
    def rawBgRef(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);
    
    def clk(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);
        
    def neg110PerOverCurrRef(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);       

    def neg90PerOverCurrRef(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);
        
    def zeroCurrVref(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);       
        
    def pos90PerOverCurrRef(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);
        
    def pos110PerOverCurrRef(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);       
        
    def bridgeRegPtat(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);
        
    def overTempRefCheck(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);     
        
    def zeroCurrTempCoFromBridgeReg(self):
    
        print(self.__bits__.test.TEST06_testMuxInfra.description);
        
    def isbMuxToFltPin(self):
    
        print(self.__bits__.test.TEST08_testMuxInfraEn.description);