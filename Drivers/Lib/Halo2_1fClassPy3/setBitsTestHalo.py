# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

from generalSetBitsTestHalo import generalSetBitsTestHalo
from isbMuxSetBitsTestHalo import isbMuxSetBitsTestHalo
from afeMuxSetBitsTestHalo import afeMuxSetBitsTestHalo

class setBitsTestHalo():
  
    def __init__(self, haloBits):
        
        self.__bits__ = haloBits;
    
        self.general = generalSetBitsTestHalo(self.__bits__);
        self.isbMux = isbMuxSetBitsTestHalo(self.__bits__);
        self.afeMux = afeMuxSetBitsTestHalo(self.__bits__);