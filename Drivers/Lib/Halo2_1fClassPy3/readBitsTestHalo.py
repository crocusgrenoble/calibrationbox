# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 08:23:41 2018

@author: alongen
"""

from generalReadBitsTestHalo import generalReadBitsTestHalo
from isbMuxReadBitsTestHalo import isbMuxReadBitsTestHalo
from afeMuxReadBitsTestHalo import afeMuxReadBitsTestHalo

class readBitsTestHalo():
  
    def __init__(self, setBits):
        
        self.__bits__ = setBits;
    
        self.general = generalReadBitsTestHalo(self.__bits__);
        self.isbMux = isbMuxReadBitsTestHalo(self.__bits__);
        self.afeMux = afeMuxReadBitsTestHalo(self.__bits__);