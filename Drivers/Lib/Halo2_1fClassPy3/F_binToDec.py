# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 15:56:35 2018

@author: alongen
"""

import sys

def F_binToDec(binVal):
    
    if type(binVal) != str:
        sys.exit('The input needs to be type string!');    
      
    decVal = int(binVal, 2);
    
    return decVal