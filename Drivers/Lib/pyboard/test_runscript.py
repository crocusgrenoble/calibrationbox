# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 10:32:28 2022

@author: eaerabi
"""

import Pyboard_lib
#try:
import time,sys
from pyboard import PyBoard
from pyboard_dvm import Pyboard_DVM
from pyboard_dut_ps import Pyboard_DUT_PS
v_ref = 5.03       
gain = 1
full_scale_output = 2**31
constant_of_adc_equation = v_ref/(gain*full_scale_output)
# =============================================================================
# pyb = Pyboard_lib.Pyboard('COM8', 115200)
# pyb.enter_raw_repl()
# =============================================================================
pyboard_1 = PyBoard(com_port = 'COM8', pcb_version='4.8')
pybrd_DUT_ps = Pyboard_DUT_PS(pyboard_1)
pybrd_dvm = Pyboard_DVM(pyboard_1)

pybrd_DUT_ps.set_voltage(5)
pybrd_DUT_ps.power_on()

time.sleep(1)
volt = pybrd_dvm.read_voltage()
print(volt)
pybrd_DUT_ps.power_off()
pyboard_1.close()
#ret = pyb.exec('import ads1263_init')
#ret = pyb.exec('out = run_script.run_script_file(\'ads1263_init\')')
#sys.exit(0)

pyb = Pyboard_lib.Pyboard('COM8', 115200)
pyb.enter_raw_repl()
for i in range(2):
    #...
    ret = pyb.exec('import run_script')
    
    ret = pyb.exec('out = run_script.run_script_file(\'dut_ps_5v_on\')')
    ret = pyb.exec('out = run_script.run_script_file(\'dut_ps_power_on\')')
    time.sleep(0.1)
    ret = pyb.exec('out = run_script.run_script_file(\'ads1263_init\')')
    ret = pyb.exec('print(out)')
    print(ret)
    output = ret[2:-4].split(b'\', \'')
    sentbytes = output[0].split(b'\\\\x')[4:]
    receivedbytes = output[1].split(b'\\\\x')[4:]   
    print(sentbytes)
    print(receivedbytes)
    ret = pyb.exec('out = run_script.run_script_file(\'ads1263_read_adc\')')
    ret = []
    ret = pyb.exec('print(out)')
    pyb.exec('out = run_script.run_script_file(\'dut_ps_power_off\')')
    print(ret)
    output = ret[3:-5].split(b'\', \'')
    # print("Raw out:",output[0])
    output_bytes = output[0].split(b'\\\\x')[2:6]
    #print(output_bytes)
    # Convert to bytes to string
    output_bytes = [x.decode('ascii') for x in output_bytes]
    # join 4 strings
    output_str = ''.join(output_bytes)
    print(output_str)
    # Convert ascii hex to bytes
    output_32bit_b = bytearray.fromhex(output_str)
    # Convert from bytes to integer
    output_32bit_int = int.from_bytes(output_32bit_b, byteorder='big', signed=True)
    diff_output = output_32bit_int*constant_of_adc_equation
    print(diff_output)
    pyb.exit_raw_repl()
    pyb.enter_raw_repl()
#time.sleep(2)
pyb.close()