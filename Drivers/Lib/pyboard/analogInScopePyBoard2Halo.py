# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 16:15:22 2019

@author: alongen
"""

from ctypes import *
from dwfconstants import *
import sys, numpy as np, time, matplotlib.pyplot as plt

class analogInScopeAnalogDiscovery2Halo():
    
    def __init__(self):#, dwf, hdwf):

        # self.__dwf__ = dwf;
        # self.__hdwf__ = hdwf;
        
        self.__availableChannels__ = [1, 2];
        self.__availableTriggerTypes__ = ['posPW', 'negPW', 'risEdge', 'falEdge'];
        
        self.__setupChannelSet__ = False;
        self.__acqFreqSet__ = False;
        self.__acqDurationSet__ = False;
        self.__triggerPositionSet__ = False;
        self.__timeoutSet__ = False;
        self.__triggerTypeSet__ = False;
        self.__triggerChannelSet__ = False;
        self.__triggerLevelSet__ = False;
        self.__triggerHysteresisSet__ = False;
        
    def setupChannel(self, ch, voltScale):

        voltScaleCheck = voltScale < 0 or voltScale > 5;
        if voltScaleCheck:
            sys.exit('Voltage scale needs to be between 0 (not inclusive) and 5 (inclusive)');
        
        self.__voltScale__ = voltScale;
        
        self.__channelCheck__(ch);
        
        self.__dwf__.FDwfAnalogInChannelEnableSet(self.__hdwf__, c_int(ch - 1), c_bool(True));
        
        self.__dwf__.FDwfAnalogInChannelRangeSet(self.__hdwf__, c_int(ch - 1), c_double(self.__voltScale__));
        
        self.__dwf__.FDwfAnalogInAcquisitionModeSet(self.__hdwf__, c_int(3)); # acqmodeRecord
        
        self.__setupChannelSet__ = True;
        
    def acqFreq(self, hzAcq):
        
        freqValCheck = hzAcq < 0 or hzAcq > 20e6;
        if freqValCheck:
            sys.exit('Acq Freq needs to be between 0 (not inclusive) and 20 MHz (inclusive)!');
        
        self.__hzAcq__ = hzAcq;
        
        self.__dwf__.FDwfAnalogInFrequencySet(self.__hdwf__, c_double(self.__hzAcq__));
        
        self.__acqFreqSet__ = True;
        
    def acqDuration(self, secAcq):
        
        durationValCheck = type(secAcq) == float or type(secAcq) == int;
        if not durationValCheck:
            sys.exit('Acq Duration needs to be in seconds as an int or float number!');
        
        self.__secAcq__ = secAcq;
        
        self.__nSamples__ = int(self.__secAcq__*self.__hzAcq__);
        
        self.__dwf__.FDwfAnalogInRecordLengthSet(self.__hdwf__, c_double(self.__secAcq__)); # -1 infinite record length
        
        self.__acqDurationSet__ = True;
        
    def triggerPosition(self, trigLoc):
        
        trigCheck = trigLoc < 0 or trigLoc > 1;
        if trigCheck:
            sys.exit('Trigger location needs to be between 0 and 1 (inclusive) as a float or int');
        
        self.__trigLoc__ = trigLoc;
        
        self.__dwf__.FDwfAnalogInTriggerPositionSet(self.__hdwf__, c_double(-1*self.__trigLoc__*self.__secAcq__)) # -0.25 = trigger at 25%
        
        self.__triggerPositionSet__ = True;
        
    def timeout(self, secTime):
        
        timeoutCheck = type(secTime) == float or type(secTime) == int;
        if not timeoutCheck:
            sys.exit('Timeout needs to be in seconds as an int or float number!');
        
        self.__dwf__.FDwfAnalogInTriggerAutoTimeoutSet(self.__hdwf__, c_double(secTime));
        
        self.__dwf__.FDwfAnalogInTriggerSourceSet(self.__hdwf__, c_ubyte(2)); # trigsrcDetectorAnalogIn
        
        self.__timeoutSet__ = True; 
    
    def triggerType(self, strType):
        
        trigTypeName = ['posPW', 'negPW', 'risEdge', 'falEdge'];
        
        trigTypeCheck = strType in trigTypeName;
        if not trigTypeCheck:
            sys.exit('The following trig types exist: "posPW", "risEdge", "negPW", and "falEdge"!');

        # Issue with trigger type idx values....try to solve later....        
        self.__dwf__.FDwfAnalogInTriggerTypeSet(self.__hdwf__, c_int(0)); # trigtypeEdge
        
        self.__triggerTypeSet__ = True;
    
    def triggerChannel(self, ch):
        
        self.__channelCheck__(ch);
        
        self.__dwf__.FDwfAnalogInTriggerChannelSet(self.__hdwf__, c_int(ch - 1)); # channel 1 or 2 (set value: 0 or 1)
    
        self.__trigChannel__ = ch;
        
        self.__triggerChannelSet__ = True;
    
    def triggerLevel(self, voltVal):
        
        trigLevelCheck = voltVal < -5 or voltVal > 5;
        if trigLevelCheck:
            sys.exit('Trig level needs to be between -5 and +5 V (inclusive) as a float or int');
    
        self.__dwf__.FDwfAnalogInTriggerLevelSet(self.__hdwf__, c_double(voltVal));
        
        self.__triggerLevelSet__ = True;
        
    def triggerHysteresis(self, hysVal):
        
        trigHysCheck = hysVal <=0 or hysVal > self.__voltScale__;
        if trigHysCheck:
            sys.exit('Hysteresis needs to be greater than 0 and less than the voltage scale!');
            
        self.__dwf__.FDwfAnalogInTriggerHysteresisSet(self.__hdwf__, c_double(hysVal));
        
        self.__dwf__.FDwfAnalogInTriggerConditionSet(self.__hdwf__, c_int(0));
        
        self.__triggerHysteresisSet__ = True;
    
    def startTrigger(self):
        
        self.__triggerReady__ = [self.__setupChannelSet__, self.__acqFreqSet__,  \
                                 self.__acqDurationSet__, self.__triggerPositionSet__, \
                                 self.__timeoutSet__, self.__triggerTypeSet__, self.__triggerChannelSet__, \
                                 self.__triggerLevelSet__, self.__triggerHysteresisSet__];
        
        trigCheckNames = ['Channel Setup', 'Acq Freq', 'Acq Duration', 'Trigger Position', 'Timeout', 'Trigger Type',
                          'Trigger Channel', 'Trigger Level', 'Trigger Hysteresis'];
                                 
        setIdx = [index for index, value in enumerate(self.__triggerReady__) if value == False];
        
        nIdx = len(setIdx);
        
        nameStr = '';
        for i in range(nIdx):            
            nameStr = nameStr + trigCheckNames[i] + ', ';
        
        nameStrLength = len(nameStr);
        
        if nameStrLength > 0:
            sys.exit(nameStr[ : -2] + ' have not been set yet!');
        
        [channelOneTime, channelOneData, channelTwoTime, channelTwoData] = self.__acqSamples__();
    
        return([channelOneTime, channelOneData, channelTwoTime, channelTwoData]);
    
    def __acqSamples__(self):

        rgdSamples = (c_double*self.__nSamples__)();
        rgdSamples2 = (c_double*self.__nSamples__)();
        sts = c_byte();
        cAvailable = c_int();
        cLost = c_int();
        cCorrupted = c_int();
        fLost = 0;
        fCorrupted = 0;
        
        iSample = 0;

        #time.sleep(2);

        # Trigger the Scope    
        self.__dwf__.FDwfAnalogInConfigure(self.__hdwf__, c_int(0), c_int(1));
        
        # Send out Main CLK and SDATA Signals
        self.__dwf__.FDwfAnalogOutConfigure(self.__hdwf__, c_int(-1), c_bool(True));

        while True:
            
            self.__dwf__.FDwfAnalogInStatus(self.__hdwf__, c_int(1), byref(sts));
            self.__dwf__.FDwfAnalogInStatusRecord(self.__hdwf__, byref(cAvailable), byref(cLost), byref(cCorrupted));
            
            iSample += cLost.value
            iSample %= self.__nSamples__
        
            if cLost.value:
                fLost = 1;
            if cCorrupted.value:
                fCorrupted = 1;
        
            iBuffer = 0
            while cAvailable.value>0:
                cSamples = cAvailable.value
                # we are using circular sample buffer, make sure to not overflow
                if iSample+cAvailable.value > self.__nSamples__:
                    cSamples = self.__nSamples__ - iSample
                    
                self.__dwf__.FDwfAnalogInStatusData2(self.__hdwf__, c_int(0), byref(rgdSamples, sizeof(c_double)*iSample), c_int(iBuffer), c_int(cSamples)); # get channel 1 data
                self.__dwf__.FDwfAnalogInStatusData2(self.__hdwf__, c_int(1), byref(rgdSamples2, sizeof(c_double)*iSample), c_int(iBuffer), c_int(cSamples)); # get channel 2 data
                iBuffer += cSamples;
                cAvailable.value -= cSamples;
                iSample += cSamples;
                iSample %= self.__nSamples__;
        
            if sts.value == 2 : # done
                break
    
        channelOneData = [0.0]*len(rgdSamples);
        channelOneTime = np.linspace(0, self.__secAcq__, self.__nSamples__);
        
        for i in range(0, len(channelOneData)):
            channelOneData[i] = rgdSamples[(i+iSample)%self.__nSamples__];
        
        channelTwoData = [0.0]*len(rgdSamples2);
        channelTwoTime = np.linspace(0, self.__secAcq__, self.__nSamples__);
        
        for i in range(0, len(channelOneData)):
            channelTwoData[i] = rgdSamples2[(i+iSample)%self.__nSamples__];
            
        plt.plot(channelOneData);
        plt.show();
        plt.plot(channelTwoData);
        
        #if fLost:
        #    print "Samples were lost! Reduce frequency";
        #if fCorrupted:
        #    print "Samples could be corrupted! Reduce frequency";
        
        return([channelOneTime, channelOneData, channelTwoTime, channelTwoData]);
    
    def __channelCheck__(self, adChannel):
    
        channelCheck = adChannel in self.__availableChannels__;
        
        if not channelCheck:
            sys.exit('Channel is not available!');