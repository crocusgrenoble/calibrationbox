# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 15:09:16 2021

@author: eaerabi
"""


class Pyboard_TEST:
    def __init__(self, pybrd):
        self.__pyboard__ = pybrd
        self.__pyboard__.run_script_file('test_init')
        
    def __del__(self):
        del(self)