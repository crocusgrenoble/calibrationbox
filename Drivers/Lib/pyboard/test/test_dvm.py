# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 08:25:31 2021

@author: eaerabi
"""
import keyboard  # using module keyboard
from pyboard import PyBoard
from pyboard_dvm import Pyboard_DVM
try :
    pyboard1.serial.close()
except NameError:
    ...
else:    
    del pyboard1
pyboard1 = PyBoard(com_port='COM5', pcb_version='1.0')
dvm = Pyboard_DVM(pyboard1)

try:
    while True:
        voltage = dvm.read_voltage()
        print(voltage)
except KeyboardInterrupt:
    pyboard1.serial.close()
    pass
# while True:  # making a loop
#     try:  # used try so that if user pressed other than the given key error will not be shown
#         if keyboard.is_pressed('q'):  # if key 'q' is pressed 
#             print('You Pressed A Key!')
#             break  # finishing the loop
#     except:

#         break  # if user pressed a key other than the given key the loop will break

