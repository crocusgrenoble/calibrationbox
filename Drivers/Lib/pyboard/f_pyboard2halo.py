# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 16:18:00 2019

@author: alongen
"""

import sys, numpy as np, time

def F_pyBoard2Halo(inputParameters):

    nBits = inputParameters[0];
    nSamplesPerHalfPeriod = inputParameters[1];
    clkFreq = inputParameters[2];
    clkChannel = inputParameters[3];
    vddMax = inputParameters[4];
    sdataChannel = inputParameters[5];
    clkEdgeStr = inputParameters[6];
    dataBitArray = inputParameters[7];
    opCodeArray = inputParameters[8];
    pyboard = inputParameters[9];
    
    readFuseMode = opCodeArray == [1, 1];
    print(repr(inputParameters))
    ad2.analogOut.sclk(nBits, nSamplesPerHalfPeriod, clkFreq, clkChannel, vddMax, dataBitArray);

    ad2.analogOut.sdata(sdataChannel, clkEdgeStr, dataBitArray);

    readBackData = [];

    if readFuseMode:
        
        [channelOneTime, channelOneData, channelTwoTime, channelTwoData] = F_setupAd2AnalogScope(ad2, vddMax, clkFreq);
        
        # Analyze the Fuse Bits
        sclkDigitizedSignal = F_digitizeSignalWithHysteresis(channelOneData, 40, 60);
        sdataDigitizedSignal = F_digitizeSignalWithHysteresis(channelTwoData, 40, 60);
        
        [rEdgeSclkIdx, fEdgeSclkIdx] = F_digitalSignalEdgeIndices(sclkDigitizedSignal);
        [rEdgeSdataIdx, fEdgeSdataIdx] = F_digitalSignalEdgeIndices(sdataDigitizedSignal);
        
        # Fuse States Start at 
        sClkIdx = 27;
        
        # Fuse States End at 
        eClkIdx = nBits - 1;
        nFuses = nBits - 27;
        
        nRiseEdgeSclk = len(rEdgeSclkIdx);
        nFallEdgeSclk = len(fEdgeSclkIdx);
        
        clkEdgeCheck = (nRiseEdgeSclk >= eClkIdx) and (nFallEdgeSclk >= eClkIdx);
        
        if not clkEdgeCheck:
            sys.exit('Readback of fuses cannot be done because there are less \n than ' + str(eClkIdx) + ' rising and/or falling SCLK edges detected by AD2 scope!');
        
        minPointsBetweenRiseFallEdges = int( 0.5*min( np.array(fEdgeSclkIdx) - np.array(rEdgeSclkIdx) ) );
        
        for i in range(sClkIdx, (eClkIdx + 1) ):
            
            sCurrIdx = rEdgeSclkIdx[i] + minPointsBetweenRiseFallEdges;
            
            eCurrIdx = fEdgeSclkIdx[i] + minPointsBetweenRiseFallEdges + 1; # don't forget python's end indexing!
            
            # Check if Fuse is 1 or 0!
            fuseBit = sdataDigitizedSignal[sCurrIdx : eCurrIdx];
            
            # Check if all index values of fuse bit state are 1 or not!
            fuseBitState = all(fuseBit);
            
            if fuseBitState:
                readBackData.append(1);
            else:
                readBackData.append(0);
                
        nFusesRead = len(readBackData);
        fuseBitReadBackCheck = nFusesRead == nFuses;
        
        if not fuseBitReadBackCheck:
            sys.exit('There were only ' + str(nFusesRead) + ' fuses read, but ' + str(nFuses) + 'should have been read back!!');
                
    else:

        time.sleep(0.5);
        ad2.analogOut.trigger();
        
    del(ad2);
    
    return(readBackData);


