# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 15:05:31 2021

@author: eaerabi
"""
import serial
import time
import os
import sys 
import Pyboard_lib
class PyBoard:
    def __init__(self, com_port, pcb_version='4.8'):
        self.pcb_version = pcb_version
#        try:
        print(com_port)
        #self.serial = serial.Serial(com_port, baudrate=115200, timeout=1)
        self.pyb = Pyboard_lib.Pyboard(com_port, 115200)
        self.pyb.enter_raw_repl()
            
        # except:
            # print("\r\n\r\n\r\nCannot open serial port\r\n\r\n\r\n")
            # sys.exit()    
        # self.path = os.path.dirname(os.path.abspath(__file__))
        
        # Init pyBoard
#        self.run_script('\4')
        
        #time.sleep(0.1)
        #file_path = os.path.join(self.path, 'scripts', 'pyboard_init.py')
        self.run_script('import run_script')
        #print('running pyboard init')
        self.run_script_file('pyboard_init')

    # def __del__(self):
        # self.pyb.close()
        # del(self)

    def close(self):
        self.pyb.close()     
        
    def run_script_file(self, textfile):
        #print("going to run the script file :", textfile)
        self.pyb.exit_raw_repl()
        self.pyb.enter_raw_repl() 
        self.run_script('import run_script')
        ret = self.pyb.exec('out = run_script.run_script_file(\''+textfile+'\')')        
        ret = self.pyb.exec('print(out)')
        #print(ret)
        #output = self.run_script("print(output)")
        output_bytes = ret[2:-4].split(b'\', \'') # Separate output parts and remove beginning and end chars 
        return output_bytes        
        # Using readlines()
# =============================================================================
#         self.serial.flushInput()
#         self.serial.timeout  = 0.05 # make readline faster
#         file_path = os.path.join(self.path, 'scripts', textfile)
#         # print(file_path)
#         file1 = open(file_path, 'r')
#         lines = file1.readlines()
#         output = []  
#         self.serial.isOpen()
#         for line in lines:
#             line = line.lstrip()
#             if not line:
#                 continue
#             if (line[0] == '#'):
#                 continue
#             if (line[0] == "*"):
#                 self.serial.flushOutput()
#                 self.serial.flushInput()
#                 b = bytearray()
#                 b.extend(map(ord, line[1:]+"\r\n"))
#                 self.serial.write(b)
#                 self.serial.flushOutput()
#                 nTimeout = 0
#                 while(1): 
#                     tdata = self.serial.readline()  # Wait forever for anything
#                     if(tdata):
#                         output.append(tdata)
#                     else:
#                         nTimeout +=1
#                         if(nTimeout>2):
#                             break
#                     
#             else:
#                 b = bytearray()
#                 b.extend(map(ord, line+"\r\n"))
#                 self.serial.write(b)
#                 self.serial.flushOutput()
#                 time.sleep(0.1)
#                 self.serial.flushInput()
#        # self.serial.close()
#         self.serial.timeout  = 1
#         return output
# =============================================================================

    def run_script(self, script):
        ret = self.pyb.exec(script)
        return ret
# =============================================================================
#         # Using readlines()
#         self.serial.flushInput()        
#         self.serial.timeout  = 0.05 # make readline faster
#         buf = io.StringIO(script)
#         lines = buf.readlines()
#         output = []  
#         self.serial.isOpen()
#         for line in lines:
#             line = line.lstrip()
#             if not line:
#                 continue
#             if (line[0] == '#'):
#                 continue
#             if (line[0] == "*"):
#                 self.serial.flushOutput()
#                 self.serial.flushInput()
#                 b = bytearray()
#                 b.extend(map(ord, line[1:]+"\r\n"))
#                 self.serial.write(b)
#                 self.serial.flushOutput()
#                 nTimeout = 0
#                 while(1):
#                     tdata = self.serial.readline()  # Wait forever for anything
#                     if(tdata):
#                         output.append(tdata)
#                     else:
#                         nTimeout+= 1
#                         if(nTimeout>2):
#                             break
#             else:
#                 b = bytearray()
#                 b.extend(map(ord, line+"\r\n"))
#                 self.serial.write(b)
#                 # self.serial.flushOutput()
#                 time.sleep(0.1)
#         self.serial.timeout  = 1 
#         return output
# =============================================================================

