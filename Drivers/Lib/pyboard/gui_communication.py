# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 10:36:21 2021

@author: eaerabi
"""
import time

class GUI_Communication:
    def __init__(self, callbackFunc, gui_communication,wait_for_gui):
        if gui_communication:
            self.gui_exist= True
            self.gui_comm = gui_communication
            self.gui_comm.myGUI_signal.connect(callbackFunc)         
            self.progress = 0
            self.set_progress(self.progress)
            self.gui_wait_event = wait_for_gui
        else:
            self.gui_exist = False
        
    def set_progress(self, progress):
        if not self.gui_exist:
            return
        self.progress = progress
        self.gui_comm.myGUI_signal.emit({"progress" : self.progress})

    def increase_progress(self, incr):
        if not self.gui_exist:
            return
        self.progress += incr
        self.gui_comm.myGUI_signal.emit({"progress" : self.progress})

    def send_message_to_gui(self, msg):
        if not self.gui_exist:
            return
        self.gui_comm.myGUI_signal.emit({"msg":msg})        

    def send_error_to_gui(self, msg):
        if not self.gui_exist:
            return
        self.gui_comm.myGUI_signal.emit({"error":msg})        

    def send_successful_end_to_gui(self, msg):
        if not self.gui_exist:
            return
        self.gui_comm.myGUI_signal.emit({"success":msg})  
        
    def wait_for_next_button(self, msg):
        if not self.gui_exist:
            return
        self.gui_comm.myGUI_signal.emit({"next":msg})
        self.gui_wait_event.clear()        
        self.gui_wait_event.wait()
        time.sleep(1);
    
    def continue_process(self, msg):
        if not self.gui_exist:
            return
        self.gui_comm.myGUI_signal.emit({"continue":msg}) 
        
    def __del__(self):
        del(self)