# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 15:02:08 2021

@author: eaerabi
"""
from pyboard import PyBoard

import time
class Pyboard_DVM:
    def __init__(self, pybrd):
        v_ref = 5.042 
        gain = 1
        full_scale_output = 2**31
        self.constant_of_adc_equation = v_ref/(gain*full_scale_output)        
        self.__pyboard__ = pybrd        
        #Old board Vref = 4.798232139519, NewBoard(com12)Vref= 5.042, com13 Vref=5.024
        self.v_ref = 5.042
        self.gain = 1
        self.full_scale_output = 2**31
        self.constant_of_adc_equation = self.v_ref/(self.gain*self.full_scale_output)
        while(1):
            output = self.__pyboard__.run_script_file('ads1263_init')
            #print(output)
            # Verify the the written and read config
            try:
                # print("Raw out:",output[0])
                #print(output)
                sentbytes = output[0].split(b'\\\\x')[4:]
                receivedbytes = output[1].split(b'\\\\x')[4:]
                #print(sentbytes)
                #print(receivedbytes)
                
            except:
                print("ADC does not reply properly:  \r\n", output)
                continue
            if(sentbytes == receivedbytes):
                print('Config ads1263 succeeded !')
                break
            else:
                print('Config ads1263 failed:\r\n Written: %s \r\n Read: %s\
                      ' % (output[1], output[0]))

    def read_voltage(self):
        
        #self.__pyboard__.run_script_file('ads1263_init')
        ret = self.__pyboard__.run_script_file('ads1263_read_adc')
        try:
            output = ret[0]
        except:
            print("ADC does not reply properly:  \r\n", output)
            return 0        
        #print(output)
        #output = output[3:-5].split(b'\', \'')
        #print("Raw out:",output)
        output_bytes = output.split(b'\\\\x')[2:6]
        #print(output_bytes)
        # Convert to bytes to string
        output_bytes = [x.decode('ascii') for x in output_bytes]
        # join 4 strings
        output_str = ''.join(output_bytes)
        #print(output_str)
        # Convert ascii hex to bytes
        output_32bit_b = bytearray.fromhex(output_str)
        # Convert from bytes to integer
        output_32bit_int = int.from_bytes(output_32bit_b, byteorder='big', signed=True)
        diff_output = output_32bit_int*self.constant_of_adc_equation
        
        return diff_output

    def averageDvm(self, nReads):
        maxim = 0
        minim = 0
        mean = 0
        minim = self.read_voltage()        
        maxim = minim
        mean = minim        
        for i in range(1, nReads):
            #print(i)
            #time.sleep(0.001)            
            v = self.read_voltage()
            #print(v)
            if v < minim:
                minim = v
            if v > maxim:
                maxim = v
            mean += v
        mean = mean / nReads        
        return [minim, mean, maxim]

    def __del__(self):
        del(self)