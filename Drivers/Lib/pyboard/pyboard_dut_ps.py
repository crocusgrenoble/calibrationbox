# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 15:09:16 2021

@author: eaerabi
"""
class Pyboard_DUT_PS:
    def __init__(self, pybrd):
        self.__pyboard__ = pybrd
        self.__pyboard__.run_script_file('dut_ps_init')

    def power_off(self):
        self.__pyboard__.run_script_file('dut_ps_power_off')

    def power_on(self):
        self.__pyboard__.run_script_file('dut_ps_power_on')

    def set_voltage(self, voltage):
                
        if(voltage == 3.3):
            self.__pyboard__.run_script_file('dut_ps_3v3_on')
        elif (voltage == 4):
            self.__pyboard__.run_script_file('dut_ps_4v_on')
        elif (voltage == 5):
            self.__pyboard__.run_script_file('dut_ps_5v_on')
        else:
            print("Voltage supply of %f is not supported in this PyBoard!! \r\n"%(voltage))
            
    def read_current():
        return 0
    def __del__(self):
        del(self)
