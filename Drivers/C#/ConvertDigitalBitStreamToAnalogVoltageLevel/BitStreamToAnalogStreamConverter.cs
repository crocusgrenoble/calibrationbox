﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertDigitalBitStreamToAnalogStream
{
    public class BitStreamToAnalogStreamConverter
    {
        double _level;
        public BitStreamToAnalogStreamConverter(double VoltageLevel) {
            _level = VoltageLevel;
        }

        public double[] Convert(bool[] Bitstream)
        {
            double[] AnalogStream = new double[Bitstream.Length+2];
            for (int i = 0; i < Bitstream.Length; i++)
            {
                if (Bitstream[i])
                { AnalogStream[i] = (3.3); }
                else { AnalogStream[i] = (0); }
            }


            return AnalogStream;
        }
    }
}
