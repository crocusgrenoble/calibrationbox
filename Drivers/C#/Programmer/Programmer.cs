﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programmer
{
    public interface Programmer
    {
        double ReadDeviceAnalogOutput();
        string ReadoutDeviceBits();

        bool WriteToDevice(string bits);
        bool SetVdd(double V);

        bool VddON();
        bool VddOFF();

    }

    public class CTC4000: Programmer
    {

        public bool Programmer() { return false; }
        public double ReadDeviceAnalogOutput() { return 0; }
        public string ReadoutDeviceBits() { return ""; }

        public bool WriteToDevice(string bits) { return false; }
        public bool SetVdd(double V) { return false; }

        public bool VddON() { return false; }
        public bool VddOFF() { return false; }

    }
}
