﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace ConvertAnalogEdgesToDigitalBits
{
    public class AnalogEdgesToDigitalBitConvertor
    {

        double[] _bitstream_clk;
        double[] _bitstream_data;
        private double _highClkThreshold ;

        public AnalogEdgesToDigitalBitConvertor()
        {
            _bitstream_clk = new double[32]{ 5, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0 };
            _bitstream_data = new double[32]{ 0, 3, 3, 0, 0, 0, 0, 3, 3, 3, 3, 0, 0, 0, 0, 3, 3, 0, 0, 3, 3, 0, 0, 3, 3, 0, 0, 3, 3, 3, 3, 0 };
            _highClkThreshold = 4;// 4 volts
        }
        public int CountNumberofBits(double[] bitarray)
        {
            int count = 0;
            foreach (double b in bitarray) {
                count++;
            }
            return count;
        }
        public bool[] ReadBitsFromAnalogInput(double[] clockArray, double[] dataArray)
        {
            List<string> msgs = new List<string>();
            List<bool> tempDataList = new List<bool>();
            bool[] outputData;
            var clockAndData = clockArray.Zip(dataArray, (c, d) => new { Clock = c, Data = d });
            bool highClockDetected = false;
            bool clkState = false;// 0: wait for risging edge, 1: wait for fallign edge
            int numberOfBits = 0;
            foreach (var cd in clockAndData)
            {
                msgs.Add(string.Format("Reading a couple of clk={0} and data={1}", cd.Clock, cd.Data));
                if (highClockDetected == false)
                {
                    if (cd.Clock > _highClkThreshold) // High voltage rising dge clk detected
                    {
                        msgs.Add(string.Format("high clock detected {0}", cd.Clock));

                        highClockDetected = true;
                        clkState = true;
                    }
                }
                else // High voltage rising dge clk already seen
                {
                    if (clkState == false) // if waiting for rising edge 
                    {
                        if (cd.Clock > 2.7)// Normal clk rising edge is detected
                        {
                            msgs.Add(string.Format("clock rise detected clk={0} and data={1}", cd.Clock, cd.Data));
                            if (cd.Data > 2.5)// Data has '1' Level
                            {
                                tempDataList.Add(true);
                                msgs.Add(string.Format("Detected digital value ={0}", true));
                            }
                            else
                            {
                                // Data has '1' Level
                                tempDataList.Add(false);
                                msgs.Add(string.Format("Detected digital value ={0}", false));
                            }
                            numberOfBits++;
                            clkState = true;// Waiting for falling edge from now on.
                        }

                    }
                    else// if waiting for falling edge 
                    {
                        if (cd.Clock < 2.2)     // Normal clk falling edge is detected
                        {
                            clkState = false;
                        }
                    }
                }
            }//foreach
            outputData = tempDataList.ToArray();
            msgs.Add(string.Format("Extracted data ={0}", string.Join(", ", outputData)));

            string[] log = msgs.ToArray();
            //File.WriteAllLines("log.txt", log);
            return outputData;
        }

    }
}
