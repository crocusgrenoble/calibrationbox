﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.CompilerServices.RuntimeHelpers;
//using CalibrationSfotware;
using Device;
namespace ConsoleApp1
{
    internal class Program
    {
        static void ErrorHanlderCallBack(String S, Device.reportLevel l)
        {
            Console.WriteLine(S);
        }
        static void Main(string[] args)
        {
            //CurrentSupply.GPIBCurrentSupply CurrSupply= new CurrentSupply.GPIBCurrentSupply();
            //CalibrationSfotware.HighLevelTrimming _TrimmingObj;
            Device.HaloTranslationLayer device = new Device.HaloTranslationLayer("Labview", "5v0", "U", "8mT", "noSwap");//, ErrorHanlderCallBack);
            device.EnterReadbackMode();
            bool[] temp = new bool[107];
            temp[26 + 20] = true;
            temp[26 + 21] = true;
            temp[26 + 22] = true;
            temp[26 + 23] = true;
            temp[26 + 24] = false;
            temp[26 + 25] = true;
            temp[26 + 26] = true;
            temp[26 + 27] = true;

            //device.ParseAndUpdateFromBitStream(temp);
            device.SetVrefScaledBandgapTrim(31);
            int vrefcode = device.GetVrefScaledBandgapTrim();
            //device.ConnectVrefToOUT();
            device.EnterReadbackMode();
            temp = device.GenerateFinalBitstream();
            device.SetRightAmpGain(240);
            device.EnterTryBeforeBuyMode();
            temp = device.GenerateFinalBitstream();
            device.DetermineTheGainBitAndMarkingBits(4, 3.323);
            device.DetermineTheNextVrefCode(0.1, 10, 1.0, 3.0);
            //Console.WriteLine(device.GenerateParsedTrimmingString());
            string st = device.ConvertBitStreamToString(device.GenerateFinalBitstream());
            Console.WriteLine(st);
            int[] gainCodes  = device.InitializeGainTrimming(4, 4.6, 0.4);
            gainCodes = device.DetermineTheNextGainCode(0.1,gainCodes[0], gainCodes[1],4.55,0.45,4);
            device.ParseAndUpdateFromDecimalParsedTrimmingString("3,44,52,52,146,137,133,133,246,246,False,True,False,10,False,False,0");
            st = device.GenerateDecimalParsedTrimmingString();
            device.SetBridgeSwap("leftAndRightSwap");
            //_TrimmingObj = new CalibrationSfotware.HighLevelTrimming("CTC4000", "3v3", "U", "8mT", ErrorHanlderCallBack, CurrSupply);
            BitArray KeyCode = new BitArray(new bool[8] { true, true, true, true, false, false, false, false });
            byte[] b = new byte[1];
            byte a;
            KeyCode.CopyTo(b, 0);
            int s = -5;
            a = (byte)s;
            b[0] = (byte)(b[0] + a);
            BitArray Barr = new BitArray( new byte[] { b[0] });
            BitArray BarrSub = new BitArray(5);//= Barr[:5];
            int[] arr = new int[8];
            Barr.Length = BarrSub.Length;
            
            Console.WriteLine(b.ToString());
            Console.WriteLine(KeyCode.ToString());
        }

    }
}
