﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Device
{
    internal class HaloBitLayer
    {
        public BitArray KeyCode;
        public BitArray OpCode;
        /*
         *    ## Trim Bits ##
        vRefBgCornerTrim: [CROCUS FACTORY]\nBandgap Curvature Trim: All chips on a wafer will have the same trim code. Default = 0. May need trim to compensate for process variations',
        vRefScaledBandgapTrim: 'Trims the voltage reference VrefOut which determines the ZeroCurrentVoltage at the output. This is verified using the test mux. Default = 0 will cause a small error that needs trimming',
       
        vRegPtatUp: '[CROCUS FACTORY]\nTrims the ptat component of the bridge regulator voltage. Increasing codes result in stronger ptat or a stronger temperature compensation. Default = 0 will have no temp compensation slope',
        vRegBgapXdown: '[CROCUS FACTORY]\nTrims the fixed voltage component of the bridge regulator. Increasing codes reduce the fixed voltage component of the bridge regulator voltage. Fixed voltage means no change with temperature. Default = 0 is a fixed voltage',
       
        'Trims the Magnetic offset of the Left AFE up/down',
        'Trims the Magnetic offset of the Right AFE up/down',
        'Trims the Electronic offset of the Left AFE up/down',
        'Trims the Electronic offset of the Right AFE up/down',
        'Trims the gain of the Left AFE up/down',
        'Trims the gain of the Right AFE up/down',
        '(Code 0) BiPolar\n(Code 1) UniPolar',
        '(Code 0) 3.3V\n(Code 1) 5.0V',
        '(Code 0) 8 mT (20A Abs Max)\n(Code 1) 12mT (30A Abs Max)',
        'No electrical effect',
        '[Anti-Hack Feature]\n(Code 0) Allows access to Try-Before-Buy\n(Code 1) Prevents access to Try-Before-Buy',
        '[Crocus Vs Murata Chip Mode]\n(Code 0) Crocus Chip Mode - Will NOT ignore "TRIM15_crousMurataModes" and "TRIM16_bridgeSwaps"\n(Code 1) Murata Chip Mode - Will ignore "TRIM15_crousMurataModes" and "TRIM16_bridgeSwaps"',
        '(Code 0) Left Bridge Default & Right Bridge Default\n(Code 1) Left Bridge Flipped & Right Bridge Default\n(Code 2) Left Bridge Default & Right Bridge Flipped\n(Code 3) Left Bridge Flipped & Right Bridge Flipped'
         */

        // Trimm Bits
        public bool FactoryBit;

        public BitArray vRefBgCornerTrim;
        public BitArray vRefScaledBandgapTrim;
        public BitArray vRefPtatUp;
        public BitArray vRefBgapXdown;
        public BitArray vosMagLeft;
        public BitArray vosMagRight;
        public BitArray vosElectLeft;
        public BitArray vosElectRight;
        public BitArray gainInstrAmpsLeft;
        public BitArray gainInstrAmpsRight;
        public bool gainUniPolar;
        public bool gainHiSupply;
        public bool gainFor12mT;
        public BitArray MarkingBits;
        public bool BlockTryBeforeBuyHi;
        public bool CrousMurataModes;
        public BitArray BridgeSwaps;




        /*
        /*
         *  ## Test Bits ##
         '(Code 1) Disables the Left Bridge Instrumentation Amplifier. It is placed in Hi-Z mode when disables and does not affect the output\n(Code 0) Enables the Left Bridge Instrumentation Amplifier',  
        
        '(Code 1) Shorts the Left Bridge Output. Used to test electronic Offset Adjustments by placing 0v differential on input to Left AFE Amplifier\n(Code 0) Unshorts the Left Bridge Output',
        
        '(Code 1) Disables the Right Bridge Instrumentation Amplifier. It is placed in Hi-Z mode when disables and does not affect the output\n(Code 0) Enables the Right Bridge Instrumentation Amplifier', 
        
        '(Code 1) Shorts the Right Bridge Output. Used to test electronic Offset Adjustments by placing 0v differential on input to right AFE Amplifier\n(Code 0) Unshorts the Right Bridge Output',
        
        '(Code 1) Activates OverTemperature Thermal Test\n(Code 0) Deactivates OverTemperature Thermal Test',
        
        '(Code 1) Disables AFE to allow current measurement of AFE\n(Code 0) Enable AFE to NOT allow current measurement of AFE',
        
        'Controls InfraStructure TestMUX which connects to Fault Pin (16 states)\n' + \
        '(Code 15) VZeroTempco from Bridge Regulator\n(Code 14) Monitor Vref1050: for OTemp Ref check\n(Code 13) Monitor VtempPtat: Monitor Ptat component of bridge regulator\n' + \
        '(Code 12) Monitor VrefOCN110: Monitor the Positive 110% Overcurrent Reference. This is 1100 mV for OT?  in Mode00\n(Code 11) Monitor VrefOCP90: Monitor the Positive 90% Overcurrent Reference\n' + \
        '(Code 10) VrefZeroI.  0.50 V, 0.65 V, 1.65 V, or 2.50 V  Use to trim reference voltages\n(Code 9) Monitor VrefOCN90: Monitor the Negative 90% Overcurrent Reference\n' + \
        '(Code 8) Monitor VrefOCN110: Monitor the Negative 110% Overcurrent Reference. This is 1100 mV for OT?  in Mode00\n(Code 7) Monitor the clock\n(Code 6) Monitor VBandgap: Monitor the raw bandgap reference\n' + \
        '(Code 5) Monitor AVCCL: Monitor the low voltage regulator for AFE\n(Code 4) Monitor AVCCBR: Monitor the bridge regulator voltage\n(Code 3) Enable OTempFaulMonitor Hi Temp Test\n' + \
        '(Code 2) Enable Room temp OTtest\n(Code 1) Enable PORtest/disable POR function\n(Code 0) DisableMux',
        
        'Controls AFE TestMUX which connects to Fault Pin (16 states)\n' + \
        '(Code 15) VrefOverCurrentPlus: Voltage Ref for Positive Overcurrent Comparator\n(Code 14) VrefOverCurrentMinus: Voltage Ref for Negative Overcurrent Comparator\n' + \
        '(Code 13) OverCurrent: Logical OR of Minus & Plus Overcurrent Digital Flags\n(Code 12) OverCurrentPlus: Comparator Output Digital Flag\n(Code 11) OverCurrentMinus: Comparator Output Digital Flag\n' + \
        '(Code 10) VrefOutReg: Buffered VrefZeroI. 0.50 V, 0.65 V, 1.65 V, or 2.50 V. Use to trim reference voltages\n(Code 9) Vnormal: Normalized +0.5 V or ±0.5 V swing node. Diff to SE Output Analog Overcurrent can be measured here. Overcurrent is not trimmable\n' + \
        '(Code 8) InPR4x: Positive output of Right Instrumentation Amplifier\n(Code 7) InNR4x: Negative output of Right Instrumentation Amplifier\n(Code 6) InPL4x: Positive output of Left Instrumentation Amplifier\n' + \
        '(Code 5) InNL4x: Negative output of Left Instrumentation Amplifier\n(Code 4) VinLbrP: Positive input from Left Bridge. Bridge sensitivity is measured at these 4 points\n' + \
        '(Code 3) VinLbrN: Negative input from Left Bridge. Bridge sensitivity is measured at these 4 points\n(Code 2) VinRbrP: Positive input from Right Bridge. Bridge sensitivity is measured at these 4 points\n' + \
        '(Code 1) VinRbrN: Negative input from Right Bridge. Bridge sensitivity is measured at these 4 points\n(Code 0) DisableMux',
        
        '(Code 1) Enables the InfraStructure Test MUX connected to Fault Pin\n(Code 0) Disables the InfraStructure Test MUX connected to Fault Pin',
        
        '(Code 1) Enables the AFE Test MUX connected to Fault Pin\n(Code 0) Disables the AFE Test MUX connected to Fault Pin'
         */
        //Test Bits
        public bool leftAmpDis;
        public bool leftBridgeShort;
        public bool rightAmpDis;
        public bool rightBridgeShort;
        public bool overTempTermalTest;
        public bool disAfe;
        public BitArray testMuxInfra;
        public BitArray testMuxAfe;
        public bool testMuxInfraEn;
        public bool testMuxAfeEn;

        public HaloBitLayer(  bool gainHiSupply, bool gainUniPolar, bool gainFor12mT, BitArray bridgeSwap)
        {
           ///bool[] Halo2code =  new bool[5] { true, true, true, true, false, false, true, false }
            KeyCode = new BitArray(new bool[8] { true, true, true, true, false, false, true, false }); 
            OpCode = new BitArray(2,false);

            testMuxInfra = new BitArray(4, false);
            testMuxAfe = new BitArray(4, false);
            
            vRefBgCornerTrim = new BitArray(3, false); 
            vRefScaledBandgapTrim = new BitArray(6, false); 
            vRefPtatUp = new BitArray(6, false); 
            vRefBgapXdown = new BitArray(6, false); 
            vosMagLeft = new BitArray(8, false);
            vosMagRight = new BitArray(8, false); 
            vosElectLeft = new BitArray(8, false); 
            vosElectRight = new BitArray(8, false); 
            gainInstrAmpsLeft = new BitArray(8, false); 
            gainInstrAmpsRight = new BitArray(8, false); 
            MarkingBits = new BitArray(4, false);
            BridgeSwaps = new BitArray(2, false); 

            this.gainUniPolar = gainUniPolar;
            this.gainHiSupply = gainHiSupply;
            this.gainFor12mT = gainFor12mT;
            this.BridgeSwaps = bridgeSwap;
        }

       
    }
}
