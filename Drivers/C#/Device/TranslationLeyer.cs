﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
//using CurrentSupply;
using static System.Runtime.CompilerServices.RuntimeHelpers;
using System.Data;

namespace Device
{
    public enum reportLevel { info, warning, error };
    public delegate void ErrorReportHandlerDel(string message, reportLevel l);

    public class HaloTranslationLayer
    {
        ErrorReportHandlerDel ErrorReportHandler;
        private HaloBitLayer _BitLevelDevice;

        private IDictionary<string, bool> _supplyString2Bool;
        private IDictionary<string, bool> _polarityString2Bool;
        private IDictionary<string, bool> _gainFor12mTString2Bool;
        private IDictionary<string, BitArray> _bridgeSwapString2Int;

        // Trimming Variables
        private IDictionary<int, int> _repeatedVrefCodes;
        private IDictionary<int, int> _repeatedElecOffsetCodes;
        private IDictionary<int, int> _repeatedMagOffsetCodes;
        private double perChange;
        double leftPerStep_2 ;
        double rightPerStep_2 ;

        double leftPerStep_1 ;
        double rightPerStep_1 ;
        double rightFinalStep;
        double leftFinalStep;
        
        bool leftORRightCodeBool = false;
        
        
            private void DummyErrorHandler(string message, reportLevel l)
        {
            return;
        }
        // convert byte to BitArray; We use BitArray data type to keep trimming and test bits. Any value must be converted to BitArray
        // The reference is used to trim to BitArray to be the same size.
        private void SetValue(ref BitArray reference, int I)
        {
            byte new_code = BitConverter.GetBytes(I)[0];
            BitArray Barr = new BitArray(new byte[] { new_code }); // VrefOutReg [Tbit<13:10> = 10] will connect Vref to out
            Barr.Length = reference.Length;
            reference =  Barr;
            return;
        }

        private byte GetValue(ref BitArray reference)
        {
            byte[] b = new byte[1];
            reference.CopyTo(b, 0); // Get the current bandgap value
            return Convert.ToByte(b[0]);
        }
            public HaloTranslationLayer(string programmerS, string supplyS, string polarityS, string gainFor12mTS,string bridgeSwapS, ErrorReportHandlerDel ErrHndlr=null)
        {
            
            if (ErrHndlr != null)
                ErrorReportHandler = ErrHndlr;
            else
                ErrorReportHandler = DummyErrorHandler;
            // Init translation dictionaries
            _supplyString2Bool = new Dictionary<string, bool>() {
                {"3v3" , false },
                { "5v0", true }
            };
            _polarityString2Bool = new Dictionary<string, bool>() {
                {"B" , false },
                { "U", true }
            };
            _gainFor12mTString2Bool = new Dictionary<string, bool>() {
                {"8mT" , false },
                { "12mT", true }
            };
            _bridgeSwapString2Int = new Dictionary<string, BitArray>() {
                {"noSwap" , new BitArray(new bool[2] { false, false }) } ,//CT450
                { "leftSwap", new BitArray(new bool[2] { true, false }) } ,//CT430/1
                { "rightSwap", new BitArray(new bool[2] { false, true }) } ,//CT452/3
                { "leftAndRightSwap", new BitArray(new bool[2] { true, true }) }
            };
            
            _BitLevelDevice = new HaloBitLayer(_supplyString2Bool[supplyS], _polarityString2Bool[polarityS], _gainFor12mTString2Bool[gainFor12mTS], _bridgeSwapString2Int[bridgeSwapS]);
            // Determining ideal Vref and step sizes



            //// Trimming values initialization
            _repeatedVrefCodes = new Dictionary<int, int>() { };
            _repeatedElecOffsetCodes = new Dictionary<int, int>() { };
            _repeatedMagOffsetCodes = new Dictionary<int, int>() { };

            leftPerStep_2 = 0.1769;
            rightPerStep_2 = leftPerStep_2;

            leftPerStep_1 = 0.1443;
             rightPerStep_1 = leftPerStep_1;


        }// Halo Translation Layer Constructor 
        // Four modes of operation 
        public bool EnterTryBeforeBuyMode()
        {
            _BitLevelDevice.OpCode.Set(0, false);
            _BitLevelDevice.OpCode.Set(1, true);
            return false;
        }
        public bool EnterReadbackMode() {
            _BitLevelDevice.OpCode.Set(0, true);
            _BitLevelDevice.OpCode.Set(1, true);
            return false; 
        }
        public bool EnterFuseMode() {
            _BitLevelDevice.OpCode.Set(0, true);
            _BitLevelDevice.OpCode.Set(1, false);
            
            return false; }
        public bool EnterNormalMode() { return false; } 
        /*
         *  This programs device with the set value in the class instance. 
         *  It does NOT fuse the device. So the changes are not permanenet.
         * 
         */
        public bool ProgramDevice()
        { return false; }
        public double ReadAnalogOutputVoltage()
        {
            return 0;
        }

        /*
        * Readout all bits from  the device. A copy of the latest readout will be
        * stored in memory to be used to copy progmammed values from device or 
        * generate a report from the trimmed bits or compare with the written trim bits.
        */
        public bool ReadTrimmingBitsFromDevice()
        {
            // Read All bits from device and refresh the Halo class object with the bit values
            //if (CopyBgapXDownFromDevice()) return true;
            //if (CopyProgrammedBandgapCornerFromDevice()) return true;
            //if (CopyPtatUPFromDevice()) return true;
            return false; 
        }
        public void ZeroAllTestBits()
        {
            _BitLevelDevice.leftAmpDis = false;
            _BitLevelDevice.leftBridgeShort = false;
            _BitLevelDevice.rightAmpDis = false;
            _BitLevelDevice.rightBridgeShort = false;
            _BitLevelDevice.overTempTermalTest = false;
            _BitLevelDevice.disAfe = false;
            SetValue(ref _BitLevelDevice.testMuxInfra, 0);
            SetValue(ref _BitLevelDevice.testMuxAfe, 0);
            _BitLevelDevice.testMuxInfraEn = false;
            _BitLevelDevice.testMuxAfeEn = false;
        }
        // Reads device's current code and keep it in the trimming object to be used again in programmig the device
        public bool CopyProgrammedBandgapCornerFromDevice()
        {
            return false;
        }
        // Reads device's current code and keep it in the trimming object to be used again in programmig the device
        public bool CopyPtatUPFromDevice()
        {
            return false;
        }
        // Reads device's current code and keep it in the trimming object to be used again in programmig the device
        public bool CopyBgapXDownFromDevice()
        {
            return false;
        }
        public bool SetBandgapCorner(int value)
        {
            SetValue(ref _BitLevelDevice.vRefBgCornerTrim, value);
            return false;
        }
        public int GetBandgapCorner()
        {
           
            return GetValue(ref _BitLevelDevice.vRefBgCornerTrim); 
        }
        public bool SetPtatUP(int value)
        {
            SetValue(ref _BitLevelDevice.vRefPtatUp, value);
            return false;
        }
        public bool SetBgapXDown(int value)
        {
            SetValue(ref _BitLevelDevice.vRefBgapXdown, value);
            return false;
        }

        public int GetPtatUP()
        {

            return GetValue(ref _BitLevelDevice.vRefPtatUp); 
        }
        public int GetBgapXDown()
        {

            return GetValue(ref _BitLevelDevice.vRefBgapXdown); 
        }

        public bool ClearTestBits()
        {
            _BitLevelDevice.leftAmpDis = false;
            _BitLevelDevice.leftBridgeShort = false;
            _BitLevelDevice.rightAmpDis = false;
            _BitLevelDevice.rightBridgeShort = false;
            _BitLevelDevice.overTempTermalTest = false;
            _BitLevelDevice.disAfe = false;
            SetValue(ref _BitLevelDevice.testMuxInfra,0);
            SetValue(ref _BitLevelDevice.testMuxAfe ,0);
            _BitLevelDevice.testMuxInfraEn = false;
            _BitLevelDevice.testMuxAfeEn = false;   

            return false;
        
        }
        
        
        public bool ConnectVrefToOUT()
        {
            _BitLevelDevice.testMuxAfeEn = true;// enableMuxAfe [Tbit<15> = 1] 
            //BitArray Barr = new BitArray(new byte[] { 0x0A }); // VrefOutReg [Tbit<13:10> = 10] will connect Vref to out
            //Barr.Length = _BitLevelDevice.testMuxAfe.Length;
            SetValue(ref _BitLevelDevice.testMuxAfe, 0x0A);  // VrefOutReg [Tbit<13:10> = 10] will connect Vref to out
            return false;
        }
        public bool DisconnectVrefToOUT()
        {
            _BitLevelDevice.testMuxAfeEn = false;// enableMuxAfe [Tbit<15> = 1] 
            //BitArray Barr = new BitArray(new byte[] { 0x0A }); // VrefOutReg [Tbit<13:10> = 10] will connect Vref to out
            //Barr.Length = _BitLevelDevice.testMuxAfe.Length;
            SetValue(ref _BitLevelDevice.testMuxAfe, 0);  // VrefOutReg [Tbit<13:10> = 10] will connect Vref to out
            return false;
        }
        public bool ShortLeftBrdige()
        {
            _BitLevelDevice.leftBridgeShort = true;
            return false; 
        }
        public bool ShortRightBrdige()
        {
            _BitLevelDevice.rightBridgeShort = true;
            return false;
        }

        public bool UnshortLeftBrdige()
        {
            _BitLevelDevice.leftBridgeShort = false;
            return false;
        }
        public bool UnshortRightBrdige()
        {
            _BitLevelDevice.rightBridgeShort = false;
            return false;
        }
        public bool RightAmpDisable()
        {
            _BitLevelDevice.rightAmpDis = true;
            return false;
        }
        public bool RightAmpEnable()
        {
            _BitLevelDevice.rightAmpDis = false;
            return false;
        }
        public bool LeftAmpDisable()
        {
            _BitLevelDevice.leftAmpDis = true;
            return false;
        }
        public bool LeftAmpEnable()
        {
            _BitLevelDevice.leftAmpDis = false;
            return false;
        }
        public bool SetLeftAmpGain(int gain)
        {
            SetValue(ref _BitLevelDevice.gainInstrAmpsLeft, gain);
            return false;        
        }
        public bool SetRightAmpGain(int gain)
        {
            SetValue(ref _BitLevelDevice.gainInstrAmpsRight, gain);
            return false;
        }
        public int GetLeftAmpGain()
        {
            return GetValue(ref _BitLevelDevice.gainInstrAmpsLeft);
        }
        public int GetRightAmpGain()
        {
            return GetValue(ref _BitLevelDevice.gainInstrAmpsRight);
        }
        public bool SetLeftElecOffset(int offset)
        {
            SetValue(ref _BitLevelDevice.vosElectLeft, offset);
            return false;
        }
        public bool SetRightElecOffset(int offset)
        {
            SetValue(ref _BitLevelDevice.vosElectRight, offset);
            return false;
        }
        public int GetLeftElecOffset()
        {
            return GetValue(ref _BitLevelDevice.vosElectLeft);
        }
        public int GetRightElecOffset()
        {
            return GetValue(ref _BitLevelDevice.vosElectRight);
        }
        public bool SetLeftMagneticOffset(int offset)
        {
            SetValue(ref _BitLevelDevice.vosMagLeft, offset);
            return false;
        }
        public bool SetRightMagneticOffset(int offset)
        {
            SetValue(ref _BitLevelDevice.vosMagRight, offset);
            return false;
        }
        public int GetLeftMagneticOffset()
        {
            return GetValue(ref _BitLevelDevice.vosMagLeft);
        }
        public int GetRightMagneticOffset()
        {
            return GetValue(ref _BitLevelDevice.vosMagRight);
        }

        public int GetBridgeSwap()
        {
            return GetValue(ref _BitLevelDevice.BridgeSwaps);
        }
        public bool SetVrefScaledBandgapTrim(int OffSetValue)
        {

                SetValue(ref _BitLevelDevice.vRefScaledBandgapTrim, OffSetValue);
                return false;

            //if (System.Math.Abs(offsetVoltage) > (vRefStepSize / 2.0F))
            //{
            //    int steps = ((int)System.Math.Round((offsetVoltage / vRefStepSize), 0));// Calculate the step size
            //byte[] b = new byte[1];
            //_BitLevelDevice.vRefScaledBandgapTrim.CopyTo(b, 0); // Get the current bandgap value
            //byte new_code = (byte)(b[0] + (byte)steps); // Add it to the step size
            //BitArray Barr = new BitArray(new byte[] { new_code }); // Convert to BitArray
            //Barr.Length = _BitLevelDevice.vRefScaledBandgapTrim.Length; // Trim Bitarray size
            //_BitLevelDevice.vRefScaledBandgapTrim = Barr; // Write new code back to the device

            //    return false;
            //}
            //else
            //{
            //    String s = String.Format("The demanded change on Vref={0} is smaller than one Vref step size {1} ", offsetVoltage, vRefStepSize);
            //    ErrorReportHandler(s, reportLevel.warning);
            //    return true;
            //}
        }

        public int GetVrefScaledBandgapTrim()
        {
            return unchecked((sbyte)GetValue(ref _BitLevelDevice.vRefScaledBandgapTrim));

        }
        public void SetSupply(string supply)
        {
            if (_supplyString2Bool.ContainsKey(supply))
            {
                _BitLevelDevice.gainHiSupply = _supplyString2Bool[supply];
            }
        }
        public bool GetSupply()
        {

            return _BitLevelDevice.gainHiSupply;
        }
        public void SetPolarity(string polarity)
        {
            if (_polarityString2Bool.ContainsKey(polarity))
            {
                _BitLevelDevice.gainUniPolar = _polarityString2Bool[polarity];
            }
        }
        public bool GetPolarity()
        {

            return _BitLevelDevice.gainUniPolar;
        }
        public bool GetGainfor12mTBit()
        {
            return _BitLevelDevice.gainFor12mT;
        }
        public void SetGainFor12mT(string deviceType)
        {
            if (_gainFor12mTString2Bool.ContainsKey(deviceType))
            {
                _BitLevelDevice.gainFor12mT = _gainFor12mTString2Bool[deviceType];
            }
        }
        public int GetMarkingBits()
        {
            return GetValue(ref _BitLevelDevice.MarkingBits);
        }
        public bool SetMarkingBits(int marbits)
        {
            SetValue(ref _BitLevelDevice.MarkingBits, marbits);
            return false;
        }


        private void F_sensorGainFbitSetting(int setIdx)    
    //   ##########   8mT (20A)   #############################
        {
            int markingvalue = 0;
            bool gainbit;
            switch (setIdx) {
                case 1:
                    markingvalue = 0;
                    gainbit = false;
                    break;
                case 2:
                    markingvalue = 4;
                    gainbit = false;
                    break;
                case 3:
                    markingvalue = 8;
                    gainbit = false;
                    break;
                case 4:
                    markingvalue = 2;
                    gainbit = false;
                    break;
                case 5:
                    markingvalue = 6;
                    gainbit = false;
                    break;
                case 6:
                    markingvalue = 10;
                    gainbit = false;
                    break;
                //##########   12mT (30A)   #############################
                case 7:
                    markingvalue = 2;
                    gainbit = true;
                    break;
                case 8:
                    markingvalue = 6;
                    gainbit = true;
                    break;
                case 9:
                    markingvalue = 10;
                    gainbit = true;
                    break;
                case 10:
                    markingvalue = 0;
                    gainbit = true;
                    break;
                case 11:
                    markingvalue = 4;
                    gainbit = true;
                    break;
                case 12:
                    markingvalue = 8;
                    gainbit = true;
                    break;

                default:
                    markingvalue = 8;
                    gainbit = true; 
                    break;



            }
            SetValue(ref _BitLevelDevice.MarkingBits, markingvalue);
            _BitLevelDevice.gainFor12mT = gainbit;

        }
       

        public bool DetermineTheGainBitAndMarkingBits(double idealRange, double measuredSensorGain)

        {
            double perDelta_setting10 = 100 * (idealRange / measuredSensorGain);
            double perDelta_setting11 = 100 * (idealRange / (0.85 * measuredSensorGain));
            double perDelta_setting12 = 100 * (idealRange / (1.15 * measuredSensorGain));

            double absDelta_setting1 = Math.Abs(501 - perDelta_setting10);
            double absDelta_setting2 = Math.Abs(426 - perDelta_setting10);
            double absDelta_setting3 = Math.Abs(576 - perDelta_setting10);

            double absDelta_setting4 = Math.Abs(252 - perDelta_setting10);
            double absDelta_setting5 = Math.Abs(214 - perDelta_setting10);
            double absDelta_setting6 = Math.Abs(289 - perDelta_setting10);

            double absDelta_setting7 = Math.Abs(165 - perDelta_setting10);
            double absDelta_setting8 = Math.Abs(140 - perDelta_setting10);
            double absDelta_setting9 = Math.Abs(190 - perDelta_setting10);

            double absDelta_setting10 = Math.Abs(100 - perDelta_setting10);
            double absDelta_setting11 = Math.Abs(100 - perDelta_setting11);
            double absDelta_setting12 = Math.Abs(100 - perDelta_setting12);

            List<double> absDeltaArray = new List<double>(){absDelta_setting1, absDelta_setting2, absDelta_setting3,
                             absDelta_setting4, absDelta_setting5, absDelta_setting6,
                             absDelta_setting7, absDelta_setting8, absDelta_setting9,
                             absDelta_setting10, absDelta_setting11, absDelta_setting12 };

            int idealSetting = absDeltaArray.IndexOf(absDeltaArray.Min()) + 1;
            F_sensorGainFbitSetting(idealSetting);
            return false;
        }

        // Parse the bitstream read from the device and update the object 
        public bool ParseAndUpdateFromBitStream(bool[] readBitStream)
        {
            bool[] finalbitstream = new bool[106];  
            
            Array.Copy(readBitStream,0, finalbitstream,0 ,106);
            BitArray bitstream = new BitArray(new byte[106]);
            bitstream.SetAll(false);

            //8-bit  Keycode. No need to copy it.

            //2-bit Opcode. No need to copy it.

            
            //16-bit Test (Control) bits
            _BitLevelDevice.leftAmpDis = finalbitstream[10];
            _BitLevelDevice.leftBridgeShort = finalbitstream[11];
            _BitLevelDevice.rightAmpDis = finalbitstream[12];
            _BitLevelDevice.rightBridgeShort = finalbitstream[13];
            _BitLevelDevice.overTempTermalTest = finalbitstream[14];
            _BitLevelDevice.disAfe = finalbitstream[15];

            BitArray temp = new BitArray(_BitLevelDevice.testMuxInfra.Length);
            temp[0] = finalbitstream[16];
            temp[1] = finalbitstream[17];
            temp[2] = finalbitstream[18];
            temp[3] = finalbitstream[19];
            _BitLevelDevice.testMuxInfra = temp;


            temp = new BitArray(_BitLevelDevice.testMuxAfe.Length);
            temp[0] = finalbitstream[20];
            temp[1] = finalbitstream[21];
            temp[2] = finalbitstream[22];
            temp[3] = finalbitstream[23];
            _BitLevelDevice.testMuxAfe = temp;


            _BitLevelDevice.testMuxInfraEn = finalbitstream[24];
            _BitLevelDevice.testMuxAfeEn = finalbitstream[25];

            //77 bits trim bits
            short TrimBitsOffset = 26;
            //bitstream.Set(TrimBitsOffset + 0, _BitLevelDevice.FactoryBit);

            temp = new BitArray(_BitLevelDevice.vRefBgCornerTrim.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 73];
            temp[1] = finalbitstream[TrimBitsOffset + 1];
            temp[2] = finalbitstream[TrimBitsOffset + 2];
            _BitLevelDevice.vRefBgCornerTrim = temp;



            temp = new BitArray(_BitLevelDevice.vRefScaledBandgapTrim.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 3];
            temp[1] = finalbitstream[TrimBitsOffset + 4];
            temp[2] = finalbitstream[TrimBitsOffset + 5];
            temp[3] = finalbitstream[TrimBitsOffset + 6];
            temp[4] = finalbitstream[TrimBitsOffset + 7];
            temp[5] = finalbitstream[TrimBitsOffset + 72];
            _BitLevelDevice.vRefScaledBandgapTrim = temp;



            temp = new BitArray(_BitLevelDevice.vRefPtatUp.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 8];
            temp[1] = finalbitstream[TrimBitsOffset + 9];
            temp[2] = finalbitstream[TrimBitsOffset + 10];
            temp[3] = finalbitstream[TrimBitsOffset + 11];
            temp[4] = finalbitstream[TrimBitsOffset + 12];
            temp[5] = finalbitstream[TrimBitsOffset + 13];
            _BitLevelDevice.vRefPtatUp = temp;


            temp = new BitArray(_BitLevelDevice.vRefBgapXdown.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 14];
            temp[1] = finalbitstream[TrimBitsOffset + 15];
            temp[2] = finalbitstream[TrimBitsOffset + 16];
            temp[3] = finalbitstream[TrimBitsOffset + 17];
            temp[4] = finalbitstream[TrimBitsOffset + 18];
            temp[5] = finalbitstream[TrimBitsOffset + 19];
            _BitLevelDevice.vRefBgapXdown = temp;


            temp = new BitArray(_BitLevelDevice.vosMagLeft.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 20];
            temp[1] = finalbitstream[TrimBitsOffset + 21];
            temp[2] = finalbitstream[TrimBitsOffset + 22];
            temp[3] = finalbitstream[TrimBitsOffset + 23];
            temp[4] = finalbitstream[TrimBitsOffset + 24];
            temp[5] = finalbitstream[TrimBitsOffset + 25];
            temp[6] = finalbitstream[TrimBitsOffset + 26];
            temp[7] = finalbitstream[TrimBitsOffset + 27];
            _BitLevelDevice.vosMagLeft = temp;

            temp = new BitArray(_BitLevelDevice.vosMagRight.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 28];
            temp[1] = finalbitstream[TrimBitsOffset + 29];
            temp[2] = finalbitstream[TrimBitsOffset + 30];
            temp[3] = finalbitstream[TrimBitsOffset + 31];
            temp[4] = finalbitstream[TrimBitsOffset + 32];
            temp[5] = finalbitstream[TrimBitsOffset + 33];
            temp[6] = finalbitstream[TrimBitsOffset + 34];
            temp[7] = finalbitstream[TrimBitsOffset + 35];
            _BitLevelDevice.vosMagRight = temp;


            temp = new BitArray(_BitLevelDevice.vosElectLeft.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 36];
            temp[1] = finalbitstream[TrimBitsOffset + 37];
            temp[2] = finalbitstream[TrimBitsOffset + 38];
            temp[3] = finalbitstream[TrimBitsOffset + 39];
            temp[4] = finalbitstream[TrimBitsOffset + 40];
            temp[5] = finalbitstream[TrimBitsOffset + 41];
            temp[6] = finalbitstream[TrimBitsOffset + 42];
            temp[7] = finalbitstream[TrimBitsOffset + 43];
            _BitLevelDevice.vosElectLeft = temp;

            temp = new BitArray(_BitLevelDevice.vosElectRight.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 44];
            temp[1] = finalbitstream[TrimBitsOffset + 45];
            temp[2] = finalbitstream[TrimBitsOffset + 46];
            temp[3] = finalbitstream[TrimBitsOffset + 47];
            temp[4] = finalbitstream[TrimBitsOffset + 48];
            temp[5] = finalbitstream[TrimBitsOffset + 49];
            temp[6] = finalbitstream[TrimBitsOffset + 50];
            temp[7] = finalbitstream[TrimBitsOffset + 51];
            _BitLevelDevice.vosElectRight = temp;

            temp = new BitArray(_BitLevelDevice.gainInstrAmpsLeft.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 52];
            temp[1] = finalbitstream[TrimBitsOffset + 53];
            temp[2] = finalbitstream[TrimBitsOffset + 54];
            temp[3] = finalbitstream[TrimBitsOffset + 55];
            temp[4] = finalbitstream[TrimBitsOffset + 56];
            temp[5] = finalbitstream[TrimBitsOffset + 57];
            temp[6] = finalbitstream[TrimBitsOffset + 58];
            temp[7] = finalbitstream[TrimBitsOffset + 59];
            _BitLevelDevice.gainInstrAmpsLeft = temp;

            temp = new BitArray(_BitLevelDevice.gainInstrAmpsRight.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 60];
            temp[1] = finalbitstream[TrimBitsOffset + 61];
            temp[2] = finalbitstream[TrimBitsOffset + 62];
            temp[3] = finalbitstream[TrimBitsOffset + 63];
            temp[4] = finalbitstream[TrimBitsOffset + 64];
            temp[5] = finalbitstream[TrimBitsOffset + 65];
            temp[6] = finalbitstream[TrimBitsOffset + 66];
            temp[7] = finalbitstream[TrimBitsOffset + 67];
            _BitLevelDevice.gainInstrAmpsRight = temp;


            _BitLevelDevice.gainUniPolar = finalbitstream[TrimBitsOffset + 68];

            _BitLevelDevice.gainHiSupply = finalbitstream[TrimBitsOffset + 69];

            _BitLevelDevice.gainFor12mT = finalbitstream[TrimBitsOffset + 70];

            temp = new BitArray(_BitLevelDevice.MarkingBits.Length);
            _BitLevelDevice.FactoryBit = finalbitstream[TrimBitsOffset + 0];
            temp[0] = finalbitstream[TrimBitsOffset + 0];
            temp[1] = finalbitstream[TrimBitsOffset + 71];
            temp[2] = finalbitstream[TrimBitsOffset + 74];
            temp[3] = finalbitstream[TrimBitsOffset + 75];
            _BitLevelDevice.MarkingBits = temp; 
            
            _BitLevelDevice.BlockTryBeforeBuyHi = finalbitstream[TrimBitsOffset + 76];

            _BitLevelDevice.CrousMurataModes = finalbitstream[TrimBitsOffset + 77];

            temp = new BitArray(_BitLevelDevice.BridgeSwaps.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 78];
            temp[1] = finalbitstream[TrimBitsOffset + 79];
            _BitLevelDevice.BridgeSwaps = temp;

            return false;
        }

        // Just for illustration purpose
        public string ConvertBitStreamToString(bool[] readBitStream)
        {
            return string.Join(", ", readBitStream.Select(b => b.ToString()).ToArray());
        }
        /* Just for illustration, we generate delimited trimming parameters*/
        public bool ParseAndUpdateFromDecimalParsedTrimmingString(string trimmingString)
        {
         
            string[] parsedTrimmingString = trimmingString.Split(',');


            //77 bits trim bits
            SetBandgapCorner(UInt16.Parse(parsedTrimmingString[0]));
            //parsedTrimmingString += GetBandgapCorner().ToString() + ",";

            SetVrefScaledBandgapTrim(UInt16.Parse(parsedTrimmingString[1]));
            //parsedTrimmingString += GetVrefScaledBandgapTrim().ToString() + ",";

            SetPtatUP(UInt16.Parse(parsedTrimmingString[2]));
            //parsedTrimmingString += GetPtatUP().ToString() + ",";

            SetBgapXDown(UInt16.Parse(parsedTrimmingString[3]));


            SetLeftMagneticOffset(UInt16.Parse(parsedTrimmingString[4]));
            //parsedTrimmingString += GetLeftMagneticOffset().ToString() + ",";

            SetRightMagneticOffset(UInt16.Parse(parsedTrimmingString[5]));
            //parsedTrimmingString += GetRightMagneticOffset().ToString() + ",";

            SetLeftElecOffset(UInt16.Parse(parsedTrimmingString[6]));
            //parsedTrimmingString += GetLeftElecOffset().ToString() + ",";

            SetRightElecOffset(UInt16.Parse(parsedTrimmingString[7]));
            //parsedTrimmingString += GetRightElecOffset().ToString() + ",";

            SetLeftAmpGain(UInt16.Parse(parsedTrimmingString[8]));
            //parsedTrimmingString += GetLeftAmpGain().ToString() + ",";

            SetRightAmpGain(UInt16.Parse(parsedTrimmingString[9]));
            //parsedTrimmingString += GetRightAmpGain().ToString() + ",";

            _BitLevelDevice.gainUniPolar = bool.Parse(parsedTrimmingString[10]);
            //parsedTrimmingString += GetPolarity().ToString() + ",";

            _BitLevelDevice.gainHiSupply= bool.Parse(parsedTrimmingString[11]);
            //parsedTrimmingString += GetSupply().ToString() + ",";

            _BitLevelDevice.gainFor12mT = bool.Parse(parsedTrimmingString[12]);
            //parsedTrimmingString += GetGainfor12mTBit().ToString() + ",";

            SetMarkingBits(UInt16.Parse(parsedTrimmingString[13]));
            //parsedTrimmingString += GetMarkingBits().ToString() + ",";

            //finalbitstream[TrimBitsOffset + 75] = _BitLevelDevice.MarkingBits.Get(3);
            //finalbitstream[TrimBitsOffset + 74] = _BitLevelDevice.MarkingBits.Get(2);
            //finalbitstream[TrimBitsOffset + 71] = _BitLevelDevice.MarkingBits.Get(1);
            //finalbitstream[TrimBitsOffset + 0] = _BitLevelDevice.MarkingBits.Get(0);


            _BitLevelDevice.BlockTryBeforeBuyHi = bool.Parse(parsedTrimmingString[14]);
            //parsedTrimmingString += _BitLevelDevice.BlockTryBeforeBuyHi.ToString() + ",";

            _BitLevelDevice.CrousMurataModes = bool.Parse(parsedTrimmingString[15]);
            //parsedTrimmingString += _BitLevelDevice.CrousMurataModes.ToString() + ",";

            string bridgeSwapS = "";
            switch (UInt16.Parse(parsedTrimmingString[16])){
                case 0: bridgeSwapS = "noSwap";
                    break;
                case 1:
                    bridgeSwapS = "leftSwap";
                    break;
                case 2:
                    bridgeSwapS = "rightSwap";
                    break;
                case 3:
                    bridgeSwapS = "leftAndRightSwap";
                    break;
                default:
                    return true;
            };

            _BitLevelDevice.BridgeSwaps = _bridgeSwapString2Int[bridgeSwapS];

            //parsedTrimmingString += GetBridgeSwap().ToString();

            return false;

        }
        public bool SetBridgeSwap(string bridgeSwapS)
        {
            _BitLevelDevice.BridgeSwaps = _bridgeSwapString2Int[bridgeSwapS];
            return false;
        }
        //public string ConvertBitStreamToString(bool[] readBitStream)
        //{
        //    return string.Join(", ", readBitStream.Select(b => b.ToString()).ToArray());
        //}
        /* Just for illustration, we generate delimited trimming parameters*/
        public string GenerateDecimalParsedTrimmingString()
        {
            string parsedTrimmingString="";


            //77 bits trim bits
            
            parsedTrimmingString +=  GetBandgapCorner().ToString()+",";
            //finalbitstream[TrimBitsOffset + 73] = _BitLevelDevice.vRefBgCornerTrim.Get(0);
            //finalbitstream[TrimBitsOffset + 1] = _BitLevelDevice.vRefBgCornerTrim.Get(1);
            //finalbitstream[TrimBitsOffset + 2] = _BitLevelDevice.vRefBgCornerTrim.Get(2);

            parsedTrimmingString += GetVrefScaledBandgapTrim().ToString() + ",";

            //finalbitstream[TrimBitsOffset + 3] = _BitLevelDevice.vRefScaledBandgapTrim.Get(0);
            //finalbitstream[TrimBitsOffset + 4] = _BitLevelDevice.vRefScaledBandgapTrim.Get(1);
            //finalbitstream[TrimBitsOffset + 5] = _BitLevelDevice.vRefScaledBandgapTrim.Get(2);
            //finalbitstream[TrimBitsOffset + 6] = _BitLevelDevice.vRefScaledBandgapTrim.Get(3);
            //finalbitstream[TrimBitsOffset + 7] = _BitLevelDevice.vRefScaledBandgapTrim.Get(4);
            //finalbitstream[TrimBitsOffset + 72] = _BitLevelDevice.vRefScaledBandgapTrim.Get(5);



            parsedTrimmingString += GetPtatUP().ToString() + ",";

            //_BitLevelDevice.vRefPtatUp.CopyTo(finalbitstream, TrimBitsOffset + 8);

            parsedTrimmingString += GetBgapXDown().ToString() + ",";

            //_BitLevelDevice.vRefBgapXdown.CopyTo(finalbitstream, TrimBitsOffset + 14);

            parsedTrimmingString += GetLeftMagneticOffset().ToString() + ",";

            //_BitLevelDevice.vosMagLeft.CopyTo(finalbitstream, TrimBitsOffset + 20);

            parsedTrimmingString += GetRightMagneticOffset().ToString() + ",";

            //_BitLevelDevice.vosMagRight.CopyTo(finalbitstream, TrimBitsOffset + 28);

            parsedTrimmingString += GetLeftElecOffset().ToString() + ",";
            //_BitLevelDevice.vosElectLeft.CopyTo(finalbitstream, TrimBitsOffset + 36);


            parsedTrimmingString += GetRightElecOffset().ToString() + ",";
            //_BitLevelDevice.vosElectRight.CopyTo(finalbitstream, TrimBitsOffset + 44);


            parsedTrimmingString += GetLeftAmpGain().ToString() + ",";
            //_BitLevelDevice.gainInstrAmpsLeft.CopyTo(finalbitstream, TrimBitsOffset + 52);

            parsedTrimmingString += GetRightAmpGain().ToString() + ",";
            //_BitLevelDevice.gainInstrAmpsRight.CopyTo(finalbitstream, TrimBitsOffset + 60);

            parsedTrimmingString += GetPolarity().ToString() + ",";
            //finalbitstream[TrimBitsOffset + 68] = _BitLevelDevice.gainUniPolar;

            parsedTrimmingString += GetSupply().ToString() + ",";
            //finalbitstream[TrimBitsOffset + 69] = _BitLevelDevice.gainHiSupply;

            parsedTrimmingString += GetGainfor12mTBit().ToString() + ",";
            //finalbitstream[TrimBitsOffset + 70] = _BitLevelDevice.gainFor12mT;

            parsedTrimmingString += GetMarkingBits().ToString() + ",";

            //finalbitstream[TrimBitsOffset + 75] = _BitLevelDevice.MarkingBits.Get(3);
            //finalbitstream[TrimBitsOffset + 74] = _BitLevelDevice.MarkingBits.Get(2);
            //finalbitstream[TrimBitsOffset + 71] = _BitLevelDevice.MarkingBits.Get(1);
            //finalbitstream[TrimBitsOffset + 0] = _BitLevelDevice.MarkingBits.Get(0);


            parsedTrimmingString += _BitLevelDevice.BlockTryBeforeBuyHi.ToString() + ",";
            //finalbitstream[TrimBitsOffset + 76] = _BitLevelDevice.BlockTryBeforeBuyHi;

            parsedTrimmingString += _BitLevelDevice.CrousMurataModes.ToString() + ",";
            //finalbitstream[TrimBitsOffset + 77] = _BitLevelDevice.CrousMurataModes;

            //parsedTrimmingString += _BitLevelDevice.CrousMurataModes.ToString() + ",";

            parsedTrimmingString += GetBridgeSwap().ToString();
            //_BitLevelDevice.BridgeSwaps.CopyTo(finalbitstream, TrimBitsOffset + 78);

           return parsedTrimmingString;

        }

        public string ConvertBitstreamToDecimalParsedString(bool[] readBitStream)
        {
            bool[] finalbitstream = new bool[106];
            string parsedTrimmingString = "";
            Array.Copy(readBitStream, 0, finalbitstream, 0, 106);
            BitArray bitstream = new BitArray(new byte[106]);
            bitstream.SetAll(false);

            //8-bit  Keycode. No need to copy it.

            //2-bit Opcode. No need to copy it.
            BitArray temp;

            //77 bits trim bits
            short TrimBitsOffset = 26;
            //bitstream.Set(TrimBitsOffset + 0, _BitLevelDevice.FactoryBit);

            temp = new BitArray(_BitLevelDevice.vRefBgCornerTrim.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 73];
            temp[1] = finalbitstream[TrimBitsOffset + 1];
            temp[2] = finalbitstream[TrimBitsOffset + 2];
            parsedTrimmingString += GetValue(ref temp).ToString()+",";



            temp = new BitArray(_BitLevelDevice.vRefScaledBandgapTrim.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 3];
            temp[1] = finalbitstream[TrimBitsOffset + 4];
            temp[2] = finalbitstream[TrimBitsOffset + 5];
            temp[3] = finalbitstream[TrimBitsOffset + 6];
            temp[4] = finalbitstream[TrimBitsOffset + 7];
            temp[5] = finalbitstream[TrimBitsOffset + 72];
            parsedTrimmingString += GetValue(ref temp).ToString() + ",";



            temp = new BitArray(_BitLevelDevice.vRefPtatUp.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 8];
            temp[1] = finalbitstream[TrimBitsOffset + 9];
            temp[2] = finalbitstream[TrimBitsOffset + 10];
            temp[3] = finalbitstream[TrimBitsOffset + 11];
            temp[4] = finalbitstream[TrimBitsOffset + 12];
            temp[5] = finalbitstream[TrimBitsOffset + 13];
            parsedTrimmingString += GetValue(ref temp).ToString() + ",";


            temp = new BitArray(_BitLevelDevice.vRefBgapXdown.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 14];
            temp[1] = finalbitstream[TrimBitsOffset + 15];
            temp[2] = finalbitstream[TrimBitsOffset + 16];
            temp[3] = finalbitstream[TrimBitsOffset + 17];
            temp[4] = finalbitstream[TrimBitsOffset + 18];
            temp[5] = finalbitstream[TrimBitsOffset + 19];
            parsedTrimmingString += GetValue(ref temp).ToString() + ",";


            temp = new BitArray(_BitLevelDevice.vosMagLeft.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 20];
            temp[1] = finalbitstream[TrimBitsOffset + 21];
            temp[2] = finalbitstream[TrimBitsOffset + 22];
            temp[3] = finalbitstream[TrimBitsOffset + 23];
            temp[4] = finalbitstream[TrimBitsOffset + 24];
            temp[5] = finalbitstream[TrimBitsOffset + 25];
            temp[6] = finalbitstream[TrimBitsOffset + 26];
            temp[7] = finalbitstream[TrimBitsOffset + 27];
            parsedTrimmingString += GetValue(ref temp).ToString() + ",";

            temp = new BitArray(_BitLevelDevice.vosMagRight.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 28];
            temp[1] = finalbitstream[TrimBitsOffset + 29];
            temp[2] = finalbitstream[TrimBitsOffset + 30];
            temp[3] = finalbitstream[TrimBitsOffset + 31];
            temp[4] = finalbitstream[TrimBitsOffset + 32];
            temp[5] = finalbitstream[TrimBitsOffset + 33];
            temp[6] = finalbitstream[TrimBitsOffset + 34];
            temp[7] = finalbitstream[TrimBitsOffset + 35];
            parsedTrimmingString += GetValue(ref temp).ToString() + ",";


            temp = new BitArray(_BitLevelDevice.vosElectLeft.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 36];
            temp[1] = finalbitstream[TrimBitsOffset + 37];
            temp[2] = finalbitstream[TrimBitsOffset + 38];
            temp[3] = finalbitstream[TrimBitsOffset + 39];
            temp[4] = finalbitstream[TrimBitsOffset + 40];
            temp[5] = finalbitstream[TrimBitsOffset + 41];
            temp[6] = finalbitstream[TrimBitsOffset + 42];
            temp[7] = finalbitstream[TrimBitsOffset + 43];
            parsedTrimmingString += GetValue(ref temp).ToString() + ",";

            temp = new BitArray(_BitLevelDevice.vosElectRight.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 44];
            temp[1] = finalbitstream[TrimBitsOffset + 45];
            temp[2] = finalbitstream[TrimBitsOffset + 46];
            temp[3] = finalbitstream[TrimBitsOffset + 47];
            temp[4] = finalbitstream[TrimBitsOffset + 48];
            temp[5] = finalbitstream[TrimBitsOffset + 49];
            temp[6] = finalbitstream[TrimBitsOffset + 50];
            temp[7] = finalbitstream[TrimBitsOffset + 51];
            parsedTrimmingString += GetValue(ref temp).ToString() + ",";

            temp = new BitArray(_BitLevelDevice.gainInstrAmpsLeft.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 52];
            temp[1] = finalbitstream[TrimBitsOffset + 53];
            temp[2] = finalbitstream[TrimBitsOffset + 54];
            temp[3] = finalbitstream[TrimBitsOffset + 55];
            temp[4] = finalbitstream[TrimBitsOffset + 56];
            temp[5] = finalbitstream[TrimBitsOffset + 57];
            temp[6] = finalbitstream[TrimBitsOffset + 58];
            temp[7] = finalbitstream[TrimBitsOffset + 59];
            parsedTrimmingString += GetValue(ref temp).ToString() + ",";

            temp = new BitArray(_BitLevelDevice.gainInstrAmpsRight.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 60];
            temp[1] = finalbitstream[TrimBitsOffset + 61];
            temp[2] = finalbitstream[TrimBitsOffset + 62];
            temp[3] = finalbitstream[TrimBitsOffset + 63];
            temp[4] = finalbitstream[TrimBitsOffset + 64];
            temp[5] = finalbitstream[TrimBitsOffset + 65];
            temp[6] = finalbitstream[TrimBitsOffset + 66];
            temp[7] = finalbitstream[TrimBitsOffset + 67];
            parsedTrimmingString += GetValue(ref temp).ToString() + ",";


            parsedTrimmingString += finalbitstream[TrimBitsOffset + 68].ToString() + ",";

            parsedTrimmingString += finalbitstream[TrimBitsOffset + 69].ToString() + ",";

            parsedTrimmingString += finalbitstream[TrimBitsOffset + 70].ToString() + ",";

            temp = new BitArray(_BitLevelDevice.MarkingBits.Length);
            
            temp[0] = finalbitstream[TrimBitsOffset + 0];
            temp[1] = finalbitstream[TrimBitsOffset + 71];
            temp[2] = finalbitstream[TrimBitsOffset + 74];
            temp[3] = finalbitstream[TrimBitsOffset + 75];
            parsedTrimmingString += GetValue(ref temp).ToString() + ",";

            parsedTrimmingString += finalbitstream[TrimBitsOffset + 76].ToString() + ",";

            parsedTrimmingString += finalbitstream[TrimBitsOffset + 77].ToString() + ",";

            temp = new BitArray(_BitLevelDevice.BridgeSwaps.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 78];
            temp[1] = finalbitstream[TrimBitsOffset + 79];
            parsedTrimmingString += GetValue(ref temp).ToString() ;
            return parsedTrimmingString;
        }
        public bool ParseAndUpdateFromBitStream_ONLY_FACTORY_BITS(bool[] readBitStream)
        {
            bool[] finalbitstream = new bool[106];

            Array.Copy(readBitStream, 0, finalbitstream, 0, 106);
            BitArray bitstream = new BitArray(new byte[106]);
            bitstream.SetAll(false);

            //8-bit  Keycode. No need to copy it.

            //2-bit Opcode. No need to copy it.

            // Init to zero non-factory bits



            BitArray temp = new BitArray(_BitLevelDevice.vRefScaledBandgapTrim.Length);
            _BitLevelDevice.vRefScaledBandgapTrim = temp;

            temp = new BitArray(_BitLevelDevice.vosMagLeft.Length);
            _BitLevelDevice.vosMagLeft = temp;

            temp = new BitArray(_BitLevelDevice.vosMagRight.Length);
            _BitLevelDevice.vosMagRight = temp;


            temp = new BitArray(_BitLevelDevice.vosElectLeft.Length);
            _BitLevelDevice.vosElectLeft = temp;

            temp = new BitArray(_BitLevelDevice.vosElectRight.Length);
            _BitLevelDevice.vosElectRight = temp;

            temp = new BitArray(_BitLevelDevice.gainInstrAmpsLeft.Length);
            _BitLevelDevice.gainInstrAmpsLeft = temp;

            temp = new BitArray(_BitLevelDevice.gainInstrAmpsRight.Length);
            _BitLevelDevice.gainInstrAmpsRight = temp;



            // Updating Factory Bits

            //77 bits trim bits
            short TrimBitsOffset = 26;
            //bitstream.Set(TrimBitsOffset + 0, _BitLevelDevice.FactoryBit);
            temp = new BitArray(_BitLevelDevice.vRefBgCornerTrim.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 73];
            temp[1] = finalbitstream[TrimBitsOffset + 1];
            temp[2] = finalbitstream[TrimBitsOffset + 2];
            _BitLevelDevice.vRefBgCornerTrim = temp;



            temp = new BitArray(_BitLevelDevice.vRefPtatUp.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 8];
            temp[1] = finalbitstream[TrimBitsOffset + 9];
            temp[2] = finalbitstream[TrimBitsOffset + 10];
            temp[3] = finalbitstream[TrimBitsOffset + 11];
            temp[4] = finalbitstream[TrimBitsOffset + 12];
            temp[5] = finalbitstream[TrimBitsOffset + 13];
            _BitLevelDevice.vRefPtatUp = temp;


            temp = new BitArray(_BitLevelDevice.vRefBgapXdown.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 14];
            temp[1] = finalbitstream[TrimBitsOffset + 15];
            temp[2] = finalbitstream[TrimBitsOffset + 16];
            temp[3] = finalbitstream[TrimBitsOffset + 17];
            temp[4] = finalbitstream[TrimBitsOffset + 18];
            temp[5] = finalbitstream[TrimBitsOffset + 19];
            _BitLevelDevice.vRefBgapXdown = temp;



            //temp = new BitArray(_BitLevelDevice.MarkingBits.Length);
            //_BitLevelDevice.FactoryBit = finalbitstream[TrimBitsOffset + 0];
            //temp[0] = finalbitstream[TrimBitsOffset + 0];
            //temp[1] = finalbitstream[TrimBitsOffset + 71];
            //temp[2] = finalbitstream[TrimBitsOffset + 74];
            //temp[3] = finalbitstream[TrimBitsOffset + 75];
            //_BitLevelDevice.MarkingBits = temp;

            
            return false;
        }


        public bool[] GenerateFinalBitstream()
        {
            //BitArray bitstream = new BitArray(new byte[106]);
            bool[] finalbitstream = new bool[106];
            //bitstream.SetAll(false);

            //8-bit  Keycode 
            _BitLevelDevice.KeyCode.CopyTo(finalbitstream, 0);
            //bitstream.Set(0, _BitLevelDevice.KeyCode.Get(7));
            //bitstream.Set(1, _BitLevelDevice.KeyCode.Get(6));
            //bitstream.Set(2, _BitLevelDevice.KeyCode.Get(5));
            //bitstream.Set(3, _BitLevelDevice.KeyCode.Get(4));
            //bitstream.Set(4, _BitLevelDevice.KeyCode.Get(3));
            //bitstream.Set(5, _BitLevelDevice.KeyCode.Get(2));
            //bitstream.Set(6, _BitLevelDevice.KeyCode.Get(1));
            //bitstream.Set(7, _BitLevelDevice.KeyCode.Get(0));

            //2-bit Opcode
            //_BitLevelDevice.OpCode.CopyTo(finalbitstream, 2);
            //bitstream.Set(8, _BitLevelDevice.OpCode.Get(1));
            //bitstream.Set(9, _BitLevelDevice.OpCode.Get(0));
            finalbitstream[8] = _BitLevelDevice.OpCode.Get(1);
            finalbitstream[9] = _BitLevelDevice.OpCode.Get(0);

            if (finalbitstream[8] && finalbitstream[9])
            {
                /* If OpCode is Read, we must fill the input fileds with '1'. 
                 * This is due to fact that the OUT pin must have a pull-up circuit during read operation
                 * we create that effect by making the DO pin to have the value '1'.
                 * 
                 */
                BitArray bitstreamOnes = new BitArray(new bool[80]);
                bitstreamOnes.SetAll(true);
                bitstreamOnes.CopyTo(finalbitstream, 26);
                return finalbitstream;
            }

            //16-bit Test (Control) bits
            finalbitstream[10] = _BitLevelDevice.leftAmpDis;
            finalbitstream[11] = _BitLevelDevice.leftBridgeShort;
            finalbitstream[12] = _BitLevelDevice.rightAmpDis;
            finalbitstream[13] = _BitLevelDevice.rightBridgeShort;
            finalbitstream[14] = _BitLevelDevice.overTempTermalTest;
            finalbitstream[15] = _BitLevelDevice.disAfe;

            //bitstream.Set(10, _BitLevelDevice.leftAmpDis);
            //bitstream.Set(11, _BitLevelDevice.leftBridgeShort);
            //bitstream.Set(12, _BitLevelDevice.rightAmpDis);
            //bitstream.Set(13, _BitLevelDevice.rightBridgeShort);
            //bitstream.Set(14, _BitLevelDevice.overTempTermalTest);
            //bitstream.Set(15, _BitLevelDevice.disAfe);

            _BitLevelDevice.testMuxInfra.CopyTo(finalbitstream, 16);
            //bitstream.Set(16, _BitLevelDevice.testMuxInfra[3]);
            //bitstream.Set(17, _BitLevelDevice.testMuxInfra[2]);
            //bitstream.Set(18, _BitLevelDevice.testMuxInfra[1]);
            //bitstream.Set(19, _BitLevelDevice.testMuxInfra[0]);

            _BitLevelDevice.testMuxAfe.CopyTo(finalbitstream, 20);
            //bitstream.Set(20, _BitLevelDevice.testMuxAfe[3]);
            //bitstream.Set(21, _BitLevelDevice.testMuxAfe[2]);
            //bitstream.Set(22, _BitLevelDevice.testMuxAfe[1]);
            //bitstream.Set(23, _BitLevelDevice.testMuxAfe[0]);

            finalbitstream[24] = _BitLevelDevice.testMuxInfraEn;
            finalbitstream[25] = _BitLevelDevice.testMuxAfeEn;
            //bitstream.Set(24, _BitLevelDevice.testMuxInfraEn);
            //bitstream.Set(25, _BitLevelDevice.testMuxAfeEn);

            //77 bits trim bits
            short TrimBitsOffset = 26;
            //bitstream.Set(TrimBitsOffset + 0, _BitLevelDevice.FactoryBit);

            finalbitstream[TrimBitsOffset + 73] = _BitLevelDevice.vRefBgCornerTrim.Get(0);
            finalbitstream[TrimBitsOffset + 1] = _BitLevelDevice.vRefBgCornerTrim.Get(1);
            finalbitstream[TrimBitsOffset + 2] = _BitLevelDevice.vRefBgCornerTrim.Get(2);
            //bitstream.Set(TrimBitsOffset + 1, _BitLevelDevice.vRefBgCornerTrim.Get(2));
            //bitstream.Set(TrimBitsOffset + 2, _BitLevelDevice.vRefBgCornerTrim.Get(1));
            //bitstream.Set(TrimBitsOffset + 73, _BitLevelDevice.vRefBgCornerTrim.Get(0));


            finalbitstream[TrimBitsOffset + 3] = _BitLevelDevice.vRefScaledBandgapTrim.Get(0);
            finalbitstream[TrimBitsOffset + 4] = _BitLevelDevice.vRefScaledBandgapTrim.Get(1);
            finalbitstream[TrimBitsOffset + 5] = _BitLevelDevice.vRefScaledBandgapTrim.Get(2);
            finalbitstream[TrimBitsOffset + 6] = _BitLevelDevice.vRefScaledBandgapTrim.Get(3);
            finalbitstream[TrimBitsOffset + 7] = _BitLevelDevice.vRefScaledBandgapTrim.Get(4);
            finalbitstream[TrimBitsOffset + 72] = _BitLevelDevice.vRefScaledBandgapTrim.Get(5);
            //bitstream.Set(TrimBitsOffset + 72, _BitLevelDevice.vRefScaledBandgapTrim.Get(5));
            //bitstream.Set(TrimBitsOffset + 3, _BitLevelDevice.vRefScaledBandgapTrim.Get(4));
            //bitstream.Set(TrimBitsOffset + 4, _BitLevelDevice.vRefScaledBandgapTrim.Get(3));
            //bitstream.Set(TrimBitsOffset + 5, _BitLevelDevice.vRefScaledBandgapTrim.Get(2));
            //bitstream.Set(TrimBitsOffset + 6, _BitLevelDevice.vRefScaledBandgapTrim.Get(1));
            //bitstream.Set(TrimBitsOffset + 7, _BitLevelDevice.vRefScaledBandgapTrim.Get(0));

            _BitLevelDevice.vRefPtatUp.CopyTo(finalbitstream, TrimBitsOffset+ 8);
            //bitstream.Set(TrimBitsOffset + 8, _BitLevelDevice.vRegPtatUp.Get(5));
            //bitstream.Set(TrimBitsOffset + 9, _BitLevelDevice.vRegPtatUp.Get(4));
            //bitstream.Set(TrimBitsOffset + 10, _BitLevelDevice.vRegPtatUp.Get(3));
            //bitstream.Set(TrimBitsOffset + 11, _BitLevelDevice.vRegPtatUp.Get(2));
            //bitstream.Set(TrimBitsOffset + 12, _BitLevelDevice.vRegPtatUp.Get(1));
            //bitstream.Set(TrimBitsOffset + 13, _BitLevelDevice.vRegPtatUp.Get(0));

            _BitLevelDevice.vRefBgapXdown.CopyTo(finalbitstream, TrimBitsOffset + 14);
            //bitstream.Set(TrimBitsOffset + 14, _BitLevelDevice.vRegBgapXdown.Get(5));
            //bitstream.Set(TrimBitsOffset + 15, _BitLevelDevice.vRegBgapXdown.Get(4));
            //bitstream.Set(TrimBitsOffset + 16, _BitLevelDevice.vRegBgapXdown.Get(3));
            //bitstream.Set(TrimBitsOffset + 17, _BitLevelDevice.vRegBgapXdown.Get(2));
            //bitstream.Set(TrimBitsOffset + 18, _BitLevelDevice.vRegBgapXdown.Get(1));
            //bitstream.Set(TrimBitsOffset + 19, _BitLevelDevice.vRegBgapXdown.Get(0));


            _BitLevelDevice.vosMagLeft.CopyTo(finalbitstream, TrimBitsOffset + 20);
            //bitstream.Set(TrimBitsOffset + 20, _BitLevelDevice.vosMagLeft.Get(7));
            //bitstream.Set(TrimBitsOffset + 21, _BitLevelDevice.vosMagLeft.Get(6));
            //bitstream.Set(TrimBitsOffset + 22, _BitLevelDevice.vosMagLeft.Get(5));
            //bitstream.Set(TrimBitsOffset + 23, _BitLevelDevice.vosMagLeft.Get(4));
            //bitstream.Set(TrimBitsOffset + 24, _BitLevelDevice.vosMagLeft.Get(3));
            //bitstream.Set(TrimBitsOffset + 25, _BitLevelDevice.vosMagLeft.Get(2));
            //bitstream.Set(TrimBitsOffset + 26, _BitLevelDevice.vosMagLeft.Get(1));
            //bitstream.Set(TrimBitsOffset + 27, _BitLevelDevice.vosMagLeft.Get(0));

            _BitLevelDevice.vosMagRight.CopyTo(finalbitstream, TrimBitsOffset + 28);
            //bitstream.Set(TrimBitsOffset + 28, _BitLevelDevice.vosMagRight.Get(7));
            //bitstream.Set(TrimBitsOffset + 29, _BitLevelDevice.vosMagRight.Get(6));
            //bitstream.Set(TrimBitsOffset + 30, _BitLevelDevice.vosMagRight.Get(5));
            //bitstream.Set(TrimBitsOffset + 31, _BitLevelDevice.vosMagRight.Get(4));
            //bitstream.Set(TrimBitsOffset + 32, _BitLevelDevice.vosMagRight.Get(3));
            //bitstream.Set(TrimBitsOffset + 33, _BitLevelDevice.vosMagRight.Get(2));
            //bitstream.Set(TrimBitsOffset + 34, _BitLevelDevice.vosMagRight.Get(1));
            //bitstream.Set(TrimBitsOffset + 35, _BitLevelDevice.vosMagRight.Get(0));

            _BitLevelDevice.vosElectLeft.CopyTo(finalbitstream, TrimBitsOffset + 36);
            //bitstream.Set(TrimBitsOffset + 36, _BitLevelDevice.vosElectLeft.Get(7));
            //bitstream.Set(TrimBitsOffset + 27, _BitLevelDevice.vosElectLeft.Get(6));
            //bitstream.Set(TrimBitsOffset + 28, _BitLevelDevice.vosElectLeft.Get(5));
            //bitstream.Set(TrimBitsOffset + 29, _BitLevelDevice.vosElectLeft.Get(4));
            //bitstream.Set(TrimBitsOffset + 40, _BitLevelDevice.vosElectLeft.Get(3));
            //bitstream.Set(TrimBitsOffset + 41, _BitLevelDevice.vosElectLeft.Get(2));
            //bitstream.Set(TrimBitsOffset + 42, _BitLevelDevice.vosElectLeft.Get(1));
            //bitstream.Set(TrimBitsOffset + 43, _BitLevelDevice.vosElectLeft.Get(0));

            _BitLevelDevice.vosElectRight.CopyTo(finalbitstream, TrimBitsOffset + 44);
            //bitstream.Set(TrimBitsOffset + 44, _BitLevelDevice.vosElectRight.Get(7));
            //bitstream.Set(TrimBitsOffset + 45, _BitLevelDevice.vosElectRight.Get(6));
            //bitstream.Set(TrimBitsOffset + 46, _BitLevelDevice.vosElectRight.Get(5));
            //bitstream.Set(TrimBitsOffset + 47, _BitLevelDevice.vosElectRight.Get(4));
            //bitstream.Set(TrimBitsOffset + 48, _BitLevelDevice.vosElectRight.Get(3));
            //bitstream.Set(TrimBitsOffset + 49, _BitLevelDevice.vosElectRight.Get(2));
            //bitstream.Set(TrimBitsOffset + 50, _BitLevelDevice.vosElectRight.Get(1));
            //bitstream.Set(TrimBitsOffset + 51, _BitLevelDevice.vosElectRight.Get(0));

            _BitLevelDevice.gainInstrAmpsLeft.CopyTo(finalbitstream, TrimBitsOffset + 52);
            //bitstream.Set(TrimBitsOffset + 52, _BitLevelDevice.gainInstrAmpsLeft.Get(7));
            //bitstream.Set(TrimBitsOffset + 53, _BitLevelDevice.gainInstrAmpsLeft.Get(6));
            //bitstream.Set(TrimBitsOffset + 54, _BitLevelDevice.gainInstrAmpsLeft.Get(5));
            //bitstream.Set(TrimBitsOffset + 55, _BitLevelDevice.gainInstrAmpsLeft.Get(4));
            //bitstream.Set(TrimBitsOffset + 56, _BitLevelDevice.gainInstrAmpsLeft.Get(3));
            //bitstream.Set(TrimBitsOffset + 57, _BitLevelDevice.gainInstrAmpsLeft.Get(2));
            //bitstream.Set(TrimBitsOffset + 58, _BitLevelDevice.gainInstrAmpsLeft.Get(1));
            //bitstream.Set(TrimBitsOffset + 59, _BitLevelDevice.gainInstrAmpsLeft.Get(0));

            _BitLevelDevice.gainInstrAmpsRight.CopyTo(finalbitstream, TrimBitsOffset + 60);
            //bitstream.Set(TrimBitsOffset + 60, _BitLevelDevice.gainInstrAmpsRight.Get(7));
            //bitstream.Set(TrimBitsOffset + 61, _BitLevelDevice.gainInstrAmpsRight.Get(6));
            //bitstream.Set(TrimBitsOffset + 62, _BitLevelDevice.gainInstrAmpsRight.Get(5));
            //bitstream.Set(TrimBitsOffset + 63, _BitLevelDevice.gainInstrAmpsRight.Get(4));
            //bitstream.Set(TrimBitsOffset + 64, _BitLevelDevice.gainInstrAmpsRight.Get(3));
            //bitstream.Set(TrimBitsOffset + 65, _BitLevelDevice.gainInstrAmpsRight.Get(2));
            //bitstream.Set(TrimBitsOffset + 66, _BitLevelDevice.gainInstrAmpsRight.Get(1));
            //bitstream.Set(TrimBitsOffset + 67, _BitLevelDevice.gainInstrAmpsRight.Get(0));

            finalbitstream[TrimBitsOffset + 68] = _BitLevelDevice.gainUniPolar;
            //bitstream.Set(TrimBitsOffset + 68, _BitLevelDevice.gainUniPolar);

            finalbitstream[TrimBitsOffset + 69] = _BitLevelDevice.gainHiSupply;
            //bitstream.Set(TrimBitsOffset + 69, _BitLevelDevice.gainHiSupply);

            finalbitstream[TrimBitsOffset + 70] = _BitLevelDevice.gainFor12mT;
            //bitstream.Set(TrimBitsOffset + 70, _BitLevelDevice.gainFor12mT);

            //_BitLevelDevice.MarkingBits.CopyTo(finalbitstream, TrimBitsOffset + 71);
            finalbitstream[TrimBitsOffset + 75] = _BitLevelDevice.MarkingBits.Get(3);
            finalbitstream[TrimBitsOffset + 74] = _BitLevelDevice.MarkingBits.Get(2);
            finalbitstream[TrimBitsOffset + 71] = _BitLevelDevice.MarkingBits.Get(1);
            finalbitstream[TrimBitsOffset + 0] = _BitLevelDevice.MarkingBits.Get(0);
            //bitstream.Set(TrimBitsOffset + 75, _BitLevelDevice.MarkingBits.Get(3));
            //bitstream.Set(TrimBitsOffset + 74, _BitLevelDevice.MarkingBits.Get(2));
            //bitstream.Set(TrimBitsOffset + 71, _BitLevelDevice.MarkingBits.Get(1));
            //bitstream.Set(TrimBitsOffset + 0, _BitLevelDevice.MarkingBits.Get(0));
           // finalbitstream[TrimBitsOffset + 0] = _BitLevelDevice.FactoryBit;


            finalbitstream[TrimBitsOffset + 76] = _BitLevelDevice.BlockTryBeforeBuyHi;
            //bitstream.Set(TrimBitsOffset + 77, _BitLevelDevice.BlockTryBeforeBuyHi);

            finalbitstream[TrimBitsOffset + 77] = _BitLevelDevice.CrousMurataModes;
            //bitstream.Set(TrimBitsOffset + 78, _BitLevelDevice.CrousMurataModes);

            _BitLevelDevice.BridgeSwaps.CopyTo(finalbitstream, TrimBitsOffset + 78);
            //bitstream.Set(TrimBitsOffset + 79, _BitLevelDevice.BridgeSwaps.Get(1));
            //bitstream.Set(TrimBitsOffset + 80, _BitLevelDevice.BridgeSwaps.Get(0));
            //bool[] finalbitstream = new bool[107];
            //bitstream.CopyTo(finalbitstream, 0);
            return finalbitstream;
        }


        /*
         *  I put some basic trimming routines here to be called in Labview. 
         *  The purpose is to clone the Python methods of CTC4000 here in C#.
         *  This is suppoesed to be in LowLevelTrimming class 
         *  to separate trimming algorithm from underlying device.
         *  
         */
        private bool RepeatCodeCheck(int cCode, ref IDictionary<int, int> cArray)
        {
            short repeatLim = 2;
            if(cArray.ContainsKey(cCode))
                cArray[cCode] = cArray[cCode] + 1;
            else
                cArray[cCode] = 1;
            if (cArray[cCode] > repeatLim)
                return true;
            return false;
        }
        public int DetermineTheNextVrefCode(double stepSize, byte currCode,double measuredVref, double idealVref)
        {
            int nextCode;
            int deltaCode = Convert.ToInt16( Math.Round((measuredVref - idealVref) / stepSize));
            nextCode = currCode + deltaCode;

            if ((nextCode < 0) && (currCode - 32 < 0))
                nextCode = 64 + nextCode;

            if ((nextCode > 63) && (currCode - 31 > 0))
                nextCode = nextCode - 64;

            bool b_reapeatedCode = RepeatCodeCheck(currCode, ref _repeatedVrefCodes);
            if (b_reapeatedCode)
                return 256; // An invalid code that says the current code is the best and we must finish trimming this part.
             else
                return nextCode;
        }
        public int[] InitializeGainTrimming(double ampGainDelta, double maxOutput, double minOutput)
        {

            int finalLeftGainCode = 0;
            int finalRightGainCode = 0;
            perChange = 100.0 * ((ampGainDelta / (maxOutput - minOutput)) - 1);
            if (perChange < 0)
            {
                finalLeftGainCode = 255 + Convert.ToInt16(Math.Round(perChange / leftPerStep_2, 0));
                finalRightGainCode = 255 + Convert.ToInt16(Math.Round(perChange / rightPerStep_2, 0));
                leftFinalStep = leftPerStep_2;
                rightFinalStep = rightPerStep_2;
            }

            else
            {
                finalLeftGainCode = 0 + Convert.ToInt16(Math.Round(perChange / leftPerStep_1, 0));
                finalRightGainCode = 0 + Convert.ToInt16(Math.Round(perChange / rightPerStep_1, 0));
                leftFinalStep = leftPerStep_1;
                rightFinalStep = rightPerStep_1;
            }

            if (perChange > 18)
                return new int[] { 256,256};
            return new int[] { finalLeftGainCode, finalRightGainCode };
        }
        public int[] DetermineTheNextGainCode(double stepSize, int currLeftCode, int currRightCode, double measuredMaxOut, double measuredMinOut, double ampGainDelta)
        {
            



            double measuredLeftAndRightAmpGain = measuredMaxOut - measuredMinOut;
            double biFactor = 1;
            bool leftAndRightGainInScale = (measuredLeftAndRightAmpGain <= biFactor * 1.0016 * ampGainDelta) && (measuredLeftAndRightAmpGain >= biFactor * 0.9984 * ampGainDelta);
            if (leftAndRightGainInScale)
            {
                return new int[] { 256, 256 };// An invalid code that says the current code is the best and we must finish trimming this part.
            }
            int leftDelta = Convert.ToInt16(Math.Round(100.0 * ((ampGainDelta / measuredLeftAndRightAmpGain) - 1) / leftFinalStep, 0));
            int rightDelta = Convert.ToInt16(Math.Round(100.0 * ((ampGainDelta / measuredLeftAndRightAmpGain) - 1) / rightFinalStep, 0));



            bool corrDeltaLeftBool = true;
            bool corrDeltaRightBool = true;
            int  nextLeftCode=0,nextRightCode=0;
            if (currLeftCode + leftDelta > 255) {
                nextLeftCode = currLeftCode + leftDelta - 256;
                corrDeltaLeftBool = false;
            }

            if (currRightCode + rightDelta > 255)
            {
                nextRightCode = currRightCode + rightDelta - 256;
                corrDeltaRightBool = false;

            }

            if (currLeftCode + leftDelta < 0)
            {
                nextLeftCode = currLeftCode + leftDelta + 256;
                corrDeltaLeftBool = false;

            }

            if(currRightCode + rightDelta < 0)
            {
                nextRightCode = currRightCode + rightDelta + 256;
                corrDeltaRightBool = false;

            }

            if (corrDeltaLeftBool)
                nextLeftCode = currLeftCode + leftDelta;

            if (corrDeltaRightBool)
                nextRightCode = currRightCode + rightDelta;
            //int returnCodes[2] = { nextLeftCode, nextRightCode };
            return new int[] { nextLeftCode, nextRightCode };
        }

        public int[] DetermineTheNextElecOffsetCode(double stepSize, byte currLeftCode, byte currRighttCode, double measuredOffset, double idealOffset)
        {
            int nextCode;
            int deltaCode = Convert.ToInt16(Math.Round((measuredOffset - idealOffset) / stepSize));
            //nextCode = currCode + deltaCode;

            byte currCode = currLeftCode;

// Current Code is Between 128 and 255
            if ((currCode >= 128) && (currCode <= 255)) {
                deltaCode = -deltaCode;

                if ((currCode + deltaCode) < 128)
                    nextCode = Math.Abs(127 - (currCode + deltaCode));


                else
                {
                    nextCode = currCode + deltaCode;

                    if (nextCode > 255)
                        nextCode = 255;
                }
            }
// Current Code is Between 0 and 127        
            else {

                if ((currCode + deltaCode) < 0)
                    nextCode = 129 + currCode + Math.Abs(deltaCode);



                else
                {
                    nextCode = currCode + deltaCode;

                    if (nextCode > 127)
                        nextCode = 127;
                }
            }

            bool b_reapeatedCode = RepeatCodeCheck(currCode, ref _repeatedElecOffsetCodes);
            if (b_reapeatedCode)
                return new int[] { 256, 256 }; // An invalid code that says the current code is the best and we must finish trimming this part.
            else
                return new int[] { nextCode, nextCode }; 
        }
        public int[] DetermineTheNextMagOffsetCode(double stepSize, byte currLeftCode, byte currRighttCode, double measuredOffset, double idealOffset)
        {
            int nextCode;
            int deltaCode = Convert.ToInt16(Math.Round((measuredOffset - idealOffset) / stepSize));
            //nextCode = currCode + deltaCode;

            byte currCode;
            if (leftORRightCodeBool)
            {
                currCode = currRighttCode;
            }
            else
            {
                currCode = currLeftCode;
            }

            // Current Code is Between 128 and 255
            if ((currCode >= 128) && (currCode <= 255))
            {
                deltaCode = -deltaCode;

                if ((currCode + deltaCode) < 128)
                    nextCode = Math.Abs(127 - (currCode + deltaCode));


                else
                {
                    nextCode = currCode + deltaCode;

                    if (nextCode > 255)
                        nextCode = 255;
                }
            }
            // Current Code is Between 0 and 127        
            else
            {

                if ((currCode + deltaCode) < 0)
                    nextCode = 129 + currCode + Math.Abs(deltaCode);



                else
                {
                    nextCode = currCode + deltaCode;

                    if (nextCode > 127)
                        nextCode = 127;
                }
            }

            bool b_reapeatedCode = RepeatCodeCheck(currCode, ref _repeatedMagOffsetCodes);
            if (b_reapeatedCode)
                return new int[] { 256, 256 }; // An invalid code that says the current code is the best and we must finish trimming this part.
            else
            {
                if (leftORRightCodeBool)
                {
                    leftORRightCodeBool = !leftORRightCodeBool;
                    return new int[] { currLeftCode, nextCode };
                }
                else
                {
                    leftORRightCodeBool = !leftORRightCodeBool;
                    return new int[] { nextCode, currRighttCode };
                }

            }
        }
        public bool IsDeviceAlreadyTrimmed(bool[] readBitStream)
        {
            //if(GetValue(ref _BitLevelDevice.vRefScaledBandgapTrim)!=0)
            //    return true;
            bool[] finalbitstream = new bool[106];

            Array.Copy(readBitStream, 0, finalbitstream, 0, 106);
            BitArray bitstream = new BitArray(new byte[106]);
            bitstream.SetAll(false);

            //8-bit  Keycode. No need to copy it.

            //2-bit Opcode. No need to copy it.

            short TrimBitsOffset = 26;
            //16-bit Test (Control) bits

            BitArray temp = new BitArray(_BitLevelDevice.vosMagLeft.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 20];
            temp[1] = finalbitstream[TrimBitsOffset + 21];
            temp[2] = finalbitstream[TrimBitsOffset + 22];
            temp[3] = finalbitstream[TrimBitsOffset + 23];
            temp[4] = finalbitstream[TrimBitsOffset + 24];
            temp[5] = finalbitstream[TrimBitsOffset + 25];
            temp[6] = finalbitstream[TrimBitsOffset + 26];
            temp[7] = finalbitstream[TrimBitsOffset + 27];
            if (GetValue(ref temp) != 0)
                return true;

            temp = new BitArray(_BitLevelDevice.vosMagRight.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 28];
            temp[1] = finalbitstream[TrimBitsOffset + 29];
            temp[2] = finalbitstream[TrimBitsOffset + 30];
            temp[3] = finalbitstream[TrimBitsOffset + 31];
            temp[4] = finalbitstream[TrimBitsOffset + 32];
            temp[5] = finalbitstream[TrimBitsOffset + 33];
            temp[6] = finalbitstream[TrimBitsOffset + 34];
            temp[7] = finalbitstream[TrimBitsOffset + 35];
            if (GetValue(ref temp) != 0)
                return true;


            temp = new BitArray(_BitLevelDevice.vosElectLeft.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 36];
            temp[1] = finalbitstream[TrimBitsOffset + 37];
            temp[2] = finalbitstream[TrimBitsOffset + 38];
            temp[3] = finalbitstream[TrimBitsOffset + 39];
            temp[4] = finalbitstream[TrimBitsOffset + 40];
            temp[5] = finalbitstream[TrimBitsOffset + 41];
            temp[6] = finalbitstream[TrimBitsOffset + 42];
            temp[7] = finalbitstream[TrimBitsOffset + 43];
            if (GetValue(ref temp) != 0)
                return true;

            temp = new BitArray(_BitLevelDevice.vosElectRight.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 44];
            temp[1] = finalbitstream[TrimBitsOffset + 45];
            temp[2] = finalbitstream[TrimBitsOffset + 46];
            temp[3] = finalbitstream[TrimBitsOffset + 47];
            temp[4] = finalbitstream[TrimBitsOffset + 48];
            temp[5] = finalbitstream[TrimBitsOffset + 49];
            temp[6] = finalbitstream[TrimBitsOffset + 50];
            temp[7] = finalbitstream[TrimBitsOffset + 51];
            if (GetValue(ref temp) != 0)
                return true;

            temp = new BitArray(_BitLevelDevice.gainInstrAmpsLeft.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 52];
            temp[1] = finalbitstream[TrimBitsOffset + 53];
            temp[2] = finalbitstream[TrimBitsOffset + 54];
            temp[3] = finalbitstream[TrimBitsOffset + 55];
            temp[4] = finalbitstream[TrimBitsOffset + 56];
            temp[5] = finalbitstream[TrimBitsOffset + 57];
            temp[6] = finalbitstream[TrimBitsOffset + 58];
            temp[7] = finalbitstream[TrimBitsOffset + 59];
            if (GetValue(ref temp) != 0)
                return true;

            temp = new BitArray(_BitLevelDevice.gainInstrAmpsRight.Length);
            temp[0] = finalbitstream[TrimBitsOffset + 60];
            temp[1] = finalbitstream[TrimBitsOffset + 61];
            temp[2] = finalbitstream[TrimBitsOffset + 62];
            temp[3] = finalbitstream[TrimBitsOffset + 63];
            temp[4] = finalbitstream[TrimBitsOffset + 64];
            temp[5] = finalbitstream[TrimBitsOffset + 65];
            temp[6] = finalbitstream[TrimBitsOffset + 66];
            temp[7] = finalbitstream[TrimBitsOffset + 67];
            if (GetValue(ref temp) != 0)
                return true;




            if (finalbitstream[TrimBitsOffset + 76])//Block TBBM
                return true;
            if (finalbitstream[TrimBitsOffset + 77] == true) // Crocus Murata Mode
                return true;
            if (finalbitstream[TrimBitsOffset + 70] == true)//Gain for 12mT
                return true;

            if (GetValue(ref _BitLevelDevice.MarkingBits) != 0)
                return true;

            return false;
        }
    }// Tnslation Layer Class

}
