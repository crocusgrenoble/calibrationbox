﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
//using static CalibrationSfotware.HaloTranslationLayer;
using CurrentSupply;
using Device;

namespace CalibrationSfotware
{
    public class HighLevelTrimming
    {
        private LowLevelTrimming _LowLevelTrimmingObj;
        //private Programmer.Programmer _ProgrammerObj;
        public HighLevelTrimming(string programmer , string supply, string polarity, string gainFor12mT, string bridgeSwapS, double maxCurrent ,ErrorReportHandlerDel ErrHndlr, CurrentSupply.CurrentSupply CurrentSupplyObj) 
        {
            LowLevelTrimming _LowLevelTrimmingObj = new LowLevelTrimming(programmer, supply, polarity, gainFor12mT, bridgeSwapS,maxCurrent, ErrHndlr, CurrentSupplyObj);
            //_ProgrammerObj = ProgramerObj;  
        }
        public bool TrimVref(double precision) { return false; }
        public bool TrimOffset(double precision) { return false; }
        public bool TrimSensitivity(double precision) { return false; }
        public bool FuseBits(double precision) { return false; }

    }
}
