﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CurrentSupply;
using Device;
//using DeviceLayer;
namespace CalibrationSfotware
{
    internal class LowLevelTrimming
    {
        private HaloTranslationLayer _HaloDevice;
        private CurrentSupply.CurrentSupply _CurrentSupplyObj;
        ErrorReportHandlerDel ErrorReportHandler;

        private double _idealVref;
        private double _vRefStepSize;
        private double _MaxCurrent;
        private double _idealOutputRange;
        private double _MaxCurr,_MinCurr;
        private string programmer;
        private string supply;
        private string polarity;
        private string gainFor12mT;
        private ErrorReportHandlerDel errHndlr;
        private CurrentSupply.CurrentSupply currentSupplyObj;

        public LowLevelTrimming(string programmer, string supply, string polarity, string gainFor12mT, string bridgeSwapS, double MaxCurrent ,ErrorReportHandlerDel ErrHndlr, CurrentSupply.CurrentSupply CurrentSupplyObj)
        {
            ErrorReportHandler = ErrHndlr;
            _CurrentSupplyObj = CurrentSupplyObj;
            _MaxCurrent = MaxCurrent;
            HaloTranslationLayer HaloDevice = new HaloTranslationLayer(programmer, supply, polarity, gainFor12mT, bridgeSwapS, ErrHndlr);
            _MaxCurr = MaxCurrent;
            if (supply == "5v0")
            {
                _idealOutputRange = 4.0; // 5v sensor output range is 4V

                if (polarity == "U")
                {
                    _idealVref = 0.5F;
                    _vRefStepSize = 0.488F;
                    _MinCurr = 0;
                }
                else if (polarity == "B")
                {
                    _idealVref = 2.5F;
                    _vRefStepSize = 2.438F;
                    _MinCurr = -MaxCurrent;
                }
                else
                    ErrorReportHandler("Polarity value is not Correct", reportLevel.error);
            }
            else if (supply == "5v0")
            {
                _idealOutputRange = 2.0; // 5v sensor output range is 4V
                if (polarity == "U")
                {
                    _idealVref = 0.65F;
                    _vRefStepSize = 0.633F;
                    _MinCurr = 0;
                }
                else if (polarity == "B")
                {
                    _idealVref = 1.65F;
                    _vRefStepSize = 1.608F;
                    _MinCurr = -MaxCurrent;
                }
                else
                    ErrorReportHandler("Polarity value is not Correct", reportLevel.error);
            }
        }

        public LowLevelTrimming(string programmer, string supply, string polarity, string gainFor12mT, ErrorReportHandlerDel errHndlr, CurrentSupply.CurrentSupply currentSupplyObj)
        {
            this.programmer = programmer;
            this.supply = supply;
            this.polarity = polarity;
            this.gainFor12mT = gainFor12mT;
            this.errHndlr = errHndlr;
            this.currentSupplyObj = currentSupplyObj;
        }

        //public bool CopyDefaultsFromDevice()
        //{
        //    // Read current trimm bits from the device
        //    if (_HaloDevice.ReadTrimmingBitsFromDevice()) return true;
        //    // Read BandgapXdown, Bandgap corner and PtatUp bits from latest read from the device
        //    if (_HaloDevice.CopyBgapXDownFromDevice()) return true;
        //    if(_HaloDevice.CopyProgrammedBandgapCornerFromDevice()) return true;
        //    if(_HaloDevice.CopyPtatUPFromDevice()) return true;

        //    return false;
        //}

        /* 
         * A device is lready have some bits programmed at factory.
         * We need to keep them in the trimming object to reprogram them again 
         */
        public bool InitObjectFromDeviceFactoryBits()
        { 
            if (_HaloDevice.ReadTrimmingBitsFromDevice())
            {
                ErrorReportHandler("Error in Trimming Vref", reportLevel.error);
                return true; 
            }
            return false;
        }   
        public bool TrimVref() {

            
            _HaloDevice.ProgramDevice();// Write Already determined bits from the object to the device
            
            _HaloDevice.ConnectVrefToOUT(); // Route Vref to the OUTput
//            _HaloDevice.ProgramDevice(); // Apply bit changes

            /* Determine the Vref offset range in Voltage */

            // Set Vref offset to max : 63
            _HaloDevice.SetVrefScaledBandgapTrim(63);
            _HaloDevice.ProgramDevice(); // Apply bit changes
            double MaxVref = _HaloDevice.ReadAnalogOutputVoltage();

            // Set Vref offset to min : 0
            _HaloDevice.SetVrefScaledBandgapTrim(0);
            _HaloDevice.ProgramDevice(); // Apply bit changes


            double MinVref = _HaloDevice.ReadAnalogOutputVoltage();

            // Set Vref offset to the middle  
            _HaloDevice.SetVrefScaledBandgapTrim(31);
            _HaloDevice.ProgramDevice(); // Apply bit changes

            double NoOffsetVref = _HaloDevice.ReadAnalogOutputVoltage();

            _vRefStepSize = (MaxVref - MinVref) / 64.0; // 64 steps exist for Vref Trimming 

            double diff = NoOffsetVref - _idealVref ; // Current difference between ideal and current Vref
            double precision = _vRefStepSize / 2.0; // Half of the Vref trimming step size
            Double Out;
            while (Math.Abs(diff) > precision)
            {
                int diffSteps = (int)Math.Round(diff/ _vRefStepSize); // Clculate how many offset trim steps needed to reach to ideal Vref
                int CurrentStep = _HaloDevice.GetVrefScaledBandgapTrim();
                if (_HaloDevice.SetVrefScaledBandgapTrim(CurrentStep + diffSteps))
                {
                    ErrorReportHandler("Error in trimming Vref offset", reportLevel.error);
                }
                _HaloDevice.ProgramDevice(); // Apply bit changes

               Out = _HaloDevice.ReadAnalogOutputVoltage(); // Read sensor output
               diff = Out - _idealVref;
            }
            _HaloDevice.ClearTestBits(); // Clear test bits including: disconnect the Vref from OUTput
            _HaloDevice.ProgramDevice(); // Apply bit changes
            return false; 
        }
        //Set Current to max and min and reads the device min and max output rage.
        private List<double> ReadDeviceAnalogOutOnCurrentSweep()
        {
            _CurrentSupplyObj.SetCurrent(_MaxCurr);
            _CurrentSupplyObj.CurrentON();
            double MaxOut = _HaloDevice.ReadAnalogOutputVoltage();
            _CurrentSupplyObj.CurrentOFF();

            _CurrentSupplyObj.SetCurrent(_MinCurr);
            _CurrentSupplyObj.CurrentON();
            double MinOut = _HaloDevice.ReadAnalogOutputVoltage();
            _CurrentSupplyObj.CurrentOFF();

            List<double> outputs = new List<double> { MaxOut, MinOut };
            return outputs;
        }
        public bool TrimRightLeftAmplifiersGain(double precision)
        {
            //_HaloDevice.ProgramDevice();// Write Already determined bits from the object to the device
            //_HaloDevice.LeftAmpEnable(); // Enalble Left Amplifier
            //_HaloDevice.ShortLeftBrdige(); // Short Left Brdige
            //_HaloDevice.RightAmpDisable();// Disable Right Amplifier
            //_HaloDevice.OpenRightBrdige(); // Open Right Bridge
            
            // Trimming Left and Right Amplifiers at the same time
            _HaloDevice.ClearTestBits(); // Clear test bits
            _HaloDevice.ProgramDevice(); // Apply Changes

            // Determining the actual output range
            List<double> DeviceOutOnCurrSweep = ReadDeviceAnalogOutOnCurrentSweep();
            
            double OutputRange = DeviceOutOnCurrSweep[1] - DeviceOutOnCurrSweep[0];
            double ActualSensitivityDifferencePercentage = 100.0 * (_idealOutputRange / OutputRange) - 100;

            if (ActualSensitivityDifferencePercentage > 18) {
                String S = String.Format("Actual and ideal output ranges are more than 18% different. {0}V vs {1}V." +
                    " The difference it too high to trim.", OutputRange, _idealOutputRange);
                ErrorReportHandler(S,Device.reportLevel.error);
            }

            double LeftAmpGainPerStep;
            double RightAmpGainPerStep;

            // Choose amplifier gain steps 
            if (ActualSensitivityDifferencePercentage < 0)
            {
                LeftAmpGainPerStep = 0.1769;
                RightAmpGainPerStep = LeftAmpGainPerStep;
            }
            else
            {
                LeftAmpGainPerStep = 0.1443;
                RightAmpGainPerStep = LeftAmpGainPerStep;
            }



            // Sweep gains to reach the optimal value
            double error =0;

            do
            {
                // Calculate the next gain value 
                ActualSensitivityDifferencePercentage = 100.0 * (_idealOutputRange / OutputRange) - 100;
                int LeftAmpGainDifference =  (int)Math.Round(ActualSensitivityDifferencePercentage / LeftAmpGainPerStep, 0);
                int RightAmpGainDifference = (int)Math.Round(ActualSensitivityDifferencePercentage / RightAmpGainPerStep, 0);

                // Set the gain 
                 int CurrentLeftGain = _HaloDevice.GetLeftAmpGain();
                 int CurrentRightGain = _HaloDevice.GetRightAmpGain();
                _HaloDevice.SetLeftAmpGain(LeftAmpGainDifference + CurrentLeftGain);
                _HaloDevice.SetRightAmpGain(RightAmpGainDifference + CurrentRightGain);
                _HaloDevice.ProgramDevice();

                // Read the device range

                DeviceOutOnCurrSweep = ReadDeviceAnalogOutOnCurrentSweep();
                OutputRange = DeviceOutOnCurrSweep[1] - DeviceOutOnCurrSweep[0];

                // Calc error
                error = Math.Abs(OutputRange - _idealOutputRange) / _idealOutputRange;


            }
            while (error > precision);
            _HaloDevice.ClearTestBits(); // Clear test bits including.
            _HaloDevice.ProgramDevice(); // Apply bit changes

            return false;

        }
        /*
         * 
         * 
         */
        public bool TrimElectricalOffset() {
            _HaloDevice.ClearTestBits(); // Clear test bits
            _HaloDevice.ProgramDevice(); // Apply Changes
                                         // 
            // Short left and right bridge 
            _HaloDevice.ShortLeftBrdige();
            _HaloDevice.ShortRightBrdige();


            // Measure min elec offset
            _HaloDevice.SetLeftElecOffset(0);
            _HaloDevice.SetRightElecOffset(0);
            _HaloDevice.ProgramDevice();

            double MinVref = _HaloDevice.ReadAnalogOutputVoltage();

            // Measure maz elec offset
            _HaloDevice.SetLeftElecOffset(255);
            _HaloDevice.SetRightElecOffset(255);
            _HaloDevice.ProgramDevice();

            double MaxVref = _HaloDevice.ReadAnalogOutputVoltage();

            // Set to the middle value 
            _HaloDevice.SetLeftElecOffset(128);
            _HaloDevice.SetRightElecOffset(128);
            _HaloDevice.ProgramDevice();

            double NoOffsetVref = _HaloDevice.ReadAnalogOutputVoltage();

            _vRefStepSize = (MaxVref - MinVref) / 256.0; // 64 steps exist for Vref Trimming 

            double diff = NoOffsetVref - _idealVref; // Current difference between ideal and current Vref
            double precision = _vRefStepSize / 2.0; // Half of the Vref trimming step size
            Double Out;
            while (Math.Abs(diff) > precision)
            {
                int diffSteps = (int)Math.Round(diff / _vRefStepSize); // Clculate how many offset trim steps needed to reach to ideal Vref
                int CurrentLeftElec = _HaloDevice.GetLeftElecOffset();
                int CurrentRightElec = _HaloDevice.GetRightElecOffset();
                if (_HaloDevice.SetLeftElecOffset(CurrentLeftElec + diffSteps))
                {
                    ErrorReportHandler("Error in trimming Elect offset", reportLevel.error);
                }
                if (_HaloDevice.SetRightElecOffset(CurrentRightElec + diffSteps))
                {
                    ErrorReportHandler("Error in trimming Elect offset", reportLevel.error);
                }
                _HaloDevice.ProgramDevice(); // Apply bit changes

                Out = _HaloDevice.ReadAnalogOutputVoltage(); // Read sensor output
                diff = Out - _idealVref;
            }
            _HaloDevice.ClearTestBits(); // Clear test bits including: disconnect the Vref from OUTput
            _HaloDevice.ProgramDevice(); // Apply bit changes
            return false;
        
        }
        /*
          * 
          * 
          */
        public bool TrimMagneticOffset()
        {
            _HaloDevice.ClearTestBits(); // Clear test bits
            _HaloDevice.ProgramDevice(); // Apply Changes



            // Measure min Magnetic offset
            _HaloDevice.SetLeftMagneticOffset(0);
            _HaloDevice.SetRightMagneticOffset(0);
            _HaloDevice.ProgramDevice();

            double MinVref = _HaloDevice.ReadAnalogOutputVoltage();

            // Measure maz Magnetic offset
            _HaloDevice.SetLeftMagneticOffset(255);
            _HaloDevice.SetRightMagneticOffset(255);
            _HaloDevice.ProgramDevice();

            double MaxVref = _HaloDevice.ReadAnalogOutputVoltage();

            // Set to the middle value 
            _HaloDevice.SetLeftMagneticOffset(128);
            _HaloDevice.SetRightMagneticOffset(128);
            _HaloDevice.ProgramDevice();

            double NoOffsetVref = _HaloDevice.ReadAnalogOutputVoltage();

            _vRefStepSize = (MaxVref - MinVref) / 256.0; // 64 steps exist for Vref Trimming 

            double diff = NoOffsetVref - _idealVref; // Current difference between ideal and current Vref
            double precision = _vRefStepSize / 2.0; // Half of the Vref trimming step size
            Double Out;
            while (Math.Abs(diff) > precision)
            {
                int diffSteps = (int)Math.Round(diff / _vRefStepSize); // Clculate how many offset trim steps needed to reach to ideal Vref
                int CurrentLeftMagnetic = _HaloDevice.GetLeftMagneticOffset();
                int CurrentRightMagnetic = _HaloDevice.GetRightMagneticOffset();
                if (_HaloDevice.SetLeftMagneticOffset(CurrentLeftMagnetic + diffSteps))
                {
                    ErrorReportHandler("Error in trimming Magnetic offset", reportLevel.error);
                }
                if (_HaloDevice.SetRightMagneticOffset(CurrentRightMagnetic + diffSteps))
                {
                    ErrorReportHandler("Error in trimming Magnetic offset", reportLevel.error);
                }
                _HaloDevice.ProgramDevice(); // Apply bit changes

                Out = _HaloDevice.ReadAnalogOutputVoltage(); // Read sensor output
                diff = Out - _idealVref;
            }
            _HaloDevice.ClearTestBits(); // Clear test bits including: disconnect the Vref from OUTput
            _HaloDevice.ProgramDevice(); // Apply bit changes
            return false;

        }
        /*
         * 
         * 
         * 
         */

        public bool FuseBits(double precision) { return false; }

        //private 
    }
}
