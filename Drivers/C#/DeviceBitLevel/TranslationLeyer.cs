﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CurrentSupply;
using static System.Runtime.CompilerServices.RuntimeHelpers;

namespace CalibrationSfotware
{
    public enum reportLevel { info, warning, error };
    public delegate void ErrorReportHandlerDel(string message, reportLevel l);

    public class HaloTranslationLayer
    {
        ErrorReportHandlerDel ErrorReportHandler;
        private HaloBitLayer _BitLevelDevice;

        private IDictionary<string, bool> _supplyString2Bool;
        private IDictionary<string, bool> _polarityString2Bool;
        private IDictionary<string, bool> _gainFor12mTString2Bool;
        private void DummyErrorHandler(string message, reportLevel l)
        {
            return;
        }

        public HaloTranslationLayer(string programmerS, string supplyS, string polarityS, string gainFor12mTS, ErrorReportHandlerDel ErrHndlr=null)
        {
            
            if (ErrHndlr != null)
                ErrorReportHandler = ErrHndlr;
            else
                ErrorReportHandler = DummyErrorHandler;
            // Init translation dictionaries
            _supplyString2Bool = new Dictionary<string, bool>() {
                {"3v3" , false },
                { "5v0", true }
            };
            _polarityString2Bool = new Dictionary<string, bool>() {
                {"B" , false },
                { "U", true }
            };
            _gainFor12mTString2Bool = new Dictionary<string, bool>() {
                {"8mT" , false },
                { "12mT", true }
            };
            _BitLevelDevice = new HaloBitLayer(_supplyString2Bool[supplyS], _polarityString2Bool[polarityS], _gainFor12mTString2Bool[gainFor12mTS]);
            // Determining ideal Vref and step sizes





        }// Halo Translation Layer Constructor 
        // Four modes of operation 
        public bool EnterTryBeforeBuyMode() { return false; }
        public bool EnterReadbackMode() {
            _BitLevelDevice.OpCode.Set(0, true);
            _BitLevelDevice.OpCode.Set(1, true);
            return false; 
        }
        public bool EnterFuseMode() { return false; }
        public bool EnterNormalMode() { return false; } 
        /*
         *  This programs device with the set value in the class instance. 
         *  It does NOT fuse the device. So the changes are not permanenet.
         * 
         */
        public bool ProgramDevice()
        { return false; }
        public double ReadAnalogOutputVoltage()
        {
            return 0;
        }

        /*
        * Readout all bits from  the device. A copy of the latest readout will be
        * stored in memory to be used to copy progmammed values from device or 
        * generate a report from the trimmed bits or compare with the written trim bits.
        */
        public bool ReadTrimmingBitsFromDevice()
        {
            // Read All bits from device and refresh the Halo class object with the bit values
            if (CopyBgapXDownFromDevice()) return true;
            if (CopyProgrammedBandgapCornerFromDevice()) return true;
            if (CopyPtatUPFromDevice()) return true;
            return false; 
        }

        // Reads device's current code and keep it in the trimming object to be used again in programmig the device
        public bool CopyProgrammedBandgapCornerFromDevice()
        {
            return false;
        }
        // Reads device's current code and keep it in the trimming object to be used again in programmig the device
        public bool CopyPtatUPFromDevice()
        {
            return false;
        }
        // Reads device's current code and keep it in the trimming object to be used again in programmig the device
        public bool CopyBgapXDownFromDevice()
        {
            return false;
        }
        public bool SetProgrammedBandgapCorner()
        {
            return false;
        }
        public bool SetPtatUP()
        {
            return false;
        }
        public bool SetBgapXDown()
        {
            return false;
        }
        
        public bool ClearTestBits()
        { return false; }
        
        
        public bool ConnectVrefToOUT()
        {
            return false;
        }
        public bool DisconnectVrefToOUT()
        {
            return false;
        }
        public bool ShortLeftBrdige()
        { 
            return false; 
        }
        public bool ShortRightBrdige()
        {
            return false;
        }

        public bool OpenLeftBrdige()
        {
            return false;
        }
        public bool OpenRightBrdige()
        {
            return false;
        }
        public bool RightAmpDisable()
        {
            return false;
        }
        public bool RightAmpEnable()
        {
            return false;
        }
        public bool LeftAmpDisable()
        {
            return false;
        }
        public bool LeftAmpEnable()
        {
            return false;
        }
        public bool SetLeftAmpGain(int gain)
        {
            return false;        
        }
        public bool SetRightAmpGain(int gain)
        { 
            return false;
        }
        public int GetLeftAmpGain()
        {
            return 0;
        }
        public int GetRightAmpGain()
        {
            return 0;
        }
        public bool SetLeftElecOffset(int offset)
        {
            return false;
        }
        public bool SetRightElecOffset(int offset)
        {
            return false;
        }
        public int GetLeftElecOffset()
        {
            return 0;
        }
        public int GetRightElecOffset()
        {
            return 0;
        }
        public bool SetLeftMagneticOffset(int offset)
        {
            return false;
        }
        public bool SetRightMagneticOffset(int offset)
        {
            return false;
        }
        public int GetLeftMagneticOffset()
        {
            return 0;
        }
        public int GetRightMagneticOffset()
        {
            return 0;
        }
        public bool SetVrefScaledBandgapTrim(int OffSetValue)
        {
            if(OffSetValue<0 || OffSetValue >= Math.Pow(2, _BitLevelDevice.vRefScaledBandgapTrim.Length))
            {
                //String s = String.Format("The demanded change on Vref={0} is smaller than one Vref step size {1} ", OffSetValue, vRefStepSize);
               // ErrorReportHandler(s, reportLevel.warning);
                return true;

            }
            else
            {
                byte new_code = BitConverter.GetBytes(OffSetValue)[0];
                BitArray Barr = new BitArray(new byte[] { new_code });
                Barr.Length = _BitLevelDevice.vRefScaledBandgapTrim.Length;
                _BitLevelDevice.vRefScaledBandgapTrim = Barr;
                return false;
            }

            //if (System.Math.Abs(offsetVoltage) > (vRefStepSize / 2.0F))
            //{
            //    int steps = ((int)System.Math.Round((offsetVoltage / vRefStepSize), 0));// Calculate the step size
            //byte[] b = new byte[1];
            //_BitLevelDevice.vRefScaledBandgapTrim.CopyTo(b, 0); // Get the current bandgap value
            //byte new_code = (byte)(b[0] + (byte)steps); // Add it to the step size
            //BitArray Barr = new BitArray(new byte[] { new_code }); // Convert to BitArray
            //Barr.Length = _BitLevelDevice.vRefScaledBandgapTrim.Length; // Trim Bitarray size
            //_BitLevelDevice.vRefScaledBandgapTrim = Barr; // Write new code back to the device

            //    return false;
            //}
            //else
            //{
            //    String s = String.Format("The demanded change on Vref={0} is smaller than one Vref step size {1} ", offsetVoltage, vRefStepSize);
            //    ErrorReportHandler(s, reportLevel.warning);
            //    return true;
            //}
        }

        public int GetVrefScaledBandgapTrim()
        {
            byte[] b = new byte[1];
            _BitLevelDevice.vRefScaledBandgapTrim.CopyTo(b, 0); // Get the current bandgap value

            //if (System.Math.Abs(offsetVoltage) > (vRefStepSize / 2.0F))
            //{
            //    int steps = ((int)System.Math.Round((offsetVoltage / vRefStepSize), 0));// Calculate the step size
            //    byte[] b = new byte[1];
            //    _BitLevelDevice.vRefScaledBandgapTrim.CopyTo(b, 0); // Get the current bandgap value
            //    byte new_code = (byte)(b[0] + (byte)steps); // Add it to the step size
            //    BitArray Barr = new BitArray(new byte[] { new_code }); // Convert to BitArray
            //    Barr.Length = _BitLevelDevice.vRefScaledBandgapTrim.Length; // Trim Bitarray size
            //    _BitLevelDevice.vRefScaledBandgapTrim = Barr; // Write new code back to the device

            //    return false;
            //}
            //else
            //{
            //    String s = String.Format("The demanded change on Vref={0} is smaller than one Vref step size {1} ", offsetVoltage, vRefStepSize);
            //    ErrorReportHandler(s, reportLevel.warning);
            //    return true;
            //}
            return 0;
        }
        public void SetSupply(string supply)
        {
            if (_supplyString2Bool.ContainsKey(supply))
            {
                _BitLevelDevice.gainHiSupply = _supplyString2Bool[supply];
            }
        }

        public void SetPolarity(string polarity)
        {
            if (_polarityString2Bool.ContainsKey(polarity))
            {
                _BitLevelDevice.gainUniPolar = _polarityString2Bool[polarity];
            }
        }
        public void SetGainFor12mT(string deviceType)
        {
            if (_gainFor12mTString2Bool.ContainsKey(deviceType))
            {
                _BitLevelDevice.gainFor12mT = _gainFor12mTString2Bool[deviceType];
            }
        }
        public vois ZeroAllTestBits()
        { 
        }

        public bool[] GenerateFinalBitstream()
        {
            BitArray bitstream = new BitArray(new byte[107]);
            bool[] finalbitstream = new bool[107];
            bitstream.SetAll(false);

            //8-bit  Keycode 
            _BitLevelDevice.KeyCode.CopyTo(finalbitstream, 0);
            //bitstream.Set(0, _BitLevelDevice.KeyCode.Get(7));
            //bitstream.Set(1, _BitLevelDevice.KeyCode.Get(6));
            //bitstream.Set(2, _BitLevelDevice.KeyCode.Get(5));
            //bitstream.Set(3, _BitLevelDevice.KeyCode.Get(4));
            //bitstream.Set(4, _BitLevelDevice.KeyCode.Get(3));
            //bitstream.Set(5, _BitLevelDevice.KeyCode.Get(2));
            //bitstream.Set(6, _BitLevelDevice.KeyCode.Get(1));
            //bitstream.Set(7, _BitLevelDevice.KeyCode.Get(0));

            //2-bit Opcode
            _BitLevelDevice.OpCode.CopyTo(finalbitstream, 8);
            //bitstream.Set(8, _BitLevelDevice.OpCode.Get(1));
            //bitstream.Set(9, _BitLevelDevice.OpCode.Get(0));


            //16-bit Test (Control) bits
            finalbitstream[10] = _BitLevelDevice.leftAmpDis;
            finalbitstream[11] = _BitLevelDevice.leftBridgeShort;
            finalbitstream[12] = _BitLevelDevice.rightAmpDis;
            finalbitstream[13] = _BitLevelDevice.rightBridgeShort;
            finalbitstream[14] = _BitLevelDevice.overTempTermalTest;
            finalbitstream[15] = _BitLevelDevice.disAfe;

            //bitstream.Set(10, _BitLevelDevice.leftAmpDis);
            //bitstream.Set(11, _BitLevelDevice.leftBridgeShort);
            //bitstream.Set(12, _BitLevelDevice.rightAmpDis);
            //bitstream.Set(13, _BitLevelDevice.rightBridgeShort);
            //bitstream.Set(14, _BitLevelDevice.overTempTermalTest);
            //bitstream.Set(15, _BitLevelDevice.disAfe);

            _BitLevelDevice.testMuxInfra.CopyTo(finalbitstream, 16);
            //bitstream.Set(16, _BitLevelDevice.testMuxInfra[3]);
            //bitstream.Set(17, _BitLevelDevice.testMuxInfra[2]);
            //bitstream.Set(18, _BitLevelDevice.testMuxInfra[1]);
            //bitstream.Set(19, _BitLevelDevice.testMuxInfra[0]);

            _BitLevelDevice.testMuxAfe.CopyTo(finalbitstream, 20);
            //bitstream.Set(20, _BitLevelDevice.testMuxAfe[3]);
            //bitstream.Set(21, _BitLevelDevice.testMuxAfe[2]);
            //bitstream.Set(22, _BitLevelDevice.testMuxAfe[1]);
            //bitstream.Set(23, _BitLevelDevice.testMuxAfe[0]);

            finalbitstream[24] = _BitLevelDevice.testMuxInfraEn;
            finalbitstream[25] = _BitLevelDevice.testMuxAfeEn;
            //bitstream.Set(24, _BitLevelDevice.testMuxInfraEn);
            //bitstream.Set(25, _BitLevelDevice.testMuxAfeEn);

            //77 bits trim bits
            short TrimBitsOffset = 26;
            finalbitstream[TrimBitsOffset+0] = _BitLevelDevice.FactoryBit;
            //bitstream.Set(TrimBitsOffset + 0, _BitLevelDevice.FactoryBit);

            finalbitstream[TrimBitsOffset + 73] = _BitLevelDevice.vRefBgCornerTrim.Get(0);
            finalbitstream[TrimBitsOffset + 1] = _BitLevelDevice.vRefBgCornerTrim.Get(1);
            finalbitstream[TrimBitsOffset + 2] = _BitLevelDevice.vRefBgCornerTrim.Get(2);
            //bitstream.Set(TrimBitsOffset + 1, _BitLevelDevice.vRefBgCornerTrim.Get(2));
            //bitstream.Set(TrimBitsOffset + 2, _BitLevelDevice.vRefBgCornerTrim.Get(1));
            //bitstream.Set(TrimBitsOffset + 73, _BitLevelDevice.vRefBgCornerTrim.Get(0));


            finalbitstream[TrimBitsOffset + 3] = _BitLevelDevice.vRefScaledBandgapTrim.Get(0);
            finalbitstream[TrimBitsOffset + 4] = _BitLevelDevice.vRefScaledBandgapTrim.Get(1);
            finalbitstream[TrimBitsOffset + 5] = _BitLevelDevice.vRefScaledBandgapTrim.Get(2);
            finalbitstream[TrimBitsOffset + 6] = _BitLevelDevice.vRefScaledBandgapTrim.Get(3);
            finalbitstream[TrimBitsOffset + 7] = _BitLevelDevice.vRefScaledBandgapTrim.Get(4);
            finalbitstream[TrimBitsOffset + 72] = _BitLevelDevice.vRefScaledBandgapTrim.Get(5);
            //bitstream.Set(TrimBitsOffset + 72, _BitLevelDevice.vRefScaledBandgapTrim.Get(5));
            //bitstream.Set(TrimBitsOffset + 3, _BitLevelDevice.vRefScaledBandgapTrim.Get(4));
            //bitstream.Set(TrimBitsOffset + 4, _BitLevelDevice.vRefScaledBandgapTrim.Get(3));
            //bitstream.Set(TrimBitsOffset + 5, _BitLevelDevice.vRefScaledBandgapTrim.Get(2));
            //bitstream.Set(TrimBitsOffset + 6, _BitLevelDevice.vRefScaledBandgapTrim.Get(1));
            //bitstream.Set(TrimBitsOffset + 7, _BitLevelDevice.vRefScaledBandgapTrim.Get(0));

            _BitLevelDevice.vRegPtatUp.CopyTo(finalbitstream, TrimBitsOffset+ 8);
            //bitstream.Set(TrimBitsOffset + 8, _BitLevelDevice.vRegPtatUp.Get(5));
            //bitstream.Set(TrimBitsOffset + 9, _BitLevelDevice.vRegPtatUp.Get(4));
            //bitstream.Set(TrimBitsOffset + 10, _BitLevelDevice.vRegPtatUp.Get(3));
            //bitstream.Set(TrimBitsOffset + 11, _BitLevelDevice.vRegPtatUp.Get(2));
            //bitstream.Set(TrimBitsOffset + 12, _BitLevelDevice.vRegPtatUp.Get(1));
            //bitstream.Set(TrimBitsOffset + 13, _BitLevelDevice.vRegPtatUp.Get(0));

            _BitLevelDevice.vRegBgapXdown.CopyTo(finalbitstream, TrimBitsOffset + 14);
            //bitstream.Set(TrimBitsOffset + 14, _BitLevelDevice.vRegBgapXdown.Get(5));
            //bitstream.Set(TrimBitsOffset + 15, _BitLevelDevice.vRegBgapXdown.Get(4));
            //bitstream.Set(TrimBitsOffset + 16, _BitLevelDevice.vRegBgapXdown.Get(3));
            //bitstream.Set(TrimBitsOffset + 17, _BitLevelDevice.vRegBgapXdown.Get(2));
            //bitstream.Set(TrimBitsOffset + 18, _BitLevelDevice.vRegBgapXdown.Get(1));
            //bitstream.Set(TrimBitsOffset + 19, _BitLevelDevice.vRegBgapXdown.Get(0));


            _BitLevelDevice.vosMagLeft.CopyTo(finalbitstream, TrimBitsOffset + 20);
            //bitstream.Set(TrimBitsOffset + 20, _BitLevelDevice.vosMagLeft.Get(7));
            //bitstream.Set(TrimBitsOffset + 21, _BitLevelDevice.vosMagLeft.Get(6));
            //bitstream.Set(TrimBitsOffset + 22, _BitLevelDevice.vosMagLeft.Get(5));
            //bitstream.Set(TrimBitsOffset + 23, _BitLevelDevice.vosMagLeft.Get(4));
            //bitstream.Set(TrimBitsOffset + 24, _BitLevelDevice.vosMagLeft.Get(3));
            //bitstream.Set(TrimBitsOffset + 25, _BitLevelDevice.vosMagLeft.Get(2));
            //bitstream.Set(TrimBitsOffset + 26, _BitLevelDevice.vosMagLeft.Get(1));
            //bitstream.Set(TrimBitsOffset + 27, _BitLevelDevice.vosMagLeft.Get(0));

            _BitLevelDevice.vosMagRight.CopyTo(finalbitstream, TrimBitsOffset + 28);
            //bitstream.Set(TrimBitsOffset + 28, _BitLevelDevice.vosMagRight.Get(7));
            //bitstream.Set(TrimBitsOffset + 29, _BitLevelDevice.vosMagRight.Get(6));
            //bitstream.Set(TrimBitsOffset + 30, _BitLevelDevice.vosMagRight.Get(5));
            //bitstream.Set(TrimBitsOffset + 31, _BitLevelDevice.vosMagRight.Get(4));
            //bitstream.Set(TrimBitsOffset + 32, _BitLevelDevice.vosMagRight.Get(3));
            //bitstream.Set(TrimBitsOffset + 33, _BitLevelDevice.vosMagRight.Get(2));
            //bitstream.Set(TrimBitsOffset + 34, _BitLevelDevice.vosMagRight.Get(1));
            //bitstream.Set(TrimBitsOffset + 35, _BitLevelDevice.vosMagRight.Get(0));

            _BitLevelDevice.vosElectLeft.CopyTo(finalbitstream, TrimBitsOffset + 36);
            //bitstream.Set(TrimBitsOffset + 36, _BitLevelDevice.vosElectLeft.Get(7));
            //bitstream.Set(TrimBitsOffset + 27, _BitLevelDevice.vosElectLeft.Get(6));
            //bitstream.Set(TrimBitsOffset + 28, _BitLevelDevice.vosElectLeft.Get(5));
            //bitstream.Set(TrimBitsOffset + 29, _BitLevelDevice.vosElectLeft.Get(4));
            //bitstream.Set(TrimBitsOffset + 40, _BitLevelDevice.vosElectLeft.Get(3));
            //bitstream.Set(TrimBitsOffset + 41, _BitLevelDevice.vosElectLeft.Get(2));
            //bitstream.Set(TrimBitsOffset + 42, _BitLevelDevice.vosElectLeft.Get(1));
            //bitstream.Set(TrimBitsOffset + 43, _BitLevelDevice.vosElectLeft.Get(0));

            _BitLevelDevice.vosElectRight.CopyTo(finalbitstream, TrimBitsOffset + 44);
            //bitstream.Set(TrimBitsOffset + 44, _BitLevelDevice.vosElectRight.Get(7));
            //bitstream.Set(TrimBitsOffset + 45, _BitLevelDevice.vosElectRight.Get(6));
            //bitstream.Set(TrimBitsOffset + 46, _BitLevelDevice.vosElectRight.Get(5));
            //bitstream.Set(TrimBitsOffset + 47, _BitLevelDevice.vosElectRight.Get(4));
            //bitstream.Set(TrimBitsOffset + 48, _BitLevelDevice.vosElectRight.Get(3));
            //bitstream.Set(TrimBitsOffset + 49, _BitLevelDevice.vosElectRight.Get(2));
            //bitstream.Set(TrimBitsOffset + 50, _BitLevelDevice.vosElectRight.Get(1));
            //bitstream.Set(TrimBitsOffset + 51, _BitLevelDevice.vosElectRight.Get(0));

            _BitLevelDevice.gainInstrAmpsLeft.CopyTo(finalbitstream, TrimBitsOffset + 52);
            //bitstream.Set(TrimBitsOffset + 52, _BitLevelDevice.gainInstrAmpsLeft.Get(7));
            //bitstream.Set(TrimBitsOffset + 53, _BitLevelDevice.gainInstrAmpsLeft.Get(6));
            //bitstream.Set(TrimBitsOffset + 54, _BitLevelDevice.gainInstrAmpsLeft.Get(5));
            //bitstream.Set(TrimBitsOffset + 55, _BitLevelDevice.gainInstrAmpsLeft.Get(4));
            //bitstream.Set(TrimBitsOffset + 56, _BitLevelDevice.gainInstrAmpsLeft.Get(3));
            //bitstream.Set(TrimBitsOffset + 57, _BitLevelDevice.gainInstrAmpsLeft.Get(2));
            //bitstream.Set(TrimBitsOffset + 58, _BitLevelDevice.gainInstrAmpsLeft.Get(1));
            //bitstream.Set(TrimBitsOffset + 59, _BitLevelDevice.gainInstrAmpsLeft.Get(0));

            _BitLevelDevice.gainInstrAmpsRight.CopyTo(finalbitstream, TrimBitsOffset + 60);
            //bitstream.Set(TrimBitsOffset + 60, _BitLevelDevice.gainInstrAmpsRight.Get(7));
            //bitstream.Set(TrimBitsOffset + 61, _BitLevelDevice.gainInstrAmpsRight.Get(6));
            //bitstream.Set(TrimBitsOffset + 62, _BitLevelDevice.gainInstrAmpsRight.Get(5));
            //bitstream.Set(TrimBitsOffset + 63, _BitLevelDevice.gainInstrAmpsRight.Get(4));
            //bitstream.Set(TrimBitsOffset + 64, _BitLevelDevice.gainInstrAmpsRight.Get(3));
            //bitstream.Set(TrimBitsOffset + 65, _BitLevelDevice.gainInstrAmpsRight.Get(2));
            //bitstream.Set(TrimBitsOffset + 66, _BitLevelDevice.gainInstrAmpsRight.Get(1));
            //bitstream.Set(TrimBitsOffset + 67, _BitLevelDevice.gainInstrAmpsRight.Get(0));

            finalbitstream[TrimBitsOffset + 68] = _BitLevelDevice.gainUniPolar;
            //bitstream.Set(TrimBitsOffset + 68, _BitLevelDevice.gainUniPolar);

            finalbitstream[TrimBitsOffset + 69] = _BitLevelDevice.gainHiSupply;
            //bitstream.Set(TrimBitsOffset + 69, _BitLevelDevice.gainHiSupply);

            finalbitstream[TrimBitsOffset + 70] = _BitLevelDevice.gainFor12mT;
            //bitstream.Set(TrimBitsOffset + 70, _BitLevelDevice.gainFor12mT);

            _BitLevelDevice.MarkingBits.CopyTo(finalbitstream, TrimBitsOffset + 71);
            //bitstream.Set(TrimBitsOffset + 71, _BitLevelDevice.MarkingBits.Get(3));
            //bitstream.Set(TrimBitsOffset + 74, _BitLevelDevice.MarkingBits.Get(2));
            //bitstream.Set(TrimBitsOffset + 75, _BitLevelDevice.MarkingBits.Get(1));
            //bitstream.Set(TrimBitsOffset + 76, _BitLevelDevice.MarkingBits.Get(0));


            finalbitstream[TrimBitsOffset + 77] = _BitLevelDevice.BlockTryBeforeBuyHi;
            //bitstream.Set(TrimBitsOffset + 77, _BitLevelDevice.BlockTryBeforeBuyHi);

            finalbitstream[TrimBitsOffset + 78] = _BitLevelDevice.CrousMurataModes;
            //bitstream.Set(TrimBitsOffset + 78, _BitLevelDevice.CrousMurataModes);

            _BitLevelDevice.BridgeSwaps.CopyTo(finalbitstream, TrimBitsOffset + 79);
            //bitstream.Set(TrimBitsOffset + 79, _BitLevelDevice.BridgeSwaps.Get(1));
            //bitstream.Set(TrimBitsOffset + 80, _BitLevelDevice.BridgeSwaps.Get(0));
            //bool[] finalbitstream = new bool[107];
            //bitstream.CopyTo(finalbitstream, 0);
            return finalbitstream;
        }

    }// Tnslation Layer Class
    
}
