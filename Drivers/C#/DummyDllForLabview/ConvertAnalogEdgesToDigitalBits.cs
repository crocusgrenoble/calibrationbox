﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace ConvertAnalogEdgesToDigitalBits
{
    public class AnalogEdgesToDigitalBitConvertor
    {

        double[] _bitstream_clk;
        double[] _bitstream_data;
        private double _highClkThreshold ;

        public AnalogEdgesToDigitalBitConvertor()
        {
            _bitstream_clk = new double[32]{ 5, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0 };
            _bitstream_data = new double[32]{ 0, 3, 3, 0, 0, 0, 0, 3, 3, 3, 3, 0, 0, 0, 0, 3, 3, 0, 0, 3, 3, 0, 0, 3, 3, 0, 0, 3, 3, 3, 3, 0 };
            _highClkThreshold = 5.5;// 4 volts
        }
        public int CountNumberofBits(double[] bitarray)
        {
            int count = 0;
            foreach (double b in bitarray) {
                count++;
            }
            return count;
        }
        public bool[] ReadBitsFromAnalogInput(double[] clockArray, double[] dataArray)
        {
            List<string> msgs = new List<string>();
            List<bool> tempDataList = new List<bool>();
            bool[] outputData;
            var clockAndData = clockArray.Zip(dataArray, (c, d) => new { Clock = c, Data = d });
            bool highClockDetected = false, highClockFallingDetected = false;
            bool clkState = false;// 0: wait for risging edge, 1: wait for fallign edge
            bool discardPreviousSignals = false;// It discards all initial clk values (bigger than low level) until arrive the first low level. 
            int numberOfBits = 0;
            int index = 0, ClkRisingIndex = 0, ClkFallingIndex = 0;
            const double LowLevel = 1, HighLevel = 2.7; // Voltage level correspoding to 0 and 1 logical values
            foreach (var cd in clockAndData)
            {
                if (discardPreviousSignals == false) { 
                    if (cd.Clock > LowLevel)
                    {
                        continue;

                    }
                    else { 
                        discardPreviousSignals= true;
                    }
                }
                msgs.Add(string.Format("Reading a couple of clk={0} and data={1}", cd.Clock, cd.Data));
                if (highClockDetected == false)
                {
                    if (cd.Clock > _highClkThreshold) // High voltage rising dge clk detected
                    {
                        msgs.Add(string.Format("high clock detected {0}", cd.Clock));

                        highClockDetected = true;
                        // clkState = true;
                    }
                }
                else if (highClockFallingDetected == false)// Waiting for highclock falling edge
                {
                    if (cd.Clock < LowLevel)     // Normal clk falling edge is detected
                    {
                        highClockFallingDetected = true;
                    }
                }

                else // High voltage rising dge clk already seen
                {
                    if (clkState == false) // if waiting for rising edge 
                    {
                        if (cd.Clock > HighLevel)// Normal clk rising edge is detected
                        {
                            ClkRisingIndex = index;
                            clkState = true;// Waiting for falling edge from now on.
                        }

                    }
                    else// if waiting for falling edge 
                    {
                        if (cd.Clock < LowLevel)     // Normal clk falling edge is detected
                        {
                            ClkFallingIndex = index;
                            clkState = false;
                            msgs.Add(string.Format("clock rise detected clk={0} and data={1}", cd.Clock, cd.Data)); // Lokking at data in the middle between rising and falling edge moments 
                            int ClkMiddleIndex = (ClkFallingIndex + ClkRisingIndex) / 2;

                            if (dataArray[ClkMiddleIndex] > 2.5)// Data has '1' Level
                            {
                                tempDataList.Add(true);
                                msgs.Add(string.Format("Detected digital value ={0}", true));
                            }
                            else
                            {
                                // Data has '1' Level
                                tempDataList.Add(false);
                                msgs.Add(string.Format("Detected digital value ={0}", false));
                            }
                            numberOfBits++;
                        }
                    }
                }
                index++;
            }//foreach
            outputData = tempDataList.ToArray();
            msgs.Add(string.Format("Extracted data ={0}", string.Join(", ", outputData)));

            string[] log = msgs.ToArray();
            //File.WriteAllLines("log.txt", log);
            return outputData;
        }

    }
}
