﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrentSupply
{
    public abstract class CurrentSupply
    {
        public abstract bool SetCurrent(double Curr);
        public abstract bool CurrentON();
        public abstract bool CurrentOFF();
    }
    public class GPIBCurrentSupply: CurrentSupply
    {
        public  bool CurrentSupply() { return false; }
        public override bool SetCurrent(double Curr) { return false; }
        public override bool CurrentON() { return false; }
        public override bool CurrentOFF() { return false; }

    }
}
